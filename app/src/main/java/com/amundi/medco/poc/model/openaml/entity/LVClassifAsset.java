package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVClassifAsset {

  /**
   * The classification is the Asset class level of the asset classification 
   */
  ASSET_CLASS,

  /**
   * The classification is the Asset group level of the asset classification 
   */
  ASSET_GROUP,

  /**
   * The classification is the Asset type level of the asset classification 
   */
  ASSET_TYPE,

  /**
   * The classification is the risk typology provided by risk department 
   */
  RISK_ASSET_TYPE,

  /**
   * The classification is the Bloomberg security type 
   */
  BB_SECURITY_TYP,

  /**
   * The classification is the Bloomberg security type 2 
   */
  BB_SECURITY_TYP2,

  /**
   * BB_TYPE_OF_BOND : Bloomberg type of bond 
   */
  BB_TYPE_OF_BOND,

  /**
   * The classification is the liquidity type classification used by liquidity risk 
   */
  LIQUIDITY_TYP,

  /**
   * UPI : Unique product identifier, ISDA classification 
   */
  UPI,

  /**
   * Basel III designation 
   */
  BASEL_III,

  /**
   * BPAM (Bond Pricing Agency Malaysia) bond class 
   */
  BPAM_BOND_CLASS,

  /**
   * CFI code 
   */
  CFI_CODE,

  /**
   * EIOPA type 
   */
  EIOPA,

  /**
   * CIC category + CIC code 
   */
  CIC,

  /**
   * CIC asset type, CIC asset category 
   */
  CIC_CATEGORY,

  /**
   * CIC asset type 2, underlying asset category 
   */
  CIC_SUBCATEGORY,

  /**
   * Loan type 
   */
  LOAN_TYPE,

  /**
   * ASSET_CLASS_MKG, product class marketing class 
   */
  ASSET_CLASS_MKG,

  /**
   * Mutual fund product class type, main asset classification 
   */
  PRODUCT_CLASS_TYPE,

  /**
   * Mutual fund product classification 
   */
  FUND_PRODUCT_CLASSIF,

  /**
   * Mutual fund or portfolio internal typology 
   */
  FUND_INTERNAL_TYPOLOGY,

}