package com.amundi.medco.poc.model.openaml.entity.analytic;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.entity.ContactInfo;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="rating", namespace="com.amundi.tech.openaml.entity.analytic")
public class Rating extends Entity {

    /**
     * Rating category : Credit rating (agency), ESG rating, Investment Grade, SRRI, etc.
     */
    private String category;

    /**
     * Rating source : the agency, etc.
     */
    private String ratingSource;

    /**
     * Rating type
     */
    private String ratingType;

    /**
     * Rating class : short term, long term, ESG_E, ESG_G, ESG_S, GLO if calculated, etc.
     */
    private String ratingClass;

    /**
     * Date of the rating value
     */
    private LocalDate ratingDate;

    /**
     * Rating value, as defined in the rating scale of the source
     */
    private String ratingValue;

    /**
     * Raw value of the rating, as defined by the source, provider
     */
    private String ratingRaw;

    /**
     * Rating meaning, investment grade, risk grade, e.g. high, low, etc.
     */
    private String grade;

    /**
     * Scoring value, or decimal rating value
     */
    private BigDecimal score;

    /**
     * Reverse scoring value, or decimal rating value
     */
    private BigDecimal reverseScore;

    /**
     * Rating perimeter (AC), or rating universe (ESG)
     */
    private String scope;

    /**
     * Rating outlook as defined by the agency
     */
    private String outlook;

    /**
     * Rating watch as defined by the agency
     */
    private String watch;

    /**
     * Optional rating comment or description linked to the rating 
     */
    private Comment comment;

    /**
     * Previous rating value if the rating has been modified
     */
    private String previousRating;

    /**
     * if not provided rating, the person in charge of this rating analysis
     */
    private ContactInfo analyst;

    /**
     * Equity rating :analyst rating style
     */
    private String analystRatingStyle;

    /**
     * undefined
     */
    private String buyWatch;

    /**
     * Confirmation date
     */
    private LocalDate confirmationDate;

    /**
     * Overall recommendation
     */
    private String overallRecommendation;

    /**
     * Default score, rating (A,EVR)
     */
    private String defaultScore;

    /**
     * External financial dependency score : N/A 1,2,3,4,5
     */
    private String dependencyScore;

    /**
     * Relative value of the rating (A,AR) :  N/A 1,2,3,4,5
     */
    private String relativeValue;

    /**
     * PIK : Payment in kind
     */
    private String paymentInKind;

    /**
     * Second lien recommendation
     */
    private String secondLien;

    /**
     * Recommendation if senior secured debt
     */
    private String seniorSecured;

    /**
     * Recommendation if senior unsecured debt
     */
    private String seniorUnsecured;

    /**
     * Recommendation if subordinated debt
     */
    private String subordinated;

    /**
     * Recommendation if junior subordinated debt
     */
    private String juniorSubordinated;

    /**
     * undefined
     */
    private String tier;

    /**
     * True if currently covered
     */
    private Boolean isCurrentlyCovered;

    /**
     * Last publication date
     */
    private LocalDate lastPublicationDate;

    /**
     * Likelihood upgrade
     */
    private String likelihoodUpgrade;

    /**
     * Probability of Default : High, medium, low
     */
    private String probDefault;

    /**
     * Probability of Downgrade to High Yield : High, medium, low
     */
    private String probDowngradeToHY;

    /**
     * Probability of Default : High, medium, low
     */
    private String probabilityOfDefault;

    /**
     * Probability of Downgrade to High Yield : High, medium, low
     */
    private String probabilityOfDowngradeToHY;

    /**
     * Equity rating : Target price currency code
     */
    private String targetPriceCcy;

    /**
     * Equity rating : Target price of equity
     */
    private BigDecimal targetPrice;

    /**
     * Equity rating : target upside  in %
     */
    private BigDecimal targetUpside;

    /**
     * Optional rated entity identifier
     */
    private Identifier ratedEntityId;

    /**
     * Optional other comments on this rating
     */
    private List<Comment> comments;

    /**
     * Optional attributes to add to the rating
     */
    private List<Attribute> attributes;

}