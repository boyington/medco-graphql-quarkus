package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="constraintcheckresult", namespace="com.amundi.tech.openaml.entity")
public class ConstraintCheckResult extends Entity {

    /**
     * Constraint id
     */
    private Identifier constraintId;

    /**
     * Description of the constraint check result
     */
    private String description;

    /**
     * Message of the constraint check result
     */
    private String message;

    /**
     * The results of the rule check
     */
    private List<RuleCheckResult> ruleCheckResults;

}