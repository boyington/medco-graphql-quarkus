package com.amundi.medco.poc.jackson.openaml.list;

import com.amundi.medco.poc.model.openaml.entity.asset.LVRateType;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class LVRateTypeDeserializer extends StdDeserializer<LVRateType> {

    public LVRateTypeDeserializer() {
        this(null);
    }

    public LVRateTypeDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LVRateType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        String strValue = p.getText();
        for(LVRateType lvRateType: LVRateType.values()) {
            if(lvRateType.toString().equals(strValue)) {
                return lvRateType;
            }
        }
        log.debug("Unable to convert {} to LVRateType", strValue);
        return null;
    }
}
