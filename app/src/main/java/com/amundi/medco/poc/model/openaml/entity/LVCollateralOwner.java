package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCollateralOwner {

  /**
   * client collateral  (CC) collateral is owed to the client because the margin balance is positive 
   */
  CLIENT_OWNED,

  /**
   * broker collateral (BC) collateral is owed to the broker because the margin balance is negative 
   */
  BROKER_OWNED,

}