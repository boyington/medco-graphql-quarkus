package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVSettlementMode {

  /**
   * Cash settlement (CC) 
   */
  CASH,

  /**
   * Domestic securities settlement, see detail in settlementModeCode 
   */
  DOMESTIC,

  /**
   * International securities settlement 
   */
  INTERNATIONAL,

}