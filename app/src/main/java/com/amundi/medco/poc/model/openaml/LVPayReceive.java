package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVPayReceive {

  /**
   * Side payable, for example : payed amount, payed leg if otc 
   */
  PAY,

  /**
   * Side receivable, for example : received amount, received leg if otc 
   */
  RECEIVE,

}