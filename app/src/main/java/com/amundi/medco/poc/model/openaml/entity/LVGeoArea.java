package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVGeoArea {

  /**
   * Custom geographic area 
   */
  CUSTOM,

  /**
   * Solvency geographic area 
   */
  SOLVENCY,

  /**
   * Fund geographic location 
   */
  FUND_GEO_LOCATION,

}