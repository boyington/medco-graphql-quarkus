package com.amundi.medco.poc.model.openaml.entity.asset;

import com.amundi.medco.poc.model.openaml.LVPayReceive;
import com.amundi.medco.poc.model.openaml.component.Amounts;
import com.amundi.medco.poc.model.openaml.component.FeesRateAmount;
import com.amundi.medco.poc.model.openaml.component.LVBusinessDayConvention;
import com.amundi.medco.poc.model.openaml.component.LVPeriodAt;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.amundi.medco.poc.model.openaml.entity.Basket;
import com.amundi.medco.poc.model.openaml.entity.SettlementInfo;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="otcleg", namespace="com.amundi.tech.openaml.entity.asset")
public class OtcLeg {

    /**
     * Leg number
     */
    private Integer legNumber;

    /**
     * Leg phase
     */
    private Integer legPhase;

    /**
     * Optional leg type : SPOT, FORWARD, RETURN, FUNDING, NEAR leg, FAR leg, etc. (see LVLegType)
     */
    private String type;

    /**
     * Leg payable side or receivable side, similar to buy/sell the amount
     */
    private LVPayReceive side;

    /**
     * Leg amount currency code
     */
    private String ccy;

    /**
     * Leg amount or nominal amount, notional
     */
    private BigDecimal nominal;

    /**
     * Previous nominal or notional if a market operation has modified the leg nominal
     */
    private BigDecimal previousNominal;

    /**
     * Initial nominal
     */
    private BigDecimal initialNominal;

    /**
     * Start date of the payment leg
     */
    private LocalDate startDate;

    /**
     * End date of the payment leg
     */
    private LocalDate endDate;

    /**
     * Roll day
     */
    private Integer rollDay;

    /**
     * Roll date corresponding to the non-broken period
     */
    private LocalDate rollDate;

    /**
     * Roll convention
     */
    private LVBusinessDayConvention rollConvention;

    /**
     * Roll convention at end date
     */
    private LVBusinessDayConvention endDateRollConv;

    /**
     * True if termination date or end date falls on 20 March, 20 June, 20 September and 20 December, mainly the date is adjusted to the third Wednesday
     */
    private Boolean immFlag;

    /**
     * True if End of month treatment and end to end, mainly the date is adjusted to the end of the month
     */
    private Boolean eomFlag;

    /**
     * True following leg1 and leg2 index and rate (mainly if 1 leg is FIXED RATE and 2nd leg is other than OIS or TEC
     */
    private Boolean adjustedFlag;

    /**
     * True if adjust maturity date of the deal
     */
    private Boolean adjustEndDate;

    /**
     * For CC swap, exchange nominal, principal is exchanged at : BEGIN, END, BOTH, NONE
     */
    private LVPeriodAt exchangeAt;

    /**
     * Payment date, settlement date of the leg cash flow
     */
    private LocalDate settlementDate;

    /**
     * Settlement info on calendar, payment lag, to calculate the settlement date, payment date
     */
    private SettlementInfo settlementInfo;

    /**
     * True if reinvestment of dividend, interests, income or return.
     */
    private Boolean reinvestment;

    /**
     * Leg interest rate definition to create a payment : rate, fixing and payment convention
     */
    private InterestRate interest;

    /**
     * Leg related asset or index
     */
    private Asset asset;

    /**
     * Leg related basket, a basket is composed of a set of financial asset
     */
    private Basket basket;

    /**
     * Leg valuations : type, date, netAmt, netPrice
     */
    private List<Amounts> valuations;

    /**
     * specific fees for the leg. Used for ETF synthetic swap 
     */
    private List<FeesRateAmount> legFees;

    /**
     * All specific data for ETF or ELS
     */
    private EtfInfo etfInfo;

    /**
     * ETF Share
     */
    private Asset share;

}