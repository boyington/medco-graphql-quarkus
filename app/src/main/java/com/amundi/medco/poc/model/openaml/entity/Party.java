package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.*;
import com.amundi.medco.poc.model.openaml.entity.analytic.Rating;
import com.amundi.medco.poc.model.openaml.entity.analytic.rating.CarbonData;
import com.amundi.medco.poc.model.openaml.entity.party.EmirInfo;
import com.amundi.medco.poc.model.openaml.entity.party.MifidInfo;
import com.amundi.medco.poc.model.openaml.entity.party.SanctionInfo;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="party", namespace="com.amundi.tech.openaml.entity")
public class Party extends Entity {

    /**
     * Third-party short name
     */
    private String shortName;

    /**
     * Third-party acronym
     */
    private String acronym;

    /**
     * Trading name, the name used by a business for commercial purposes
     */
    private String tradingName;

    /**
     * Optional Party description
     */
    private String description;

    /**
     * Indicates the role of the party in the context
     */
    private LVPartyRole role;

    /**
     * Location or address including country of the party
     */
    private Location location;

    /**
     * Main contactInfo if need to be in relation with
     */
    private ContactInfo contactInfo;

    /**
     * Link type between the party and the business entity
     */
    private String linkType;

    /**
     * Risk country iso code
     */
    private String riskCtry;

    /**
     * Headquarters country iso code
     */
    private String headquartersCtry;

    /**
     * Incorporation country iso code
     */
    private String incorporationCtry;

    /**
     * Tax country iso code, residential country iso code
     */
    private String taxCtry;

    /**
     * Business country iso code, principal business country iso code
     */
    private String businessCtry;

    /**
     * Registered country iso code, legal country iso code
     */
    private String registeredCtry;

    /**
     * Country of operation iso code
     */
    private String operationCtry;

    /**
     * True if this is the ultimate parent
     */
    private Boolean ultimate;

    /**
     * Intra group, e.g. in the context of Amundi, the legal entity is member of Amundi group
     */
    private Boolean intraGroup;

    /**
     * Expended affiliated group, e.g.  in the context of Amundi, the legal entity is member of CASA group
     */
    private Boolean expendedAffilGroup;

    /**
     * Private, Government, Association, etc.
     */
    private Classif type;

    /**
     * More detail type : 2nd level of type
     */
    private Classif subtype;

    /**
     * True if monitored by risk team
     */
    private Boolean monitored;

    /**
     * Relationship status for the third-party/legal entity  (authorized or not authorized)
     */
    private String relationshipStatus;

    /**
     * List of party classifs
     */
    private List<Classif> classifs;

    /**
     * List of party sectors
     */
    private List<Classif> sectors;

    /**
     * List of party ratings
     */
    private List<Rating> ratings;

    /**
     * Carbon data
     */
    private CarbonData carbonData;

    /**
     * List of addresses
     */
    private List<Location> addresses;

    /**
     * Compliance data
     */
    private ComplianceInfo compliance;

    /**
     * First financial Year-end
     */
    private String firstFinancialYearEnd;

    /**
     * Financial information for legal entity, or person/individual
     */
    private List<FinancialInfo> financialInfos;

    /**
     * Information related to EMIR regulatory
     */
    private EmirInfo emirInfo;

    /**
     * Information related to MIFID regulatory
     */
    private MifidInfo mifidInfo;

    /**
     * Information related to sanction
     */
    private SanctionInfo sanctionInfo;

    /**
     * List of documents
     */
    private List<EDocument> documents;

    /**
     * Optional comments on this third-party, legal entity
     */
    private List<Comment> comments;

    /**
     * Risk profile
     */
    private String riskProfile;

    /**
     * Risk scope
     */
    private String riskScope;

    /**
     * Party guarantee type
     */
    private String guaranteeType;

    /**
     * True if legal entity, false if individual or human being
     */
    private Boolean isLegalEntity;

    /**
     * Client tiering if the party is a client
     */
    private String clientTiering;

    /**
     * Client first subscription date
     */
    private LocalDate clientFirstSubscriptionDate;

    /**
     * Distributor code and name of the client
     */
    private Codification clientDistributor;

    /**
     * Person first name, last name, etc.
     */
    private PersonName personName;

    /**
     * Person date of birth
     */
    private LocalDate dateOfBirth;

    /**
     * Person place of birth : city, state, country
     */
    private Location placeOfBirth;

    /**
     * Use ISO code : 1 (male), 2 (female), 0 (Not known), 9 (Not applicable)
     */
    private String gender;

    /**
     * Person nationality
     */
    private Nationality nationality;

    /**
     * Dual nationality, second nationality
     */
    private Nationality secondNationality;

    /**
     * Multiple nationality, third nationality
     */
    private Nationality thirdNationality;

    /**
     * Co-holders identifiers of a joint account for instance
     */
    private List<Identifier> coHolderIds;

    /**
     * List of custom attributes associated to the Party
     */
    private List<Attribute> attributes;

    /**
     * key defining rules / policy data  have been provided to build the object
     */
    private String dataProvidingPolicy;

    /**
     * VAT status, VAT rate to be applied to a client
     */
    private FeesRateAmount vat;

    /**
     * all custom data organised by universe with each a list fields ( key + value)
     */
    private List<DataUniverse> customData;

}