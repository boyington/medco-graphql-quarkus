package com.amundi.medco.poc.model.openaml.entity.analytic;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="ratingscale", namespace="com.amundi.tech.openaml.entity.analytic")
public class RatingScale extends Entity {

    /**
     * The name of the agency or the client that has defined the rating scale
     */
    private String scaleSource;

    /**
     * List of ratingValue, and optionally the associated score
     */
    private List<Rating> ratings;

}