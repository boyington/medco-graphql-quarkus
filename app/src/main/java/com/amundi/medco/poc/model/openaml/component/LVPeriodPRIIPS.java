package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPeriodPRIIPS {

  /**
   * 1 year, also used by PRIIPS: 31010_Period_1 
   */
  ONE_YEAR,

  /**
   * Half recommended statement period, PRIIPS: 31020_Period_2 
   */
  HALF_RHP,

  /**
   * recommended statement period, PRIIPS: 31030_RHP_period_3 
   */
  RHP,

}