package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVInstructionType {

  /**
   * Subcription instruction 
   */
  SUBSCRIPTION,

  /**
   * Redemption instruction 
   */
  REDEMPTION,

  /**
   * Switch instruction 
   */
  SWITCH,

}