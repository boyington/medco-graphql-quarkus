package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVComment {

  /**
   * Bloomberg security des 
   */
  SECURITY_DES,

  /**
   * Value comment 
   */
  VALUE_COMMENT,

}