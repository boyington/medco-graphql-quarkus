package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.component.LVFrequency;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="benchmark", namespace="com.amundi.tech.openaml.entity")
public class Benchmark extends Entity {

    /**
     * Benchmark typology : OFFICIAL, MANAGEMENT (see LVBenchmarkType)
     */
    private String benchType;

    /**
     * Benchmark typology description or label
     */
    private String benchTypeDesc;

    /**
     * Benchmark currency code
     */
    private String ccy;

    /**
     * Benchmark starting date
     */
    private LocalDate startingDate;

    /**
     * Benchmark closing date
     */
    private LocalDate closingDate;

    /**
     * Benchmark rebalancing frequency
     */
    private LVFrequency rebalancingFreq;

    /**
     * Benchmark last rebalancing date
     */
    private LocalDate rebalancingDate;

    /**
     * Benchmark next rebalancing date
     */
    private LocalDate nextRebalancingDate;

    /**
     * Associated portfolio identifier
     */
    private Identifier portfolioId;

    /**
     * Benchmark composition (index or other benchmark)
     */
    private Basket composition;

    /**
     * Date used to protect earlier data, i.e. no valuation computation on data prior to this date
     */
    private LocalDate valuationProtectionDate;

    /**
     * Benchmark valuation : open, close, netReturn, grossReturn
     */
    private Quotation valuation;

    /**
     * List of Benchmark valuation : open, close, netReturn, grossReturn
     */
    private List<Quotation> valuations;

    /**
     * Benchmark analytics : market capitalization, risk analytics
     */
    private Analytics analytics;

    /**
     * List of Benchmark analytics : market capitalization, risk analytics
     */
    private List<Analytics> allAnalytics;

    /**
     * Benchmark risk indicators
     */
    private List<AnalyticsValue> riskIndicators;

    /**
     * Benchmark performances
     */
    private List<Performance> performances;

}