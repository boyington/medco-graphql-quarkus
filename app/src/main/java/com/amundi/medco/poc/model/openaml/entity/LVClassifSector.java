package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVClassifSector {

  /**
   * Bloomberg industry sector 
   */
  INDUSTRY_SECTOR,

  /**
   * Bloomberg industry group 
   */
  INDUSTRY_GROUP,

  /**
   * Bloomberg industry subgroup 
   */
  INDUSTRY_SUBGROUP,

  /**
   * Bloomberg industry subgroup num 
   */
  INDUSTRY_SUBGROUP_NUM,

  /**
   * Bloomberg market sector 
   */
  MARKET_SECTOR_DES,

  /**
   * Global Industry Classification Standard 
   */
  GICS,

  /**
   * Industrial Classification Benchmark 
   */
  ICB,

  /**
   * BICS - Bloomberg Industry Classification Systems 
   */
  BICS,

  /**
   * MSCI sectors 
   */
  MSCI,

  /**
   * Merrill sectors 
   */
  MERRILL,

  /**
   * Merrill industry 
   */
  MERRILL_INDUSTRY,

  /**
   * BPAM sectors - Bond Pricing Agency Malaysia 
   */
  BPAM_SECTOR,

  /**
   * Issuer industry 
   */
  ISSUER_INDUSTRY,

  /**
   * NACE : codification of economic activity in european union 
   */
  NACE,

  /**
   * NAF : French nomenclature for APE activite principale exercee 
   */
  NAF,

  /**
   * NAICS : North American Industry Classification System 
   */
  NAICS,

  /**
   * Barclays sector 
   */
  BARCLAYS,

  /**
   * Global Industry Classification Standard level 1 
   */
  GICS_1,

  /**
   * Global Industry Classification Standard level 2 
   */
  GICS_2,

  /**
   * Global Industry Classification Standard level 3 
   */
  GICS_3,

  /**
   * Global Industry Classification Standard level 4 
   */
  GICS_4,

}