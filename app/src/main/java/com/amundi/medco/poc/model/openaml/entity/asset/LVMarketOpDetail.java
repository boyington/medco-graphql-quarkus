package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVMarketOpDetail {

  /**
   * Market op executed on partial nominal amount of the otc deal 
   */
  PARTIAL,

  /**
   * Market op executed on total nominal amount of the otc deal 
   */
  TOTAL,

}