package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCollateralType {

  /**
   * Cash as collateral 
   */
  CASH,

  /**
   * Security as collateral 
   */
  SECURITY,

}