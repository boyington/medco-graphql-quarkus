package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVInvestmentMandateType {

  /**
   * Investment mandate for Individual client 
   */
  INDIVIDUAL,

  /**
   * Investment mandate for association 
   */
  ASSOCIATION,

  /**
   * Investment mandate for a company 
   */
  COMPANY,

  /**
   * Investment mandate for a joint account 
   */
  JOINT_ACCOUNT,

}