package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="accessright", namespace="com.amundi.tech.openaml.entity")
public class AccessRight extends Entity {

    /**
     * By default true, if false this access right is not allowed
     */
    private Boolean allowed;

    /**
     * RPID of the person, also called the user
     */
    private Identifier personId;

    /**
     * code of the action or functionality, see LVAction
     */
    private String action;

    /**
     * name of the action
     */
    private String actionName;

    /**
     * Resource type : PORTFOLIO, LEGAL_ENTITY (use LVEntityType if entity)
     */
    private String resourceType;

    /**
     * identifier of the resource, portfolio code if the resource is a portfolio
     */
    private Identifier resourceId;

    /**
     * name of the resource
     */
    private String resourceName;

}