package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVMultiCouponSchedule {

  /**
   * Not multi coupon 
   */
  NONE,

  /**
   * Multi coupon schedule in rate and percentage 
   */
  MULTI_CPN_RATE,

  /**
   * Multi coupon schedule in amount 
   */
  MULTI_CPN_AMT,

}