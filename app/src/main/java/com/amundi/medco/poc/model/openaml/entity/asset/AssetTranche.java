package com.amundi.medco.poc.model.openaml.entity.asset;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="assettranche", namespace="com.amundi.tech.openaml.entity.asset")
public class AssetTranche {

    /**
     * Type of tranche
     */
    private String type;

    /**
     * The attachment point of the tranche is the portfolio loss lower threshold above which the tranche's principal gets hit if one or several reference entities default within the portfolio. Value is expressed in %, e.g. 20 means 20%.
     */
    private BigDecimal attachmentPt;

    /**
     * The detachment point of the tranche is the portfolio loss upper threshold above which the tranche's principal gets wiped out after one or several events of default. Value is expressed in %, e.g. 20 means 20%.
     */
    private BigDecimal detachmentPt;

}