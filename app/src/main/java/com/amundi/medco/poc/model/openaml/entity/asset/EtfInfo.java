package com.amundi.medco.poc.model.openaml.entity.asset;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="etfinfo", namespace="com.amundi.tech.openaml.entity.asset")
public class EtfInfo {

    /**
     * Swap spread rate 
     */
    private BigDecimal swapSpreadRate;

    /**
     * The parity of the  index leg vs the underling index applied in the context of ETF/ELS
     */
    private BigDecimal parity;

    /**
     * The swap spread  for index leg applied in the context of ETF/ELS
     */
    private BigDecimal cumulSwapSpread;

    /**
     * Dividends for index leg applied in the context of ETF/ELS
     */
    private BigDecimal cumulDividends;

    /**
     * Management Fees  for index leg applied in the context of ETF/ELS
     */
    private BigDecimal cumulManagementFees;

}