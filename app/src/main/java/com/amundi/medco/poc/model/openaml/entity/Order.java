package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.component.AccountingStandard;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="order", namespace="com.amundi.tech.openaml.entity")
public class Order extends Entity {

    /**
     * Order request type : CREATION, CONFIRMATION, DIRECT, PROGRAM, ARBITRAGE, EFP, CFD
     */
    private LVOrderRequestType requestType;

    /**
     * Sign of the order : BUY, SELL, UNWIND see LVOrderSign
     */
    private String sign;

    /**
     * Transaction type or movement type : MI_FUNDIN / MC_SR_SUB / MI_FNDOUT / MC_SR_RED
     */
    private String tradeType;

    /**
     * OPEN if the trade is to enter a position, CLOSE if the trade is to leave a position. Use LVClosingOpening
     */
    private String openingClosingCode;

    /**
     * Order type : AT_BEST, LIMIT, etc.
     */
    private LVOrderType orderType;

    /**
     * Case order limit or stop : the limit price or the stop price
     */
    private BigDecimal orderTypePrice;

    /**
     * Order validity : DAY, GOOD_TILL_DATE, etc.
     */
    private LVOrderValidity validity;

    /**
     * Order validity date if GOOD_TILL_DATE
     */
    private LocalDate validityDate;

    /**
     * Fund manager, portfolio manager : use name and contactInfo
     */
    private ContactInfo fundManager;

    /**
     * Fund manager comment or investor comment
     */
    private Comment investorComment;

    /**
     * Investor that has send the order
     */
    private Party investor;

    /**
     * Date when the order was sent to the trading desk
     */
    private LocalDate sendingDate;

    /**
     * Date when the order was opened by trading desk (trader)
     */
    private LocalDate openingDate;

    /**
     * Trader : name, contact information
     */
    private ContactInfo trader;

    /**
     * Comment entered by trader
     */
    private Comment traderComment;

    /**
     * Last order modification event : SPLIT, GROUP
     */
    private String lastModifEvent;

    /**
     * Last order modification event date : date of the split, group or add to program trade, linked order
     */
    private LocalDate lastModifEventDate;

    /**
     * Quantity expression for order quantity and allocations quantity. QUANTITY, NOMINAL, CONTRACT, CASH
     */
    private LVQuantityExprMode quantityExprMode;

    /**
     * Global quantity if order on same security
     */
    private BigDecimal orderQuantity;

    /**
     * Order investment rational, see LVOrderInvestRational
     */
    private String orderInvestRational;

    /**
     * Requested Nav Date
     */
    private LocalDate orderNavDate;

    /**
     * Asset of the order : Asset, Security (equity, bond, MMI, future, option), OTC (Forex, swap deriv, option otc, SFT) with fields partially filled
     */
    private Asset asset;

    /**
     * true if Placed order (after placement of the order)
     */
    private Boolean placement;

    /**
     * Identifier of the Placed order (after placement of the order)
     */
    private Identifier placementId;

    /**
     * Order Placement : trade date
     */
    private LocalDate tradeDate;

    /**
     * Order Placement : trade currency code
     */
    private String tradeCcy;

    /**
     * Order Placement : settlement date
     */
    private LocalDate settlementDate;

    /**
     * Order Placement : settlement currency code
     */
    private String settlementCcy;

    /**
     * True if the Order is an order with a block of allocations, meaning there is more than 1 allocation with this order
     */
    private Boolean isBlock;

    /**
     * Order allocations by portfolio
     */
    private List<Allocation> allocations;

    /**
     * Parent order of this singleOrder if this order comes from a split order
     */
    private List<Order> parentOrders;

    /**
     * Accounting Standard for IFRS, IAS
     */
    private AccountingStandard accountingStd;

}