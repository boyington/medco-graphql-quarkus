package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCollateralReuse {

  /**
   * Not eligible 
   */
  NONE,

  /**
   * ESMA eligible monetary funds 
   */
  MONETARY_FUNDS,

  /**
   * ESMA eligible non monetary funds 
   */
  NON_MONETARY_FUNDS,

  /**
   * ESMA eligible monetary and non monetary funds 
   */
  ALL_FUNDS,

}