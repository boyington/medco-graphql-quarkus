package com.amundi.medco.poc.model;

import com.amundi.medco.poc.model.openaml.entity.Asset;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Assets {

    private List<Asset> assets = new ArrayList<>();
}
