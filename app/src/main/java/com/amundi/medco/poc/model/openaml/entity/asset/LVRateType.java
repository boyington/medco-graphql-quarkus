package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVRateType {

  /**
   * Fixed rate 
   */
  FIXED,

  /**
   * Floating interest rate is also known as variable rate or adjustable rate. 
   */
  FLOATING,

  /**
   * Reset rate is also known as revisable rate. 
   */
  RESET,

}