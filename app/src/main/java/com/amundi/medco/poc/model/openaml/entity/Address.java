package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="address", namespace="com.amundi.tech.openaml.entity")
public class Address extends Location {

}