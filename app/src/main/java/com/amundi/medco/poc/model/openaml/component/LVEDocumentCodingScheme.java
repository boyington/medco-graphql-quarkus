package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVEDocumentCodingScheme {

  /**
   * EDM document unique identifier 
   */
  DOC_ID,

}