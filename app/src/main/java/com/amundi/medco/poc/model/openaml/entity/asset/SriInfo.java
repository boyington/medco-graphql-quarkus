package com.amundi.medco.poc.model.openaml.entity.asset;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="sriinfo", namespace="com.amundi.tech.openaml.entity.asset")
public class SriInfo {

    /**
     * Overall impact 
     */
    private String overallImpact;

    /**
     * Project type : Percentage of renewable energy
     */
    private BigDecimal renewableEnergy;

    /**
     * Project type : Percentage of alternative energy
     */
    private BigDecimal alternativeEnergy;

    /**
     * Project type : Percentage of energy Distribution and Management
     */
    private BigDecimal energyDistribAndMgmt;

    /**
     * Project type : Percentage of green building
     */
    private BigDecimal greenBuilding ;

    /**
     * Project type : Percentage of green industry
     */
    private BigDecimal greenIndustry ;

    /**
     * Project type : Percentage of green transport
     */
    private BigDecimal greenTransport;

    /**
     * Project type : Percentage of water management
     */
    private BigDecimal waterManagement;

    /**
     * Project type : Percentage of waste and pollutants management
     */
    private BigDecimal wasteAndPollutantsMgmt;

    /**
     * Project type : Percentage of other greens
     */
    private BigDecimal otherGreens;

    /**
     * Percentage of proceeds that are undisclosed ,  not disbursed
     */
    private BigDecimal pctProceedsUndisclosed;

    /**
     * Total percentage use of proceeds that are Green
     */
    private BigDecimal pctGreenProceeds;

    /**
     * Total percentage use of proceeds that are not Green
     */
    private BigDecimal pctNotGreenProceeds;

    /**
     * Region : Africa
     */
    private BigDecimal africa;

    /**
     * Region : Asia
     */
    private BigDecimal asia;

    /**
     * Region : Europe 
     */
    private BigDecimal europe ;

    /**
     * Region : North America
     */
    private BigDecimal northAmerica;

    /**
     * Region : Pacific
     */
    private BigDecimal pacific;

    /**
     * Region : South America
     */
    private BigDecimal southAmerica;

    /**
     * Region : Undisclosed region
     */
    private BigDecimal undisclosedRegion;

    /**
     * Energy saved (GWh)
     */
    private BigDecimal energySaved;

    /**
     * Avoided Energy Intensity (GWh / EUR. million invested) - Total
     */
    private BigDecimal avoidedEnergyIntensity;

    /**
     * Avoided Water Use (m3)
     */
    private BigDecimal avoidedWaterUse ;

    /**
     * Avoided Emissions Attributable to the bond (tCO2-eq)
     */
    private BigDecimal avoidedEmissionsAttribToBond;

    /**
     * Veterans Administration guarantee
     */
    private BigDecimal avoidedEmissionsIntensity;

    /**
     * Capex, Opex, Mixte
     */
    private String expendituresType;

    /**
     * True if Eligible GRECO
     */
    private Boolean eligibilityGRECO;

    /**
     * True if GREEN under AP EGO guidelines
     */
    private Boolean greenUnderAPEGOGuidelines;

}