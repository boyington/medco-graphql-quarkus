package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="location", namespace="com.amundi.tech.openaml.entity")
public class Location extends Entity {

    /**
     * Language code ISO 639-1 (alpha 2), for example ko for Korean language or language code ISO 639-2 (alpha 3)
     */
    private String lang;

    /**
     * Type of filled location/address, normalized value, see LVLocationType/LVAddressType
     */
    private String type;

    /**
     * Other code that is not a country code
     */
    private String code;

    /**
     * Country iso code
     */
    private String ctry;

    /**
     * Country name
     */
    private String ctryName;

    /**
     * Country zone, for example Europe, Asia, etc.
     */
    private String ctryZone;

    /**
     * State, province,  district, territory
     */
    private String state;

    /**
     * City name
     */
    private String city;

    /**
     * Zip code, postal code
     */
    private String zipCode;

    /**
     * First line of the address
     */
    private String address1;

    /**
     * Second line of the address
     */
    private String address2;

    /**
     * Third line of the address
     */
    private String address3;

    /**
     * GPS coordinates, for instance : 48.8392614, 2.3161796
     */
    private String gps;

}