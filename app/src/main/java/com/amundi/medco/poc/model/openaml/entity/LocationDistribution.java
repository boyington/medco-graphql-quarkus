package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="locationdistribution", namespace="com.amundi.tech.openaml.entity")
public class LocationDistribution extends Location {

    /**
     * Category if need to group location types
     */
    private String category;

    /**
     * Distribution percentage of this location
     */
    private BigDecimal pct;

    /**
     * Start date. For instance start date of this distribution percentage
     */
    private LocalDate startDate;

    /**
     * End date,  For instance end date of this distribution percentage
     */
    private LocalDate endDate;

}