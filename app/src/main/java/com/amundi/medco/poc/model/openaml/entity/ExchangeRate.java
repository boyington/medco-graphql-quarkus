package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@JsonRootName(value="exchangerate", namespace="com.amundi.tech.openaml.entity")
public class ExchangeRate extends Entity {

    /**
     * Fixing source
     */
    private String fixingSource;

    /**
     * The main currency or unit currency, also called currency code 1
     */
    private String unitCcy;

    /**
     * The quoted currency, also called currency code 2
     */
    private String quotedCcy;

    /**
     * Exchange rate value
     */
    private BigDecimal rate;

    /**
     * Exchange rate date
     */
    private LocalDate date;

    /**
     * Quote time.
     */
    private LocalTime quoteTime;

    /**
     * Exchange rate type
     */
    private String type;

    /**
     * Percentage in point or price interest point (pip) is the size of points in which the currency was quoted, usually 1bp
     */
    private BigDecimal percentageInPoint;

}