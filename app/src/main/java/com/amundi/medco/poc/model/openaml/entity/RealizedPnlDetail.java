package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.entity.positionkeeping.Movement;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="realizedpnldetail", namespace="com.amundi.tech.openaml.entity")
public class RealizedPnlDetail {

    /**
     * The movement for which we compute the pnl ( generally the sell movement)
     */
    private Movement movement;

    /**
     * The quantity for which we compute the pnl. In fifo for example it can be different for the one in movement
     */
    private BigDecimal quantity;

    /**
     * The origin movement serving as the basis for the computation
     */
    private Movement linkedMovement;

    /**
     * Realised pnl in ptf currency
     */
    private BigDecimal realisedPnlAmt;

    /**
     * Realised pnl in instrument currency
     */
    private BigDecimal realisedLocalPnlAmt;

    /**
     * Unit cost price serving the basis for the computation
     */
    private BigDecimal ucp;

}