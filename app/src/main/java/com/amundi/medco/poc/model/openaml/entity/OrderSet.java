package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="orderset", namespace="com.amundi.tech.openaml.entity")
public class OrderSet extends Entity {

    /**
     * Order set type : PROGRAM_TRADE, LINKED_ORDERS, CROSS_ORDER, MISC
     */
    private LVOrderSetType type;

    /**
     * Order set request type : CREATION, CONFIRMATION, DIRECT, PROGRAM, ARBITRAGE, EFP, CFD
     */
    private LVOrderRequestType requestType;

    /**
     * Fund manager : use name and contactInfo
     */
    private ContactInfo fundManager;

    /**
     * Fund manager comment
     */
    private Comment investorComment;

    /**
     * Investor that has send this set of orders
     */
    private Party investor;

    /**
     * Number of orders in the program trade
     */
    private Integer size;

    /**
     * Program trade orders
     */
    private List<Order> orders;

}