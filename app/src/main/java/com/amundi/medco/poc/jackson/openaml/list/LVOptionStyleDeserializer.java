package com.amundi.medco.poc.jackson.openaml.list;

import com.amundi.medco.poc.model.openaml.entity.asset.LVOptionStyle;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class LVOptionStyleDeserializer extends StdDeserializer<LVOptionStyle> {

    private static final List<String> AMERICAN_VALUES = Arrays.asList("A");
    private static final List<String> EUROPEAN_VALUES = Arrays.asList("E");
    public LVOptionStyleDeserializer() {
        this(null);
    }
    public LVOptionStyleDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LVOptionStyle deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        String strValue = p.getText();
        for(LVOptionStyle lvOptionStyle: LVOptionStyle.values()) {
            if(lvOptionStyle.toString().equals(strValue)) {
                return lvOptionStyle;
            }
        }
        if(AMERICAN_VALUES.contains(strValue)) {
            return LVOptionStyle.AMERICAN;
        }
        if(EUROPEAN_VALUES.contains(strValue)) {
            return LVOptionStyle.EUROPEAN;
        }
        throw new InvalidFormatException(p, String.format("Unable to %s to LVOptionStyle", strValue), strValue, LVOptionStyle.class);
    }
}
