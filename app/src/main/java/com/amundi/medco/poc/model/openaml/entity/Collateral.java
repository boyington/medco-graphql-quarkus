package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.Amounts;
import com.amundi.medco.poc.model.openaml.entity.positionkeeping.Transaction;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="collateral", namespace="com.amundi.tech.openaml.entity")
public class Collateral extends Transaction {

    /**
     * Agreement id : reference between collateral agreement, portfolio and counterparty
     */
    private String refCollateral;

    /**
     * Collateral group : OTC | LB | REPO | REPO_BSK | FX | CFD | MBS | REUSE | OTHER
     */
    private LVCollateralGroup collateralGroup;

    /**
     * Collateral type : CASH or SECURITY
     */
    private LVCollateralType collateralType;

    /**
     * Indicates if the margin is an initial margin (IM) or a variable margin (VM)
     */
    private LVCollateralMarginType marginType;

    /**
     * Safekeeper : CSD, safekeeping place or counterparty, use role CUSTODIAN, CLEARING_BROKER, CLEARING_HOUSE, COUNTERPARTY, TRIPARTY_AGENT
     */
    private Party safekeeper;

    /**
     * Legal form of the transfer: PLEDGED | PLEDGED_REUSE | FULL_OWNERSHIP
     */
    private LVCollateralTransferMode transferMode;

    /**
     * True if the collateral is reused
     */
    private Boolean reused;

    /**
     * Collateral amounts with date and ccy
     */
    private Amounts amounts;

    /**
     * Cash collateral : gives the collateral owner CC or BC
     */
    private LVCollateralOwner collateralOwner;

}