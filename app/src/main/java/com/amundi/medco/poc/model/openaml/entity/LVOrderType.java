package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVOrderType {

  /**
   * Order will be traded at best 
   */
  AT_BEST,

  /**
   * Order will be traded at discretion 
   */
  AT_DISCRETION,

  /**
   * Order will be submitted and will be executed if the price is at or better than the limit price defined in orderTypePrice 
   */
  LIMIT,

  /**
   * Market order 
   */
  MARKET,

  /**
   * undefined 
   */
  CALLABLE,

  /**
   * Order will be traded at market close 
   */
  MARKET_CLOSE,

  /**
   * Order will be traded at volume-weighted average price (vwap) 
   */
  VWAP,

  /**
   * Order will be submitted at the close and will be executed if the closing price is at or better than the limit price 
   */
  LIMIT_CLOSE,

  /**
   * Order will be submitted and will be executed if the price has reached the stop price defined in orderTypePrice 
   */
  STOP,

  /**
   * Order will be traded at the market open 
   */
  MARKET_OPEN,

  /**
   * Order will be submitted at the open and will be executed if the opening price is at or better than the limit price 
   */
  LIMIT_OPEN,

  /**
   * undefined 
   */
  MULTI,

  /**
   * Order will be traded at NAV price 
   */
  NAV,

}