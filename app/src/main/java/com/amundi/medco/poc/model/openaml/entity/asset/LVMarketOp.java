package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVMarketOp {

  /**
   * Creation of the otc, insert new deal 
   */
  CREATE,

  /**
   * Cancel of the otc deal 
   */
  CANCEL,

  /**
   * Cancel the previous version of the deal and reissue a new deal 
   */
  REISSUE,

  /**
   * Exercise option 
   */
  EXERCISE,

  /**
   * Modification of the otc counterparty 
   */
  COUNTERPART_AMENDMENT,

  /**
   * Counterparty step out, or novation (partial or total) with the new counterparty (transferee) 
   */
  STEPOUT,

  /**
   * Unwind ot the otc deal (partial or total) 
   */
  UNWIND,

  /**
   * Expiration of the option 
   */
  EXPIRY,

  /**
   * Knock option 
   */
  KNOCK,

  /**
   * Nominal, notional amount (or capital) is decreased 
   */
  CAPITAL_DECREASE,

  /**
   * Nominal, notional amount (or capital) is increased 
   */
  CAPITAL_INCREASE,

  /**
   * Close detail account 
   */
  ACCOUNT_CLOSING,

  /**
   * Amendment of confirmed otc, excluding modification of party or counterparty 
   */
  RESTRUCTURE,

  /**
   * Modify the otc before confirmation 
   */
  AMENDMENT,

  /**
   * Payment of CDS coverage 
   */
  CREDIT_SETTLEMENT,

}