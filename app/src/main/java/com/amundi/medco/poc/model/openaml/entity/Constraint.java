package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVComparisonOperator;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="constraint", namespace="com.amundi.tech.openaml.entity")
public class Constraint extends Entity {

    /**
     * Constraint type : FORBIDDEN, POSITION (meaning position weight), etc.
     */
    private String type;

    /**
     * Constraint subtype
     */
    private String subtype;

    /**
     * Constraint description
     */
    private String description;

    /**
     * Constraint priority
     */
    private Integer priority;

    /**
     * Constraint level: HARD, SOFT, NONE (see LVConstraintLevel)
     */
    private String level;

    /**
     * Resource type on which the constraint is applied to : PORTFOLIO, LEGAL_ENTITY (use LVEntityType if entity)
     */
    private String resourceType;

    /**
     * identifier of the resource on which the constraint is applied to, for instance the portfolio code
     */
    private Identifier resourceId;

    /**
     * Optional resource name
     */
    private String resourceName;

    /**
     * List of similar identifiers on which the constraint is applied to
     */
    private List<Identifier> onIds;

    /**
     * List of except identifiers, excluded identifiers from this constraint
     */
    private List<Identifier> exceptIds;

    /**
     * Comparison operator apply to criterion, criterion2 or criteria :  EQ, NEQ, LT, LTE, GT, GTE, etc.
     */
    private LVComparisonOperator operator;

    /**
     * Constraint criterion value, name can be used to complete the Constraint type/subtype
     */
    private Attribute criterion;

    /**
     * Second constraint criterion value if operator is BETWEEN or NOT_BETWEEN
     */
    private Attribute criterion2;

    /**
     * Constraint criteria
     */
    private List<Attribute> criteria;

}