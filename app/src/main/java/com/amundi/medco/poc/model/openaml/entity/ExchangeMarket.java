package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.Period;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="exchangemarket", namespace="com.amundi.tech.openaml.entity")
public class ExchangeMarket extends Party {

    /**
     * True if regulated
     */
    private Boolean regulated;

    /**
     * Market type
     */
    private String marketType;

    /**
     * Specific primary exchange name in english, for example if Japanese primary exchange, will contain specific market type name
     */
    private String specificPrimaryExch;

    /**
     * True if the exchange market country is an emerging country
     */
    private Boolean emergingMarket;

    /**
     * Opening hours defined as a text, for instance 9:00am - 7:00pm
     */
    private String openingHours;

    /**
     * Opening hours defined as a period of time. Use Period.fromTime and Period.endTime
     */
    private Period openingPeriod;

    /**
     * Operating MIC code
     */
    private String operatingMIC;

}