package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVConversionOption {

  /**
   * Rate type is modified from fixed rate to float rate 
   */
  FIX_TO_FLOAT,

  /**
   * Rate type is modified from fixed rate to float rate 
   */
  FLOAT_TO_FIX,

  /**
   * If need to indicate none conversion option 
   */
  NONE,

}