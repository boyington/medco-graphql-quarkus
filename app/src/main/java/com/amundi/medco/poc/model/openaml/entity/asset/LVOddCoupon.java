package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVOddCoupon {

  /**
   * First coupon period is long, first long odd coupon 
   */
  FL,

  /**
   * First short odd coupon period 
   */
  FS,

  /**
   * Last coupon period is long 
   */
  LL,

  /**
   * Last coupon period is short 
   */
  LS,

  /**
   * First odd coupon and full coupon 
   */
  FF,

  /**
   * Last odd coupon and full coupon 
   */
  LF,

  /**
   * First long odd coupon, case both ends backward (backward schedule calculation from roll date) 
   */
  BL,

  /**
   * Both first coupon and last coupon period are short (backward schedule calculation from roll date) 
   */
  BS,

  /**
   * First full odd coupon, case both ends backward (backward schedule calculation from roll date) 
   */
  BF,

  /**
   * Last long odd coupon, case both ends forward  (forward schedule calculation from roll date) 
   */
  OL,

  /**
   * Last short odd coupon, case both ends forward (forward schedule calculation from roll date) 
   */
  OS,

  /**
   * Last full odd coupon, case both ends forward (forward schedule calculation from roll date) 
   */
  OF,

  /**
   * Both first coupon and last coupon period are odd either long or short 
   */
  BOTH,

  /**
   * Not odd coupon period 
   */
  NONE,

}