package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPartyRole {

  /**
   * A client, a customer 
   */
  CLIENT,

  /**
   * A prospect, a potential client 
   */
  PROSPECT,

  /**
   * A lead contact, in commercial relations, a contact can a lead before becoming a prospect, and then a client/customer 
   */
  LEAD,

  /**
   * A contact in a relation 
   */
  CONTACT,

  /**
   * Asset manager, or asset management company 
   */
  ASSET_MANAGER,

  /**
   * The custodian safekeeps assets on behalf of the owner, mainly this is a custodian bank. Custodian banks are also known as global custodian when they safe keep assets for clients on a world  perimeter. For instance, mutual fund custodians hold and safekeep the securities that are owned by all the fund's investors. 
   */
  CUSTODIAN,

  /**
   * Fund administrator, or fund accountant that is responsible to calculated fund net asset value (NAV). 
   */
  ACCOUNTANT,

  /**
   * Clearer, Clearing agent, clearing broker 
   */
  CLEARER,

  /**
   * The clearing house 
   */
  CLEARING_HOUSE,

  /**
   * Transfer agent maintains records of shareholder transactions and balances 
   */
  TRANSFER_AGENT,

  /**
   * Party responsible for executing an order 
   */
  BROKER,

  /**
   * Electronic platform, for example EMS 
   */
  ELECTRONIC_PLATFORM,

  /**
   * Counterparty, in the case of an OTC contract 
   */
  COUNTERPARTY,

  /**
   * Agent which acts as an intermediary between two parties for the administration of tri-party collateral transactions 
   */
  TRIPARTY_AGENT,

  /**
   * Issuer, in the case of a security (bond, equity, MMI) 
   */
  ISSUER,

  /**
   * Obligor, mainly the Issuer, in the case of debt instrument 
   */
  OBLIGOR,

  /**
   * Account keeper, e.g. the management company of corporate savings plans 
   */
  ACCOUNT_KEEPER,

  /**
   * Organisation used as the safekeeping place for the securities (PSAF) 
   */
  SAFEKEEPING_PLACE,

  /**
   * A depository is mainly a bank that hold securities and enable clearing and settlements. It also manages the transfer of ownership of shares. The fund depository is a custodian, plus must ensures that the fund's assets are held independently of the investment manager. 
   */
  DEPOSITORY,

  /**
   * Provider of market data, e.g. Reuters, Bloomberg, etc. 
   */
  MARKET_DATA_PROVIDER,

  /**
   * Fund as a third party 
   */
  FUND,

  /**
   * Fund manager, portfolio manager 
   */
  FUND_MANAGER,

  /**
   * Research analyst 
   */
  RESEARCH_ANALYST,

  /**
   * Performance analyst 
   */
  PERFORMANCE_ANALYST,

  /**
   * Risk manager 
   */
  RISK_MANAGER,

  /**
   * Reference Data Management 
   */
  REFERENCE_DATA_MGMT,

  /**
   * Regulatory authority, eg: AMF 
   */
  REGULATORY_AUTHORITY,

  /**
   * Place of settlement (PSET), where settlement of securities occurs 
   */
  SETTLEMENT_PLACE,

  /**
   * Financial place : stock exchange 
   */
  STOCK_EXCHANGE,

  /**
   * Financial place : over-the-counter place 
   */
  OTC_PLACE,

  /**
   * Venue, trading place, trading venue 
   */
  VENUE,

  /**
   * Banking institution 
   */
  BANK,

}