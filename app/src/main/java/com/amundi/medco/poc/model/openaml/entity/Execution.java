package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.component.Amounts;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.FeesRateAmount;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="execution", namespace="com.amundi.tech.openaml.entity")
public class Execution extends Entity {

    /**
     * True if this is a trade confirmation, i.e. the broker has confirmed trade details
     */
    private Boolean confirmation;

    /**
     * Confirmation type if needed
     */
    private String confirmationType;

    /**
     * Execution trade type group
     */
    private String tradeTypeGroup;

    /**
     * Execution trade type 
     */
    private String tradeType;

    /**
     * Date and time of the trade
     */
    private LocalDateTime tradeDateTime;

    /**
     * Settlement date of the execution
     */
    private LocalDate settlementDate;

    /**
     * Broker of the trade execution
     */
    private Party broker;

    /**
     * Broker venue
     */
    private String brokerVenue;

    /**
     * Counterparty of the trade execution when using electronic platform. It is the entity on the other side of the financial transaction.
     */
    private Party counterparty;

    /**
     * Trading venue
     */
    private String tradingVenue;

    /**
     * Trading platform : NOWCP, TRITON, etc.
     */
    private String tradingPlatform;

    /**
     * Trader
     */
    private ContactInfo trader;

    /**
     * Trader comment
     */
    private Comment traderComment;

    /**
     * Used in RTO context (mapping MCE feesCurCod) with values ALGO, DMA, PT (dealing neither ALGO, nor DMA)
     */
    private String emsTradingType;

    /**
     * Execution venue, can be the equity stock exchange (MIC)
     */
    private String executionVenue;

    /**
     * Stipulation for TBA dealing, STIP trade
     */
    private String stipulation;

    /**
     * Asset, otc (forex, irs, option otc), security (listed)
     */
    private Asset asset;

    /**
     * Sign of trade: + or -, will be applied to the quantity
     */
    private String sign;

    /**
     * Quantity of asset
     */
    private BigDecimal quantity;

    /**
     * Expression of the quantity
     */
    private LVQuantityExprMode quantityExprMode;

    /**
     * Execution amounts in trading currency
     */
    private Amounts tradingAmounts;

    /**
     * Execution amounts in settlement currency
     */
    private Amounts settlementAmounts;

    /**
     * Broker fees rate and/or amount
     */
    private FeesRateAmount brokerFees;

    /**
     * VAT fees rate and/or amount
     */
    private FeesRateAmount vatFees;

    /**
     * Taxes rate and/or amount
     */
    private FeesRateAmount taxes;

    /**
     * Other fees and/or amount
     */
    private FeesRateAmount otherFees;

    /**
     * Free fees,  misceallenous fees rate and/or amount
     */
    private FeesRateAmount miscFees;

    /**
     * Transfer fees or Movement fees : rate, amout, and currency,  CMV in french.
     */
    private FeesRateAmount transferFees;

    /**
     * Entf fees, entrance commission rate and/or amount
     */
    private FeesRateAmount entranceCommissionFees;

    /**
     * Mutual fund fees rate and/or amount
     */
    private FeesRateAmount mutualFundFees;

    /**
     * Distribution fees rate and/or amount
     */
    private FeesRateAmount distributionFees;

    /**
     * Withholding tax rate and/or amount
     */
    private FeesRateAmount withholdingTax;

    /**
     * ADI - advisory - fees rate and/or amount
     */
    private FeesRateAmount adiFees;

    /**
     * Clearing fees rate and/or amount
     */
    private FeesRateAmount clearingFees;

    /**
     * Market clearing fees rate and/or amount
     */
    private FeesRateAmount marketClearingFees;

    /**
     * Market execution fees rate and/or amount
     */
    private FeesRateAmount marketExecutionFees;

    /**
     * The execution price (executionPrice.netPrice) used to calculate execution amounts
     */
    private Quotation executionPrice;

    /**
     * True if the Execution is an exacution with a block of allocations, meaning there is more than 1 allocation with this execution
     */
    private Boolean isBlock;

    /**
     * Execution allocations by portfolio
     */
    private List<Allocation> allocations;

    /**
     * Number of matching days difference
     */
    private Integer matchingDayDiff;

    /**
     * Date and time of the execution
     */
    private LocalDateTime executionDateTime;

    /**
     * Date and time of confirmation
     */
    private LocalDateTime confirmationDateTime;

    /**
     * Settlement and delivery instructions, or SSI associated to the trade execution
     */
    private Sdi sdi;

    /**
     * Middle Officer
     */
    private ContactInfo middleOfficer;

    /**
     * Middle officer comment
     */
    private Comment middleOfficerComment;

    /**
     * True if the order has been created to reinvest cash collateral
     */
    private Boolean collateralReinvestment;

    /**
     * Identifier of the placed order
     */
    private Identifier placementId;

    /**
     * Order linked to this execution
     */
    private Order order;

    /**
     * Next highest bids offered by competing/covering broker. It is also called cover bids. Use competingPrice.netPrice
     */
    private List<Quotation> competingPrices;

}