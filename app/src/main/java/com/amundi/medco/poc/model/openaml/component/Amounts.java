package com.amundi.medco.poc.model.openaml.component;

import com.amundi.medco.poc.model.openaml.LVAdjustedValue;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.entity.LVPriceType;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="amounts", namespace="com.amundi.tech.openaml.component")
public class Amounts {

    /**
     * Type of amounts : actual price, trade amounts, valuation amounts, etc.
     */
    private String type;

    /**
     * Calculation date of the amounts
     */
    private LocalDate date;

    /**
     * Currency code associated to followings amounts and prices
     */
    private String ccy;

    /**
     * Mark to market amount (if netPrice is the market price) or Net amount (netAmt = cleanAmt + accrIntAmt)
     */
    private BigDecimal netAmt;

    /**
     * Gross amount : grossAmt = netAmt + feesAmt if feesAmt  is negative
     */
    private BigDecimal grossAmt;

    /**
     * Net amount without interests amounts, clean net amount : netAmt = cleanAmt + accrIntAmt
     */
    private BigDecimal cleanAmt;

    /**
     * Accrued interests amounts : netAmt = cleanAmt + accrIntAmt
     */
    private BigDecimal accrIntAmt;

    /**
     * Fees amount = grossAmt - netAmt
     */
    private BigDecimal feesAmt;

    /**
     * Redemption amount is the amount paid at maturity
     */
    private BigDecimal redemptionAmt;

    /**
     * Premium amount is the upfront premium amount
     */
    private BigDecimal premiumAmt;

    /**
     * Next accrued interests amounts (1 period) related to the date of the accrIntAmt
     */
    private BigDecimal nextAccrIntAmt;

    /**
     * Total interest amounts (N periods) without redemption amount, all interests amounts, or all remaining interest amounts
     */
    private BigDecimal interestsAmt;

    /**
     * Optional date of the exchange rate
     */
    private LocalDate fxRateDate;

    /**
     * Exchange rate used to calculate the amount if a fx conversion was needed
     */
    private BigDecimal fxRate;

    /**
     * Unit currency code of exchange rate or fx conversion, i.e  1 unitCcy = fxRate quotedCcy
     */
    private String fxUnitCcy;

    /**
     * Quoted currency code of exchange rate or fx conversion, i.e 1 unitCcy = fxRate quotedCcy
     */
    private String fxQuotedCcy;

    /**
     * Price date of prices/values
     */
    private LocalDate priceDate;

    /**
     * Net price or valuation price expressed in price unit : netPrice = cleanPrice + accrIntPrice
     */
    private BigDecimal netPrice;

    /**
     * Gross price expressed in price unit
     */
    private BigDecimal grossPrice;

    /**
     * Clean net price
     */
    private BigDecimal cleanPrice;

    /**
     * Accrued interests expressed as price or unit : netPrice = cleanPrice + accrIntPrice
     */
    private BigDecimal accrIntPrice;

    /**
     * Fees expressed as price : feesPrice + grossPrice = netPrice assuming feesPrice is negative
     */
    private BigDecimal feesPrice;

    /**
     * Redemption price, redemption value
     */
    private BigDecimal redemptionPrice;

    /**
     * Premium price, premium value is the upfront premium expressed as price or unit
     */
    private BigDecimal premiumPrice;

    /**
     * Coupon settlement date in a transaction, or the period end date used to calculate the accrued interests
     */
    private LocalDate accrIntDate;

    /**
     * Number of accrued interest days linked to the accrIntPrice or accrIntAmt based on previous coupon date and coupon settlement date (accrIntDate)
     */
    private Integer accrIntDays;

    /**
     * Next accrued interests expressed as price or unit
     */
    private BigDecimal nextAccrIntPrice;

    /**
     * Yield such as yield to maturity (YTM), internal rate of return (IRR), etc., it is often forward-looking.
     */
    private BigDecimal yield;

    /**
     * Price type : OPEN, CLOSE, etc.
     */
    private LVPriceType priceType;

    /**
     * Unit expression of prices : PERCENT, YIELD (if return), RATIO, etc.
     */
    private LVUnit priceUnit;

    /**
     * Price has been adjusted with ratio, e.g. price with inflation mid, return coefficient
     */
    private LVAdjustedValue adjusted;

    /**
     * True if price used in Amounts has been forced
     */
    private Boolean hasForcedPrice;

    /**
     * True if the price is final, definitive if NAV price
     */
    private Boolean finalPrice;

    /**
     * A ratio to apply to the price, the return, e.g. index ratio, return coefficient, see adjusted field
     */
    private BigDecimal ratio;

}