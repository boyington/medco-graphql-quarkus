package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.entity.positionkeeping.Movement;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="cashbalance", namespace="com.amundi.tech.openaml.entity")
public class CashBalance extends Entity {

    /**
     * Optional cash balance type
     */
    private String type;

    /**
     * Cash balance value date
     */
    private LocalDate valueDate;

    /**
     * Cash balance portfolio
     */
    private Portfolio portfolio;

    /**
     * List of movements that are part of this cash balance
     */
    private List<Movement> movements;

}