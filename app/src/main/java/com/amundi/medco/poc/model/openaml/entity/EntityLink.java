package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="entitylink", namespace="com.amundi.tech.openaml.entity")
public class EntityLink extends Entity {

    /**
     * Type of link
     */
    private String type;

    /**
     * Start date of the link
     */
    private LocalDate startDate;

    /**
     * End date of the link
     */
    private LocalDate endDate;

    /**
     * Percentage of this link
     */
    private BigDecimal pct;

}