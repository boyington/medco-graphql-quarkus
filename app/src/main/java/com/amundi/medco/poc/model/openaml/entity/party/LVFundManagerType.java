package com.amundi.medco.poc.model.openaml.entity.party;

/**
 * undefined
 */
public enum LVFundManagerType {

  /**
   * Principal fund manager 
   */
  PRINCIPAL,

  /**
   * Backup fund manager 
   */
  BACKUP,

  /**
   * Additional fund manager 
   */
  ADDITIONAL,

}