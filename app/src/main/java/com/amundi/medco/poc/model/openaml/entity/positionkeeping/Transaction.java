package com.amundi.medco.poc.model.openaml.entity.positionkeeping;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.component.Amounts;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.FeesRateAmount;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.amundi.medco.poc.model.openaml.entity.LVPosTransLifecycle;
import com.amundi.medco.poc.model.openaml.entity.Party;
import com.amundi.medco.poc.model.openaml.entity.Portfolio;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="transaction", namespace="com.amundi.tech.openaml.entity.positionkeeping")
public class Transaction extends Entity {

    /**
     * Stock transaction type, or Cash transaction type
     */
    private String transType;

    /**
     * Fill with external type codification
     */
    private String transExtType;

    /**
     * Fill with external type codification name or label
     */
    private String transExtTypeName;

    /**
     * Specifies the lifecycle step of the transaction. The ordered lifecycle steps are SIMULATED, ORDERED, ALLOCATED, CONFIRMED, BOOKED, SETTLED.
     */
    private LVPosTransLifecycle transLifecycle;

    /**
     * Category of the transaction
     */
    private String category;

    /**
     * True if the transaction will only affect cash account (cash movements)
     */
    private Boolean isCash;

    /**
     * Identifier of the initial transaction
     */
    private Identifier transInitialId;

    /**
     * Identifier of the cancelled transaction linked to this one
     */
    private Identifier cancelId;

    /**
     * Portfolio
     */
    private Portfolio portfolio;

    /**
     * Transaction on this asset : security, currency, index, otc
     */
    private Asset asset;

    /**
     * Sign of Transaction: + or -, will be applied to the quantity
     */
    private String sign;

    /**
     * Unsigned quantity
     */
    private BigDecimal quantity;

    /**
     * Expression mode of the quantity :QUANTITY, NOMINAL, CONTRACT, CASH
     */
    private LVQuantityExprMode quantityExprMode;

    /**
     * Strategy code
     */
    private String strategyCode;

    /**
     * Strategy name
     */
    private String strategyName;

    /**
     * Trade date of this transaction
     */
    private LocalDate tradeDate;

    /**
     * Transaction amounts in trade currency.
     */
    private Amounts tradingAmounts;

    /**
     * Settlement date of this transaction
     */
    private LocalDate settlementDate;

    /**
     * Transaction amounts in settlement currency. If cash transaction, this is equivalent to cash amount.
     */
    private Amounts settlementAmounts;

    /**
     * Broker fees rate and/or amount
     */
    private FeesRateAmount brokerFees;

    /**
     * VAT fees rate and/or amount
     */
    private FeesRateAmount vatFees;

    /**
     * Taxes rate and/or amount
     */
    private FeesRateAmount taxes;

    /**
     * Other fees and/or amount
     */
    private FeesRateAmount otherFees;

    /**
     * Free fees,  misceallenous fees rate and/or amount
     */
    private FeesRateAmount miscFees;

    /**
     * Entf fees, entrance commission rate and/or amount
     */
    private FeesRateAmount entranceCommissionFees;

    /**
     * Mutual fund fees rate and/or amount
     */
    private FeesRateAmount mutualFundFees;

    /**
     * Distribution fees rate and/or amount
     */
    private FeesRateAmount distributionFees;

    /**
     * Withholding tax rate and/or amount
     */
    private FeesRateAmount withholdingTax;

    /**
     * Transaction market value in portfolio currency (mtm and price). If cash transaction, this is equivalent to cash amount in portfolio currency.
     */
    private Amounts marketValue;

    /**
     * Optional Transaction market value in alternative currency.
     */
    private Amounts altMarketValue;

    /**
     * Transaction Market value in local (asset, security or otc) currency.
     */
    private Amounts localMarketValue;

    /**
     * Broker scale
     */
    private String brokerScale;

    /**
     * Broker (_id, name) of the execution
     */
    private Party broker;

    /**
     * Counterparty (_id, name) of the transaction
     */
    private Party counterparty;

    /**
     * Transfer agent (TA) if SR transaction, if no transfer agent, it can be the account keeper (role = ACCOUNT_KEEPER) of the account that has originated this transaction 
     */
    private Party transferAgent;

    /**
     * Position type, holding type impacted by this transaction
     */
    private String positionType;

    /**
     * Comment associated to the transaction
     */
    private Comment comment;

    /**
     * risk level associated to transaction
     */
    private String riskLevel;

}