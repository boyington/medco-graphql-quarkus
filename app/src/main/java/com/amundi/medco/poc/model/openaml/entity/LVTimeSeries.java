package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVTimeSeries {

  /**
   * Time series of prices 
   */
  PRICE,

  /**
   * Time series of Net Asset Values 
   */
  NAV,

  /**
   * Time series of price returns expressed in percent 
   */
  RETURN,

  /**
   * Time series of mandate valuations 
   */
  MANDATE_VALUATION,

  /**
   * Time series of portfolio valuations 
   */
  PORTFOLIO_VALUATION,

}