package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVAssetStatus {

  /**
   * Open status means the asset is alive 
   */
  OPEN,

  /**
   * Closed status if the asset has matured, or has been closed 
   */
  CLOSED,

}