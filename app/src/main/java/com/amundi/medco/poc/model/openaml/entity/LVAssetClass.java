package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVAssetClass {

  /**
   * Currency 
   */
  CURRENCY,

  /**
   * Equity 
   */
  EQUITY,

  /**
   * Long term fixed income, including bonds, securitizations, insurance linked securities 
   */
  FIXED_INCOME,

  /**
   * Mutual fund, ETF, HEDGE_FUND, REIT, SCPI, etc. 
   */
  FUND_SHARE,

  /**
   * Short term money market instrument (MMI), Short term fixed income 
   */
  MONEY_MARKET,

  /**
   * Derivative instrument (listed or OTC) 
   */
  DERIVATIVE,

  /**
   * Foreign exchange: FX forward, FX spot, FX swap 
   */
  FOREX,

  /**
   * Securities lending, Repo, Buy sell backs (SFT = Securities financing transaction) 
   */
  SECURITIES_FINANCING_TRANSACTION,

  /**
   * Stock index, rate index, economic indicator, credit index 
   */
  INDEX,

  /**
   * Real asset 
   */
  REAL_ASSET,

  /**
   * Commodity, for example, oil, gold, forestry, etc. 
   */
  COMMODITY,

}