package com.amundi.medco.poc.model.openaml.entity.asset;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="interestamount", namespace="com.amundi.tech.openaml.entity.asset")
public class InterestAmount {

    /**
     * Date corresponding to the accrued interest value date to calculate accrInt and accrIntN
     */
    private LocalDate accrIntValueDate;

    /**
     * Provided accrued interest unit at D without modifications, mainly Bloomberg INT_ACC
     */
    private BigDecimal accrIntRaw;

    /**
     * Accrued interest unit, or price equivalent at D
     */
    private BigDecimal accrInt;

    /**
     * Accrued interest unit at settle date, or price equivalent at D+N = next settlement date
     */
    private BigDecimal accrIntN;

    /**
     * Accrued interest unit fo 1 day
     */
    private BigDecimal accrIntDaily;

    /**
     * AccrInt at trade date
     */
    private BigDecimal tradeDateAccrInt;

    /**
     * Currency code of interest amounts
     */
    private String ccy;

    /**
     * Date of the next settlement date, the interest period calculation end date if accrIntN
     */
    private LocalDate settlementDate;

    /**
     * Trade date
     */
    private LocalDate tradeDate;

    /**
     * Adjusted factor on accrued interest
     */
    private BigDecimal accruedFactor;

    /**
     * #days since previous coupon date
     */
    private Integer accruedDays;

    /**
     * # days between settlement date and next coupon date
     */
    private Integer daysToReset;

    /**
     * Indicates which type of odd coupon period is calculated : FL, FS, LL, LS
     */
    private LVOddCoupon oddCoupon;

    /**
     * Accrual date, the start date of accrued interest period
     */
    private LocalDate accrualDate;

    /**
     * Previous of prevCouponDate compared to accrued interest value date
     */
    private LocalDate baseAccRtDate;

    /**
     * Previous of baseAccRtDate compared to accrued interest value date
     */
    private LocalDate baseAccRtDateBack1;

    /**
     * Cash flow amount to be paid to the next payment date
     */
    private BigDecimal nextCashFlow;

    /**
     * Date of the next cash flow linked to the coupon payment scheduling and redemption scheduling, can be the next settlement date
     */
    private LocalDate nextCashFlowDate;

    /**
     * Net Coupon Rate
     */
    private BigDecimal netCouponRate;

}