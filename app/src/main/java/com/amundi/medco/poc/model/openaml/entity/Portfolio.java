package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.*;
import com.amundi.medco.poc.model.openaml.entity.analytic.Rating;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="portfolio", namespace="com.amundi.tech.openaml.entity")
public class Portfolio extends Entity {

    /**
     * Portfolio level type : FUND (compartment or mutual fund), BUCKET,  POCKET (parent is the fund), MANDATE, PORTFOLIO, MODEL
     */
    private LVPtfLevelType type;

    /**
     * Portfolio nature as defined in the portfolio system, if fund, see the legalNature
     */
    private Codification ptfNature;

    /**
     * Portfolio currency code
     */
    private String ccy;

    /**
     * Portfolio starting date, when the fund began investing in the market
     */
    private LocalDate startingDate;

    /**
     * Portfolio closing date
     */
    private LocalDate closingDate;

    /**
     * Portfolio strategy code
     */
    private String strategyCode;

    /**
     * This portfolio is part of this credit risk group
     */
    private String creditRiskGroup;

    /**
     * Portfolio risk process
     */
    private Classif riskProcess;

    /**
     * Portfolio risk sites
     */
    private List<Codification> sites;

    /**
     * Portfolio main classif
     */
    private Classif mainClassif;

    /**
     * Portfolio other classifs
     */
    private List<Classif> classifs;

    /**
     * Portfolio management strategy level1
     */
    private Classif strategy1;

    /**
     * Portfolio management strategy level2
     */
    private Classif strategy2;

    /**
     * Portfolio management strategy level3
     */
    private Classif strategy3;

    /**
     * Accounting standard type (IAS39, IFRS9), business model (IFRS9), accounting class (IFRS9), management intent (IAS39)
     */
    private AccountingStandard accountingStd;

    /**
     * Portfolio security account
     */
    private Account securityAccount;

    /**
     * Portfolio other security account
     */
    private List<Account> otherAcounts;

    /**
     * Portfolio main cash account
     */
    private Account mainCashAccount;

    /**
     * Portfolio other cash accounts
     */
    private List<Account> cashAccounts;

    /**
     * Portfolio accounting sector division
     */
    private String accountingSectorDivision;

    /**
     * Portfolio fund administrator or portfolio accountant
     */
    private Party accountant;

    /**
     * In case of mirror valuation, the fund administrator or accountant that is performing the mirror valuation
     */
    private Party mirrorAccountant;

    /**
     * Portfolio custodian/depositary company
     */
    private Party custodian;

    /**
     * Portfolio clearing agent or clearer
     */
    private Party clearer;

    /**
     * True if Portfolio is eligible to collateral management
     */
    private Boolean collatEligible;

    /**
     * Collateral OTC : Portfolio default clearing broker,  clearing member 
     */
    private Party clearingBroker;

    /**
     * Collateral OTC  : List of portfolio accounts and their structure for the Central counterparty clearing (CCP, clearing house) by asset types (IRS, CDS, etc.) in relations with the clearing broker
     */
    private List<Account> ccpAccounts;

    /**
     * Order managemet system (OMS)  routing code
     */
    private String omsRoutingCode;

    /**
     * Corporate action process code and name
     */
    private Codification corpActionProcess;

    /**
     * Portfolio asset manager or management company
     */
    private Party assetManager;

    /**
     * If delegated, the delegation mode
     */
    private String delegationMode;

    /**
     * Portfolio delegation management type
     */
    private Codification managementType;

    /**
     * Portfolio delegated asset manager, portfolio management type
     */
    private Party delegatedAssetManager;

    /**
     * Portfolio auditor
     */
    private Party auditor;

    /**
     * Portfolio investment advisor, or financial adviser, is a third party company or individual  who makes recommendations that have to be approved by the investor.
     */
    private Party investmentAdvisor;

    /**
     * Portfolio official/default pricing policy. For the portfolio inventory, this pricing policy can be adjusted to specific valuation context.
     */
    private PricingPolicy pricingPolicy;

    /**
     * Launch date is the date of 1st subscription in the sub series, inception date in the context of performances
     */
    private LocalDate launchDate;

    /**
     * Portfolio valuation frequency : DAILY, MONTHLY, etc.
     */
    private LVFrequency valuationFreq;

    /**
     * Fund NAV valuation periodicity description
     */
    private Period navPeriodicity;

    /**
     * NAV type code and name
     */
    private Codification navType;

    /**
     * Money market fund NAV policy can be VNAV, LVNAV, CNAV
     */
    private String navPolicy;

    /**
     * Indicates the cut-off date time for NAV receipt, for instance D_18H, etc.
     */
    private String cutOffDateTimeForNAVReceipt;

    /**
     * Expected date and time for NAV receipt expressed as a timestamp
     */
    private LocalDateTime expectedDateTimeForNAVReceipt;

    /**
     * Delay for NAV receipt in number of hours, can be half an hour
     */
    private BigDecimal delayForNAVReceipt;

    /**
     * True if the swing pricing method is applied to the fund shares
     */
    private Boolean swingPricingFlag;

    /**
     * Swing pricing comment
     */
    private Comment swingPricingComment;

    /**
     * swing pricing start date
     */
    private LocalDate swingPricingStartDate;

    /**
     * swing pricing end date
     */
    private LocalDate swingPricingEndDate;

    /**
     * Fund redemption frequency, code and name
     */
    private Period redemptionFrequency;

    /**
     * Date of the termination letter sent to the trading manager in order to finalize the management delegation over the fund
     */
    private LocalDate terminationDate;

    /**
     * Reason for the portfolio termination, or mandate termination
     */
    private String terminationReason;

    /**
     * Portfolio manager, or fund manager
     */
    private ContactInfo fundManager;

    /**
     * List of backup fund manager
     */
    private List<ContactInfo> backupManagers;

    /**
     * List of additional fund manager
     */
    private List<ContactInfo> additionalManagers;

    /**
     * List of teams related to this portfolio and the officer in charge of this portfolio
     */
    private List<TeamAndOfficer> teamAndOfficers;

    /**
     * List of fund documents
     */
    private List<EDocument> documents;

    /**
     * Deposits master agreement with list of counterparties
     */
    private Agreement depositFmkAgreement;

    /**
     * List of framework agreement with list of counterparties
     */
    private List<Agreement> fmkAgreements;

    /**
     * Collateral master agreement with list of counterparties
     */
    private Agreement collateralAgreement;

    /**
     * List of master confirmations agreement with list of counterparties
     */
    private List<Agreement> masterConfirmations;

    /**
     * Legal data : ESG status
     */
    private String esgStatus;

    /**
     * ESG beat benchmark : the code indicates if the portfolio is in the scope of this process
     */
    private Codification esgBeatBench;

    /**
     * Legal data : European directive
     */
    private Codification europeanDirective;

    /**
     * Legal data : European distribution (Coordinated, Non-Coordinated)
     */
    private Codification europeanDistribution;

    /**
     * Legal data : Fund customer type (Dedicated, All subscribers), investors nature
     */
    private Codification customerType;

    /**
     * Country law or domicile related to the residential country of the legal entity, e.g. French if mutual fund domicile
     */
    private Codification countryLaw;

    /**
     * Fund Legal data : Offshore or Domestic based on current country of the company's principal executive offices
     */
    private String residenceType;

    /**
     * Fund Legal data : geographic location name
     */
    private Location geographicLocation;

    /**
     * Fund Legal data : investment sector code and name
     */
    private Classif investmentSector;

    /**
     * Fund Legal data : Fund legal nature
     */
    private Codification legalNature;

    /**
     * Fund Legal data : Fund streamlined
     */
    private Codification streamlined;

    /**
     * Fund Legal data : Fund tax regime, for example DSK, PEA, etc.
     */
    private Codification taxRegime;

    /**
     * Fund Legal data : Fund fiscality, for example DSK, PEA, etc.
     */
    private Codification fiscality;

    /**
     * Fund Legal data : Fund vocation
     */
    private Codification vocation;

    /**
     * Fund Legal data : Pension fund sector law
     */
    private Codification sectorLaw;

    /**
     * Fund Legal data : Fund type, master/feeder
     */
    private Codification fundType;

    /**
     * Fund Legal data : Fund Characteristics
     */
    private Codification fundCharacteristics;

    /**
     * Portfolio legal name
     */
    private String legalName;

    /**
     * Portfolio legal structure
     */
    private String legalStructure;

    /**
     * Portfolio indexed 
     */
    private String indexed;

    /**
     * Portfolio feeder 
     */
    private String feeder;

    /**
     * True if the portfolio is eligible to PEA (see fiscality)
     */
    private Boolean eligiblePEA;

    /**
     * True if the portfolio is eligible to DSK fiscality  (see fiscality)
     */
    private Boolean eligibleDSK;

    /**
     * Fund : True if this fund is structured as a multiple compartment fund, also known as umbrella fund
     */
    private Boolean multipleCompartment;

    /**
     * Fund guarantee end date
     */
    private LocalDate guaranteeEndDate;

    /**
     * Fund : guarantee liquidity amount
     */
    private BigDecimal guaranteeLiquidityAmount;

    /**
     * Guarantee status
     */
    private String guaranteeStatus;

    /**
     * Fund : guaranteed amount
     */
    private BigDecimal guaranteedAmt;

    /**
     * Protection level of the capital
     */
    private BigDecimal protectionLevel;

    /**
     * Cushion : Theorical max loss without breaking Guarantee (in %)
     */
    private BigDecimal cushion;

    /**
     * Authorisation ratio on cash borrowings, limit on cash borrowing
     */
    private BigDecimal borrowingCashLimit;

    /**
     * Fund : modified duration max
     */
    private BigDecimal modifiedDurationMax;

    /**
     * Fund : modified duration min
     */
    private BigDecimal modifiedDurationMin;

    /**
     * Mutual fund holding limit percentage, for example 90 means 90%
     */
    private BigDecimal pctMutualFundHoldingLimit;

    /**
     * Mutual fund holding minimum percentage, for example 20 means 20%
     */
    private BigDecimal pctMutualFundHoldingMin;

    /**
     * Mutual fund : true if has minimum investment Equity at 75% 
     */
    private Boolean hasMinPctEqty75;

    /**
     * Max commitment exposure
     */
    private BigDecimal maxCommitmentExposure;

    /**
     * Portfolio VaR limit
     */
    private BigDecimal ptfVaRLimit;

    /**
     * France legal data : uncalled amount for DSK fund
     */
    private BigDecimal uncalledAmt;

    /**
     * FCPE legal form, legal system
     */
    private Codification fcpeLegalForm;

    /**
     * Scope of the intervention of the Supervisory board on the fund's prospectus (FCPE)
     */
    private String supervisoryBoardScope;

    /**
     * True if this fund or legal entity is owned by platinum client
     */
    private Boolean platinum;

    /**
     * Portfolio client tiering : internal classification
     */
    private String clientTiering;

    /**
     * Portfolio client
     */
    private Party client;

    /**
     *  Distributor code of the portfolio client
     */
    private Codification clientDistributor;

    /**
     * Product type : the type of product associated to this portfolio
     */
    private Codification productType;

    /**
     * Investment policy
     */
    private Codification investPolicy;

    /**
     * Portfolio MMF Classification
     */
    private Codification classificationMMF;

    /**
     * True if this is a fund with a fixed number of shares
     */
    private Boolean closedEndFund;

    /**
     * True if this fund is a reserve fund
     */
    private Boolean reserveFund;

    /**
     * Withholding tax rate and type
     */
    private FeesRateAmount withholdingTax;

    /**
     * Portfolio fiscal period end date
     */
    private LocalDate fiscalEndPeriodDate;

    /**
     * Fund approval date, the date on which the regulator approved the product
     */
    private LocalDate approvalDate ;

    /**
     * Fund supervisory authority, financial conduct authority, Local registration authority
     */
    private Party supervisoryAuthority;

    /**
     * Fund or sub-fund legal entity part
     */
    private LegalEntity legalEntity;

    /**
     * Portfolio pocket scope : FRONT, ACCOUNTANT
     */
    private LVPtfPocketScope pocketScope;

    /**
     * Pocket management type, pocket management guidance
     */
    private String pocketManagementType;

    /**
     * ISIN code of the main share
     */
    private String mainShareCode;

    /**
     * Master fund ISIN (code) and name
     */
    private Codification masterFund;

    /**
     * Portfolio parent can be a mandate or a fund (umbrella entity) in case of a multiple compartment fund
     */
    private Portfolio parent;

    /**
     * List of share class related to this fund or sub-fund
     */
    private List<ShareClass> shareClasses;

    /**
     * Shares, here can be all shares shares related to same multi-compartment fund
     */
    private List<Asset> shares;

    /**
     * Portfolio subfunds if the portfolio is the fund, umbrella
     */
    private List<Portfolio> subFunds;

    /**
     * Portfolio pockets (or buckets) of the fund or the mandate
     */
    private List<Portfolio> pockets;

    /**
     * Associated portfolio models
     */
    private List<Portfolio> portfolioModels;

    /**
     * Associated benchmarks : official, etc.
     */
    private List<Benchmark> benchmarks;

    /**
     * List of ratings at portfolio level such as ESG ratings
     */
    private List<Rating> ratings;

    /**
     * SRRI : synthetic risk and reward indicator
     */
    private Rating srri;

    /**
     * Reference date of the portfolio, for instance date to calculate the portfolio treasury
     */
    private LocalDate referenceDate;

    /**
     * Last valuation date or NAV date of the portfolio
     */
    private LocalDate valuationDate;

    /**
     * Portfolio nav (netPrice) and performance (netReturn)
     */
    private Quotation valuation;

    /**
     * List of Portfolio nav (netPrice) and performances (netReturn)
     */
    private List<Quotation> valuations;

    /**
     * Portfolio analytics : market capitalization, risk analytics
     */
    private Analytics analytics;

    /**
     * List of portfolio analytics : market capitalization, risk analytics
     */
    private List<Analytics> allAnalytics;

    /**
     * List of portfolio risk indicators
     */
    private List<AnalyticsValue> riskIndicators;

    /**
     * Portfolio performance
     */
    private AnalyticsValue performance;

    /**
     * List of portfolio performances
     */
    private List<Performance> performances;

    /**
     * List of custom attributes associated to the Portfolio
     */
    private List<Attribute> attributes;

    /**
     * key defining rules / policy data  have been provided to build the object
     */
    private String dataProvidingPolicy;

    /**
     * true if the fund is exchange traded
     */
    private Boolean exchangeTraded;

    /**
     * true if the fund is considered fund of funds 
     */
    private Boolean fundOfFunds;

    /**
     * true if the fund is a leveraged fund
     */
    private Boolean leveragedFund;

    /**
     * Replication Strategy used by  the fund especially ETFs to replicate the index the fund's track 
     */
    private String replicationStrategy;

    /**
     * true if it's a private fund
     */
    private Boolean privateFund;

    /**
     * true if the fund is ethical
     */
    private Boolean ethical;

    /**
     * Fund rating focus
     */
    private String ratingFocus;

    /**
     * Fund objective
     */
    private String objective;

    /**
     * Fund management style
     */
    private String managementStyle;

    /**
     * Fund asset class focus
     */
    private String assetClassFocus;

    /**
     * Fund maturity focus
     */
    private String maturityFocus;

    /**
     * Fund domicile type
     */
    private String domicileType;

    /**
     * all custom data organised by universe with each a list fields ( key + value)
     */
    private List<DataUniverse> customData;

}