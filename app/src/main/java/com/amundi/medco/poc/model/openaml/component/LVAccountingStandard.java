package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVAccountingStandard {

  /**
   * IFRS : IAS39 and IFRS9 
   */
  IFRS,

  /**
   * standard IFRS  IAS39 
   */
  IAS39,

  /**
   * standard IFRS9 
   */
  IFRS9,

  /**
   * No accounting standard 
   */
  NA,

}