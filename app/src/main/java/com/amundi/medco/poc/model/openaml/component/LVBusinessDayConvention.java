package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVBusinessDayConvention {

  /**
   * The date will be adjusted, for example in case of end of month 
   */
  ADJUSTED,

  /**
   * The date will not be adjusted if it falls on a day that is not a business day 
   */
  NO_ADJUSTMENT,

  /**
   * NONE, no adjustment 
   */
  NONE,

  /**
   * The non-business date will be adjusted to the first previous day that is a business day 
   */
  PREVIOUS,

  /**
   * The non-business date will be adjusted to the first following day that is a business day 
   */
  FOLLOWING,

  /**
   * The non-business date will be adjusted to the first following day that is a business day unless that day falls in a different month, in which case that date will be the first preceding day that is a business day. 
   */
  MODIFIED_FOLLOWING,

  /**
   * The non-business date will be adjusted to the first previous day that is a business day unless that day falls in a different month, in which case that date will be the first following day that is a business day. 
   */
  MODIFIED_PREVIOUS,

  /**
   * Second day after 
   */
  SECOND_DAY,

  /**
   * FRN Convention, Floating Rate Convention or Eurodollar Convention 
   */
  FRN,

  /**
   * End-of-month no adjustment : All cash flows are assumed to be distributed on the final day of the month (even if it is a non-business day) 
   */
  END_OF_MONTH,

  /**
   * All cash flows are assumed to be distributed on the final day of the month. If the final day of the month is a non-business day, the previous business day is adopted 
   */
  END_OF_MONTH_PREVIOUS,

  /**
   * All cash flows are assumed to be distributed on the final day of the month. If the final day of the month is a non-business day, the following business day is adopted 
   */
  END_OF_MONTH_FOLLOWING,

}