package com.amundi.medco.poc.model.openaml.component;

import com.amundi.medco.poc.model.openaml.LVRoundModel;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="feesrateamount", namespace="com.amundi.tech.openaml.component")
public class FeesRateAmount {

    /**
     * Fees type, a way to identify the type of fees or tax
     */
    private String type;

    /**
     * Fees name
     */
    private String name;

    /**
     * Fees rate expressed in percentage, e.g. 1 means 1%
     */
    private BigDecimal rate;

    /**
     * Fees amount value expressed in ccy
     */
    private BigDecimal amt;

    /**
     * Fees amount currency ISO code
     */
    private String ccy;

    /**
     * Calculation date of the fees rate and/or amount
     */
    private LocalDate date;

    /**
     * Calculation period of the fees rate and/or amount
     */
    private Period period;

    /**
     * Optional base amount on which the fees/charges are calculated.
     */
    private BigDecimal baseAmt;

    /**
     * Threshold, or maximum/upper level
     */
    private BigDecimal maxRate;

    /**
     * Threshold, or minimum/lower level
     */
    private BigDecimal minRate;

    /**
     * Maximum fees amount
     */
    private BigDecimal maxAmt;

    /**
     * Minimum fees amount
     */
    private BigDecimal minAmt;

    /**
     * Optional Round model to apply, by default this is half even model
     */
    private LVRoundModel roundModel;

    /**
     * status of the fees Rate Amount : Correction / Cancellation / Empty
     */
    private String status;

}