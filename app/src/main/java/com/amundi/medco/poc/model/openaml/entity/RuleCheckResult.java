package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Codification;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="rulecheckresult", namespace="com.amundi.tech.openaml.entity")
public class RuleCheckResult extends Entity {

    /**
     * Rule id
     */
    private Identifier ruleId;

    /**
     * Description of the rule check result
     */
    private String description;

    /**
     * True if the check has been performed with missing data
     */
    private Boolean missingData;

    /**
     * True if the breach  is active
     */
    private Boolean active;

    /**
     * Display of the indicator used in the rule check
     */
    private String indicator;

    /**
     * The list of references to get the diagnosis data
     */
    private List<Codification> diagnosis;

    /**
     * The list of error messages
     */
    private List<String> errorMessages;

    /**
     * The results of the rule check by indicator
     */
    private List<ControlResult> controlResults;

}