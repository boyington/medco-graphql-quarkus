package com.amundi.medco.poc.model.openaml.entity.asset;

import com.amundi.medco.poc.model.openaml.component.*;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="interestrate", namespace="com.amundi.tech.openaml.entity.asset")
public class InterestRate {

    /**
     * Rate value of the interest either fixed rate or variable rate (index value + spread (margin add * margin mult)
     */
    private BigDecimal rate;

    /**
     * Rate type: FIXED, FLOATING, RESET
     */
    private LVRateType rateType;

    /**
     * Coupon type: FIXED, VARIABLE, ZEROCOUPON, etc.
     */
    private String couponType;

    /**
     * CPN_TYP_SPECIFIC : Bond type as defined on the security
     */
    private String couponTypeSpecific;

    /**
     * STEP_UP_DOWN_PROVISION : true if the coupon can be increased or decreased following rating change, the issuer's failure to register a security, or any other change.
     */
    private Boolean isStepProvision;

    /**
     * STEP_UP_DOWN_PROVISION_TRIGGER : the event that has triggered the coupon modification
     */
    private String stepProvisionTrigger;

    /**
     * Reset frequency, fixing frequency, with code and name
     */
    private Period resetFrequency;

    /**
     * Reset frequency, fixing frequency, usually the same as payment frequency but can be different
     */
    private LVFrequency resetFreq;

    /**
     * Number of days between reset dates, or refix dates
     */
    private Integer resetDays;

    /**
     * Additional margin part of the spread to add to the index rate value, by default the value is in percentage. variable rate  = index value + spread (margin add * margin mult)
     */
    private BigDecimal marginAdd;

    /**
     * Multiple of additional margin to compute the spread to add to the index rate value. variable rate  = index value + spread (margin add * margin mult)
     */
    private BigDecimal marginMult;

    /**
     * Optional: true to indicate if the marginAdd is expressed in basis points (by default the value is in percentage)
     */
    private Boolean marginInBP;

    /**
     * Margin method: Weighted, Unweighted
     */
    private String marginMethod;

    /**
     * Wich index is used to get the index value that is part of the variable rate
     */
    private Index index;

    /**
     * Index rate value that is part of the variable rate         variable rate  = index value + spread (margin add * margin mult)          if inflation index, variable rate  = index value * ratio + spread (margin add * margin mult)          Use indexValue.net, indexValue.unitExpr, indexValue.date, indexValue.ratio
     */
    private BigDecimal indexValue;

    /**
     * Date of the index rate value
     */
    private LocalDate indexValueDate;

    /**
     * Index initial value
     */
    private BigDecimal initialValue;

    /**
     * Fix to float : Next reset index, the ticker of the future indexation
     */
    private String nextResetIndexCode;

    /**
     * Fix to float : Next margin add. The float spread at the future indexation.
     */
    private BigDecimal nextMarginAdd;

    /**
     * Cap value of interest rate. If debt instrument, the highest value of coupon rate
     */
    private BigDecimal capRate;

    /**
     * Floor value of interest rate. If debt instrument, the lowest value of coupon rate
     */
    private BigDecimal floorRate;

    /**
     * Payment frequency with code and name
     */
    private Period paymentFrequency;

    /**
     * Payment frequency
     */
    private LVFrequency paymentFreq;

    /**
     * Text that explained the coupon calculation convention : business day convention + adjusted coupon
     */
    private String couponConvention;

    /**
     * Provide the business day convention to apply on the next settlement date of the coupon
     */
    private LVBusinessDayConvention businessDayConv;

    /**
     * True if the coupon date follow the business day convention of the settlement date
     */
    private Boolean adjustedCoupon;

    /**
     * Multi coupon schedule type : NONE, MULTI_CPN_RATE, MULTI_CPN_AMT
     */
    private LVMultiCouponSchedule multiCouponSchedule;

    /**
     * InterestInfo calculation mode : ACTUARIAL, LINEAR, DISCOUNT, ZERO_COUPON
     */
    private LVIntCalcMode intCalcMode;

    /**
     * Day count basis by year : 360, 365
     */
    private BigDecimal dayCountPerYear;

    /**
     * Day count basis : code
     */
    private BigDecimal dayCountBasis;

    /**
     * Day count fraction name, interest computation method:  30/360, etc.
     */
    private String dayCountMethod;

    /**
     * Day count method identifier (code / codScheme)
     */
    private Identifier dayCountId;

    /**
     * Calculation method used to calculate interests amount
     */
    private Comment calcMethod;

    /**
     * Rate on line (ROL) is the ratio of premium paid to loss recoverable in a reinsurance contract.
     */
    private BigDecimal rateOnLine;

    /**
     * Actuarial coupon rate
     */
    private BigDecimal curCoupon;

    /**
     * Factor to apply on amount calculation, used if brady bond
     */
    private BigDecimal curFactor;

    /**
     * Capitalization factor, mainly Brady bond
     */
    private BigDecimal capFactor;

    /**
     * Next factor date
     */
    private LocalDate nextFactorDate;

    /**
     * Next coupon date compared to the current day
     */
    private LocalDate nextCouponDate;

    /**
     * Previous coupon date compared to the current day
     */
    private LocalDate prevCouponDate;

    /**
     * Accrual start date of the first coupon/payment period
     */
    private LocalDate firstAccrualStartDate;

    /**
     * First settlement date, mortgage first settle date
     */
    private LocalDate firstSettlementDate;

    /**
     * First coupon date
     */
    private LocalDate firstCouponDate;

    /**
     * Accrual start date of the future on rate
     */
    private LocalDate accrualStartDate;

    /**
     * Accrual end date of the future on rate
     */
    private LocalDate accrualEndDate;

    /**
     * Penultimate coupon date, or the last coupon date before maturity
     */
    private LocalDate penultimateCouponDate;

    /**
     * Last coupon type reset date, i.e. fix to float, or float to fix
     */
    private LocalDate couponTypeResetDate;

    /**
     * Step Up rate
     */
    private BigDecimal stepUpRate;

    /**
     * Last step Up or step down date
     */
    private LocalDate stepUpDate;

    /**
     * Next reset date, or next refix date
     */
    private LocalDate nextResetDate;

    /**
     * Next rate type conversion date
     */
    private LocalDate rateTypeConversionDate;

    /**
     * Next fixing date
     */
    private LocalDate nextFixingDate;

    /**
     * Offset in open day of the fixing date. Values can be 0,-1,-2.
     */
    private Integer fixingLag;

    /**
     * Fixing at
     */
    private String fixingAt;

    /**
     * Fixing calendar
     */
    private String fixingCalendar;

    /**
     * True if compound interests amount, for example otc leg compound
     */
    private Boolean compound;

    /**
     * Compound frequency
     */
    private LVFrequency compoundFreq;

    /**
     * Stub period type of the initial period
     */
    private String initialStubPeriod;

    /**
     * Stub period type of the final period
     */
    private String finalStubPeriod;

    /**
     * If the coupon period is a broken period: FL if first long (B: upfront), LL if last long (E: in arrears), NONE
     */
    private LVOddCoupon longPeriod;

    /**
     * Broken period, odd coupon period or stub period
     */
    private LVOddCoupon brokenPeriod;

    /**
     * Broken interpolation indicator : NONE, BEGIN, END, BOTH - use LVPeriodAt
     */
    private String brokenInterpolation;

    /**
     * Index 1  for Interpolation Calculation on first period/coupon (BEGIN) or last (END) period/coupon
     */
    private Index brokenPeriodIndex1;

    /**
     * Index 2  for Interpolation Calculation on first period/coupon (BEGIN) or last (END) period/coupon
     */
    private Index brokenPeriodIndex2;

    /**
     * Index 1  for Interpolation Calculation on  last period/coupon if brokenInterpolation BOTH (meaning BEGIN and END, here the END part)
     */
    private Index brokenPeriod2Index1;

    /**
     * Index 2  for Interpolation Calculation on  last period/coupon if brokenInterpolation BOTH (meaning BEGIN and END, here the END part)
     */
    private Index brokenPeriod2Index2;

    /**
     * Days count for a fix-to-float bond. 
     */
    private Integer fixToFloatDayCount;

    /**
     * Fix-to-float coupon frequency
     */
    private String fixToFloatCouponFreq;

    /**
     * Indicates the fixing method.  Will return either NORMAL or RESET AT END.  NORMAL indicates the coupon rate is determined at the beginning of each floater accrual period, while RESET AT END means the rate will not be determined until the end of the current period. 
     */
    private String couponRateFixingMethod;

    /**
     * Sink frequency : can be the number of sink by year
     */
    private String sinkFrequency;

}