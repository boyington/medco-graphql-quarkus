package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="basket", namespace="com.amundi.tech.openaml.entity")
public class Basket extends Entity {

    /**
     * Date of basket constituents
     */
    private LocalDate basketDate;

    /**
     * Basket version
     */
    private Integer basketVersion;

    /**
     * Basket currency code
     */
    private String basketCcy;

    /**
     * Basket composition type, if index can be the close composition, next day composition or proforma composition
     */
    private String basketType;

    /**
     * Exchange rate used by  all constituents : fixing date, fixing source, unit currency, quoted currency
     */
    private ExchangeRate exchangeRate;

    /**
     * List of asset constituents or proxy constituents, the basket composition
     */
    private List<BasketConstituent> constituents;

}