package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="agreement", namespace="com.amundi.tech.openaml.entity")
public class Agreement {

    /**
     * Convention type of the master agreement
     */
    private String type;

    /**
     * Name of framework agreement, for instance  Master Confirmation Variance Swap - Europe
     */
    private String name;

    /**
     * Version of the convention type
     */
    private String version;

    /**
     * Signing date
     */
    private LocalDate date;

    /**
     * List of counterparties with which a master agreement or a framework agreement has been signed, or list of counterparties whose binding contract with Amundi refers to a particular asset type
     */
    private List<Party> counterparties;

}