package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Codification;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="codificationgroup", namespace="com.amundi.tech.openaml.entity")
public class CodificationGroup extends Entity {

    /**
     * Category of this group of codifications
     */
    private String category;

    /**
     * Group code of these codifications
     */
    private String group;

    /**
     * List of codifs
     */
    private List<Codification> codifications;

}