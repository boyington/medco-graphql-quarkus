package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="fianalytics", namespace="com.amundi.tech.openaml.entity")
public class FIAnalytics extends Analytics {

}