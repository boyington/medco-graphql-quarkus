package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVBasisDateMode {

  /**
   * The statement is based on contractual settlement date irrespective of settled data positions, i.e. accountant statement 
   */
  CONTRACTUAL,

  /**
   * The statement is based on settled date positions to the knowledge of the sender at the time of the statement preparation. 
   */
  SETTLED,

  /**
   * The statement is based on matched and confirmed execution, i.e. positions created as soon as the execution is confirmed 
   */
  CONFIRMED,

  /**
   * The statement is based on trade date positions. 
   */
  TRADED,

  /**
   * The based date of the statement is unknown 
   */
  UNKNOWN,

}