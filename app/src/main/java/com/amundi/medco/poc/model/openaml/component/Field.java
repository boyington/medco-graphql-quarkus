package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonRootName(value="field", namespace="com.amundi.tech.openaml.component")
public class Field {

    /**
     * The id of the field (we prefered key)
     */
    private String fieldKey;

    /**
     * the value  of the field (exposed as string)
     */
    private String fieldValue;

    /**
     * type of the field as defined in the list of values FieldType
     */
    private FieldType fieldType;

    /**
     * the category  of the field
     */
    private String fieldCategory;

    /**
     * the display value of the field used in application
     */
    private String fieldDisplayValue;

    /**
     * The date of the last modification of the  field value ( functional date)
     */
    private LocalDateTime valueDateTime;

    /**
     * The user who modified the field (functional)
     */
    private String modifiedBy;

    /**
     * The name of the field
     */
    private String fieldName;

}