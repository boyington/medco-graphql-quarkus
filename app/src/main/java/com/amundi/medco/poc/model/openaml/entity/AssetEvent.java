package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVRoundModel;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.Amounts;
import com.amundi.medco.poc.model.openaml.component.FeesRateAmount;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.entity.asset.InterestRate;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="assetevent", namespace="com.amundi.tech.openaml.entity")
public class AssetEvent extends Entity {

    /**
     * Event type : CALL, PUT, etc.
     */
    private String type;

    /**
     * Event sub type
     */
    private String subType;

    /**
     * Alternative or other types, use codScheme/code
     */
    private List<Identifier> eventTypes;

    /**
     * Event date
     */
    private LocalDate eventDate;

    /**
     * Event date type
     */
    private String eventDateType;

    /**
     * Quantity or security lot size of the event
     */
    private BigDecimal eventLotSize;

    /**
     * Unit gross amount
     */
    private BigDecimal unitGrossAmt;

    /**
     * Unit net amount
     */
    private BigDecimal unitNetAmt;

    /**
     * Settle date for cash or security
     */
    private LocalDate settleDate;

    /**
     * Settlement currency code
     */
    private String settlementCcy;

    /**
     * Exchange rate
     */
    private BigDecimal settleExchangeRate;

    /**
     * Payment date for market op
     */
    private LocalDate paymentDate;

    /**
     * Confirmation date time if market op
     */
    private LocalDateTime confirmationDateTime;

    /**
     * Event calculation start date
     */
    private LocalDate startDate;

    /**
     * Event calculation end date
     */
    private LocalDate endDate;

    /**
     * Event rate characteristics (rate, marginAdd, marginMult, indexValue, indexValueDate, dayCountBasis
     */
    private InterestRate eventRate;

    /**
     * Price or quotation associated to this event
     */
    private BigDecimal price;

    /**
     * Price Unit associated to this event
     */
    private LVUnit priceUnit;

    /**
     * Rounding model apply when calculating an amount
     */
    private LVRoundModel rounding;

    /**
     * Motif or cause of the event / market op
     */
    private String eventCause;

    /**
     * Explanation or justification of the event / market op (description)
     */
    private String eventReason;

    /**
     * Event probability, a value between 0 and 1
     */
    private BigDecimal probability;

    /**
     * Impacted nominal : nominal to withdraw after unwind, nominal to add after restructure
     */
    private BigDecimal impactedNominal;

    /**
     * Currency code of impacted nominal
     */
    private String impactedNominalCcy;

    /**
     * Event fees (amt, ccy) such as termination fees, event fees payment date (date)
     */
    private FeesRateAmount eventFees;

    /**
     * Event fees counterparty identifier, transferee identifier if stepout
     */
    private Identifier counterpartyId;

    /**
     * Event amounts : trading amounts, corporate action amount, etc.
     */
    private Amounts eventAmounts;

    /**
     * Settlement amounts linked to this event
     */
    private Amounts settlementAmounts;

}