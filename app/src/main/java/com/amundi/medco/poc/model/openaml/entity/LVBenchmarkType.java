package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVBenchmarkType {

  /**
   * Official benchmark defined in the fund legal document 
   */
  OFFICIAL,

  /**
   * Fund management benchmark 
   */
  MANAGEMENT,

  /**
   * Technical benchmark or proxy used by transparency 
   */
  TRANSPARENCY,

  /**
   * Technical benchmark : for example the benchmark used to calculate SRRI to complete historical NAV of the portfolio 
   */
  TECHNICAL,

  /**
   * Used in PAMS : Technical benchmark that is another portfolio 
   */
  TECHNICAL_PTF,

}