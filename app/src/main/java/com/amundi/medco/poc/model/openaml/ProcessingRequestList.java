package com.amundi.medco.poc.model.openaml;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="processingrequestlist", namespace="com.amundi.tech.openaml")
public class ProcessingRequestList {

    /**
     * Version library attribute, filled by default
     */
    private String openaml;

    /**
     * Content type is a list of AnalyticsCalculation
     */
    private String contentType;

    /**
     * List of processing request or response
     */
    private List<ProcessingRequest> requests;

}