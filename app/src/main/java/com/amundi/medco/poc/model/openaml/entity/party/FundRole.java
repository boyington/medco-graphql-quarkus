package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="fundrole", namespace="com.amundi.tech.openaml.entity.party")
public class FundRole extends PartyRole {

}