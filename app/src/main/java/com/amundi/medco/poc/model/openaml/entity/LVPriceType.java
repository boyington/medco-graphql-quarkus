package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPriceType {

  /**
   * Market price retrieved at market closure, or the price get from a provider at a time near the market closure. 
   */
  CLOSE,

  /**
   * Market price retrieved at market opening, or the price get from a provider at a time near the open closure,  or the open price get from a provider 
   */
  OPEN,

  /**
   * Max price 
   */
  HIGH,

  /**
   * Min price 
   */
  LOW,

  /**
   * ASK price 
   */
  ASK,

  /**
   * BID price 
   */
  BID,

  /**
   * MID price : price midway between the bid and ask price 
   */
  MID,

  /**
   * Last price : the dealing price get from the provider at a specific date and time 
   */
  LAST,

  /**
   * Clearing price, or settle price 
   */
  CLEARING,

  /**
   * Fixing price, if official price. 
   */
  FIXING,

  /**
   * Official mutual fund share price or nav per share. 
   */
  NAV,

  /**
   * Official mutual fund subscription nav per share or share price if subscription 
   */
  SUBSCRIPTION_NAV,

  /**
   * Official mutual fund redemption nav per share or share price if redemption 
   */
  REDEMPTION_NAV,

  /**
   * Unswung NAV if swing pricing 
   */
  VALUATION_NAV,

  /**
   * Swing NAV if swing pricing 
   */
  TRANSACTION_NAV,

  /**
   * Shadow NAV calculated in case of constant nav fund  for instance 
   */
  SHADOW_NAV,

  /**
   * NAV adjusted with entrance/exit fees 
   */
  OFFER_TO_BID,

  /**
   * Public offering price: NAV without subscription fees, entrance fees 
   */
  POP,

  /**
   * Net return price 
   */
  NET_RETURN,

  /**
   * Price return : the return of an index or portfolio in percentage 
   */
  PRICE_RETURN,

  /**
   * Total return : the price return included the dividends or revenues.   
   */
  TOTAL_RETURN,

  /**
   * Adjustment ratio used for hedging 
   */
  HEDGED_TOTAL_RETURN,

  /**
   * Non official price, this is an indicative price 
   */
  INDICATIVE,

  /**
   * Mark to market value 
   */
  MTM,

  /**
   * Simulated price 
   */
  SIMULATED,

  /**
   * Calculated valuation, this is a local valuation 
   */
  CALCULATED_VALUATION,

}