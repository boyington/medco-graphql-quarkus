package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPosTransLifecycle {

  /**
   * Position or transaction lifecycle step is SIMULATED 
   */
  SIMULATED,

  /**
   * Position or transaction lifecycle step is ORDERED, i.e. an order has been sent 
   */
  ORDERED,

  /**
   * Position or transaction lifecycle step is ALLOCATED, i.e. the order has been executed 
   */
  ALLOCATED,

  /**
   * Position or transaction lifecycle step is CONFIRMED, i.e. the order execution has been matched and confirmed and the transaction corresponding to this execution can impact the position keeping system. 
   */
  CONFIRMED,

  /**
   * Position or transaction lifecycle step is BOOKED, i.e. an entry has been made on the book of record 
   */
  BOOKED,

  /**
   * Position or transaction lifecycle step is SETTLED, i.e. after settlement 
   */
  SETTLED,

}