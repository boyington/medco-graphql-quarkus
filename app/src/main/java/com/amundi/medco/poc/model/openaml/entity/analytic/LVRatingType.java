package com.amundi.medco.poc.model.openaml.entity.analytic;

/**
 * undefined
 */
public enum LVRatingType {

  /**
   * Rating Fitch long term  (F,LT) 
   */
  FITCH_DEBT_FC_LT,

  /**
   * Rating Fitch short term  (F,CT) 
   */
  FITCH_DEBT_ST,

  /**
   * Rating Fitch short term local currency  (F,CTL) 
   */
  FITCH_DEBT_STL,

  /**
   * Rating Fitch national long term (N,LT) 
   */
  FITCH_NATIONAL_LT,

  /**
   * Rating Moody long term  (M,LT) 
   */
  MOODYS_SEC,

  /**
   * Rating Moody short term  (M,CT) 
   */
  MOODY_DEBT_ST,

  /**
   * Rating Standard and Poor's long term  (S,LT) 
   */
  SP_DEBT_LT,

  /**
   * Rating Standard and Poor's short term (S,CT) 
   */
  SP_ST,

  /**
   * CARE provides credit rating   (D,LT) 
   */
  RATING_CARE,

  /**
   * Rating RAM credit rating in Asia (H,LT) 
   */
  RAM_LT,

  /**
   * Rating RAM credit rating in Asia (H,CT) 
   */
  RAM_ST,

  /**
   * Rating ICRA in India  (I,LT) 
   */
  ICRA_LT,

  /**
   * Rating MARC, Malaysian Rating Corporation, long term (J,LT) 
   */
  MARC_LT,

  /**
   * Rating MARC, Malaysian Rating Corporation, short term (J,ST) 
   */
  MARC_ST,

  /**
   * Rating Japan Rating and Investment long term (K,LT) 
   */
  RI,

  /**
   * Rating Japan Credit rating agency  long term (P,LT) 
   */
  JCR_LT,

  /**
   * Rating Crisil long term  (X,LT) 
   */
  CRISIL_LT,

  /**
   * Rating Min LT following PFI internal method (G,LT) 
   */
  PFI_MIN_LT,

  /**
   * Rating Max LT following PFI internal method (U,LT) 
   */
  PFI_MAX_LT,

  /**
   * Rating Median LT following PFI internal method (V,LT) 
   */
  PFI_MED_LT,

  /**
   * Rating Median LT following PFI internal method (V,CT) 
   */
  PFI_MED_ST,

  /**
   * Rating Min Long term Moodys/SandP (1,LT) 
   */
  PFI_MIN_LT_2A1,

  /**
   * Rating Max Long term  Moodys/SandP (2,LT) 
   */
  PFI_MAX_LT_2A1,

  /**
   * Rating Min Long term   Moodys/Fitch (3,LT) 
   */
  PFI_MIN_LT_2A2,

  /**
   * Rating Max Long term   Moodys/Fitch (4,LT) 
   */
  PFI_MAX_LT_2A2,

  /**
   * Rating Min Long term  SandP/Fitch (5,LT) 
   */
  PFI_MIN_LT_2A3,

  /**
   * Rating Max Long term SandP/Fitch (6,LT) 
   */
  PFI_MAX_LT_2A3,

  /**
   * Rating Long term DBRS (, LT) 
   */
  DBRS_LT,

  /**
   * Rating Long term Kroll (, LT) 
   */
  KROLL_LT,

  /**
   * Rating Long term Morningstar (, LT) 
   */
  MORNINGSTAR_LT,

  /**
   * Rating Bloomberg composite LT 
   */
  BBG_COMP,

  /**
   * Equity rating and analysis 
   */
  EQUITY_RATING,

  /**
   * Equity rating and analysis 
   */
  CREDIT_RATING,

  /**
   * Issuer Rating Moody's LT 
   */
  MOODYS_ISSUER_LT,

  /**
   * Issuer Rating Moody's PFI ST 
   */
  MOODYS_ISSUER_PFI_ST,

  /**
   * Issuer Rating Fitch LT 
   */
  FITCH_ISSUER_LT,

  /**
   * Issuer Rating Fitch PFI ST 
   */
  FITCH_ISSUER_PFI_ST,

  /**
   * Rating SP LICRFC 
   */
  SP_LICRFC,

  /**
   * Issuer Rating SP PFI ST 
   */
  SP_ISSUER_PFI_ST,

}