package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@JsonRootName(value="quotation", namespace="com.amundi.tech.openaml.entity")
public class Quotation extends Entity {

    /**
     * Quotation date
     */
    private LocalDate date;

    /**
     * Quotation next date
     */
    private LocalDate nextDate;

    /**
     * Quotation previous date
     */
    private LocalDate previousDate;

    /**
     * Quotation time
     */
    private LocalTime quoteTime;

    /**
     * Quotes currency code, the currency code associated to netPrice for instance
     */
    private String ccy;

    /**
     * Unit expression of the quotes, PERCENT, YIELD, UNIT etc.
     */
    private LVUnit quoteUnit;

    /**
     * Indicate the quote units if necessary, for instance, USD/1000 KRW  (future on currency quote units indication)
     */
    private String quoteUnits;

    /**
     * Quotation provider, REFERENCE if golden record, see the sourceId.source for the provider name
     */
    private String quoteProvider;

    /**
     * The contributor of the price
     */
    private String quoteContributor;

    /**
     * Quote type : PRICE_QUOTE, DISCOUNT_QUOTE, YIELD_QUOTE, if NAV, is used for the type of NAV
     */
    private String quoteType;

    /**
     * Diffusion of the quote
     */
    private String diffusion;

    /**
     * Last quote
     */
    private BigDecimal last;

    /**
     * Close quote if stock market.
     */
    private BigDecimal close;

    /**
     * Open quote
     */
    private BigDecimal open;

    /**
     * High quote, the max quote
     */
    private BigDecimal high;

    /**
     * Low quote, the min quote
     */
    private BigDecimal low;

    /**
     * Ask quote, if associated with grossAsk, this is netAsk
     */
    private BigDecimal ask;

    /**
     * Bid quote, if associated with grossBid, this is netBid
     */
    private BigDecimal bid;

    /**
     * Mid quote, if associated with grossMid, this is netMid
     */
    private BigDecimal mid;

    /**
     * Spread between ask and bid
     */
    private BigDecimal spread;

    /**
     * True if accrued interests are included in the quote, mainly for fixed income security
     */
    private Boolean dirty;

    /**
     * Net return, for example, net return (NR) of a stock index
     */
    private BigDecimal netReturn;

    /**
     * Gross return, for example, total return (TR or GR) of a stock index
     */
    private BigDecimal grossReturn;

    /**
     * Optional: Type of netReturn, grossReturn, for example: OPEN, CLOSE.
     */
    private LVPriceType returnType;

    /**
     * Net return, for example, open net return (NR_OPEN) of a stock index
     */
    private BigDecimal netReturn2;

    /**
     * Gross return, for example, total return (TR_OPEN) of a stock index
     */
    private BigDecimal grossReturn2;

    /**
     * Optional: Type of netReturn2, grossReturn2, for example: OPEN, CLOSE.
     */
    private LVPriceType returnType2;

    /**
     * Share class : hedged
     */
    private String hedged;

    /**
     * Share class : share group (link of N shares)
     */
    private String shareGroup;

    /**
     * Mutual fund : share identifier
     */
    private Identifier shareId;

    /**
     * Mutual fund : share code
     */
    private String shareCode;

    /**
     * Mutual fund : share type (C, D)
     */
    private String shareType;

    /**
     * Net price, for example, the net NAV per share, this field is associated to priceType
     */
    private BigDecimal netPrice;

    /**
     * Gross price, for example, the gross NAV per share, this field is associated to priceType
     */
    private BigDecimal grossPrice;

    /**
     * Type of netPrice or grossPrice, for example: OPEN, CLOSE, NAV etc.
     */
    private LVPriceType priceType;

    /**
     * Valuation price, can be the valuation NAV, the unswung NAV in case of swing pricing, and the netPrice is the swing NAV or transaction NAV. For private banking, valuation price can contain the valuation amount (total net amount) of the investment account
     */
    private BigDecimal valuationPrice;

    /**
     * Interests or dividends part of the price
     */
    private BigDecimal accIntPrice;

    /**
     * True if netPrice has been forced
     */
    private Boolean forcedPrice;

    /**
     * True if the netPrice is final, definitive NAV price, false if estimated NAV price
     */
    private Boolean finalPrice;

    /**
     * Mutual fund : Total net asset
     */
    private BigDecimal totalNetAsset;

    /**
     * Mutual fund : Total gross asset
     */
    private BigDecimal totalGrossAsset;

    /**
     * Mutual fund : Total net asset in ptf currency
     */
    private BigDecimal totalNetAssetPtfCur;

    /**
     * Mutual fund : outstanding shares
     */
    private BigDecimal outstandingShares;

    /**
     * Mutual fund : outstanding shares at previous NAV date
     */
    private BigDecimal previousOutstandingShares;

    /**
     * Mutual fund : redemption total amount at NAV calculation
     */
    private BigDecimal redemptionAmount;

    /**
     * Mutual fund : subscription total amount at NAV calculation
     */
    private BigDecimal subscriptionAmount;

    /**
     * Cash amount that can be part of the totalNetAsset
     */
    private BigDecimal cashAmount;

    /**
     * Securities amount that can be part of the totalNetAsset
     */
    private BigDecimal securitiesAmount;

    /**
     * Fund or sub-fund AUM, Aggregate fund value, fund total net asset (Fund's total market value less all liabilities as of any historical TNA date)
     */
    private BigDecimal fundTotalNetAsset;

    /**
     * Fund or sub-fund total gross asset
     */
    private BigDecimal fundTotalGrossAsset;

    /**
     * Portfolio, Fund or sub-fund AUM, Aggregate fund value, fund total net asset (Fund's total market value less all liabilities as of any historical TNA date)
     */
    private BigDecimal ptfTotalNetAsset;

    /**
     * Portfolio, Fund or sub-fund total gross asset
     */
    private BigDecimal ptfTotalGrossAsset;

    /**
     * Fees management maximum
     */
    private BigDecimal feesManagementMax;

    /**
     * Fees management real, actual fees management
     */
    private BigDecimal feesManagementReal;

    /**
     * Fees management total amount since last NAV
     */
    private BigDecimal feesManagementAmt;

    /**
     * Cumulative Fees management  for the current fiscal year
     */
    private BigDecimal feesManagementCumulative;

    /**
     * Provided sum of market value of the positions
     */
    private BigDecimal marketValue;

    /**
     * Portfolio currency, Fund or sub-fund currency code. This currency is linked to fundTotalNetAsset and fundTotalGrossAset
     */
    private String ptfCcy;

    /**
     * Portfolio code
     */
    private String ptfCode;

    /**
     * Mutual fund : bench code
     */
    private String benchCode;

    /**
     * Mutual fund : bench name
     */
    private String benchName;

    /**
     * Mutual fund : bench value
     */
    private BigDecimal benchValue;

    /**
     * Mutual fund : bench previous value date
     */
    private LocalDate benchPreviousValueDate;

    /**
     * Mutual fund : variation between NAV and bench value  provided by custodian
     */
    private BigDecimal variationBenchCustodian;

    /**
     * Mutual fund : variation between NAV and bench value
     */
    private BigDecimal variationBench;

    /**
     * Mutual fund : annual variation between NAV and bench value provided by custodian
     */
    private BigDecimal variationBenchAnnualCustodian;

    /**
     * Mutual fund : annual variation between NAV and bench value
     */
    private BigDecimal variationBenchAnnual;

    /**
     * Mutual fund : share type 2, D for instance
     */
    private String shareType2;

    /**
     * Mutual fund : the Total net asset associated to the net price 2
     */
    private BigDecimal totalNetAsset2;

    /**
     * Mutual fund : outstanding shares 2 associated to the net price 2
     */
    private BigDecimal outstandingShares2;

    /**
     * Net price 2, for example, the NAV per share of the share D
     */
    private BigDecimal netPrice2;

    /**
     * Price adjusted with inflation
     */
    private BigDecimal priceWihInflation;

    /**
     * Price factor, such inflation ratio
     */
    private BigDecimal priceFactor;

    /**
     * Volume of the trading day
     */
    private BigDecimal volume;

    /**
     * Lot size that has been taken into account to calculate the quote
     */
    private BigDecimal quoteLotSize;

    /**
     * Quote variation in % between previous quote and current quote (close, open, or any quote), or any other kind of quote variation
     */
    private BigDecimal quoteVariation;

    /**
     * Prior Ask quote
     */
    private BigDecimal priorAsk;

    /**
     * Prior Bid quote
     */
    private BigDecimal priorBid;

    /**
     * Prior Mid quote
     */
    private BigDecimal priorMid;

    /**
     * Clean Ask quote
     */
    private BigDecimal cleanAsk;

    /**
     * Clean Bid quote
     */
    private BigDecimal cleanBid;

    /**
     * Clean Mid quote
     */
    private BigDecimal cleanMid;

    /**
     * Gross Ask quote
     */
    private BigDecimal grossAsk;

    /**
     * Gross Bid quote
     */
    private BigDecimal grossBid;

    /**
     * Gross Mid quote
     */
    private BigDecimal grossMid;

    /**
     * Dirty Mid quote
     */
    private BigDecimal dirtyMid;

    /**
     * Quote clearing, mostly the last price
     */
    private BigDecimal quoteClearing;

    /**
     * Quote clearing date, can be null
     */
    private LocalDate quoteClearingDate;

    /**
     * QUOTE_WAVG
     */
    private BigDecimal quoteWavg;

    /**
     * Price of the future
     */
    private BigDecimal futurePrice;

    /**
     * Quote of the option
     */
    private BigDecimal optionQuote;

    /**
     * Quote score, quote quality
     */
    private BigDecimal quoteScore;

    /**
     * Date of quote score, quote quality
     */
    private LocalDate quoteScoreDate;

    /**
     * Number of days used to calculate the quote
     */
    private BigDecimal quoteDays;

    /**
     * Coupon settlement date, or the period end date used to calculate the accrued interests
     */
    private LocalDate accIntDate;

    /**
     * Number of accrued interest days linked to the accrIntPrice based on previous coupon date and coupon settlement date (accrIntDate)
     */
    private Integer accIntDays;

    /**
     * Optional date of the exchange rate associated to this quotation
     */
    private LocalDate fxRateDate;

    /**
     * Optional exchange rate in quoted ccy for 1 unit ccy or fx conversion, i.e  1 unitCcy = fxRate quotedCcy
     */
    private BigDecimal fxRate;

    /**
     * Optional Unit currency code of exchange rate or fx conversion, i.e  1 unitCcy = fxRate quotedCcy
     */
    private String fxUnitCcy;

    /**
     * Optional Quoted currency code of exchange rate or fx conversion, i.e 1 unitCcy = fxRate quotedCcy
     */
    private String fxQuotedCcy;

    /**
     * Percent change in price over the last five days. ( Last price vs Close 5D Ago)
     */
    private BigDecimal priceChangePercentage5D;

    /**
     * Price Change Month To Date Percent : Percentage price change computed from the Price Change last month To Dat
     */
    private BigDecimal priceChangePercentageMTD;

    /**
     * Price Change Quarter To Date Percent : Percentage price change computed from the Price Change last quarter To Date
     */
    private BigDecimal priceChangePercentageQTD;

    /**
     * Closing price from the last trading day of the most recently completed month.     
     */
    private BigDecimal closePricePreviousMonth;

    /**
     * Closing price from the last trading day of the most recently completed quarter. The quarters end on the last trading day of March, June, September and December. 
     */
    private BigDecimal closePricePreviousQuarter;

    /**
     * Estimated Price
     */
    private BigDecimal estimatedPrice;

    /**
     * Evaluation price (PX_EVAL)
     */
    private BigDecimal evaluationPrice;

}