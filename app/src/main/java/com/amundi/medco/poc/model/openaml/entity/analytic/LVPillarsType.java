package com.amundi.medco.poc.model.openaml.entity.analytic;

/**
 * undefined
 */
public enum LVPillarsType {

  /**
   * List of key rate duration at specific maturity 
   */
  KEY_RATE_DURATION,

  /**
   * List of libor key rate duration at specific maturity 
   */
  LIBOR_KEY_RATE_DURATION,

  /**
   * List of maturity pillars 
   */
  MATURITY,

}