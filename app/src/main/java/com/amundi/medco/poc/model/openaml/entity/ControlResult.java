package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.Codification;
import com.amundi.medco.poc.model.openaml.component.DataUniverse;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="controlresult", namespace="com.amundi.tech.openaml.entity")
public class ControlResult {

    /**
     * Control result id
     */
    private String _id;

    /**
     * Control result global status
     */
    private String status;

    /**
     * Control message
     */
    private String message;

    /**
     * True if the check has been performed with missing data
     */
    private Boolean missingData;

    /**
     * True if the breach  is active
     */
    private Boolean active;

    /**
     * The list of indicators used by the rule
     */
    private List<ControlIndicator> indicators;

    /**
     * The list of references to get the diagnosis data
     */
    private List<Codification> diagnosis;

    /**
     * all custom data organised by universe with each a list fields ( key + value)
     */
    private List<DataUniverse> customData;

}