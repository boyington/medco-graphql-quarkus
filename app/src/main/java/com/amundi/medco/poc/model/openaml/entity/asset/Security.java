package com.amundi.medco.poc.model.openaml.entity.asset;

import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="security", namespace="com.amundi.tech.openaml.entity.asset")
public class Security extends Asset {

}