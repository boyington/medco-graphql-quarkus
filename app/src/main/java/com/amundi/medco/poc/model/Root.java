package com.amundi.medco.poc.model;

import com.amundi.medco.poc.model.openaml.entity.Asset;
import lombok.Data;
import one.microstream.reference.Lazy;

import java.util.HashMap;
import java.util.Map;

@Data
public class Root {
    private Map<String, Lazy<Asset>> assets = new HashMap<>();
}
