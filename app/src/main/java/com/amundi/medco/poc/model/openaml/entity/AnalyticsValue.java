package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVNetGross;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.amundi.medco.poc.model.openaml.component.Period;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="analyticsvalue", namespace="com.amundi.tech.openaml.entity")
public class AnalyticsValue extends Entity {

    /**
     * Type of the analytics value, see LVBreakdown etc.
     */
    private String type;

    /**
     * Value, price, or unit amount
     */
    private BigDecimal value;

    /**
     * Value unit : RATE (rate in percentage),  YIELD (yield, annualized value expressed in percentage), RETURN (performance or yield expressed in percent, i.e. based on 100), RATIO (the rate is a dimensionless ratio or fraction)
     */
    private LVUnit valueUnit;

    /**
     * Amount value expressed in ccy
     */
    private BigDecimal amt;

    /**
     * Amount currency ISO code
     */
    private String ccy;

    /**
     * Date of the value and amt
     */
    private LocalDate date;

    /**
     * Period associated to the value, for example the volatility period, or the bucket number of years
     */
    private Period period;

    /**
     * This is a net or gross value
     */
    private LVNetGross netOrGross;

    /**
     * True if ex-ante (measurement,expected), False if ex-post (evaluation,realised)
     */
    private Boolean exAnte;

    /**
     * Performance mode can be ANNUALIZED or CUMULATIVE
     */
    private LVPerfMode mode;

    /**
     * Optional performance expression : ABSOLUTE, RELATIVE, if present and RELATIVE, it means the performance used is relative to a benchmark value
     */
    private LVPerfExpression expression;

    /**
     * VaR: Confidence level in %, for example VAR(99), confidence = 99
     */
    private BigDecimal confidence;

    /**
     * VaR : coeff = 1 - Confidence level in %, for example 1% if VAR(99)
     */
    private BigDecimal coeff;

    /**
     * Time step used to calculate risk indicator : fill period+timeFlow, for example monthly:MONTH+SLIDING, calendar weekly:WEEK+CALENDAR, weekly:WEEK+SLIDING. Period by year: Number of periods (N) for a year, for example 52 week.
     */
    private Period timeStep;

    /**
     * Requested calculation date
     */
    private LocalDate calcDate;

    /**
     * Period used to calculate risk indicator (perimeter). Number of basic period (T) within the perimeter.
     */
    private Period calcPeriod;

    /**
     * Calculation method name, for example, standard for volatility, historical or parametric for VAR
     */
    private String calcMethod;

    /**
     * List of output results, the calculated analytics
     */
    private List<Attribute> calcResults;

    /**
     * List of input attributes to calculate the analytics
     */
    private List<Attribute> calcAttributes;

}