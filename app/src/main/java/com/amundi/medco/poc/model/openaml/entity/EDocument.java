package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.LocalName;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="edocument", namespace="com.amundi.tech.openaml.entity")
public class EDocument extends Entity {

    /**
     * Document type
     */
    private String type;

    /**
     * Document category
     */
    private String category;

    /**
     * The headline of the document  (the title of the document should be in Edocument.name)
     */
    private String headline;

    /**
     * Document description (extract of the document, highlights, or the document abstract)
     */
    private String description;

    /**
     * The publication date of the document
     */
    private LocalDate publicationDate;

    /**
     * The document publisher
     */
    private String publisher;

    /**
     * The last modification date of the document
     */
    private LocalDate lastModifiedDate;

    /**
     * Version of the document
     */
    private String version;

    /**
     * Link to the document, url, filepath
     */
    private String link;

    /**
     * The document is visible by
     */
    private String visibleBy;

    /**
     * Document default author, or editor of the article.
     */
    private ContactInfo author;

    /**
     * List of co-authors of the document, or joint authors of the document
     */
    private List<ContactInfo> coAuthors;

    /**
     * List of document contributors
     */
    private List<ContactInfo> contributors;

    /**
     * Optional document body, use documentBody.text and documentBody.contentType to indicate if the text is html, text, or other content type.
     */
    private Comment documentBody;

    /**
     * The file associated to this document
     */
    private DocumentFile documentFile;

    /**
     * Attachment files
     */
    private List<DocumentFile> attachments;

    /**
     * Document associated tags (use tag.name for the tag value, and if needed, use tag.type)
     */
    private List<LocalName> tags;

    /**
     * Document associated comments
     */
    private List<Comment> comments;

    /**
     * Optional attributes to add to the Edocument
     */
    private List<Attribute> attributes;

}