package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="custodianrole", namespace="com.amundi.tech.openaml.entity.party")
public class CustodianRole extends PartyRole {

}