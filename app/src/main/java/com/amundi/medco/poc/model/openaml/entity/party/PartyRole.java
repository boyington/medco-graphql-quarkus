package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.entity.LVPartyRole;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@JsonRootName(value="partyrole", namespace="com.amundi.tech.openaml.entity.party")
public class PartyRole {

    /**
     * Party role type
     */
    private LVPartyRole role;

    /**
     * Last update date and time of the role
     */
    private LocalDateTime lastUpdateDateTime;

    /**
     * Party role status
     */
    private String status;

    /**
     * Party role sub-status
     */
    private String subStatus;

    /**
     * Status date 
     */
    private LocalDate statusDate;

    /**
     * Party role description
     */
    private String description;

    /**
     * Start date of the role
     */
    private LocalDate startDate;

    /**
     * End date of the role
     */
    private LocalDate endDate;

}