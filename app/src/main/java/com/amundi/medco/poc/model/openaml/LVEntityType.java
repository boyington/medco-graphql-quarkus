package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVEntityType {

  /**
   * Specific object to have a list of business object, http://openaml.io/EntityList 
   */
  ENTITY_LIST,

  /**
   * Access right business object, or authorization business object, http://openaml.io/AccessRight 
   */
  ACCESS_RIGHT,

  /**
   * Analytics business object, http://openaml.io/Analytics 
   */
  ANALYTICS,

  /**
   * Analytics value business object, http://openaml.io/AnalyticsValue 
   */
  ANALYTICS_VALUE,

  /**
   * Analytics calculation service object, http://openaml.io/AnalyticsCalculation 
   */
  ANALYTICS_CALCULATION,

  /**
   * Asset business object, http://openaml.io/Asset 
   */
  ASSET,

  /**
   * Asset event business object, http://openaml.io/AssetEvent 
   */
  ASSET_EVENT,

  /**
   * Asset link business object, http://openaml.io/AssetLink 
   */
  ASSET_LINK,

  /**
   * Basket business object, http://openaml.io/Basket 
   */
  BASKET,

  /**
   * Benchmark business object, http://openaml.io/Benchmark 
   */
  BENCHMARK,

  /**
   * BeneficialOwner business object, http://openaml.io/BeneficialOwner 
   */
  BENEFICIAL_OWNER,

  /**
   * Calendar business object 
   */
  CALENDAR,

  /**
   * CalendarDay business object 
   */
  CALENDAR_DAY,

  /**
   * CarbonData business object, http://openaml.io/CarbonData 
   */
  CARBON_DATA,

  /**
   * Classif business object, http://openaml.io/Classif 
   */
  CLASSIF,

  /**
   * CodificationGroup business object 
   */
  CODIFICATION_GROUP,

  /**
   * Collateral business object, http://openaml.io/Collateral 
   */
  COLLATERAL,

  /**
   * Constraint business object, http://openaml.io/Constraint 
   */
  CONSTRAINT,

  /**
   * ConstraintSet business object, http://openaml.io/ConstraintSet 
   */
  CONSTRAINT_SET,

  /**
   * ConstraintCheckRequest object 
   */
  CONSTRAINT_CHECK_REQUEST,

  /**
   * ConstraintCheckReport business object 
   */
  CONSTRAINT_CHECK_REPORT,

  /**
   * ConstraintCheckResult business object 
   */
  CONSTRAINT_CHECK_RESULT,

  /**
   * ContactInfo business object, http://openaml.io/ContactInfo 
   */
  CONTACT_INFO,

  /**
   * CorporateAction business object, http://openaml.io/CorporateAction 
   */
  CORPORATE_ACTION,

  /**
   * Country business object, http://openaml.io/Country 
   */
  COUNTRY,

  /**
   * Currency business object, http://openaml.io/Currency 
   */
  CURRENCY,

  /**
   * EDocument business object, http://openaml.io/EDocument 
   */
  EDOCUMENT,

  /**
   * ExchangeMarket business object 
   */
  EXCHANGE_MARKET,

  /**
   * ExchangeRate business object, http://openaml.io/ExchangeRate 
   */
  EXCHANGE_RATE,

  /**
   * Execution business object,  http://openaml.io/Execution 
   */
  EXECUTION,

  /**
   * FeesScale business object,  http://openaml.io/FeesScale 
   */
  FEES_SCALE,

  /**
   * Index business object, http://openaml.io/Index 
   */
  INDEX,

  /**
   * Instruction business object, http://openaml.io/Instruction 
   */
  INSTRUCTION,

  /**
   * Inventory definition business object 
   */
  INVENTORY_DEFINITION,

  /**
   * Investment Mandate business object 
   */
  INVESTMENT_MANDATE,

  /**
   * Legal entity business object that is also a party, http://openaml.io/LegalEntity 
   */
  LEGAL_ENTITY,

  /**
   * LinkedOrder business object, http://openaml.io/LinkedOrder 
   */
  LINKED_ORDER,

  /**
   * Location business object, http://openaml.io/Location 
   */
  LOCATION,

  /**
   * LocationDistribution business object, http://openaml.io/LocationDistribution 
   */
  LOCATION_DISTRIBUTION,

  /**
   * Movement business object, http://openaml.io/Movement 
   */
  MOVEMENT,

  /**
   * Order business object 
   */
  ORDER,

  /**
   * OrderSet business object 
   */
  ORDER_SET,

  /**
   * OTC business object, http://openaml.io/Otc 
   */
  OTC,

  /**
   * Party business object, http://openaml.io/Party 
   */
  PARTY,

  /**
   * PartyLink business object, http://openaml.io/PartyLink 
   */
  PARTY_LINK,

  /**
   * Performance business object,  http://openaml.io/Performance 
   */
  PERFORMANCE,

  /**
   * Person business object that is also a party, http://openaml.io/Person 
   */
  PERSON,

  /**
   * Portfolio business object, http://openaml.io/Portfolio 
   */
  PORTFOLIO,

  /**
   * PortfolioOptimization business object, http://openaml.io/PortfolioOptimization 
   */
  PORTFOLIO_OPTIMIZATION,

  /**
   * ProgramTrade business object, http://openaml.io/ProgramTrade 
   */
  PROGRAM_TRADE,

  /**
   * ShareClass business object, http://openaml.io/ShareClass 
   */
  SHARE_CLASS,

  /**
   * Quotation business object,  http://openaml.io/Quotation 
   */
  QUOTATION,

  /**
   * Rating business object, http://openaml.io/Rating 
   */
  RATING,

  /**
   * RatingScale business object, the definition of the rating scale 
   */
  RATING_SCALE,

  /**
   * RealAsset business object, https://www.openamlanguage.io/RealAsset 
   */
  REAL_ASSET,

  /**
   * RuleCheckResult business object, https://www.openamlanguage.io/RuleCheckResult 
   */
  RULE_CHECK_RESULT,

  /**
   * Security business object, http://openaml.io/Security 
   */
  SECURITY,

  /**
   * Portfolio Inventory business object, http://openaml.io/PortfolioInventory 
   */
  PORTFOLIO_INVENTORY,

  /**
   * Team business object, http://openaml.io/TeamAndOfficer 
   */
  TEAM_AND_OFFICER,

  /**
   * Time series business object 
   */
  TIME_SERIES,

  /**
   * Transaction business object, http://openaml.io/Transaction 
   */
  TRANSACTION,

  /**
   * A list of miscellaneous business objects 
   */
  MISC,

}