package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCollateralMarginType {

  /**
   * Initial margin 
   */
  IM,

  /**
   * Variable margin 
   */
  VM,

}