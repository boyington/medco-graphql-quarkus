package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVConstraintStatus {

  /**
   * Constraint status is blocking after checking 
   */
  BLOCKING,

  /**
   * Constraint status is warning  after checking 
   */
  WARNING,

  /**
   * Constraint status is informational  after checking 
   */
  INFO,

}