package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVQuoteType {

  /**
   * undefined 
   */
  DISCOUNT_QUOTE,

  /**
   * undefined 
   */
  PRICE_QUOTE,

  /**
   * undefined 
   */
  YIELD_QUOTE,

}