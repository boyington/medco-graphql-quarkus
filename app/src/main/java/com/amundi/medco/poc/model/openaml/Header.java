package com.amundi.medco.poc.model.openaml;

import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonRootName(value="header", namespace="com.amundi.tech.openaml")
public class Header {

    /**
     * See LVMessageType
     */
    private String messageType;

    /**
     * Type of entity encapsulated in the message, for example, ASSET, PARTY, PORTFOLIO, EXECUTION, etc.
     */
    private LVEntityType entityType;

    /**
     * In addition to the entity type
     */
    private String entitySubtype;

    /**
     * Event which produced the message to be issued, see list of values
     */
    private String event;

    /**
     * Sub event related to the event if necessary
     */
    private String subEvent;

    /**
     * Message unique identifier, for example TRN
     */
    private String messageId;

    /**
     * Sender of the message
     */
    private String sender;

    /**
     * Date and time at which the message was issued
     */
    private LocalDateTime sendingDateTime;

    /**
     * User name
     */
    private String user;

    /**
     * Batch identifier
     */
    private String batchId;

    /**
     * Sequence number of the message during the batch execution
     */
    private Integer batchMsgSeqNum;

    /**
     * Messages count of this batch identified by its batchId
     */
    private Integer batchMsgCount;

    /**
     * Message id of the related message, the meaning depends on the type of message
     */
    private String relatedMessageId;

    /**
     * Message id of the replaced message
     */
    private String replacedMessageId;

    /**
     * External reference of the message before integration in local system, e.g. the name of the received file
     */
    private Identifier messageExtRef;

    /**
     * In case of an error, this is the error code
     */
    private String errorCode;

    /**
     * In case of an error, this is the error label
     */
    private String errorLabel;

}