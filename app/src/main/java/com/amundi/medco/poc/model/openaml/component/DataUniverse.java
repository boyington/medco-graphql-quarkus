package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="datauniverse", namespace="com.amundi.tech.openaml.component")
public class DataUniverse {

    /**
     * the data universe id
     */
    private String dataUniverseId;

    /**
     * the list of fields of the universe
     */
    private List<Field> fields;

}