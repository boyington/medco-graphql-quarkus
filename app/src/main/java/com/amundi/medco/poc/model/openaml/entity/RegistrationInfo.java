package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="registrationinfo", namespace="com.amundi.tech.openaml.entity")
public class RegistrationInfo {

    /**
     * Registration type: LEI, LEGAL, ...
     */
    private String type;

    /**
     * Registration status
     */
    private String status;

    /**
     * Initial registration date, or assigned date
     */
    private LocalDate registrationDate;

    /**
     * Last update date
     */
    private LocalDate lastUpdateDate;

    /**
     * Date on which the registration, for example LEI, entered a disabled status
     */
    private LocalDate disabledDate;

    /**
     * Next renewal date
     */
    private LocalDate nextRenewalDate;

    /**
     * Country, state/province of registration or formation
     */
    private Location location;

    /**
     * Agreement number defined by regulator authority
     */
    private String authorityAgreementNum;

    /**
     * Regulator authority number
     */
    private String authorityNum;

    /**
     * Local registration authority, or if GLEIF, this is the managingLOU
     */
    private Party authority;

    /**
     * Date on which the regulator approved the product.
     */
    private LocalDate approvalDate;

    /**
     * Approval number defined by regulator
     */
    private String approvalNumber;

}