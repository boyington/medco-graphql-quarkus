package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="localname", namespace="com.amundi.tech.openaml.component")
public class LocalName {

    /**
     * Language code ISO 639-1 (alpha 2), for example ko for Korean language or language code ISO 639-2 (alpha 3)
     */
    private String lang;

    /**
     * Name expressed in the language code
     */
    private String name;

    /**
     * Type of the local name
     */
    private String type;

}