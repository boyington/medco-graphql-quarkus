package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@JsonRootName(value="period", namespace="com.amundi.tech.openaml.component")
public class Period {

    /**
     * Optional code of the period in the system
     */
    private String code;

    /**
     * Term name, or period name, e.g. 1M
     */
    private String name;

    /**
     * Number of periods, e.g. 1.0 for 1YEAR
     */
    private BigDecimal nbOfPeriod;

    /**
     * Period type, e.g. DAY, MONTH, YEAR
     */
    private LVPeriod period;

    /**
     * Period start date, the period is between fromDate and toDate
     */
    private LocalDate fromDate;

    /**
     * Period end date, the period is between fromDate and toDate
     */
    private LocalDate toDate;

    /**
     * Period start time, the period is between fromTime and toTime
     */
    private LocalTime fromTime;

    /**
     * Period end time, the period is between fromTime and toTime
     */
    private LocalTime toTime;

    /**
     * Direction of period shift, e.g. ASC if ascending dates, SLIDING if sliding forward
     */
    private LVPeriodShift shift;

    /**
     * Convention of date adjustment if the date is not a business day. NONE or NO_ADJUSTMENT = calendar day
     */
    private LVBusinessDayConvention businessDayConv;

    /**
     * Which calendar is associated to this period
     */
    private String calendar;

}