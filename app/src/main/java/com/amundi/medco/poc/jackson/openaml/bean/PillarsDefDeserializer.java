package com.amundi.medco.poc.jackson.openaml.bean;

import com.amundi.medco.poc.jackson.openaml.BeanFromArrayDeserializer;
import com.amundi.medco.poc.model.openaml.entity.AnalyticsValue;
import com.amundi.medco.poc.model.openaml.entity.analytic.PillarsDef;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;

@Slf4j
public class PillarsDefDeserializer extends BeanFromArrayDeserializer<PillarsDef> {

    public PillarsDefDeserializer(JsonDeserializer<?> defaultDeserializer) {
        super(defaultDeserializer, PillarsDef.class);
    }

    @Override
    protected PillarsDef getFromArray(JsonParser p, DeserializationContext ctxt) throws IOException {
        PillarsDef pillarsDef = new PillarsDef();
        pillarsDef.setPillars(new ArrayList<>());
        while(p.nextToken() == JsonToken.START_OBJECT) {
            pillarsDef.getPillars().add(p.readValueAs(AnalyticsValue.class));
        }
        log.debug(pillarsDef.toString());
        return pillarsDef;
    }


}
