package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVValidationStatus {

  /**
   * Statement without validated or pre-validated notion 
   */
  CREATED,

  /**
   * Statement with validated data, i.e. validated by fund manager 
   */
  VALIDATED,

  /**
   * Statement with prevalidated data, i.e. validated by fund administrator/accountant, but not yet validated by fund manager 
   */
  PRE_VALIDATED,

}