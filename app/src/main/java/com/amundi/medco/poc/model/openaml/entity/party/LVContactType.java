package com.amundi.medco.poc.model.openaml.entity.party;

/**
 * undefined
 */
public enum LVContactType {

  /**
   * ContactInfo information for business purpose 
   */
  BUSINESS,

  /**
   * Legal contact 
   */
  LEGAL,

  /**
   * ContactInfo is followed by compliance department 
   */
  COMPLIANCE,

  /**
   * Other contact type 
   */
  OTHER,

}