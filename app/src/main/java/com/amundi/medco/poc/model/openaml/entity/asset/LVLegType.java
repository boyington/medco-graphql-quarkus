package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVLegType {

  /**
   * Interest leg (floating or fixed) if total return swap 
   */
  INTEREST,

  /**
   * Return leg if total return swap 
   */
  RETURN,

  /**
   * Forward leg of Forex leg 
   */
  FORWARD,

  /**
   * Spot leg of Forex leg 
   */
  SPOT,

}