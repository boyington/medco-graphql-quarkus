package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVSecurityForm {

  /**
   * Financial instrument in registered form, for which a register of ownership is kept by the company 
   */
  REGISTERED,

  /**
   * Financial instrument in bearer form 
   */
  BEARER,

  /**
   * Financial instrument in bearer and registered  form 
   */
  BEARER_REGISTERED,

  /**
   * Financial instrument in other form 
   */
  OTHER,

}