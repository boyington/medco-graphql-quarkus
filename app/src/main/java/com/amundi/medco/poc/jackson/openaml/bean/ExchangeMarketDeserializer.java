package com.amundi.medco.poc.jackson.openaml.bean;

import com.amundi.medco.poc.jackson.openaml.BeanFromArrayDeserializer;
import com.amundi.medco.poc.model.openaml.entity.ExchangeMarket;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class ExchangeMarketDeserializer extends BeanFromArrayDeserializer<ExchangeMarket> {

    public ExchangeMarketDeserializer(JsonDeserializer<?> defaultDeserializer) {
        super(defaultDeserializer, ExchangeMarket.class);
    }

    @Override
    protected ExchangeMarket getFromArray(JsonParser p, DeserializationContext ctxt) throws IOException {
        return (ExchangeMarket) this.defaultDeserializer.deserialize(p, ctxt);
    }
}
