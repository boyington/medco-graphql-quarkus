package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="feesscale", namespace="com.amundi.tech.openaml.entity")
public class FeesScale extends Entity {

    /**
     * Descritpion of fees scale
     */
    private String description;

    /**
     * Type of fees scale for example Management Fees
     */
    private String type;

    /**
     * sub type of fees scale for example Performance Fees
     */
    private String subtype;

    /**
     * Start validity date for this scale
     */
    private LocalDate startDate;

    /**
     * End validity date for this scale
     */
    private LocalDate endDate;

    /**
     * Fees rate
     */
    private BigDecimal feesRate;

    /**
     * Minimun fees amount
     */
    private BigDecimal minFeesAmount;

    /**
     * Maximum fees amount
     */
    private BigDecimal maxFeesAmount;

    /**
     * Maximum fees amount
     */
    private List<Attribute> feesScaleAttributes;

}