package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="amountweight", namespace="com.amundi.tech.openaml.component")
public class AmountWeight {

    /**
     * Type of weight exposure, for example : Equity exposure, Bond exposure, etc.
     */
    private String type;

    /**
     * Weight of the net amount or mark to market (weight = netAmt / baseAmt) provided by statement or portfolio
     */
    private BigDecimal weight;

    /**
     * Unit amount of exposure
     */
    private BigDecimal unitAmt;

    /**
     * Optional amount used to calculate the exposure in percentage, for example the market exposure amount
     */
    private BigDecimal amt;

    /**
     * Optional base amount
     */
    private BigDecimal baseAmt;

    /**
     * Optional max amount
     */
    private BigDecimal maxAmt;

    /**
     * Optional min amount
     */
    private BigDecimal minAmt;

    /**
     * Currency code of the amount and base amount
     */
    private String ccy;

    /**
     * True if critical amount has been reached and exceeded
     */
    private Boolean critical;

    /**
     * Optional impact in percentage, for instance the impact in the total amount gap
     */
    private BigDecimal impact;

}