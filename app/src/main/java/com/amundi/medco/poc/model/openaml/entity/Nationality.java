package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="nationality", namespace="com.amundi.tech.openaml.entity")
public class Nationality {

    /**
     * Identifier of the nationality
     */
    private Identifier id;

    /**
     * The nationality name (American, French, ...), by default in en
     */
    private String name;

    /**
     * Country ISO code
     */
    private String ctry;

    /**
     * Country name
     */
    private String ctryName;

    /**
     * true if no citizenship
     */
    private Boolean stateless;

}