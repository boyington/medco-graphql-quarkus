package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPerfMode {

  /**
   * Annualized performance 
   */
  ANNUALIZED,

  /**
   * Cumulative performance 
   */
  CUMULATIVE,

}