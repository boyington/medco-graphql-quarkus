package com.amundi.medco.poc.jackson.openaml.bean;

import com.amundi.medco.poc.jackson.openaml.BeanFromArrayDeserializer;
import com.amundi.medco.poc.model.openaml.entity.LegalEntity;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class LegalEntityDeserializer extends BeanFromArrayDeserializer<LegalEntity> {

    public LegalEntityDeserializer(JsonDeserializer<?> defaultDeserializer) {
        super(defaultDeserializer, LegalEntity.class);
    }

    @Override
    protected LegalEntity getFromArray(JsonParser p, DeserializationContext ctxt) throws IOException {
        LegalEntity legalEntity = null;
        while(p.nextToken() == JsonToken.START_OBJECT) {
            if(legalEntity!=null) {
                log.debug("Multiple legal entities");
                //throw new InvalidFormatException(p, "Unable to get DebtInfo from array with more than one element", null, DebtInfo.class);
            } else {
                legalEntity = (LegalEntity) this.defaultDeserializer.deserialize(p, ctxt);
            }
        }
        return legalEntity;
    }
}
