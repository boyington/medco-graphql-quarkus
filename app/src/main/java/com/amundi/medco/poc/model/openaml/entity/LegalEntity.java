package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.entity.party.PartyRole;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@JsonRootName(value="legalentity", namespace="com.amundi.tech.openaml.entity")
public class LegalEntity extends Party {

    /**
     * Roles of the party
     */
    private List<PartyRole> partyRoles;

    /**
     * Party ultimate Parent. If multi-compartment fund, this can be the umbrella
     */
    private Party ultimateParent;

    /**
     * Party parent, for example, the issuer group
     */
    private Party parent;

    /**
     * List of additionnal contact infos
     */
    private List<ContactInfo> moreContactInfos;

    /**
     * KYC Due diligence data
     */
    private DueDiligenceInfo dueDil;

    /**
     * Legal name
     */
    private String legalName;

    /**
     * Brand name or trademark
     */
    private String brandName;

    /**
     * Business name, main business name
     */
    private String businessName;

    /**
     * Previous legal name
     */
    private String prevLegalName;

    /**
     * List of Previous legal names
     */
    private List<String> previousLegalNames;

    /**
     * The juridiction of the legal formation and registration of the legal entity
     */
    private String legalJuridiction;

    /**
     * Legal form of the legal entity
     */
    private String legalForm;

    /**
     * Code of the Legal form
     */
    private String legalFormCode;

    /**
     * True if the legal entity has been registered
     */
    private Boolean registered;

    /**
     * Information on local registration authority
     */
    private RegistrationInfo registrationInfo;

    /**
     * Entity structure : holding, subsidiary
     */
    private String entityStructure;

    /**
     * Parent Relationship (BB data)
     */
    private String parentRelationship;

    /**
     * true if this legal entity is acquired by parent
     */
    private Boolean acquiredbyParent;

    /**
     * Ownership type such as private or public ownership
     */
    private String ownershipType;

    /**
     * True if publicly listed
     */
    private Boolean publiclyListed;

    /**
     * Primary exchange if publicy listed
     */
    private ExchangeMarket primaryExch;

    /**
     * Number of employees
     */
    private BigDecimal nbOfEmployees;

    /**
     * List of persons linked to this legal entity such as stakeholders (person who has relation with the business entity)
     */
    private List<Person> persons;

    /**
     * List of this legal entity shareholders (person or legal entity), for example the parent company of a subsidiary
     */
    private List<PartyLink> shareholders;

    /**
     * List of this party beneficial owners
     */
    private List<BeneficialOwner> beneficialOwners;

    /**
     * List of the third-parties held by this legal entity with ownership information
     */
    private List<PartyLink> holderOfs;

    /**
     * The third-party obligor
     */
    private Party obligor;

    /**
     * LEI registration information
     */
    private RegistrationInfo leiInfo;

    /**
     * Legal entity business segment
     */
    private String businessSegment;

    /**
     * List of party business businessActivities
     */
    private List<LocationDistribution> businessActivities;

    /**
     * List of main business countries
     */
    private List<Location> businessCountries;

    /**
     * List of main markets, sectors
     */
    private List<Classif> businessMarketSectors;

    /**
     * List of suppliers' country
     */
    private List<Location> supplierCountries;

    /**
     * Legal entity auditor, fund auditor, CAC
     */
    private Party auditor;

    /**
     * List of teams related to this legal entity and the officer in charge of this legal entity
     */
    private List<TeamAndOfficer> teamAndOfficers;

}