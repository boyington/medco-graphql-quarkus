package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="constraintcheckreport", namespace="com.amundi.tech.openaml.entity")
public class ConstraintCheckReport extends Entity {

    /**
     * The id of the checked entity
     */
    private Identifier entityId;

    /**
     * Define the perimeter of the rules
     */
    private String constraintSet;

    /**
     * The reference date  of the constraint check, for instance the NAV date if the check is related to portfolio valuation.
     */
    private LocalDate checkRefDate;

    /**
     * The effective date and time of the constraint check execution
     */
    private LocalDateTime checkDateTime;

    /**
     * The results of the constraint check
     */
    private List<ConstraintCheckResult> constraintCheckResults;

}