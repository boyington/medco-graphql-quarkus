package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="emirinfo", namespace="com.amundi.tech.openaml.entity.party")
public class EmirInfo {

    /**
     * Client category
     */
    private String category;

    /**
     * True if the party is regarded as US person in the context of Dodd Frank law
     */
    private Boolean usPerson;

    /**
     * True if the EMIR reporting is delegated
     */
    private Boolean delegatedReporting;

    /**
     * Financial counterparty (EMIR classification): FC, NFC, NFC+, NFC-
     */
    private String financial;

    /**
     * Segregated account structure
     */
    private String accountStructure;

}