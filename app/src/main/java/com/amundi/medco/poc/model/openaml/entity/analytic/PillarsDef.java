package com.amundi.medco.poc.model.openaml.entity.analytic;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.entity.AnalyticsValue;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="pillarsdef", namespace="com.amundi.tech.openaml.entity.analytic")
public class PillarsDef extends Entity {

    /**
     * Can be libor key rate duration, dv01, etc.
     */
    private String type;

    /**
     * Date of the analytics, or date of calculation, or reference date to compute these analytics
     */
    private LocalDate analyticsDate;

    /**
     * Liquid value if liquidity pillar
     */
    private BigDecimal valueLiq;

    /**
     * Value at 1M
     */
    private BigDecimal value1M;

    /**
     * Value at 2M
     */
    private BigDecimal value2M;

    /**
     * Value at 3M
     */
    private BigDecimal value3M;

    /**
     * Value at 4M
     */
    private BigDecimal value4M;

    /**
     * Value at 5M
     */
    private BigDecimal value5M;

    /**
     * Value at 6M
     */
    private BigDecimal value6M;

    /**
     * Value at 7M
     */
    private BigDecimal value7M;

    /**
     * Value at 8M
     */
    private BigDecimal value8M;

    /**
     * Value at 9M
     */
    private BigDecimal value9M;

    /**
     * Value at 10M
     */
    private BigDecimal value10M;

    /**
     * Value at 11M
     */
    private BigDecimal value11M;

    /**
     * Value at 1 year
     */
    private BigDecimal value1Y;

    /**
     * Value at 2 years
     */
    private BigDecimal value2Y;

    /**
     * Value at 3 years
     */
    private BigDecimal value3Y;

    /**
     * Value at 4 years
     */
    private BigDecimal value4Y;

    /**
     * Value at 5 years
     */
    private BigDecimal value5Y;

    /**
     * Value at 7 years
     */
    private BigDecimal value7Y;

    /**
     * Value at 10 years
     */
    private BigDecimal value10Y;

    /**
     * Value at 15 years
     */
    private BigDecimal value15Y;

    /**
     * Value at 20 years
     */
    private BigDecimal value20Y;

    /**
     * Value at 30 years
     */
    private BigDecimal value30Y;

    /**
     * List of pillars, for instance maturity pillars
     */
    private List<AnalyticsValue> pillars;

}