package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPerfExpression {

  /**
   * Absolute return: performance without reference to the benchmark or market trends 
   */
  ABSOLUTE,

  /**
   * Relative performance: portfolio performance compared to its benchmark or its market, for example, perf P-B 
   */
  RELATIVE,

}