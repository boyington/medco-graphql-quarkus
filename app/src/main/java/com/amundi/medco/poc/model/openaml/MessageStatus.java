package com.amundi.medco.poc.model.openaml;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonRootName(value="messagestatus", namespace="com.amundi.tech.openaml")
public class MessageStatus {

    /**
     * Message unique identifier, for example TRN
     */
    private String messageId;

    /**
     * Message satus as defined in the workflow, the ack/nack status
     */
    private String status;

    /**
     * Date and time at which the ack/nack message was issued
     */
    private LocalDateTime sendingDateTime;

    /**
     * Event which produced the message to be issued, see list of values
     */
    private String event;

    /**
     * Sub event related to the event if necessary
     */
    private String subEvent;

    /**
     * In case of error, the error code
     */
    private String errorCode;

    /**
     * Optional error description
     */
    private String errorDescription;

}