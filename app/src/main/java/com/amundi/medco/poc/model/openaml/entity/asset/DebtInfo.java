package com.amundi.medco.poc.model.openaml.entity.asset;

import com.amundi.medco.poc.model.openaml.entity.Party;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="debtinfo", namespace="com.amundi.tech.openaml.entity.asset")
public class DebtInfo {

    /**
     * Nominal amount, or face amount
     */
    private BigDecimal nominal;

    /**
     * Previous nominal amount, for example in case of amortization
     */
    private BigDecimal previousNominal;

    /**
     * Initial nominal amount, can be par amount
     */
    private BigDecimal initialNominal;

    /**
     * Face value, par value or principal of the debt instrument, if MBS, the face amount at issue date
     */
    private BigDecimal faceValue;

    /**
     * Issue Amount, initial face amount of the debt instrument, if municipals, the dollar amount of bonds issued under this maturity
     */
    private BigDecimal issueAmount;

    /**
     * Municipals : The aggregate of the entire deal size (in USD), comprising all the series of bonds brought to market by the same lead underwriter in the deal to which the security belongs as detailed in the official statement.
     */
    private BigDecimal muniIssueSize;

    /**
     * Municipals : the dollar amount of bonds issued under this maturity
     */
    private BigDecimal muniMtySize;

    /**
     * Inflation level of the inflation indexed debt instrument. The value of the consumer price index at first settlement date.
     */
    private BigDecimal baseCPI;

    /**
     * Deprecated DO NOT USE
     */
    private LocalDate maturityDate;

    /**
     * Maturity type
     */
    private String maturityType;

    /**
     * Maturity constraint
     */
    private String maturityConstraint;

    /**
     * Redemption amount currency code
     */
    private String redemptionCcy;

    /**
     * Maturity amount, redemption amomunt
     */
    private BigDecimal redemptionAmount;

    /**
     * True if the bond is in payment default
     */
    private Boolean defaulted;

    /**
     * Subordinated, Unsubordinated, Unknown, None
     */
    private String subordinated;

    /**
     * Credit seniority
     */
    private String creditSeniority;

    /**
     * Subordination as provided
     */
    private String subordination;

    /**
     * Guarantor type as provided
     */
    private String guarantorType;

    /**
     * Guaranty as provided
     */
    private String guaranty;

    /**
     * Payment Rank as provided
     */
    private String paymentRank;

    /**
     * Deprecated DO NOT USE
     */
    private String paymentBank;

    /**
     * True if debt type is senior
     */
    private Boolean senior;

    /**
     * True if debt type is junior
     */
    private Boolean junior;

    /**
     * Tier capital type
     */
    private String tierCapital;

    /**
     * Indicates the classification of the capital adequacy of a bank.
     */
    private String lowerTier2Capital;

    /**
     * Indicates the classification of the capital adequacy of a bank.
     */
    private String upperTier2Capital;

    /**
     * Indicates the classification of the capital adequacy of a bank.
     */
    private String tier2CapitalNonSpecific;

    /**
     * Indicates the classification of the capital adequacy of a bank.
     */
    private String tier1Capital;

    /**
     * True if index linked
     */
    private Boolean indexLinked;

    /**
     * True if the debt instrument is credit linked - used by risk
     */
    private Boolean creditLinked;

    /**
     * True if the bond is linked to inflation debt
     */
    private Boolean inflationLinked;

    /**
     * True if the debt instrument is amortizable, e.g. insurance fund
     */
    private Boolean amortization;

    /**
     * Amortization type for amortizable instrument
     */
    private String amortizationType;

    /**
     * True if the debt instrument (bond or mmi) is a fixed rate instrument
     */
    private Boolean fixed;

    /**
     * True if the debt instrument is a floating rate instrument, a FRN
     */
    private Boolean floater;

    /**
     * True if the debt instrument is a zero-coupon debt instrument
     */
    private Boolean zeroCoupon;

    /**
     * True if the treasury bond or treasury note is on-the-run (BB: IS_CURRENT_GOVT)
     */
    private Boolean onTheRun;

    /**
     * True if government bond, provided value
     */
    private Boolean govt;

    /**
     * Bond series number, Loan tranche, Mortgage ABS/CMO series
     */
    private String series;

    /**
     * True if the Fixed income is perpetual
     */
    private Boolean perpetual;

    /**
     * True if this is a bullet
     */
    private Boolean bullet;

    /**
     * True if this is a sinkable
     */
    private Boolean sinkable;

    /**
     * Next sinking date
     */
    private LocalDate nextSinkingDate;

    /**
     * True if this is a  Brady bond, i.e. emerging countries debt
     */
    private Boolean brady;

    /**
     * True if this is a structured note
     */
    private Boolean structuredNote;

    /**
     * Loan, YES note or Securitized (ILS) registration
     */
    private String registration;

    /**
     * Loan use of proceeds
     */
    private String useOfProceeds;

    /**
     * Loan covenant lite
     */
    private String loanCovenantLite;

    /**
     * Loan number of covenants
     */
    private Integer loanNbOfCovenants;

    /**
     * True if the fixed income security is a LPN - loan participation note
     */
    private Boolean loanParticipationNote;

    /**
     * Second issuer code if LPN
     */
    private Party embeddedIssuer;

    /**
     * Indicates Fix to float, or Float to fix conversion rate
     */
    private LVConversionOption rateConversion;

    /**
     * Rate conversion Option characteristics
     */
    private OptionInfo rateConversionOption;

    /**
     * Strip type
     */
    private String stripType;

    /**
     * True if covered, provided value
     */
    private Boolean covered;

    /**
     * Type covered, provided value
     */
    private String typeCovered;

    /**
     * True if private placement
     */
    private Boolean privatePlacement;

    /**
     * True if secured because of collateralization, provided value
     */
    private Boolean secured;

    /**
     * True if extendible bond
     */
    private Boolean extendible;

    /**
     * True if make whole call, mainly US bonds
     */
    private Boolean makeWholeCall;

    /**
     * Definition of make whole call option characteristics and current values
     */
    private OptionInfo makeWholeOption;

    /**
     * Name of the treasury issue (bond) that can be used as a benchmark
     */
    private String benchmarkTreasuryIssue;

    /**
     * Bond is primary : provided by BB, by business application
     */
    private Boolean primary;

    /**
     * True if high yield bond
     */
    private Boolean highYield;

    /**
     * True if credit eligible as investment in a credit investment portfolio, see specific field IS_CREDIT_ELIGIBLE
     */
    private Boolean creditEligible;

    /**
     * True if mortgage most senior
     */
    private Boolean mostSenior;

    /**
     * Tranche : senior, junior, mezzanine
     */
    private AssetTranche tranche;

    /**
     * Effective date of the debt Info data for Mortgage
     */
    private LocalDate mtgInfoDate;

    /**
     * Paydown factor, from 0 if fully redeemed, to 1 if nothing was redeemed
     */
    private BigDecimal mortgageFactor;

    /**
     * Date of the last mortgage Factor
     */
    private LocalDate mortgageFactorDate;

    /**
     * Mortgage : date of the most recent reported factor
     */
    private LocalDate mtgFactorPayDate;

    /**
     * Mortgage final pay date
     */
    private LocalDate mtgFinalPayDate;

    /**
     * Mortgage : Principal balance factor determined by settle date
     */
    private BigDecimal mtgFactorBySettleDate;

    /**
     * Mortgage :amount of interest corresponding to the most recent factor
     */
    private BigDecimal mtgFactorInterestPay;

    /**
     * Mortgage : amount of principal corresponding to the most recent factor
     */
    private BigDecimal mtgFactorPrincipalPay;

    /**
     * Mortgage : average life until the last repayment since the issue date
     */
    private BigDecimal mtgOrigWal;

    /**
     * Mortgage term value, for instance 360
     */
    private BigDecimal mtgTerm;

    /**
     * Mortgage nature type (BB MTG_GEN_TICKER)
     */
    private String mortgageNatureType;

    /**
     * Mortgage deal name
     */
    private String mtgDealName;

    /**
     * Mortgage deal type
     */
    private String mtgDealType;

    /**
     * Mortgage deal original total bond amount
     */
    private BigDecimal mtgDealOriginalAmount;

    /**
     * Mortgage deal cur collateral amount
     */
    private BigDecimal mtgDealCurCollatAmt;

    /**
     * Mortgage : Weighted average original credit score of a pool or loan, in integer form.
     */
    private Integer mtgWAOCS;

    /**
     * True if mortgage is agency backed
     */
    private Boolean mtgAgencyBacked;

    /**
     * Mortgage prepayment type
     */
    private String mtgPrepaymentType;

    /**
     * Mortgage prepayment speed
     */
    private BigDecimal mtgPrepaymentSpeed;

    /**
     * Mortgage pay delay, for example : 0 DAYS, 24 DAYS
     */
    private String mtgPayDelay;

    /**
     * Mortgage ABS/CMO class
     */
    private String mtgClass;

    /**
     * Mortgage ABS/CMO group
     */
    private String mtgGroup;

    /**
     * Mortgage collateral type, BB MTG_COLLAT_TYP
     */
    private String mtgCollatType;

    /**
     * Mortgage collateral quality
     */
    private String collatQuality;

    /**
     * Return true if eligible Real Estate Mortgage Investment Conduit (REMIC)
     */
    private Boolean remic;

    /**
     * Mortgage type (BB MTG_TYP: MTG_TRANCHE_TYP or MTG_POOL_TYP)
     */
    private String mtgType;

    /**
     * Essential characteristics of multi-class mortgage and asset-backed securities (CMO, ABS, CMBS)
     */
    private String mtgTrancheType;

    /**
     * Mortgage class call date
     */
    private LocalDate mtgClassCallDate;

    /**
     * Mortgage expected maturity date
     */
    private LocalDate mtgExpMtyDate;

    /**
     * Number of loans in pool
     */
    private BigDecimal numLoans;

    /**
     * Loan originator
     */
    private String originator;

    /**
     * CPR 3M : Conditional prepayment rate 3 months
     */
    private BigDecimal condPrepayRate3M;

    /**
     * CPR 6M : Conditional prepayment rate 6 months
     */
    private BigDecimal condPrepayRate6M;

    /**
     * Largest geographic loan distribution state
     */
    private String geoDistribState;

    /**
     * Largest geographic loan distribution percent
     */
    private BigDecimal pctGeoDistrib;

    /**
     * Largest geographic loan distribution state as provided
     */
    private String geoDistribState1;

    /**
     * 2nd Largest geographic loan distribution state as provided
     */
    private String geoDistribState2;

    /**
     * 3rd Largest geographic loan distribution state as provided
     */
    private String geoDistribState3;

    /**
     * 4th Largest geographic loan distribution state as provided
     */
    private String geoDistribState4;

    /**
     * Weighted average remaining months
     */
    private BigDecimal wAvgRemainingMonths;

    /**
     * Weighted average loan to value
     */
    private BigDecimal wAvgLoanToValue;

    /**
     * Weighted average original loan term
     */
    private BigDecimal wAvgOriginalLoanTerm;

    /**
     * Production year, for example 2018
     */
    private Integer productionYear;

    /**
     * Mortgage loan gross margin, BLP_MTG_LOAN_MRGN
     */
    private BigDecimal mtgLoanMargin;

    /**
     * Loan purchase percent
     */
    private BigDecimal pctLoanPurchase;

    /**
     * Loan refinance percent
     */
    private BigDecimal pctLoanRefinance;

    /**
     * Mortgage insurance percent
     */
    private BigDecimal pctMortgageInsurance;

    /**
     * Multi-unit housing percent
     */
    private BigDecimal pctMultiUnit;

    /**
     * Single-unit housing percent
     */
    private BigDecimal pctSingleUnit;

    /**
     * Owner occupied second home
     */
    private BigDecimal pctOwnerOccSecondHome;

    /**
     * Owner occupied investment home
     */
    private BigDecimal pctOwnerOccInvestHome;

    /**
     * Owner occupied
     */
    private BigDecimal pctOwnerOcc;

    /**
     * Third party origination (TPO)
     */
    private BigDecimal poolThirdPartyOrigination;

    /**
     * Third party origination - broker
     */
    private BigDecimal pctTPOBroker;

    /**
     * Third party origination - correspondent lender
     */
    private BigDecimal pctTPOCorrespondent;

    /**
     * Third party origination - Retail
     */
    private BigDecimal pctTPORetail;

    /**
     * Third party origination - Unspecified
     */
    private BigDecimal pctTPOUnspecified;

    /**
     * Percent sponsored by Rural Housing Service
     */
    private BigDecimal pctSponsoredByRuralHousingSvc;

    /**
     * Federal Housing Administration (FHA) buydown percent
     */
    private BigDecimal pctBuydownFHA;

    /**
     * Veterans Administration (VA) buydown percent
     */
    private BigDecimal pctBuydownVA;

    /**
     * Federal Housing Administration loss mitigation percent
     */
    private BigDecimal pctFHALossMitigate;

    /**
     * Federal Housing Administration  other purpose  percent
     */
    private BigDecimal pctFHAOtherPurpose;

    /**
     * Federal Housing Administration purchase  percent
     */
    private BigDecimal pctFHAPurchase;

    /**
     * Federal Housing Administration refinance percent
     */
    private BigDecimal pctFHARefinance;

    /**
     * Office of Public and Indian Housing mitigation percent
     */
    private BigDecimal pctPIHLossMitigate;

    /**
     * Office of Public and Indian Housing other purpose percent
     */
    private BigDecimal pctPIHOtherPurpose;

    /**
     * Office of Public and Indian Housing purchase percent
     */
    private BigDecimal pctPIHPurchase;

    /**
     * Office of Public and Indian Housing refinance percent
     */
    private BigDecimal pctPIHRefinance;

    /**
     * Rural Housing Service (RHS) mitigation percent
     */
    private BigDecimal pctRHSLossMitigate;

    /**
     * Rural Housing Service (RHS) Other Purpose percent
     */
    private BigDecimal pctRHSOtherPurpose;

    /**
     * Rural Housing Service Purchase percent
     */
    private BigDecimal pctRHSPurchase;

    /**
     * Rural Housing Service (RHS) Refinance percent
     */
    private BigDecimal pctRHSRefinance;

    /**
     * Veterans Administration (VA) mitigation percent
     */
    private BigDecimal pctVALossMitigate;

    /**
     * Veterans Administration (VA) other purpose  percent
     */
    private BigDecimal pctVAOtherPurpose;

    /**
     * Veterans Administration (VA) purchase percent
     */
    private BigDecimal pctVAPurchase;

    /**
     * Veterans Administration (VA) refinance percent
     */
    private BigDecimal pctVARefinance;

    /**
     * Average original loan size
     */
    private BigDecimal avgOriginalLoanSize;

    /**
     * Maximum original loan size
     */
    private BigDecimal maxOriginalLoanSize;

    /**
     * Maximum original loan size
     */
    private BigDecimal minOriginalLoanSize;

    /**
     * Average original loan size Q1
     */
    private BigDecimal avgOriginalLoanSizeQ1;

    /**
     * Average original loan size Q2
     */
    private BigDecimal avgOriginalLoanSizeQ2;

    /**
     * Average original loan size Q3
     */
    private BigDecimal avgOriginalLoanSizeQ3;

    /**
     * Average FICO score
     */
    private BigDecimal avgFICOScore;

    /**
     * Maximum FICO score
     */
    private BigDecimal maxFICOScore;

    /**
     * Minimum FICO score
     */
    private BigDecimal minFICOScore;

    /**
     * Average FICO score Q1
     */
    private BigDecimal avgFICOScoreQ1;

    /**
     * Average FICO score Q2
     */
    private BigDecimal avgFICOScoreQ2;

    /**
     * Average FICO score Q3
     */
    private BigDecimal avgFICOScoreQ3;

    /**
     * Average loan to value
     */
    private BigDecimal avgLoanToValue;

    /**
     * Maximum loan to value
     */
    private BigDecimal maxLoanToValue;

    /**
     * Minimum loan to value
     */
    private BigDecimal minLoanToValue;

    /**
     * Average loan to value Q1
     */
    private BigDecimal avgLoanToValueQ1;

    /**
     * Average loan to value Q2
     */
    private BigDecimal avgLoanToValueQ2;

    /**
     * Average loan to value Q3
     */
    private BigDecimal avgLoanToValueQ3;

    /**
     * Weighted Average coupon
     */
    private BigDecimal wAvgCpn;

    /**
     * Maximum Weighted Average coupon
     */
    private BigDecimal maxWAvgCpn;

    /**
     * Minimum Weighted Average coupon
     */
    private BigDecimal minWAvgCpn;

    /**
     * Weighted average coupon Q1
     */
    private BigDecimal wAvgCpnQ1;

    /**
     * Weighted average coupon Q2
     */
    private BigDecimal wAvgCpnQ2;

    /**
     * Weighted average coupon Q3
     */
    private BigDecimal wAvgCpnQ3;

    /**
     * Weighted average loan age
     */
    private BigDecimal wAvgLoanAge;

    /**
     * Weighted average loan age Date
     */
    private LocalDate wAvgLoanAgeDate;

    /**
     * Maximum Weighted average loan age
     */
    private BigDecimal maxWAvgLoanAge;

    /**
     * Minimum Weighted average loan age
     */
    private BigDecimal minWAvgLoanAge;

    /**
     * Weighted average loan age Q1
     */
    private BigDecimal wAvgLoanAgeQ1;

    /**
     * Weighted average loan age Q2
     */
    private BigDecimal wAvgLoanAgeQ2;

    /**
     * Weighted average loan age Q3
     */
    private BigDecimal wAvgLoanAgeQ3;

    /**
     * Weighted average maturity
     */
    private BigDecimal wAvgMty;

    /**
     * Maximum Weighted average maturity
     */
    private BigDecimal maxWAvgMty;

    /**
     * Minimum Weighted average maturity
     */
    private BigDecimal minWAvgMty;

    /**
     * Weighted average maturity Q1
     */
    private BigDecimal wAvgMtyQ1;

    /**
     * Weighted average maturity Q2
     */
    private BigDecimal wAvgMtyQ2;

    /**
     * Weighted average maturity Q3
     */
    private BigDecimal wAvgMtyQ3;

    /**
     * Federal Housing Administration guarantee
     */
    private BigDecimal guaranteeFHA;

    /**
     * Office of Public and Indian Housing guarantee
     */
    private BigDecimal guaranteePIH;

    /**
     * Rural Housing Service guarantee
     */
    private BigDecimal guaranteeRHS;

    /**
     * Veterans Administration guarantee
     */
    private BigDecimal guaranteeVA;

    /**
     * Delinquency rate 30 days
     */
    private BigDecimal delinquency30D;

    /**
     * Delinquency rate 60 days
     */
    private BigDecimal delinquency60D;

    /**
     * Delinquency rate 90 days
     */
    private BigDecimal delinquency90D;

    /**
     * Delinquency rate 120 days
     */
    private BigDecimal delinquency120D;

}