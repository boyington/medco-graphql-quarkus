package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="portfoliooptimization", namespace="com.amundi.tech.openaml.entity")
public class PortfolioOptimization extends Entity {

    /**
     * Identifier of the portfolio
     */
    private Identifier portfolioId;

    /**
     * Identifier of the partner
     */
    private Identifier partnerId;

    /**
     * Identifier of the requester
     */
    private Identifier requesterId;

    /**
     * Identifier of the constraint set
     */
    private Identifier constraintSetId;

    /**
     * Reference date of the portfolio data, for instance the accountant date
     */
    private LocalDate referenceDate;

    /**
     * Optimization mode, auto or manual
     */
    private String optimMode;

    /**
     * Current step in the optimization workflow. This is not the status.
     */
    private String workflowStep;

}