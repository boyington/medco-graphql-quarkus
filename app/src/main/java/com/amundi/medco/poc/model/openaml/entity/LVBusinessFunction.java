package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVBusinessFunction {

  /**
   * Portfolio management team (official) 
   */
  PORTFOLIO_MGMT,

  /**
   * Additional portfolio management team 1 
   */
  PORTFOLIO_MGMT_1,

  /**
   * Additional portfolio management team 2 
   */
  PORTFOLIO_MGMT_2,

  /**
   * Additional portfolio management team 3 
   */
  PORTFOLIO_MGMT_3,

  /**
   * Middle office stock team 
   */
  MO_STOCK,

  /**
   * Additional Middle office stock team 2 
   */
  MO_STOCK_2,

  /**
   * Middle office flow team 
   */
  MO_FLOW,

  /**
   * Master data team 
   */
  MASTER_DATA_MGMT,

  /**
   * Risk management team 
   */
  RISK_MANAGEMENT,

  /**
   * Performance analysis team 
   */
  PERF_ANALYSIS,

  /**
   * Performance attribution team 
   */
  PERF_ATTRIBUTION,

  /**
   * Compliance team 
   */
  COMPLIANCE,

  /**
   * Commercial team 
   */
  COMMERCIAL,

  /**
   * Client servicing team 
   */
  CLIENT_SERVICING,

  /**
   * Corporate action team 
   */
  CORPORATE_ACTION,

  /**
   * Treasury team 
   */
  TREASURY,

  /**
   * Portfolio DM Name 
   */
  DM_NAME,

}