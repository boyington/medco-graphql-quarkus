package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.*;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="allocation", namespace="com.amundi.tech.openaml.entity")
public class Allocation {

    /**
     * Allocation status
     */
    private String status;

    /**
     * Status information, use code and text to input status additiional information
     */
    private Comment statusInfo;

    /**
     * #allocation in the block : 1 - 9999
     */
    private Integer allocNum;

    /**
     * Allocation identifier
     */
    private Identifier allocationId;

    /**
     * External Allocation identifier
     */
    private Identifier externalId;

    /**
     * Portfolio id, name, accountingStandard
     */
    private Portfolio portfolio;

    /**
     * Accounting standard with management intent or business model
     */
    private AccountingStandard accountingStd;

    /**
     * Strategy code
     */
    private String strategyCode;

    /**
     * OPEN if the trade is to enter a position, CLOSE if the trade is to leave a position. Use LVClosingOpening
     */
    private String openingClosingCode;

    /**
     * Allocation quantity by portfolio
     */
    private BigDecimal quantity;

    /**
     * Allocation amounts in trading currency
     */
    private Amounts tradingAmounts;

    /**
     * Allocation amounts in settlement currency
     */
    private Amounts settlementAmounts;

    /**
     * Allocation amounts in portfolio currency, also called account currency
     */
    private Amounts portfolioAmounts;

    /**
     * Broker fees rate and/or amount
     */
    private FeesRateAmount brokerFees;

    /**
     * VAT fees rate and/or amount
     */
    private FeesRateAmount vatFees;

    /**
     * Taxes rate and/or amount
     */
    private FeesRateAmount taxes;

    /**
     * Other fees and/or amount
     */
    private FeesRateAmount otherFees;

    /**
     * Free fees,  misceallenous fees rate and/or amount
     */
    private FeesRateAmount miscFees;

    /**
     * Transfer fees or Movement fees : rate, amout, and currency,  CMV in french.
     */
    private FeesRateAmount transferFees;

    /**
     * Entf fees, entrance commission rate and/or amount
     */
    private FeesRateAmount entranceCommissionFees;

    /**
     * Mutual fund fees rate and/or amount
     */
    private FeesRateAmount mutualFundFees;

    /**
     * Distribution fees rate and/or amount
     */
    private FeesRateAmount distributionFees;

    /**
     * Withholding tax rate and/or amount
     */
    private FeesRateAmount withholdingTax;

    /**
     * ADI - advisory - fees rate and/or amount
     */
    private FeesRateAmount adiFees;

    /**
     * Clearing fees rate and/or amount
     */
    private FeesRateAmount clearingFees;

    /**
     * Market clearing fees rate and/or amount
     */
    private FeesRateAmount marketClearingFees;

    /**
     * Market execution fees rate and/or amount
     */
    private FeesRateAmount marketExecutionFees;

    /**
     * Allocation market value in portfolio currency (mtm and price).
     */
    private Amounts marketValue;

    /**
     * Allocation Market value in local (asset, security) currency.
     */
    private Amounts localMarketValue;

    /**
     * True if has delegated change
     */
    private Boolean hasDelegatedChange;

    /**
     * True if SSI for delegated change
     */
    private Boolean delegatedChangeSSI;

    /**
     * In case of delegated change, the amounts are in the delegated change currency. Use delegatedChange.ccy for the delegated change currency, and delegatedChange.fxRate for the exchange rate.
     */
    private Amounts delegatedChange;

    /**
     * Initial quantity or fixed amount
     */
    private BigDecimal initialQuantity;

    /**
     * Remaining quantity of the allocation after execution
     */
    private BigDecimal remainingQuantity;

    /**
     * The average price of executions prices
     */
    private BigDecimal avgExecutedPrice;

    /**
     * Fixed text associated to the allocation, e.g. email/fax contact details
     */
    private Comment comment;

}