package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.LVBusinessDayConvention;
import com.amundi.medco.poc.model.openaml.entity.asset.LVPaymentAt;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="settlementinfo", namespace="com.amundi.tech.openaml.entity")
public class SettlementInfo {

    /**
     * Settlement currency code
     */
    private String settlementCcy;

    /**
     * Asset settlement place that is defined for the security (CSD)
     */
    private Party settlementPlace;

    /**
     * Custody type can be DOMESTIC or INTERNATIONAL, see LVSettlementMode
     */
    private String custodyType;

    /**
     * Delivery type, settlement mode :  CASH, PHYSICAL or BOTH (see LVDeliveryType) for derivatives
     */
    private String deliveryType;

    /**
     * Indicates if the security does not have a firm settlement. Fixed Income:  Indicates a security has been announced, but is prior to being offered and priced.  Municipals:  Returns N until within T+3 days of the first settle date, regardless of whether the bond has a firm settlement date or not.
     */
    private String firmSettlement;

    /**
     * Business day convention to calculate the settlement date: FOLLOWING, MODIFIED_FOLLOWING, etc.
     */
    private LVBusinessDayConvention businessDayConv;

    /**
     * Payment lag, offset in open day of the payment/settlement date. Values can be 0,-1,-2 or 1, 2 etc. 
     */
    private Integer daysToSettle;

    /**
     * Day type such as Calendar day,  business day, etc. related to daysToSettle field 
     */
    private String dayType;

    /**
     * Payment calendar or business center (multiple calendars): calendar code, to calculate settle date (BB providing if security).
     */
    private String calendarSettlement;

    /**
     * Calendar code, here country iso2 code to calculate settle date (BB providing if security)
     */
    private String calendarCountry;

    /**
     * Payment at : UPFRONT, IN_ARREARS
     */
    private LVPaymentAt paymentAt;

    /**
     * Matching mode : AUTO or MANUAL
     */
    private LVMatchingMode matchingMode;

    /**
     * Matching platform if AUTO
     */
    private String matchingPlatform;

}