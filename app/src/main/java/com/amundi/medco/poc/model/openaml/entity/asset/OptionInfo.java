package com.amundi.medco.poc.model.openaml.entity.asset;

import com.amundi.medco.poc.model.openaml.LVBuySell;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.LVBusinessDayConvention;
import com.amundi.medco.poc.model.openaml.component.LVFrequency;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="optioninfo", namespace="com.amundi.tech.openaml.entity.asset")
public class OptionInfo {

    /**
     * Option holder, option buyer : the party that controls the choice to exercise the option
     */
    private String optionHolder;

    /**
     * Option writer : the party that has the obligation to fullfill the option holder choice
     */
    private String optionWriter;

    /**
     * Option type can be CALL or PUT
     */
    private LVOptionType optionType;

    /**
     * Option exercise style : American, European, Bermudan, Other, etc.
     */
    private LVOptionStyle optionStyle;

    /**
     * Option : exotic style (Vanilla, Barrier, Exotic) for instance FX option
     */
    private String exoticStyle;

    /**
     * Option direction : BUY or SELL associated to optionPrice
     */
    private LVBuySell optionDirection;

    /**
     * Strike if listed derivative, call price if callable bond, put price if putable bond
     */
    private BigDecimal optionPrice;

    /**
     * Optional option price unit if need to indicate the strike/price expression : PERCENT, YIELD, etc.
     */
    private LVUnit optionPriceUnit;

    /**
     * For option whose price is expressed as a spread to add to a yield or rate. The spread, or margin, is expressed in rate and percentage, i.e. 0.3 means 0.3%.
     */
    private BigDecimal optionSpread;

    /**
     * ratio : The number of shares of underlying equity received at conversion per specific par value
     */
    private BigDecimal optionRatio;

    /**
     * Effective Date of the option exercise, determination date if YES NOTE convertible option terms
     */
    private LocalDate effectiveDate;

    /**
     * Date when the right to exercise the option has expired
     */
    private LocalDate expirationDate;

    /**
     * Option feature : can be anytime, once, etc. in case of putable, callable
     */
    private String optionFeature;

    /**
     * The frequency at which the option can be exercised
     */
    private LVFrequency optionFreq;

    /**
     * Business day convention to calculate next option date. NONE or NO_ADJUSTMENT if calendar days
     */
    private LVBusinessDayConvention businessDayConv;

    /**
     * Option days notice: the number of days between the exercise announcement and the effective exercise of the option.
     */
    private Integer daysNotice;

    /**
     * Option notification max days for a put or a call.
     */
    private Integer notificationMaxDays;

    /**
     * Day type such as calendar day, business day, etc.  Related to daysNotice or notificationMaxDays
     */
    private String dayType;

    /**
     * First date from which the option can be exercised. If there is a lockout period, this is the first possible date after the end of the lockout period.
     */
    private LocalDate earliestExerciseDate;

    /**
     * Next call date, next put date, if bermuda option, next coupon date from the reference date, here today
     */
    private LocalDate nextOptionDate;

    /**
     * Second next option date, e.g. second call date, second put date.
     */
    private LocalDate secondOptionDate;

    /**
     * Second call price, second put price
     */
    private BigDecimal secondOptionPrice;

    /**
     * Optional option price unit if need to indicate the price expression : PERCENT, YIELD, etc.
     */
    private LVUnit secondOptionPriceUnit;

    /**
     * Second option type can be CALL, PUT
     */
    private LVOptionType secondOptionType;

    /**
     * Second option direction : BUY or SELL, for example in case of cap spread
     */
    private LVBuySell secondOptionDirection;

    /**
     * FX option : barrier type DOWN_IN, DOWN_OUT, UP_IN, UP_OUT, use LVBarrier
     */
    private String barrierType;

    /**
     * FX option : barrier, or first barrier if double barrier option
     */
    private BigDecimal barrier;

    /**
     * FX option : second barrier if double barrier option
     */
    private BigDecimal barrier2;

    /**
     * Window start date if barrier option
     */
    private LocalDate windowStartDate;

    /**
     * Window end date if barrier option
     */
    private LocalDate windowEndDate;

    /**
     * Cash refund
     */
    private BigDecimal rebate;

    /**
     * Rebate type
     */
    private String rebateType;

    /**
     * True if knockout option
     */
    private Boolean knockout;

    /**
     * Cut-off location : city code
     */
    private String cutOffLocation;

    /**
     * Cut-off time 1
     */
    private String cutOffTime1;

    /**
     * Cut-off time 2
     */
    private String cutOffTime2;

    /**
     * HIT, EXPIRY : use LVPaymentAt, if barrier option, default value is HIT
     */
    private LVPaymentAt payoutAt;

    /**
     * Final payout definition from portfolio side, for example : fixed rate receiver, floating rate receiver, fixed rate payer, floating rate payer. If combined options, can display the option strategy (see LVOptionType)
     */
    private String payout;

    /**
     * Payout rate expressed in % of nominal, for example Digital option
     */
    private BigDecimal payoutPct;

    /**
     * Payout amount in payoutCcy, for example Digital option
     */
    private BigDecimal payoutAmt;

    /**
     * Payout amount currency code, for example Digital option
     */
    private String payoutCcy;

    /**
     * For trading purpose, the multi strike minimum
     */
    private BigDecimal multiStrikeMin;

    /**
     * For trading purpose, the multi strike maximum
     */
    private BigDecimal multiStrikeMax;

    /**
     * For trading purpose, the multi strike step
     */
    private BigDecimal multiStrikeStep;

}