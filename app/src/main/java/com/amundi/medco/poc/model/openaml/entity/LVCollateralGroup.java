package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCollateralGroup {

  /**
   * Collateral in the context of OTC deal 
   */
  OTC,

  /**
   * Collateral in the context of loan borrow 
   */
  LB,

  /**
   * Collateral in the context of repo, reverse repo 
   */
  REPO,

  /**
   * Collateral in the context of repo with basket 
   */
  REPO_BSK,

  /**
   * Collateral in the context of forex 
   */
  FX,

  /**
   * Collateral in the context of CFD (contract for difference) 
   */
  CFD,

  /**
   * Collateral in the context of listed derivatives (Exchange Traded Derivative) 
   */
  ETD,

  /**
   * Collateral in the context of MBS 
   */
  MBS,

  /**
   * Collateral in the context of TBA 
   */
  TBA,

  /**
   * Collateral in the context of reuse when receive a title as collateral from the counterparty that has reused paid collateral 
   */
  REUSE,

  /**
   * Collateral in any other context 
   */
  OTHER,

}