package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVClassifCategory {

  /**
   * The classification is a business activity 
   */
  ACTIVITY,

  /**
   * The classification is an asset category 
   */
  ASSET,

  /**
   * The classification is a sector 
   */
  SECTOR,

}