package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVCodingScheme {

  /**
   * Security ISIN code, International Securities Identification Numbers 
   */
  ISIN,

  /**
   * Security ISIN code at security creation in local system 
   */
  ISIN_CREATION,

  /**
   * Sedol code, Stock Exchange Daily Official List 
   */
  SEDOL,

  /**
   * CUSIP code 
   */
  CUSIP,

  /**
   * Bloomberg FIGI code, Financial Instrument Global Identifier 
   */
  FIGI,

  /**
   * Bloomberg code, can be a ticker (not corporate ticker) 
   */
  BLOOMBERG,

  /**
   * Bloomberg ID_BB_SEC_NUM 
   */
  BB_SEC_NUM,

  /**
   * Bloomberg corporate ticker code, e.g. LLOYDS 
   */
  BB_TICKER,

  /**
   * WKN code, i.e. Wertpapierkennnummer is a German securities identification code, can also be called WPK. 
   */
  WKN,

  /**
   * Ric Reuters code 
   */
  RIC,

  /**
   * Reuters code 
   */
  REUTERS,

  /**
   * BARRAOne code 
   */
  BARRA,

  /**
   * LoanX ID : unique identifier for syndicated loans 
   */
  LOANX_ID,

  /**
   * Markit RED code 
   */
  REDCODE,

  /**
   * Markit RED pair code - 9 digit 
   */
  RED_PAIRCODE,

  /**
   * Factset code 
   */
  FACTSET,

  /**
   * CLC code 
   */
  CLC,

  /**
   * GMI code, usually for listed derivative 
   */
  GMI,

  /**
   * BARCLAYS code 
   */
  BARCLAYS,

  /**
   * BPAM facility code 
   */
  BPAM_FACILITY_CODE,

  /**
   * EUROCLEAR code 
   */
  EUROCLEAR,

  /**
   * Unique transaction identifier 
   */
  UTI,

  /**
   * Unique transaction identifier if fx spot 
   */
  UTI_SPOT,

  /**
   * Unique transaction identifier if fx forward 
   */
  UTI_FORWARD,

  /**
   * Convertible : ticker of the underlying equity 
   */
  BOND_TO_EQY_TICKER,

  /**
   * OTC deal number 
   */
  OTCNUM,

  /**
   * OTC initial or original deal number 
   */
  OTCNUM_INI,

  /**
   * OTC mirror id 
   */
  OTCMIRROR_ID,

  /**
   * OTC deal execution number 
   */
  EXECNUM,

  /**
   * DO NOT USE, for Alto internal use 
   */
  ASSET_ID,

  /**
   * Local code for security 
   */
  ASSET_CODE,

  /**
   * Instrument number for security or otc 
   */
  INST_NUM,

  /**
   * Original instrument number for otc 
   */
  INST_NUM_ORIGIN,

  /**
   * Hedge reference, for instance, it is the bond reference id to link the bond and the otc 
   */
  HEDGE_REF,

  /**
   * ISIN code of the security hedged by this other asset. for instance, it is the bond ISIN code hedged by the otc 
   */
  HEDGE_ISIN,

  /**
   * Identifier contains the corresponding non-hedged index 
   */
  NON_HEDGED_INDEX,

  /**
   * ALTO : Instrument number for security or otc 
   */
  ALTO_INST_NUM,

  /**
   * Asset external code 
   */
  ASSET_EXT_CODE,

}