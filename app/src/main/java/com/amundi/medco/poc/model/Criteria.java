package com.amundi.medco.poc.model;

import lombok.Data;

import java.util.List;

@Data
public class Criteria {
    private String name;
    private List<String> values;
}
