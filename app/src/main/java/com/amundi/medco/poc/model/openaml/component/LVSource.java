package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVSource {

  /**
   * Standard and Poor's 
   */
  SP,

  /**
   * Moody's Investors service 
   */
  MOODYS,

  /**
   * Fitch Ratings 
   */
  FITCH,

  /**
   * Dominion Bond Rating Service (DBRS) 
   */
  DBRS,

  /**
   * CARE Ratings 
   */
  CARE,

  /**
   * Crisil : India rating agency 
   */
  CRISIL,

  /**
   * ICRA limited : Indian credit rating agency 
   */
  ICRA,

  /**
   * Japan Credit Rating Agency 
   */
  JCR,

  /**
   * Malaysian Rating Corporation 
   */
  MARC,

  /**
   * RAM Ratings, credit rating agency in Malaysia and ASEAN (South-East Asia) 
   */
  RAM,

  /**
   * Kroll Bond Rating Agency (KBRA) : CMBS 
   */
  KROLL,

  /**
   * Morningstar 
   */
  MORNINGSTAR,

}