package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVComparisonOperator {

  /**
   * EQ : comparison operator EQUAL  
   */
  EQ,

  /**
   * NEQ : comparison operator NOT EQUAL 
   */
  NEQ,

  /**
   * GT : comparison operator GREATER THAN 
   */
  GT,

  /**
   * GTE : comparison operator GREATER THAN OR EQUAL 
   */
  GTE,

  /**
   * LT : comparison operator LOWER THAN 
   */
  LT,

  /**
   * LTE : comparison operator LOWER THAN OR EQUAL 
   */
  LTE,

  /**
   * BETWEEN : comparison operator BETWEEN value1 and value2, if decimal, value1 lower than decimal value lower than value2, if date, BETWEEN date1 and date2 
   */
  BETWEEN,

  /**
   * NOT_BETWEEN : comparison operator NOT BETWEEN value1 and value2, if date, NOT BETWEEN  date1 and date2 
   */
  NOT_BETWEEN,

  /**
   * LIKE : comparison operator LIKE for text value 
   */
  LIKE,

  /**
   * NOT_LIKE : comparison operator NOT LIKE for text value 
   */
  NOT_LIKE,

  /**
   * If list of values, membership operator IN 
   */
  IN,

  /**
   * If list of values, membership operator NOT IN 
   */
  NOT_IN,

  /**
   * If list of values, INTERSECTION of values 
   */
  INTER,

  /**
   * If list of values, UNION of values 
   */
  UNION,

}