package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Account;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="sdi", namespace="com.amundi.tech.openaml.entity")
public class Sdi extends Entity {

    /**
     * CASH, DOMESTIC or INTERNATIONAL settlement mode
     */
    private LVSettlementMode settlementMode;

    /**
     * Detailed code of the settlement mode, mainly if domestic
     */
    private String settlementModeCode;

    /**
     * Reason code
     */
    private String reasonCode;

    /**
     * True if Continuous Linked Settlement
     */
    private Boolean cls;

    /**
     * SDI Security : Global custodian id and account
     */
    private Account globalCustodian;

    /**
     * SDI Security : local agent id and account
     */
    private Account localAgent;

    /**
     * SDI Security : PSET, settlement place id and account, paying agent
     */
    private Account placeOfSettlement;

    /**
     * SDI Security : Cross border delegation of the Asset Manager to the global custodian. (Tag 94F present or not in the swift format)
     */
    private Boolean crossBorderDelegation;

    /**
     * SDI Cash : Cash correspondent bank (CC), or similar institution used by the settlement system to make or receive payments
     */
    private Account cashCorrespondent;

    /**
     * SDI Cash :  optional Cash beneficiary (CB),only if different from settlement place
     */
    private Account cashBeneficiary;

}