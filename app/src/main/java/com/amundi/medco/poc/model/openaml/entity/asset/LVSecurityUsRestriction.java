package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVSecurityUsRestriction {

  /**
   * Unrestricted 
   */
  UNRESTRICTED,

  /**
   * US restriction is Rule 144A 
   */
  RULE_144A,

  /**
   * US restriction is Regulation S 
   */
  REGULATION_S,

  /**
   * US restriction is rule 144A with registration right. 
   */
  RULE_144A_REG_RIGHTS,

  /**
   * US restriction is Rule 144A and Regulation S 
   */
  BOTH,

  /**
   * Unknown US restriction 
   */
  UNKNOWN,

}