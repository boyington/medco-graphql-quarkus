package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="collateralgroup", namespace="com.amundi.tech.openaml.entity")
public class CollateralGroup {

    /**
     * Agreement id : reference between collateral agreement, portfolio and counterparty
     */
    private String refCollateral;

    /**
     * Collateral group : OTC | LB | REPO | REPO_BSK | FX | CFD | MBS | REUSE | OTHER
     */
    private LVCollateralGroup collateralGroup;

    /**
     * Collateral type : CASH or SECURITY
     */
    private LVCollateralType collateralType;

    /**
     * Indicates if the margin is an initial margin (IM) or a variable margin (VM)
     */
    private LVCollateralMarginType marginType;

    /**
     * Safekeeper : CSD, safekeeping place or counterparty, use role CUSTODIAN, CLEARING_BROKER, CLEARING_HOUSE, COUNTERPARTY, TRIPARTY_AGENT
     */
    private Party safekeeper;

    /**
     * Counterparty of the otc contract
     */
    private Party counterparty;

    /**
     * Legal form of the transfer: PLEDGED | PLEDGED_REUSE | FULL_OWNERSHIP
     */
    private LVCollateralTransferMode transferMode;

    /**
     * True if the collateral is reused
     */
    private Boolean reused;

}