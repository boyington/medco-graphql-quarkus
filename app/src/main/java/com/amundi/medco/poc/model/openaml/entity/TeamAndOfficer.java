package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="teamandofficer", namespace="com.amundi.tech.openaml.entity")
public class TeamAndOfficer extends Entity {

    /**
     * Code to identify the team function, see LVBusinessFunction
     */
    private String function;

    /**
     * Company's division
     */
    private String division;

    /**
     * Department unique name, for example : organisation / direction / division / department
     */
    private String department;

    /**
     * Name of the portfolio team/department
     */
    private String departmentName;

    /**
     * Person in charge of this function in this department
     */
    private ContactInfo officer;

}