package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVBuySell;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.*;
import com.amundi.medco.poc.model.openaml.entity.analytic.Rating;
import com.amundi.medco.poc.model.openaml.entity.asset.*;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="asset", namespace="com.amundi.tech.openaml.entity")
public class Asset extends Entity {

    /**
     * Asset class:  1st level in asset classification
     */
    private LVAssetClass assetClass;

    /**
     * Asset group:  2nd level in asset classification
     */
    private LVAssetGroup assetGroup;

    /**
     * Asset type:  detail level in asset classification
     */
    private String assetType;

    /**
     * Asset CFI code, Classification of financial instruments
     */
    private String cfiCode;

    /**
     * List of others asset classifications
     */
    private List<Classif> classifs;

    /**
     * Asset quantity expression mode : QUANTITY, NOMINAL, CONTRACT, CASH
     */
    private LVQuantityExprMode quantityExprMode;

    /**
     * Asset short name
     */
    private String shortName;

    /**
     * Asset acronym
     */
    private String acronym;

    /**
     * Asset description, for instance the BB SECURITY_DES
     */
    private String description;

    /**
     * List of  comment, ex: text = BB comment, createdBy = BB
     */
    private List<Comment> comments;

    /**
     * List of asset events
     */
    private List<AssetEvent> events;

    /**
     * Asset quotation unit on exchange market, quotation mode
     */
    private LVUnit quotationUnit;

    /**
     * More detail on quotation type
     */
    private Identifier quotationExtId;

    /**
     * Security quotations : OPEN, CLOSE, ASK, BID, MID, etc.
     */
    private List<Quotation> quotations;

    /**
     * Asset exchange market
     */
    private ExchangeMarket exchangeMarket;

    /**
     * Asset valuation threshold in weight or amt
     */
    private AmountWeight valuationThreshold;

    /**
     * Valuation haircut, or margin percentage
     */
    private BigDecimal haircut;

    /**
     * Minimum transfer amount to eliminate negligible margin calls
     */
    private BigDecimal marginMinTransferAmt;

    /**
     * True if has been cleared
     */
    private Boolean isCleared;

    /**
     * Issue currency code if bond or fund share, asset trading currency code if equity
     */
    private String ccy;

    /**
     * The asset is listed or not listed (other, if otc for example)
     */
    private LVTradingMode tradingMode;

    /**
     * True if the asset is listed
     */
    private Boolean listed;

    /**
     * True if the asset is quoted
     */
    private Boolean quoted;

    /**
     * True if the asset is provided
     */
    private Boolean provided;

    /**
     * True if the asset provider is known, it means the asset is provided
     */
    private Boolean feedIsKnown;

    /**
     * If request/force a specific pricing source, this is the requested/forced source
     */
    private String forcedPricingSource;

    /**
     * Daily, Intraday, other values
     */
    private String pricingSourceType;

    /**
     * Fair value assessment : for ASB, FAS157 code
     */
    private String fairValueAssessment;

    /**
     * Country iso code for risk country exposition
     */
    private String exposureCtry;

    /**
     * Asset exposure, the market exposure
     */
    private AmountWeight exposure;

    /**
     * Commitment, specific asset exposure
     */
    private AmountWeight commitment;

    /**
     * True if exposure country is emerging
     */
    private Boolean emergingExpoCtry;

    /**
     * True if inflation-indexed security, otc, index
     */
    private Boolean inflationIndexed;

    /**
     * List of asset sectors
     */
    private List<Classif> sectors;

    /**
     * List of asset geographic zone
     */
    private List<Location> geoAreas;

    /**
     * List of analytics
     */
    private List<Analytics> allAnalytics;

    /**
     * List of other Analytics
     */
    private List<Analytics> allFiAnalytics;

    /**
     * List of asset ratings
     */
    private List<Rating> ratings;

    /**
     * Optional maturity date of the financial asset
     */
    private LocalDate maturityDate;

    /**
     * Share class can be A, B, C, I, etc.
     */
    private String shareClassType;

    /**
     * Share type : A (capitalized, accumulation), D (distributed, income), UNKNOWN
     */
    private LVShareType shareType;

    /**
     * True if reinvestment of dividend, interests, income or return.
     */
    private Boolean reinvestment;

    /**
     * True if simulated financial instrument
     */
    private Boolean simulated;

    /**
     * True if simulated OTC instrument has been used
     */
    private Boolean otcUsed;

    /**
     * Tax law in case the security is linked to specific tax law : DSK, PEA, etc.
     */
    private Codification taxLaw;

    /**
     * True if follow 144A regulation
     */
    private Boolean is144A;

    /**
     * List of asset documents, For example, if mutual fund, the link to the reportings
     */
    private List<EDocument> documents;

    /**
     * List of custom attributes associated to the Asset
     */
    private List<Attribute> attributes;

    /**
     * Issuer of the security : equity, debt instrument or fund share
     */
    private LegalEntity issuer;

    /**
     * Security nominal, Nominal or notional of the OTC as defined in the term sheet, in case of Variance swap this is the Vega-notional
     */
    private BigDecimal nominal;

    /**
     * Previous nominal or notional if a market operation has modified the OTC notional
     */
    private BigDecimal previousNominal;

    /**
     * Initial nominal
     */
    private BigDecimal initialNominal;

    /**
     * Variance notional amount
     */
    private BigDecimal varianceAmount;

    /**
     * Quoted currency code, counter currency code
     */
    private String ccy2;

    /**
     * Nominal or notional in ccy2, if forex is the spot amount 2 or forward amount 2
     */
    private BigDecimal nominal2;

    /**
     * OTC BUY or SELL. Forex (BUY or SELL ccy, the main currency/ unit currency). Credit derivative (BUY or SELL the protection). Total return swap : BUY or SELL
     */
    private LVBuySell buySell;

    /**
     * OTC start date
     */
    private LocalDate startDate;

    /**
     * OTC end date
     */
    private LocalDate endDate;

    /**
     * Tenor period (1Y, 2Y, etc.)
     */
    private String tenorPeriod;

    /**
     * OTC undwind date, or early redemption date if the OTC has been/could be terminated early
     */
    private LocalDate unwindDate;

    /**
     * Unwind spread
     */
    private BigDecimal unwindSpread;

    /**
     * Number of days to notify before unwind the otc, or performperform early redemption
     */
    private Integer daysNotice;

    /**
     * Trade/deal date and time
     */
    private LocalDateTime tradeDateTime;

    /**
     * Trader comment text and code
     */
    private Comment traderComment;

    /**
     * Contract settlement date
     */
    private LocalDate settlementDate;

    /**
     * Delivery type, matching part
     */
    private SettlementInfo settlementInfo;

    /**
     * Contractual matrix for collateral, Otc settlement matrix
     */
    private ContractualMatrix contractualMatrix;

    /**
     * Portfolio name, code
     */
    private Portfolio portfolio;

    /**
     * Strategy code associated to the deal
     */
    private String strategyCode;

    /**
     * Contract convention, master agreement
     */
    private Agreement masterAgreement;

    /**
     * Counterparty of the financial deal/trade or agreement.
     */
    private LegalEntity counterparty;

    /**
     * Initial counterparty if financial deal/trade or agreement with triparty
     */
    private LegalEntity initialCounterparty;

    /**
     * True if the deal is eligible to clearing
     */
    private Boolean clearing;

    /**
     * Clearing broker
     */
    private LegalEntity clearingBroker;

    /**
     * Clearing house
     */
    private LegalEntity clearingHouse;

    /**
     * Date of clearing start
     */
    private LocalDateTime clearingDateTime;

    /**
     * Date of confirmation
     */
    private LocalDateTime confirmationDateTime;

    /**
     * Triparty agent if triparty repo
     */
    private LegalEntity tripartyAgent;

    /**
     * Final counterparty of the contract if back-to-back booking
     */
    private LegalEntity finalCounterparty;

    /**
     * If portfolio compression, indicates the compression (for regulatory reporting)
     */
    private String compression;

    /**
     * Interest rate definition : rate, rateType, resetFreq. Security Coupon rate or dividend rate is the percentage of nominal that is the debt instrument yield. InterestRate contains the coupon/dividend rate details if it depends on the index rate value.
     */
    private InterestRate interestRate;

    /**
     * Upfront payment date
     */
    private LocalDate upfrontDate;

    /**
     * Upfront payment or balancing adjustment/payment, upfront can be a part of the premium if option
     */
    private Amount upfront;

    /**
     * Forward Price determination date in the context of cash settlement
     */
    private LocalDate fwdPriceDeterminationDate ;

    /**
     * Final payment date
     */
    private LocalDate finalPaymentDate;

    /**
     * Final payment as defined in the term sheet
     */
    private Amount finalPayment;

    /**
     * Premium date
     */
    private LocalDate premiumDate;

    /**
     * Payed premium in amount
     */
    private Amount premium;

    /**
     * Payed premium in % of the nominal
     */
    private BigDecimal premiumPct;

    /**
     * If option, the option characteristics : listed derivative, or option otc, Credit index option for example
     */
    private OptionInfo option;

    /**
     * Exchange rate expressed as 1 CCY1 = rate CCY2, can be the spot rate or the forward rate
     */
    private BigDecimal forwardRate;

    /**
     * Spot rate is the market rate of quoted currency pair.
     */
    private BigDecimal spotRate;

    /**
     * Forward points are the interest rate differential between CCY1 and CCY2, forward rate = spotRate + forwardPoints
     */
    private BigDecimal forwardPoints;

    /**
     * Date and time of the exchange rate fixing, spot rate or/and forward rate
     */
    private LocalDateTime fixingDateTime;

    /**
     * Non deliverable currency code
     */
    private String nonDeliverableCcy;

    /**
     * True if FX swap to cover fx risk (MMI or other instrument), or if simulated OTC, true means that the dealing desk will need to cover by FX spot the OTC
     */
    private Boolean fxCover;

    /**
     * Lending : True if evergreen
     */
    private Boolean evergreen;

    /**
     * Lending : true if open
     */
    private Boolean lendingOpen;

    /**
     * CDS: Issuer debt type, for example : Senior unsecured, Senior subordinated, etc.
     */
    private String seniority;

    /**
     * CDS: level of default covered by the protection
     */
    private String docClause;

    /**
     * True if CDS deal is sensitive to issuer restructuring
     */
    private Boolean sensitiveToRestructuring;

    /**
     * True if the CDS contract is SRO (Standard Reference Obligation). A standardized reference obligation on the same reference and seniority level.
     */
    private Boolean isSRO;

    /**
     * CDS: recoveryRate
     */
    private BigDecimal recoveryRate;

    /**
     * CDS: Leverage factor
     */
    private BigDecimal leverageFactor;

    /**
     * Spread of the CDS
     */
    private BigDecimal cdsSpread;

    /**
     * Itraxx tranche, CDX tranche
     */
    private AssetTranche tranche;

    /**
     * True if the interest rate swap is multi-currency, with different currency in each leg, also called cross currency swap
     */
    private Boolean multiCurrency;

    /**
     * OTC legs : swap legs, forex legs, or other payment legs
     */
    private List<OtcLeg> legs;

    /**
     * Quantity of the underlying asset. OTC : In case of Variance swap, this is the variance amount. Security : Underlying quantity expressed in the quantity expression mode of the underlying asset, Equity (Right lot size)
     */
    private BigDecimal underlyingQty;

    /**
     * Underlying price is the price of the underlying, not the strike which is in option.optionPrice
     */
    private BigDecimal underlyingPrice;

    /**
     * Underlying of the OTC option :Asset, Index, Otc, Security
     */
    private Asset underlyingAsset;

    /**
     * Optional underlying basket to display the basket composition, i.e.  a set of financial asset
     */
    private Basket underlyingBasket;

    /**
     * Other Otc asset valuation : type, date, netAmt, netPrice
     */
    private List<Amounts> valuations;

    /**
     * List of corporate actions related to this security
     */
    private List<CorporateAction> corporateActions;

    /**
     * Issue country code
     */
    private String issueCtry;

    /**
     * Country of domicile, iso code
     */
    private String ctryOfDomicile;

    /**
     * Securitized : ISO code for the country of a Mtge CMO collateral group or all collateral of a Mtge CMO deal when the collateral is not grouped. 'Mixed' will be displayed if more than 1 country.
     */
    private String ctryCollatIso;

    /**
     * underwriter
     */
    private String underwriter;

    /**
     * Issuing date
     */
    private LocalDate issueDate;

    /**
     * Price at issuance
     */
    private BigDecimal issuePrice;

    /**
     * Currency code at issuance, it is the same as ccy if no modification of security currency
     */
    private String issueCcy;

    /**
     * Number of shares issued at IPO (initial public offering)
     */
    private BigDecimal issueShares;

    /**
     * Announce date, for instance ILS market announce date
     */
    private LocalDate announceDate;

    /**
     * First issue as an Asset
     */
    private Asset firstIssue;

    /**
     * Nominal currency code
     */
    private String nominalCcy;

    /**
     * Asset lot size, asset round lot
     */
    private BigDecimal lotSize;

    /**
     * Asset previous lot size, also called nominal obsolete
     */
    private BigDecimal previousLotSize;

    /**
     * MIN_PIECE : Asset min size
     */
    private BigDecimal minSize;

    /**
     * Asset trade lot size, PX_TRADE_LOT_SIZE
     */
    private BigDecimal tradeLotSize;

    /**
     * CARRYOVER_LOTSIZE : Asset carryover lot size
     */
    private BigDecimal carryoverLotSize;

    /**
     * Security form REGISTERED or BEARER
     */
    private LVSecurityForm form;

    /**
     * True if
     */
    private Boolean prospectus;

    /**
     * True if the security is eligible to the Employee Retirement Income Security Act (ERISA law in US)
     */
    private Boolean erisa;

    /**
     * The securities is US restricted or not (as per Rule 144): UNRESTRICTED, RULE_144A, REGULATION_S, BOTH, UNKNOWN
     */
    private LVSecurityUsRestriction usRestriction;

    /**
     * True if euro regulation for mutual fund
     */
    private Boolean euroRegulation;

    /**
     * EUSD (EU Savings directive), BB : EU_DIRECTIVE
     */
    private Boolean euDirective;

    /**
     * 122A retention method
     */
    private String retentionMethod;

    /**
     * True if eligible to directive 122A, capital requirement from EBA (european banking authority)
     */
    private Boolean capitalRequirementEBA;

    /**
     * True if eligible to the tax on financial transaction
     */
    private Boolean eligibleTTF;

    /**
     * True if eligible in PEA, PEA PME
     */
    private Boolean eligiblePEA;

    /**
     * Date of PEA eligibility
     */
    private LocalDate eligiblePEADate;

    /**
     * True if eligible in PEA PME
     */
    private Boolean eligiblePEAPME;

    /**
     * Indicates whether the security is defined as a bail-in or resolution note by the issuer or in the credit agreement
     */
    private String bailIn;

    /**
     * Collateral type for a security used as guarantee
     */
    private String collateralType;

    /**
     * True if security eligible as ECB collateral
     */
    private Boolean collateralECB;

    /**
     * Security eligible as reinvestment for cash collateral
     */
    private LVCollateralReuse collateralReuse;

    /**
     * Reference to an issuer id that can be different from the security issuer
     */
    private Identifier referenceIssuerId;

    /**
     * Reference to an index
     */
    private Asset referenceIndex;

    /**
     * Reference to another security or asset, for instance main bond
     */
    private Asset referenceAsset;

    /**
     * Depository receipt custodian for ADR or GDR
     */
    private LegalEntity adrCustodian;

    /**
     * Currency code of the underlying equity for ADR or GDR
     */
    private String adrUnderlyingCcy;

    /**
     * True if equity principal, equity is listed on main stock exchange, main market
     */
    private Boolean equityPrincipal;

    /**
     * True if hybrid
     */
    private Boolean hybrid;

    /**
     * True if the fixed income has a convertible option
     */
    private Boolean convertible;

    /**
     * True if the convertible bond is reverse convertible, ie, conversion ratio is variable based on underlying price and if this price is under a limit, the convertible bond can be redeemed (reverse).
     */
    private Boolean reverseConvertible;

    /**
     * True if the convertible bond is exchangeable
     */
    private Boolean exchangeable;

    /**
     * Initial margin convertible in price after convertible option has been exercised
     */
    private BigDecimal initialConversionPremium;

    /**
     * True if it is a capital contingent security, such as a capital Contingent convertible bond (coco)
     */
    private Boolean capitalContingent;

    /**
     * BB : CAPITAL_TYP_COCO_INITIAL_TRIGGER
     */
    private String capitalTypeCocoInitialTrigger;

    /**
     * Conversion rate price, contingent convertible trigger rate (CONTINGENT_CNVS_TRIGGER)
     */
    private BigDecimal contingentConvertibleTrigger;

    /**
     * Conversion ratio, conversion factor  (CV_CNVS_RATIO)
     */
    private BigDecimal conversionRatio;

    /**
     * Date of the conversion ratio, conversion factor
     */
    private LocalDate conversionRatioDate;

    /**
     * Capital Trigger Type 
     */
    private String capitalTriggerType;

    /**
     * Conversion parity (CV_PARITY)
     */
    private BigDecimal parity;

    /**
     * Definition of the conversion option terms
     */
    private ConversionInfo conversion;

    /**
     * True if EMTN  (EMTN_FLAG)
     */
    private Boolean emtn;

    /**
     * True if green bond
     */
    private Boolean greenBond;

    /**
     * True if social bond
     */
    private Boolean socialBond;

    /**
     * True if sustainable bond, sustainable fund
     */
    private Boolean sustainable;

    /**
     * True if catastrophe bond
     */
    private Boolean catastrophe;

    /**
     * True if putable bond
     */
    private Boolean putable;

    /**
     * Definition of put option characteristics and current values
     */
    private OptionInfo putOption;

    /**
     * True if callable bond
     */
    private Boolean callable;

    /**
     * Definition of call option characteristics and current values
     */
    private OptionInfo callOption;

    /**
     * True if the call feature is a Soft call protection
     */
    private Boolean softCall;

    /**
     * Soft call trigger %
     */
    private BigDecimal softCallTrigger;

    /**
     * True if the fixed income call option has been executed
     */
    private Boolean called;

    /**
     * Last date on which a call option has been executed
     */
    private LocalDate calledDate;

    /**
     * The price at which a call has been executed
     */
    private BigDecimal calledPrice;

    /**
     * True if the callable security is still callable
     */
    private Boolean stillCallable;

    /**
     * First call date of the bond based on issuance date
     */
    private LocalDate firstCallDateIssuance;

    /**
     * Code linked to the call or put option of the debt instrument
     */
    private String callPut;

    /**
     * True if dim sum bond, bond denominated in Chinese renminbi and issued in Hong Kong.
     */
    private Boolean dimSumBond;

    /**
     * True if sukuk bond, sukuk product
     */
    private Boolean sukuk;

    /**
     * The type of sukuk option
     */
    private String sukukOptionType;

    /**
     * True if sukuk international, false if sukuk domestic
     */
    private Boolean sukukInternational;

    /**
     * True if SUKUK_AL_IJARA
     */
    private Boolean sukukAlIjara;

    /**
     * True if SUKUK_AL_ISTISNAA
     */
    private Boolean sukukAlIstisnaa;

    /**
     * True if SUKUK_AL_MUDARABAH
     */
    private Boolean sukukAlMudarabah;

    /**
     * True if SUKUK_AL_MURABAHA
     */
    private Boolean sukukAlMurabaha;

    /**
     * True if SUKUK_AL_MUSHARAKAH
     */
    private Boolean sukukAlMusharakah;

    /**
     * True if SUKUK_AL_WAKALA_BEL_ISTITHMAR
     */
    private Boolean sukukAlWakalaBelIstithmar;

    /**
     * True if SUKUK_AL_WAKALAH
     */
    private Boolean sukukAlWakalah;

    /**
     * True if SUKUK_BAI_BITHAMAN_AJIL
     */
    private Boolean sukukBaiBithamanAjil;

    /**
     * True if SUKUK_BAI_DAYN
     */
    private Boolean sukukBaiDayn;

    /**
     * True if SUKUK_BAI_INAH
     */
    private Boolean sukukBaiInah;

    /**
     * True if SUKUK_BAI_TAWARRUQ
     */
    private Boolean sukukBaiTawarruq;

    /**
     * True if IS_ISLAMIC
     */
    private Boolean islamic;

    /**
     * Real estate type if equity
     */
    private String realEstateType;

    /**
     * True if Real estate equity
     */
    private Boolean realEstateEqy;

    /**
     * True if quant
     */
    private Boolean quant;

    /**
     * True if equity linked security
     */
    private Boolean equityLinked;

    /**
     * True if partly paid  IS_PARTLY_PAID
     */
    private Boolean partlyPaid;

    /**
     * True if payment in kind (PIK), i.e. interest will be paid in additional bonds
     */
    private Boolean paymentInKind;

    /**
     * Equity float (in million) is the number of equity shares available to the public
     */
    private BigDecimal equityFloat;

    /**
     * Equity rating and analysis including recommendations, target price, target upside
     */
    private Rating equityRating;

    /**
     * Credit rating and analysis including recommendations
     */
    private Rating creditRating;

    /**
     * Ex-dividend date
     */
    private LocalDate exDividendDate;

    /**
     * The number of days of the ex-dividend period
     */
    private Integer exDividendDays;

    /**
     * Interest Amount contains accrInt, accrIntN, accruedDays, nextCouponDate, etc.
     */
    private InterestAmount interestAmount;

    /**
     * SRI info : specific fields to Socially Responsible Investment (SRI)  including data related to Green, Social and Sustainability Bonds.
     */
    private SriInfo sriInfo;

    /**
     * Debt instrument data, including mortgage data
     */
    private DebtInfo debtInfo;

    /**
     * List of location distribution : percentage and its location name
     */
    private List<LocationDistribution> geoDistributions;

    /**
     * Indicates if a security is currently paying down or capitalizing on a factor basis, has the potential to pay down or capitalize, or has paid down or capitalized on a factor basis.
     */
    private Boolean securityFactorable;

    /**
     * True if swapped security
     */
    private Boolean swapped;

    /**
     * True if illiquid
     */
    private Boolean illiquid;

    /**
     * Illiquid status date
     */
    private LocalDate illiquidDate;

    /**
     * Illiquid status notes
     */
    private String illiquidNotes;

    /**
     * Loan originator name
     */
    private String originatorName;

    /**
     * First name of the list of asset managers involved in the deal.
     */
    private String dealAssetManagerList;

    /**
     * Private equity / real estate : Current loan to value
     */
    private BigDecimal currentLoanToValue;

    /**
     * Capital protection type : full, partial, etc.
     */
    private String capitalProtection;

    /**
     * Level of protection in percentage : 100 if full capital proctection or principal protected, 90 if level of protection is 90%
     */
    private BigDecimal pctCapitalProtection;

    /**
     * True if structured
     */
    private Boolean structured;

    /**
     * True if mortgage
     */
    private Boolean mortgage;

    /**
     * Pool num, if mortgage, agency pool number
     */
    private String poolNum;

    /**
     * Agency pool program code. Agency who issued the MBS. FG=Freddie Gold, FH=Freddie PC, GN=GNMA, G2=GNMA2, FN=Fannie, CA=CA.
     */
    private String poolType;

    /**
     * Facility name, if mortgage is the lead manager or underwriter of the original underwriting of the current security
     */
    private String facilityName;

    /**
     * The majority type of property the loans were written against. Applies to CMO/ABS. i.e MF= MultiFamily
     */
    private String propertyType;

    /**
     * Administrator name of the loan, mortgage servicer name
     */
    private String servicerName;

    /**
     * Insurance security sub type : cat bond, CR - Primary, CR - Retro, ILW, Quota Share, Sidecar
     */
    private String insuranceSecurityType;

    /**
     * Type of loss : index, indemnity, modeled losses, parametric
     */
    private String insuranceTrigger;

    /**
     * Type of catastrophe : cyclone, earthquake, flood, hurricane, multiperil, pandemic, typhoon, windstorm, winter storm
     */
    private String perilType;

    /**
     * Part of world the catastrophe has to occur in : specific US state, a country or a regional description, for example worldwide
     */
    private String insuranceRegion;

    /**
     * ILS : risk period. The reinsurance agreement and the cat bond only cover events taking place during the risk period. Use fromDate (risk start date) and to toDate (risk end date)
     */
    private Period riskPeriod;

    /**
     * ILS : sponsor name as String, can be ETF sponsor
     */
    private String sponsor;

    /**
     * Sponsor code
     */
    private String sponsorCode;

    /**
     * High level funding source (market issue) : general obligation, revenue, pre-refunded.
     */
    private String funding;

    /**
     * True if this is a derivative. Municipals : True if this is a derived municipal security: Dealers put fixed-cpn munis into a trust and split cash flows from them into two synthetic securities.
     */
    private Boolean derivative;

    /**
     * Type of municipal : Revenue anticipation note, Bond anticipation note, General obligation Note, etc.
     */
    private String muniType;

    /**
     * Municipal project code
     */
    private String muniProjectCode;

    /**
     * Capital type : advanced refunding, crossover refinancing, new financing, ...
     */
    private String capitalType;

    /**
     * Indicates if the bond is refunded or currently anticipated to be refunded.
     */
    private String refundStatus;

    /**
     * Municipal pre refunding type : Shows the type of pre-refunding as escrowed to maturity pre-refunded to a call date.
     */
    private String preRefundType;

    /**
     * Pledge Funding : appropriation and lease, assessment, gross revenue, ...
     */
    private String pledgeFunding;

    /**
     * Municipal escrow type : agencies, bank CD, cash, FFCB Obligations, FHLB, FHLMC, FICO Obligations, FNMA Securities, GIC, GNMA Securities, other escrow
     */
    private String escrowType;

    /**
     * Credit type : insurance, letter of credit (LOC), standby purchase agreement, guarantor, gic, credit agreement
     */
    private String creditType;

    /**
     * Offering : public, private, competitive, negotiated, limited, remarketed
     */
    private String offering;

    /**
     * Tax status, tax provision
     */
    private String taxStatus;

    /**
     * Municipals : Flag (true/false) if the bond is subject to the alternative minimum tax
     */
    private Boolean altMinTaxFlag;

    /**
     * Municipals : state code, associated with the issuer of the security. Municipals and Money Markets: The state postal code.
     */
    private String stateCode;

    /**
     * Municipals : lead manager, or underwriter of the original underwriting of the current security
     */
    private String leadManager;

    /**
     * The insurance or other program that backs the debt service on the bonds and enhances their credit quality.
     */
    private String creditEnhancement;

    /**
     * The authority that has issued the municipal bond
     */
    private LegalEntity issuingAuthority;

    /**
     * Main obligor of the debt instrument, mainly the issuer
     */
    private LegalEntity obligor;

    /**
     * Guarantor of the security. If Municipals, the bank providing the guaranty agreement for the bond
     */
    private LegalEntity guarantor;

    /**
     * The bank providing the Letter of Credit for the bonds. Field describes Liquidity Facility provider
     */
    private LegalEntity locProvider;

    /**
     * The company providing the Standby Purchase Agreement on the bonds. Field describes Liquidity Facility provider. Muni purchase agreement.
     */
    private LegalEntity spaProvider;

    /**
     * ILS Trust provider
     */
    private LegalEntity trustProvider;

    /**
     * Deal Administrator
     */
    private LegalEntity dealAdministrator;

    /**
     * True if margined option, i.e. option with call margin
     */
    private Boolean marginedOption;

    /**
     * Maturity date or expiry date of the option, warrant, or future, also named call date if option
     */
    private LocalDate expiryDate;

    /**
     * Expected maturity date
     */
    private LocalDate expectedMaturityDate;

    /**
     * Derivative : Last transaction call date (or put date) before option expiry or future expiry
     */
    private LocalDate lastTransactionCallDate;

    /**
     * Trading hours of the listed derivative
     */
    private String tradingHours;

    /**
     * To indicate the size of the underlying commodity : barrels, gallons, buschels of corn, etc.
     */
    private String tradingUnits;

    /**
     * Futures contract size, or other listed derivatives
     */
    private BigDecimal contractSize;

    /**
     * Previous futures contract size, or other listed derivatives
     */
    private BigDecimal previousContractSize;

    /**
     * Derivative : contract value is the value of 1 derivative (contractSize * point value)
     */
    private BigDecimal contractValue;

    /**
     * Tick size, minimum price increment
     */
    private BigDecimal tickSize;

    /**
     * Tick value per contract
     */
    private BigDecimal tickValue;

    /**
     * Future point value (BB FUT_VAL_PT). or unit value
     */
    private BigDecimal pointValue;

    /**
     * Derivative : contract value of the underlying for instance option on future, the value of 1 derivative (contractSize * point value)
     */
    private BigDecimal underlyingContractValue;

    /**
     * Optional underlying ccy
     */
    private String underlyingCcy;

    /**
     * Optional description of the listed derivative underlying
     */
    private String underlyingDesc;

    /**
     * Underlying maturity date
     */
    private LocalDate underlyingMaturityDate;

    /**
     * Notional issue coupon representative of the derivative on rate
     */
    private BigDecimal notionalIssueCoupon;

    /**
     * Future delivery months, for instance HMUZ, or All 12 months, January = F, February = G, March = H, April = J, May = K, June = M, July = N, August = Q, September = U, October = V, November = X, December = Z
     */
    private String futureDeliveryMonths;

    /**
     * Month and year expiry of the future, for instance DEC 17
     */
    private String futureContractExpiration;

    /**
     * Year of the expiration on 4 digit
     */
    private String expirationYear;

    /**
     * Month of the expiration on 2 digit
     */
    private String expirationMonth;

    /**
     * Futures : 1st day of notice before delivery by the clearing agent
     */
    private LocalDate futureNoticeDate;

    /**
     * Futures : 1st day of notice before delivery by the clearing agent
     */
    private LocalDate futureFirstDeliveryDate;

    /**
     * True if  the future on bond has a cheapest to deliver
     */
    private Boolean futureHasCTD;

    /**
     * Future on bond : Name of the cheapest to deliver as defined in BB (FUT_CTD)
     */
    private String futCheapestToDeliver;

    /**
     * Future on bond : ISIN of the cheapest to deliver (FUT_CTD_ISIN)
     */
    private String futCheapestToDeliverISIN;

    /**
     * Future on bond : maturity date of the cheapest to deliver
     */
    private LocalDate futureCTDMaturityDate;

    /**
     * Future on bond : frequency of the cheapest to deliver
     */
    private String futureCTDFrequency;

    /**
     * Future on bond : coupon rate of the cheapest to deliver
     */
    private BigDecimal futureCTDCoupon;

    /**
     * Future : conversion Factor used to calculate CTD = Current Bond Price - Settlement Price x Conversion Factor
     */
    private BigDecimal futConversionFactor;

    /**
     * True if future has option trading, FUT_OPT_TRADING
     */
    private Boolean futureHasOptionTrading;

    /**
     * True if dividend future on equity index, company or basket companies
     */
    private Boolean isDividendIndexFuture;

    /**
     * Shares per warrant : conversion parity of the warrant
     */
    private BigDecimal sharesPerWarrant;

    /**
     * True if this is a fund with a fixed number of shares
     */
    private Boolean closedEndFund;

    /**
     * Mutual fund : data shared by fund and fund share
     */
    private Portfolio fund;

    /**
     * Share of a mutual fund : share class data
     */
    private ShareClass shareClass;

    /**
     * Fund or Share Class promoter code and name, usually same than management company
     */
    private Codification promoter;

    /**
     * Transfer agent
     */
    private LegalEntity transferAgent;

    /**
     * SR category num and name
     */
    private Codification srCategory;

    /**
     * True if Indicates whether a security is a financial instrument under the MiFID II framework according to the definition provided in Section C of Annex 1 of the Markets in Financial Instruments Directive (MiFID).  BB MIFID_II_INSTRUMENT_INDICATOR
     */
    private Boolean mifid2Indicator;

    /**
     * True indicates if an instrument is reported in the Financial Instrument Reference Data System (FIRDS).
     */
    private Boolean mifid2FIRDSIndicator;

    /**
     * MIFID tick size liquidity band for shares, depositary receipts and exchange-traded funds as detailed in Regulatory Technical Standards (RTS) 11 under the MiFID II (Markets in Financial Instruments Directive) framework. Possible returns are:   BAND 1   BAND 2   BAND 3   BAND 4   BAND 5   BAND 6       Please note that for exchange-traded Funds (ETFs), this field will return a value for ETFs where the asset class focus (FUND_ASSET_CLASS_FOCUS) is 'equity.'  BB TICK_SIZE_LIQUIDITY_BAND
     */
    private String liquidityBand;

    /**
     * MIFID Indicates if an instrument is traded on a trading venue (TOTV) or admitted to trading on a trading venue (ATT) in line with the requirements of Article 26 of the Markets in Financial Instruments Regulation (MiFIR) and Regulatory Technical Standards (RTS) 22 and 23 under the MiFID II (Markets in Financial Instruments Directive) framework.
     */
    private Boolean isAdmittedToTrading;

    /**
     * MIFID Admitted to trading on a trading venue (ATT) Termination date.
     */
    private LocalDate attTerminationDate;

    /**
     * MIFID Maturity date (Bloomberg).
     */
    private LocalDate mifidMaturityDate;

    /**
     * MIFID_IS_LIQUID_INSTRUMENT : Indicates if the security is liquid, as defined by ESMA (European Securities and Markets Authority) under the Markets in Financial Instruments Directive (MiFID) framework. 
     */
    private Boolean isLiquidInstrument;

    /**
     * True if SME Growth market
     */
    private Boolean isSMEGrowthMarket;

    /**
     * If isSMEGrowthMarket is true, indicates the MIC
     */
    private String growthMarketMIC;

    /**
     * MMF : Bond proxy1 identifier
     */
    private String mmfProxy1;

    /**
     * MMF : Bond proxy1 identifier
     */
    private String mmfProxy2;

    /**
     * MMF :  valuation method MTM or PROXY
     */
    private String mmfValuationMethod;

    /**
     * MMF : number of contributors used to calculate the price of private bonds
     */
    private Integer mmfNbContrib;

    /**
     * Real estate or building : building space
     */
    private BigDecimal buildingSpace;

    /**
     * True if this is a hard currency, example : USD or EUR or JPY
     */
    private Boolean hardCurrency;

    /**
     * True if this is a non deliverable currency, example : RUB
     */
    private Boolean nonDeliverable;

    /**
     * Currency exchange rates
     */
    private List<ExchangeRate> exchangeRates;

    /**
     * Index type if Itraxx/CDX: define the debt type (XOVER, EUROPE, NAHY, etc.)
     */
    private String indexType;

    /**
     * Other index type
     */
    private String otherIndexType;

    /**
     * Version of the serie number
     */
    private String indexSerie;

    /**
     * Period term name, for example, Itraxx/CDX: 1Y, 2Y, etc.
     */
    private String indexTerm;

    /**
     * CDS Index: discount on initial nominal after the default of a counterparty in the basket.
     */
    private BigDecimal indexRatio;

    /**
     * Index valuation frequency
     */
    private LVFrequency indexFreq;

    /**
     * Index region, country or territory
     */
    private String indexRegion;

    /**
     * Index initial price, index first fixing
     */
    private BigDecimal firstFixing;

    /**
     * date of Index initial price, index first fixing
     */
    private LocalDate firstFixingDate;

    /**
     * Index rate type
     */
    private String indexRateType;

    /**
     * Index rate calculation basis
     */
    private String indexCalcBasis;

    /**
     * termination date of the index
     */
    private LocalDate terminationDate;

    /**
     * Number of index constituents
     */
    private Integer constituentsSize;

    /**
     * Last index rebalancing date
     */
    private LocalDate rebalancingDate;

    /**
     * Next index rebalancing date
     */
    private LocalDate nextRebalancingDate;

    /**
     * True if basket Index, i.e. an index based on a basket
     */
    private Boolean basketIndex;

    /**
     * Detailed index composition
     */
    private Basket basket;

    /**
     * key defining rules / policy data  have been provided to build the object
     */
    private String dataProvidingPolicy;

    /**
     * True if private placement
     */
    private Boolean privatePlacement;

    /**
     * all custom data organised by universe with each a list fields ( key + value)
     */
    private List<DataUniverse> customData;

    /**
     * Ongoing Cost Estimated
     */
    private BigDecimal ongoingCostEstimated;

    /**
     * KIID_Ongoing_Charge
     */
    private BigDecimal ongoingChargesKiid;

    /**
     * Fund fees
     */
    private List<FeesRateAmount> assetFees;

    /**
     * Swap Model : ex for ELS NAV_TARGET, CUBE
     */
    private String swapModel;

    /**
     * Swap spread rate 
     */
    private BigDecimal swapSpreadRate;

    /**
     * Computation formula for the SWAP/ELS. MULTIPLICATIVE,ADDITIVE,CUBE,AMUNDI
     */
    private String swapFormula;

    /**
     * Warrant : covered indication
     */
    private String warrantCovered;

    /**
     * true if the index is hedged
     */
    private Boolean hedged;

    /**
     * Indicates the code of the hedged currency for instance for index
     */
    private String hedgedCurrency;

    /**
     * maximum Nominal of the SWAP
     */
    private BigDecimal maxSwapNominal;

    /**
     * The minimum size of a nominal change
     */
    private BigDecimal minNominalChange;

    /**
     * The maximum size of a nominal change
     */
    private BigDecimal maxNominalChange;

    /**
     * OTC SDI : settlement and delivery instructions
     */
    private Sdi sdi;

    /**
     * True if the bond is in payment default
     */
    private Boolean defaulted;

    /**
     * Subordinated, Unsubordinated, Unknown, None
     */
    private String subordinated;

    /**
     * True if the Fixed income is perpetual
     */
    private Boolean perpetual;

    /**
     * True if covered, provided value
     */
    private Boolean covered;

    /**
     * Maturity type
     */
    private String maturityType;

    /**
     * True if  the security is a part of the TRACE (Trade Reporting And Compliance Engine) Dissemination System. 
     */
    private Boolean traceDisseminated;

    /**
     * True if  the security is eligible for TRACE, the NASDAQ Fixed Income Pricing System. 
     */
    private Boolean traceEligible;

    /**
     * Payment Rank as provided
     */
    private String paymentRank;

    /**
     * True if the fixed income security is a LPN - loan participation note
     */
    private Boolean loanParticipationNote;

    /**
     * The cash account related to the asset.
     */
    private Account cashAccount;

    /**
     * Bond series number, Loan tranche, Mortgage ABS/CMO series
     */
    private String series;

    /**
     * Issue Amount, initial face amount of the debt instrument, if municipals, the dollar amount of bonds issued under this maturity
     */
    private BigDecimal issueAmount;

    /**
     * Roll date
     */
    private LocalDate rollDate;

    /**
     * Last corporate meeting date
     */
    private LocalDate lastCorporateMeetingDate;

    /**
     * Hedge security code
     */
    private String hedgeSecurityCode;

    /**
     * List of index variants
     */
    private List<Asset> indexVariants;

    /**
     * True if ex dividend
     */
    private Boolean exDivFlag;

    /**
     * True if prepayment Sensitive 
     */
    private Boolean prepaySensitive;

    /**
     * True if UCITS
     */
    private Boolean ucitsFlag;

    /**
     * Indicates that the issuer has self-reported that the net proceeds of the fixed income instrument will be applied toward green projects or activities that promote climate change mitigation or adaptation, or other environmental sustainability purposes
     */
    private Boolean selfReportedGreen;

    /**
     * True if  is regualtion D
     */
    private Boolean regulationD;

    /**
     * True if  is regualtion S
     */
    private Boolean regulationS;

    /**
     * trade status of the asset
     */
    private String tradeStatus;

}