package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVAddressType {

  /**
   * Legal address defined in the legal documents to create the legal entity 
   */
  REGISTERED,

  /**
   * Principal business address 
   */
  PRINCIPAL_BUSINESS,

  /**
   * Headquarters address, head office address 
   */
  HEADQUARTERS,

  /**
   * Incorporation address, domicile 
   */
  INCORPORATION,

  /**
   * Mailing or Postal address 
   */
  MAILING,

  /**
   * Residential address, also tax or fiscal address 
   */
  RESIDENTIAL,

  /**
   * Operational address 
   */
  OPERATIONAL,

}