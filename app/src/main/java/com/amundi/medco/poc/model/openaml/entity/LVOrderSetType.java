package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVOrderSetType {

  /**
   * The orders are part of a program trade, with multiple instrument on same exchange market 
   */
  PROGRAM_TRADE,

  /**
   * The orders are linked, or are part of a multi-leg orders 
   */
  LINKED_ORDERS,

  /**
   * The orders are part of a cross order 
   */
  CROSS_ORDER,

  /**
   * This is a simple set of orders 
   */
  MISC,

}