package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="partylink", namespace="com.amundi.tech.openaml.entity")
public class PartyLink extends EntityLink {

    /**
     * Channel, or way of relationship
     */
    private String channel;

    /**
     * Provided service
     */
    private String providedService;

    /**
     * The linked third-party or the party in relationship.
     */
    private Party linkedParty;

    /**
     * Percentage of shares held by this third party in the legal entity, direct ownership
     */
    private BigDecimal pctOwnership;

    /**
     * Percentage of voting rights held
     */
    private BigDecimal pctVotingRights;

    /**
     * Percentage of own capital
     */
    private BigDecimal pctOwnCapital;

}