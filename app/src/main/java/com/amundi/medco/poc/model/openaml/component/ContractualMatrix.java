package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="contractualmatrix", namespace="com.amundi.tech.openaml.component")
public class ContractualMatrix {

    /**
     * Matrix type, form of applicable matrix
     */
    private String type;

    /**
     * Matrix name
     */
    private String name;

    /**
     * Matrix code
     */
    private String code;

    /**
     * Publication date
     */
    private LocalDate publicationDate;

}