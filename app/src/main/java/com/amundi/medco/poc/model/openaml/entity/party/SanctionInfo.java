package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="sanctioninfo", namespace="com.amundi.tech.openaml.entity.party")
public class SanctionInfo {

    /**
     * undefined
     */
    private String sanctionFRASC;

    /**
     * undefined
     */
    private String sanctionOFAC;

    /**
     * undefined
     */
    private String sanctionOther;

    /**
     * undefined
     */
    private String sanctionUESC;

    /**
     * undefined
     */
    private String sanctionUKSC;

    /**
     * undefined
     */
    private String sanctionUNSC;

}