package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPtfPocketScope {

  /**
   * Front pocket set up by the portfolio asset manager 
   */
  FRONT,

  /**
   * Accountant pocket set up by the portfolio accountant 
   */
  ACCOUNTANT,

}