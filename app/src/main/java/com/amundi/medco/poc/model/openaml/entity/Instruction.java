package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Codification;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.component.LVFrequency;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="instruction", namespace="com.amundi.tech.openaml.entity")
public class Instruction extends Entity {

    /**
     * External identifier or reference such as provided by the client
     */
    private Identifier externalId;

    /**
     * Cancel and replace : Identifier of the cancelled instruction
     */
    private Identifier cancelId;

    /**
     * Client identifier
     */
    private Identifier clientId;

    /**
     * Risk profile
     */
    private Codification riskProfile;

    /**
     * Mandate identifier
     */
    private Identifier mandateId;

    /**
     * Sender portfolio of this instruction, or payer if it is a payment instruction
     */
    private Portfolio sender;

    /**
     * Recipient portfolio of this instruction, or beneficiary if it is payment instruction
     */
    private Portfolio recipient;

    /**
     * Instruction type
     */
    private String type;

    /**
     * Instruction subtype
     */
    private String subtype;

    /**
     * Instruction amount
     */
    private BigDecimal amt;

    /**
     * Instruction amount currency code
     */
    private String ccy;

    /**
     * Instruction frequency, for instance NONE (if standing once), MONTHLY, QUATERLY, BIANNUAL, YEARLY
     */
    private LVFrequency frequency;

    /**
     * Instruction start date, effective date
     */
    private LocalDate startDate;

    /**
     * Instruction expiry date as defined at the creation of the instruction
     */
    private LocalDate expiryDate;

    /**
     * Instruction effective closing date. This date can occur before expiry date if the instruction has been closed at client instruction
     */
    private LocalDate closingDate;

    /**
     * Allocations of the instruction
     */
    private List<InstructionAllocation> allocations;

}