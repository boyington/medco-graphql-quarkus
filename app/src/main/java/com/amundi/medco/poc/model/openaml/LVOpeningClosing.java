package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVOpeningClosing {

  /**
   * Opening transaction if derivative trade (future, option, CFD, etc.). Open a position. 
   */
  OPEN,

  /**
   * Clsoing transaction if derivative trade (future, option, CFD, etc.). Close a position 
   */
  CLOSE,

}