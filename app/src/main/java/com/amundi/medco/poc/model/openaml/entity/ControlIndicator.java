package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="controlindicator", namespace="com.amundi.tech.openaml.entity")
public class ControlIndicator {

    /**
     * Indicator name
     */
    private String indicatorName;

    /**
     * Indicator bucket
     */
    private String bucket;

    /**
     * Indicator value
     */
    private String value;

    /**
     * The value alert, the limit value to have an alert
     */
    private String alert;

    /**
     * The value info, the limit value as information
     */
    private String info;

    /**
     * Indicator operator used with info value and alert value
     */
    private String operator;

    /**
     * Between : The 2nd value limit to have an alert
     */
    private String alert2;

    /**
     * Between : The 2nd value limit as information
     */
    private String info2;

    /**
     * Between : The operator of the 2nd limit
     */
    private String operator2;

    /**
     * Status of this control indicator
     */
    private String status;

}