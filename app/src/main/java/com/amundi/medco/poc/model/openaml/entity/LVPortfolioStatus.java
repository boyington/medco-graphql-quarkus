package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPortfolioStatus {

  /**
   * Open status means the portfolio is open to S/R and portfolio management 
   */
  OPEN,

  /**
   * Closed status if the asset has matured, or has been closed 
   */
  CLOSED,

  /**
   * Pre-closed status means the fund is closed to S/R, and will be closed 
   */
  PRE_CLOSED,

}