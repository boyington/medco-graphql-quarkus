package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVAssetGroup {

  /**
   * Equity : Equity ordinary, common stock 
   */
  EQUITY_ORDINARY,

  /**
   * Equity : ADR, GDR, EDR 
   */
  DEPOSITORY_RECEIPT,

  /**
   * Equity : preference shares, privileged shares 
   */
  PREFERENCE_SHARES,

  /**
   * Private equity 
   */
  PRIVATE_EQUITY,

  /**
   * Participation structured products includes Open End Certificates (= tracker certificates),Outperformance Certificates, Bonus Certificates and Outperformance Bonus Certificates, etc. 
   */
  PARTICIPATION_PRODUCT,

  /**
   * Fixed income: corporate bond 
   */
  CORPORATE,

  /**
   * Fixed income: preferred stock with dividend rate 
   */
  PREFERRED_STOCK,

  /**
   * Fixed income: government related bond 
   */
  GOVERNMENT,

  /**
   * Fixed income: municipal bond 
   */
  MUNICIPAL,

  /**
   * Securitization, securitized instrument : ABS, TBA, MBS_PASSTHROUGH, C-MBS, CMO, CDO, CLO, etc. 
   */
  SECURITIZED,

  /**
   * Insurance-linked securities : Catastrophe bonds, other insurance linked securities 
   */
  INSURANCE_LINKED,

  /**
   * Structured product, structured bond 
   */
  STRUCTURED_PRODUCT,

  /**
   * Bank loan, Corporate loan, real estate loan, real asset loan 
   */
  LOAN,

  /**
   * Yes note (yield enhancement note) 
   */
  YES_NOTE,

  /**
   * Convertible bond 
   */
  CONVERTIBLE_BOND,

  /**
   * Hybrid fixed income security 
   */
  HYBRID,

  /**
   * Right can be bond right or equity right 
   */
  RIGHT,

  /**
   * Money market : Certificate of deposit, 
   */
  CERTIFICATE_OF_DEPOSIT,

  /**
   * Money market : Commercial paper, Neu CP, BMTN, Neu BMTN 
   */
  MEDIUM_TERM_NOTE,

  /**
   * Treasury note : BTAN, etc. 
   */
  TREASURY_NOTE,

  /**
   * Treasury bill : US-TBILL, BTF, etc. 
   */
  TREASURY_BILL,

  /**
   * Mutual fund, standard investment fund 
   */
  MUTUAL_FUND,

  /**
   * ETF : Exchange Traded Funds 
   */
  ETF,

  /**
   * Fund of funds (FOF) 
   */
  FUND_OF_FUNDS,

  /**
   * Hedge funds 
   */
  HEDGE_FUND,

  /**
   * Private equity funds for capital investment 
   */
  PRIVATE_EQUITY_FUND,

  /**
   * REITs : Real Estate Investment Trusts, real estate investment funds 
   */
  REIT,

  /**
   * SCPI : specific french real estate funds 
   */
  SCPI,

  /**
   * Pension funds 
   */
  PENSION_FUND,

  /**
   * SFT : Securities lending and borrowing 
   */
  BORROW_LEND_SECURITIES,

  /**
   * SFT : Repo or Reverse repo 
   */
  REPO,

  /**
   * SFT : Buy and sell back, or sell and buy back 
   */
  BUY_SELL_BACKS,

  /**
   * Forex forward 
   */
  FX_FORWARD,

  /**
   * Forex spot 
   */
  FX_SPOT,

  /**
   * Forex swap 
   */
  FX_SWAP,

  /**
   * Option derivative : Mainly listed option 
   */
  OPTION,

  /**
   * Option derivative : Warrant 
   */
  WARRANT,

  /**
   * Option derivative : Forex option 
   */
  FX_OPTION,

  /**
   * Option derivative : Interest Rate Option with a cap, floor or similar 
   */
  CAP_FLOOR,

  /**
   * Option derivative : Option on swap 
   */
  SWAPTION,

  /**
   * Option derivative : Option on CDS, option on CDX 
   */
  CREDIT_DEFAULT_OPTION,

  /**
   * Option derivative : OTC option 
   */
  EXOTIC_OPTION,

  /**
   * Credit derivative : CDS single name, CDS index 
   */
  CREDIT_DEFAULT_SWAP,

  /**
   * Credit derivative : CDS with underlying basket 
   */
  CREDIT_DEFAULT_BASKET,

  /**
   * Future derivative : Future 
   */
  FUTURE,

  /**
   * Forward derivative such as equity forward, bond forward or forward rate agreement 
   */
  FORWARD,

  /**
   * Swap derivative : Asset swap = Bond + swap derivative 
   */
  ASSET_SWAP,

  /**
   * Swap derivative : IRS, swap on rate 
   */
  INTEREST_RATE_SWAP,

  /**
   * Swap derivative : Cross currency swap 
   */
  CROSS_CURRENCY,

  /**
   * Swap derivative : Contract for difference : single stock, basket 
   */
  CFD,

  /**
   * Swap derivative : Total return swap: equity swap, TRS bond index, ... 
   */
  TOTAL_RETURN_SWAP,

  /**
   * Swap derivative based on inflation 
   */
  INFLATION_SWAP,

  /**
   * Swap derivative : variance swap 
   */
  VARIANCE_SWAP,

  /**
   * Swap derivative : dividend swap 
   */
  DIVIDEND_SWAP,

  /**
   * Swap derivative : volatility swap 
   */
  VOLATILITY_SWAP,

  /**
   * Swap derivative : Exotic swap 
   */
  EXOTIC_SWAP,

  /**
   * Stock index - can be bond index, equity index 
   */
  STOCK_INDEX,

  /**
   * Rate index - can be monetary index 
   */
  RATE_INDEX,

  /**
   * Index on commodity, for example oil, gold etc. 
   */
  COMMODITY_INDEX,

  /**
   * Credit derivative debt: Itraxx, CDX 
   */
  CREDIT_INDEX,

  /**
   * Economic indicator, and economics index 
   */
  ECONOMIC_INDICATOR,

  /**
   * Real asset : infrastructure 
   */
  INFRASTRUCTURE,

  /**
   * Real asset : real estate, property 
   */
  REAL_ESTATE,

}