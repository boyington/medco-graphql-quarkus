package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVNavType {

  /**
   * Official NAV, mostly validated accountant NAV 
   */
  OFFICIAL,

  /**
   * Official NAV without any S/R 
   */
  OFFICIAL_NO_SR,

  /**
   * Estimated NAV, for example a pre-validated accountant NAV 
   */
  ESTIMATED,

  /**
   * Shadow NAV 
   */
  SHADOW,

  /**
   * GIPS NAV 
   */
  GIPS,

  /**
   * Technical NAV 
   */
  TECHNICAL,

  /**
   * Front net asset, i.e. calculated by front IT system 
   */
  FRONT,

  /**
   * Proxy NAV, example proxy nav to complete a nav series 
   */
  PROXY,

}