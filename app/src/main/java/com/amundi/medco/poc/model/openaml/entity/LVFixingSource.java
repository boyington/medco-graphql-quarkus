package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVFixingSource {

  /**
   * Fixing ECB 14H30  (E) 
   */
  BCE,

  /**
   * Fixing Bloomberg 11H  (B) 
   */
  BLOOMBERG,

  /**
   * Fixing Bloomberg 11H  (B) 
   */
  BLOOMBERG_FIXING_11H,

  /**
   * Fixing TTM 
   */
  TTM,

  /**
   * Fixing WMR Reuters 17H  (W) 
   */
  WMR,

}