package com.amundi.medco.poc.model.openaml.entity.positionkeeping;

/**
 * undefined
 */
public enum LVMovementType {

  /**
   * INSTRUMENT: Securities  (bond, equity, future, option, fund share) in 
   */
  MI_SECUIN,

  /**
   * INSTRUMENT: Securities (bond, equity, future, option, fund share)  out 
   */
  MI_SECOUT,

  /**
   * INSTRUMENT: Equities in 
   */
  MI_EQUIIN,

  /**
   * INSTRUMENT: Equities out 
   */
  MI_EQUOUT,

  /**
   * INSTRUMENT: negotiable debt securities in 
   */
  MI_MMI_IN,

  /**
   * INSTRUMENT: Negotiable debt security out 
   */
  MI_MMIOUT,

  /**
   * INSTRUMENT: Bonds in 
   */
  MI_BONDIN,

  /**
   * INSTRUMENT: Bonds out 
   */
  MI_BONOUT,

  /**
   * INSTRUMENT : Bonds in on primary market 
   */
  MI_PMY_IN,

  /**
   * INSTRUMENT : Bonds out on primary market 
   */
  MI_PMYOUT,

  /**
   * INSTRUMENT: Futures in 
   */
  MI_FUTUIN,

  /**
   * INSTRUMENT: Futures out 
   */
  MI_FUTOUT,

  /**
   * INSTRUMENT: Options in 
   */
  MI_OPT_IN,

  /**
   * INSTRUMENT: Options out 
   */
  MI_OPTOUT,

  /**
   * INSTRUMENT: Fund share in 
   */
  MI_FUNDIN,

  /**
   * INSTRUMENT: Fund share out 
   */
  MI_FNDOUT,

  /**
   * INSTRUMENT: Buy CDS 
   */
  MI_BUYCDS,

  /**
   * INSTRUMENT: Sell CDS 
   */
  MI_SELCDS,

  /**
   * INSTRUMENT : Securities Buy and sell Back 
   */
  MI_BUYSBK,

  /**
   * INSTRUMENT : Securities Sell and buy back 
   */
  MI_SELBBK,

  /**
   * INSTRUMENT: Securities in for Securities borrowing 
   */
  MI_BORSEC,

  /**
   * INSTRUMENT: Securities out for lending 
   */
  MI_LENSEC,

  /**
   * INSTRUMENT: Securities repurchase agreement 
   */
  MI_REPO,

  /**
   * INSTRUMENT: Securities reverse repurchase agreement   
   */
  MI_RVREPO,

  /**
   * INSTRUMENT: Free issue of shares 
   */
  MI_FRESHA,

  /**
   * INSTRUMENT: Corp Act - Assimilation 
   */
  MI_ASSIMI,

  /**
   * INSTRUMENT: Corp Act - Split 
   */
  MI_ASPLIT,

  /**
   * INSTRUMENT: Corp Act - Split to blocked position 
   */
  MI_BSPLIT,

  /**
   * INSTRUMENT: Corp Act - Reverse split 
   */
  MI_RSPLIT,

  /**
   * INSTRUMENT: Corp Act - Reverse split from blocked position 
   */
  MI_BRSPLI,

  /**
   * INSTRUMENT: Corp Act - Spin off 
   */
  MI_SPINOF,

  /**
   * INSTRUMENT: Corp Act - Exchange 
   */
  MI_EXCHAN,

  /**
   * INSTRUMENT: Corp Act - Conversion 
   */
  MI_CONVER,

  /**
   * INSTRUMENT: Quantity adjustment/amortisation 
   */
  MI_ADJQAM,

  /**
   * INSTRUMENT: Market operations: modification of codes 
   */
  MI_MOCODE,

  /**
   * INSTRUMENT: Market operations: assignment 
   */
  MI_ASSIGN,

  /**
   * INSTRUMENT: Market operation unwind, early redemption, partial redemption 
   */
  MI_EALRED,

  /**
   * INSTRUMENT: Market operation - Reverse repo early redemption 
   */
  MI_EAREVR,

  /**
   * INSTRUMENT: Market operation - Repo early redemption 
   */
  MI_EAREPO,

  /**
   * INSTRUMENT: Market operation - Loan early redemption 
   */
  MI_EALOAN,

  /**
   * INSTRUMENT: Market operation - Borrow early redemption 
   */
  MI_EABOR,

  /**
   * INSTRUMENT: Market operation - Busy sell back early redemption 
   */
  MI_EABSB,

  /**
   * INSTRUMENT: Market operation - Sell buy back early redemption 
   */
  MI_EASBB,

  /**
   * INSTRUMENT: Market operation - Reverse repo early redemption 
   */
  MI_RDREVR,

  /**
   * INSTRUMENT: Market operation - Repo early redemption 
   */
  MI_RDREPO,

  /**
   * INSTRUMENT: Market operation - Loan early redemption 
   */
  MI_RDLOAN,

  /**
   * INSTRUMENT: Market operation - Borrow early redemption 
   */
  MI_REDBOR,

  /**
   * INSTRUMENT: Market operation - Busy sell back early redemption 
   */
  MI_REDBSB,

  /**
   * INSTRUMENT: Market operation - Sell buy back early redemption 
   */
  MI_REDSBB,

  /**
   * INSTRUMENT: Market operation  compensation, clearing 
   */
  MI_CLEARG,

  /**
   * INSTRUMENT: Market operation   exercice 
   */
  MI_EXERCI,

  /**
   * INSTRUMENT: Market operation expiration, final maturity 
   */
  MI_EXPMAT,

  /**
   * INSTRUMENT: Market operation   reset, renewal        
   */
  MI_RESET,

  /**
   * INSTRUMENT : Market operation - option abandon, out of the money option 
   */
  MI_ABDOPT,

  /**
   * INSTRUMENT: Corp Act - odd lot 
   */
  MI_ODDLOT,

  /**
   * INSTRUMENT: Security inflow, security received 
   */
  MI_RECEIV,

  /**
   * INSTRUMENT: Security outflow, security withdrawal 
   */
  MI_WTHDRA,

  /**
   * CASH: odd lot     sign + is in, sign - is out 
   */
  MC_ODDLOT,

  /**
   * CASH: Profit on securities 
   */
  MC_SECPRO,

  /**
   * CASH: Loss on securities 
   */
  MC_SECLOS,

  /**
   * CASH: Profit on Forex 
   */
  MC_FX_PRO,

  /**
   * CASH: Loss on Forex 
   */
  MC_FXLOSS,

  /**
   * CASH: Fictive calculation on subscription rights 
   */
  MC_FICRGT,

  /**
   * CASH: Recapitalisation on coupon/dividend 
   */
  MC_CAPLOA,

  /**
   * CASH: Foreign exchange, sign + is in, sign - is out 
   */
  MC_FOREX,

  /**
   * CASH: Cash movement 
   */
  MC_MOVEMT,

  /**
   * CASH: OTC margin call      sign + is in, sign - is out 
   */
  MC_MGCOTC,

  /**
   * CASH: Margin call on Futures     sign + is in, sign - is out 
   */
  MC_MGCFUT,

  /**
   * CASH: Margin call on Options     sign + is in, sign - is out 
   */
  MC_MGCOPT,

  /**
   * CASH : Fees on futures 
   */
  MC_FUTFEE,

  /**
   * CASH : Fees on deposit 
   */
  MC_DEPFEE,

  /**
   * CASH : Increase of deposit 
   */
  MC_DEPOIN,

  /**
   * CASH : Decrease of deposit 
   */
  MC_DEPOUT,

  /**
   * CASH : In credit interest on deposit 
   */
  MC_INTDPC,

  /**
   * CASH : Debtor interest on deposit 
   */
  MC_INTDPD,

  /**
   * CASH: Capital calls, funding appeal, sign + is in, sign - is out 
   */
  MC_CAPCAL,

  /**
   * CASH: Cash received 
   */
  MC_RECEIV,

  /**
   * CASH: Cash Withdrawal 
   */
  MC_WTHDRA,

  /**
   * CASH: Provision (expenses),  sign + is in, sign - is out 
   */
  MC_PROVIS,

  /**
   * CASH: Fixed Management fee 
   */
  MC_MNGFEF,

  /**
   * CASH: Variable Management fee 
   */
  MC_MNGFEV,

  /**
   * CASH: Swing pricing 
   */
  MC_SWINGP,

  /**
   * CASH: Taxes 
   */
  MC_TAXES,

  /**
   * CASH: Trading fees 
   */
  MC_TRDFEE,

  /**
   * CASH: Miscellaneous charges excluding trading fees 
   */
  MC_MISCHR,

  /**
   * CASH: Distribution 
   */
  MC_DISTRI,

  /**
   * CASH: Commissions on performance 
   */
  MC_PERFCM,

  /**
   * CASH: turnover commission 
   */
  MC_TURNCM,

  /**
   * CASH: transaction fee, costs 
   */
  MC_TRAFEE,

  /**
   * CASH: custodian fees 
   */
  MC_CUSTFE,

  /**
   * CASH: advisory fees 
   */
  MC_ADVFEE,

  /**
   * CASH: trustee fees 
   */
  MC_TRUSFE,

  /**
   * CASH: Payment of Invoice,  sign + is in, sign - is out 
   */
  MC_PAYINV,

  /**
   * CASH: Redemption on bond/negotiable debt security 
   */
  MC_REDEMP,

  /**
   * CASH: Cash in 
   */
  MC_CASHIN,

  /**
   * CASH: Cash in for Securities lending 
   */
  MC_INLEND,

  /**
   * CASH: Cash out for Securities borrowing 
   */
  MC_OUTBOR,

  /**
   * CASH: Cash in for repurchase agreement 
   */
  MC_INREPO,

  /**
   * CASH: Cash out for reverse repurchase agreement 
   */
  MC_OUTREV,

  /**
   * CASH: cash out for  sell and buy back 
   */
  MC_IN_SBB,

  /**
   * CASH : Cash in for  buy and sell back 
   */
  MC_OUTBSB,

  /**
   * CASH:  OTC cash sign + is in, sign - is out 
   */
  MC_OTC_DV,

  /**
   * CASH: Cash in for Income, Rights 
   */
  MC_ININCO,

  /**
   * CASH: for Dividend, Coupons, Interests, sign + is in, sign - is out 
   */
  MC_INTDVD,

  /**
   * CASH: Interest from swap otc, or otc, sign + is in, sign - is out 
   */
  MC_INTOTC,

  /**
   * CASH: Interest from repo, loan, bsb, sbb 
   */
  MC_INTLEN,

  /**
   * CASH: Interest from rev repo, borrow 
   */
  MC_INTBOR,

  /**
   * CASH: Interest from CCS 
   */
  MC_INTCCS,

  /**
   * CASH: Cap assignation , exercice 
   */
  MC_CAPLET,

  /**
   * CASH: Cash in (dividend, coupon)  for pledged Securities 
   */
  MC_PLGCOL,

  /**
   * CASH: Interest from received collateral 
   */
  MC_RCVCOL,

  /**
   * CASH: Cash transfer between pockets, sign + is in, sign - is out 
   */
  MC_TRFPCK,

  /**
   * CASH: Cash in for Futures, Options 
   */
  MC_FO_IN,

  /**
   * CASH: Cash in for Retrocession 
   */
  MC_IN_RET,

  /**
   * CASH: Cash out 
   */
  MC_CSHOUT,

  /**
   * CASH: Interest from paid collateral 
   */
  MC_PAICOL,

  /**
   * CASH: balancing cash adjustement (swap,collat,otc)  sign + is receiv, sign - is paid 
   */
  MC_BALADJ,

  /**
   * CASH: premium on CDS,   sign + is in, sign - is out 
   */
  MC_PRVCDS,

  /**
   * CASH: interest premium  CDS, sign + is in, sign - is out 
   */
  MC_INTCDS,

  /**
   * CASH: Dividend out 
   */
  MC_DIVOUT,

  /**
   * CASH: Subscription 
   */
  MC_SR_SUB,

  /**
   * CASH: Redemption 
   */
  MC_SR_RED,

  /**
   * CASH: money transfer, sign + is in, sign - is out 
   */
  MC_TRANSF,

  /**
   * CASH: Cash out for Futures, Options 
   */
  MC_FO_OUT,

  /**
   * CASH: Cash out for Retrocession 
   */
  MC_OUTRET,

  /**
   * CASH: Coupon received without stock movement 
   */
  MC_CPRWSI,

  /**
   * CASH: Coupon paid without stock movement 
   */
  MC_CPPWSI,

  /**
   * CASH: Redemption of repo 
   */
  MC_REDREP,

  /**
   * CASH: Redemption of  reverse repo 
   */
  MC_REDREV,

  /**
   * CASH: Redemption of borrow 
   */
  MC_REDBOR,

  /**
   * CASH: Redemption of Loan 
   */
  MC_REDLEN,

  /**
   * CASH: Redemption of BSB 
   */
  MC_REDBSB,

  /**
   * CASH: Redemption of SBB 
   */
  MC_REDSBB,

  /**
   * CASH: Adjustments, sign + is in, sign - is out 
   */
  MC_ADJUST,

  /**
   * CASH: Corporate action in 
   */
  MC_CA_IN,

  /**
   * CASH: Corporate action out 
   */
  MC_CA_OUT,

  /**
   * MISC: movement of securities without stock impact, sign + is in, sign - is out 
   */
  MI_MVTWIM,

  /**
   * MISC: NAV adjustment 
   */
  MC_NAVADJ,

  /**
   * MISC: Miscellaneous not transcoded 
   */
  MI_MISCEL,

  /**
   * INSTRUMENT: Corporate action in 
   */
  MI_CA_IN,

  /**
   * INSTRUMENT: Corporate action out 
   */
  MI_CA_OUT,

  /**
   * INSTRUMENT: frozen in 
   */
  MI_FRZ_IN,

  /**
   * INSTRUMENT: frozen out 
   */
  MI_FRZOUT,

  /**
   * INSTRUMENT:  partial redemption 
   */
  MI_PREDEM,

  /**
   * INSTRUMENT: Corp Act - maturity extension 
   */
  MI_MATEXT,

  /**
   * INSTRUMENT:  OTC derivative in 
   */
  MI_OTD_IN,

  /**
   * INSTRUMENT: OTC derivative out 
   */
  MI_OTDOUT,

}