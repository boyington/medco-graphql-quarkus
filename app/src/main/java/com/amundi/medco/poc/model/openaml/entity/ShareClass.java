package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="shareclass", namespace="com.amundi.tech.openaml.entity")
public class ShareClass extends Entity {

    /**
     * Share class type : A, B, C, I, etc.
     */
    private String shareClassType;

    /**
     * Share class currency code
     */
    private String ccy;

    /**
     * Share class launch date, inception date in the context of performances
     */
    private LocalDate launchDate;

    /**
     * Share class closing date, marketing end date
     */
    private LocalDate closingDate;

    /**
     * True if this is a currency-hedged share class
     */
    private Boolean hedged;

}