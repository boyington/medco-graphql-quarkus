package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPeriodShift {

  /**
   * Time period is ascending 
   */
  ASC,

  /**
   * Time period is descending 
   */
  DESC,

  /**
   * Time period is going from the reference day to a past date. 
   */
  BACK,

  /**
   * Time period is going from the reference day to a forward date 
   */
  FORWARD,

  /**
   * No period shift 
   */
  NONE,

  /**
   * Time period is sliding keeping same amount of time, from date to date in forward direction. 
   */
  SLIDING,

  /**
   * Calendar date 
   */
  CALENDAR,

}