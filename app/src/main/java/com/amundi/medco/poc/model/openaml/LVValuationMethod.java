package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVValuationMethod {

  /**
   * Weighted average cost method of inventory valuation 
   */
  WAC,

  /**
   * First in, first out method of inventory valuation 
   */
  FIFO,

  /**
   * Last in, first out method of inventory valuation 
   */
  LIFO,

}