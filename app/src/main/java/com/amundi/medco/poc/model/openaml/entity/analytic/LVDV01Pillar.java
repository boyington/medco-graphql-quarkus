package com.amundi.medco.poc.model.openaml.entity.analytic;

/**
 * undefined
 */
public enum LVDV01Pillar {

  /**
   * undefined 
   */
  DV01_1Y,

  /**
   * undefined 
   */
  DV01_2Y,

  /**
   * undefined 
   */
  DV01_3Y,

  /**
   * undefined 
   */
  DV01_4Y,

  /**
   * undefined 
   */
  DV01_5Y,

  /**
   * undefined 
   */
  DV01_7Y,

  /**
   * undefined 
   */
  DV01_10Y,

  /**
   * undefined 
   */
  DV01_15Y,

  /**
   * undefined 
   */
  DV01_20Y,

  /**
   * undefined 
   */
  DV01_30Y,

}