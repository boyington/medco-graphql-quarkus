package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPositionCategory {

  /**
   * Main stock 
   */
  ASSET,

  /**
   * Liability stock 
   */
  LIABILITY,

  /**
   * IAS39 
   */
  IAS,

  /**
   * IAS39 stock, old used value 
   */
  IFRS,

  /**
   * IFRS9 stock 
   */
  IFRS9,

  /**
   * Shareholding 
   */
  SHAREHOLDING,

}