package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCollateralTransferMode {

  /**
   * Collateral transfer mode : full ownership 
   */
  FULL_OWNERSHIP,

  /**
   * Collateral transfer mode : pledged 
   */
  PLEDGED,

  /**
   * Collateral transfer mode : pledged reuse 
   */
  PLEDGED_REUSE,

}