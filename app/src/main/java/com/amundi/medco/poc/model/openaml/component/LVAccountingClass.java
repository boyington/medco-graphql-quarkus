package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVAccountingClass {

  /**
   * Amortised cost 
   */
  AC,

  /**
   * Fair value other comprehensive income 
   */
  OCI,

  /**
   * Fair value other comprehensive income recyclable  (JVOCI-R) 
   */
  JVOCI_R,

  /**
   * Fair value OCI with transfer 
   */
  FVOCI,

  /**
   * Fair Value OCI without transfer (JVOCI-NR) 
   */
  JVOCI_NR,

  /**
   * Fair value through results 
   */
  JVR,

  /**
   * Fair value through P and L 
   */
  FVTPL,

}