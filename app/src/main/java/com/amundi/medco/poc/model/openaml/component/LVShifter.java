package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVShifter {

  /**
   * Nav date at D, coupon at D, D+0 from pivot date 
   */
  D,

  /**
   * Coupon at D+N, here N is the delay between trade date and settlement date, 
   */
  DN,

  /**
   * Nav date at D+1, Coupon at D+1, D+1 day from pivot date 
   */
  D1,

  /**
   * Nav date at D+2, Coupon at D+2, D+2 days from pivot date 
   */
  D2,

  /**
   * Nav date at D+3, Coupon at D+3, D+3 days from pivot date 
   */
  D3,

  /**
   * Nav date at D+4, Coupon at D+4, D+4 days from pivot date 
   */
  D4,

  /**
   * Nav date at D+5, D+5 days from pivot date 
   */
  D5,

  /**
   * Nav date at D+7, D+7 days from pivot date 
   */
  D7,

  /**
   * D+10 days from pivot date, D+10 days from pivot date 
   */
  D10,

  /**
   * D+30 days from pivot date, D+30 days from pivot date 
   */
  D30,

}