package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVIntCalcMode {

  /**
   * Actuarial interests 
   */
  ACTUARIAL,

  /**
   * Linear interests, or calculation method : simple 
   */
  LINEAR,

  /**
   * Original issue discount 
   */
  DISCOUNT,

  /**
   * zero coupon 
   */
  ZERO_COUPON,

  /**
   * Calculation method : Compounded 
   */
  COMPOUNDED,

  /**
   * Calculation method : yield quote 
   */
  YIELD_QUOTE,

}