package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="counterpartyrole", namespace="com.amundi.tech.openaml.entity.party")
public class CounterpartyRole extends PartyRole {

    /**
     * Deprecated DO NOT USE
     */
    private String financial;

    /**
     * External id identifier
     */
    private Identifier externalId;

}