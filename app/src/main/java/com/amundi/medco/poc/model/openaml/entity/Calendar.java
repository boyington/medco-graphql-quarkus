package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="calendar", namespace="com.amundi.tech.openaml.entity")
public class Calendar extends Entity {

    /**
     * Calendar type
     */
    private String type;

    /**
     * Calendar name
     */
    private List<CalendarDay> days;

}