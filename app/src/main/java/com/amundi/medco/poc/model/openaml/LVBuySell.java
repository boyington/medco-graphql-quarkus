package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVBuySell {

  /**
   * Direction or action : buy 
   */
  BUY,

  /**
   * Direction or action : sell 
   */
  SELL,

}