package com.amundi.medco.poc.model.openaml;

import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.component.LocalName;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="entity", namespace="com.amundi.tech.openaml")
public class Entity {

    /**
     * Technical field needed for Json (not visible in XML), used to manage object type if extends another object
     */
    private String _typ;

    /**
     * The entity unique id
     */
    private String _id;

    /**
     * Deprecated
     */
    private String uid;

    /**
     * version of openAML library (not filled by default, only if needed)
     */
    private String openaml;

    /**
     * Entity as at date, the date of the entity data
     */
    private LocalDate asAtDate;

    /**
     * Entity status, see status list of values, for example : OPEN, CLOSED
     */
    private String status;

    /**
     * Sub entity status, see status enumerations
     */
    private String subStatus;

    /**
     * Identifier of the entity
     */
    private Identifier id;

    /**
     * The entity simple name, by default in en
     */
    private String name;

    /**
     * More or other identifiers of the entity : a list of id  (repetitive structure of Identifier)
     */
    private List<Identifier> moreIds;

    /**
     * Creation date as a timestamp
     */
    private LocalDateTime creationDateTime;

    /**
     * Update date as a timestamp
     */
    private LocalDateTime updateDateTime;

    /**
     * Status date as a timestamp
     */
    private LocalDateTime statusDateTime;

    /**
     * Status information, use code and text to input status additiional information
     */
    private Comment statusInfo;

    /**
     * Who/what has created the entity
     */
    private String createdBy;

    /**
     * Who/what has modified the entity
     */
    private String modifiedBy;

    /**
     * Source identifier of this business object
     */
    private Identifier sourceId;

    /**
     * If localNames are needed, LocalName for name with another language code, or other specific Name objets
     */
    private List<LocalName> localNames;

}