package com.amundi.medco.poc.model;

import lombok.Data;

import java.util.List;

@Data
public class Criterias {

    private List<Criteria> criterias;
}
