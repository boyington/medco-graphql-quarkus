package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCountryCodeIso {

  /**
   * Country code is expressed following ISO 3166-1-alpha-2 
   */
  ISO2,

  /**
   * Country code is expressed following ISO 3166-1-alpha-3 
   */
  ISO3,

}