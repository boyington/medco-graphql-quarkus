package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVOptionStyle {

  /**
   * American option : may be exercised on any trading day on or before expiry 
   */
  AMERICAN,

  /**
   * European option : may be exercised only on expiration date, exercised one time 
   */
  EUROPEAN,

  /**
   * Bermuda option : may be exercised only at predetermined dates, typically every month 
   */
  BERMUDAN,

  /**
   * Asian option 
   */
  ASIAN,

  /**
   * Canary option 
   */
  CANARY,

  /**
   * Event linked option exercise style 
   */
  EVENT_LINKED,

  /**
   * Other type of option style 
   */
  OTHER,

}