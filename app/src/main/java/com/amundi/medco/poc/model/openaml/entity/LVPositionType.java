package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPositionType {

  /**
   * Position, Holding with available quantity, for a simple position on security or cash. It can be used as the default type of holding. 
   */
  AVAILABLE,

  /**
   * Position, Holding whose quantity is blocked (frozen statement), i.e. sell this quantity is not possible 
   */
  BLOCKED,

  /**
   * Position, Holding on the same underlying instrument of borrow contracts (securities financing transaction) 
   */
  SECURITY_BORROW,

  /**
   * Position, Holding on the same underlying instrument of lending security contracts (securities financing transaction) 
   */
  SECURITY_LOAN,

  /**
   * Position, Holding of type Buy and sell back (securities financing transaction) 
   */
  BUY_SELL_BACK,

  /**
   * Position, Holding of type Sell and buy back (securities financing transaction) 
   */
  SELL_BUY_BACK,

  /**
   * Position, Holding of type Repurchase agreement (securities financing transaction) 
   */
  REPO,

  /**
   * Position, Holding of type Reverse repurchase agreement (securities financing transaction) 
   */
  REVERSE_REPO,

  /**
   * Position, Holding of type Repurchase agreement on security coming from received security as collateral (collateral_in) 
   */
  REPO_COLLATERAL,

  /**
   * Position, Holding of type received security as collateral 
   */
  COLLATERAL_IN,

  /**
   * Position, Holding of type payed security as collateral 
   */
  COLLATERAL_OUT,

  /**
   * Position, Holding on instrument that is registered 
   */
  REGISTERED,

  /**
   * Position, Holding on pledged securities 
   */
  PLEDGED,

  /**
   * Position, Holding on securities in primary market 
   */
  PRIMARY,

  /**
   * Position on pending order 
   */
  PENDING_ORDER,

  /**
   * Unknown position cash 
   */
  UNKNOWN,

}