package com.amundi.medco.poc.jackson.openaml.list;

import com.amundi.medco.poc.model.openaml.component.LVFrequency;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LVFrequencyDeserializer extends StdDeserializer<LVFrequency> {

    private static final Map<LVFrequency, List<String>> OVERRIDE_VALUES;

    static {
        OVERRIDE_VALUES = new HashMap<>();
        OVERRIDE_VALUES.put(LVFrequency.DAILY, Arrays.asList("DAY"));
        OVERRIDE_VALUES.put(LVFrequency.WEEKLY, Arrays.asList("WEEK"));
        OVERRIDE_VALUES.put(LVFrequency.MONTHLY, Arrays.asList("MONTH"));
        OVERRIDE_VALUES.put(LVFrequency.EVERY_5_YEARS, Arrays.asList("5Y"));
        OVERRIDE_VALUES.put(LVFrequency.BIANNUAL, Arrays.asList("YEAR2"));
    }

    public LVFrequencyDeserializer() {
        this(null);
    }

    public LVFrequencyDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LVFrequency deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        String strValue = p.getText();
        for (LVFrequency lvFrequency : LVFrequency.values()) {
            if (lvFrequency.toString().equals(strValue)) {
                return lvFrequency;
            }
        }
        for(Map.Entry<LVFrequency, List<String>> entry: OVERRIDE_VALUES.entrySet()) {
            if(entry.getValue().contains(strValue)) {
                return entry.getKey();
            }
        }
        throw new InvalidFormatException(p, String.format("Unable to map %s as LVFrequency", strValue), strValue, LVFrequency.class);
    }
}
