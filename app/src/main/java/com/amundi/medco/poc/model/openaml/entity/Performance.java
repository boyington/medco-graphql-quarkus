package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="performance", namespace="com.amundi.tech.openaml.entity")
public class Performance extends AnalyticsValue {

    /**
     * Net performance, rate of return, yield
     */
    private BigDecimal netReturn;

    /**
     * Gross performance, rate of return, yield
     */
    private BigDecimal grossReturn;

    /**
     * Type of netReturn, grossReturn, for example: OPEN, CLOSE, OFFER_TO_BID, etc.
     */
    private LVPriceType returnType;

}