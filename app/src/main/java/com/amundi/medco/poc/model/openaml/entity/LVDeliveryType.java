package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVDeliveryType {

  /**
   * Cash settlement if derivative 
   */
  CASH,

  /**
   * Physical delivery if derivative 
   */
  PHYSICAL,

  /**
   * Cash or Physical delivery if derivative 
   */
  BOTH,

  /**
   * Unknown delivery type 
   */
  UNKNOWN,

}