package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVLocationType {

  /**
   * Country of business 
   */
  BUSINESS,

  /**
   * Country of headoffices 
   */
  HEADQUARTERS,

  /**
   * Incorporation country, country of domicile 
   */
  INCORPORATION,

  /**
   * Legal country 
   */
  REGISTERED,

  /**
   * Tax residential location or country 
   */
  TAX,

  /**
   * Risk country 
   */
  RISK,

}