package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVUnit {

  /**
   * The value is expressed per unit, or per share 
   */
  UNIT,

  /**
   * Also known as ParValue, this unit is a price expressed in percentage of face value as decimal, e.g. 99.95 or 101.5. The decimal value is relative to basis 100. 
   */
  PERCENT,

  /**
   * Basis points, e.g. 4 means 4 bp or 0.04% 
   */
  BASIS_POINTS,

  /**
   * A rate, e.g. 0.015 is a rate of 1.5% 
   */
  RATE,

  /**
   * Unit is an annualized rate expressed in percentage, for e.g. a yield of 1.5%, it is often forward-looking 
   */
  YIELD,

  /**
   * Performance yield or rate of return expressed in percentage, it is often backward-looking 
   */
  RETURN,

  /**
   * Unit is a spread, a difference in rates or prices, expressed in percentage, e.g. 0.3 means 0.3% or 30bp 
   */
  SPREAD,

  /**
   * Unit is a spread, a difference in rates or prices, expressed in basis points, e.g. 30 means 30bp 
   */
  SPREAD_BPS,

  /**
   * Dimensionless conversion rate, mainly for FX. 
   */
  EXCHANGE_RATE,

  /**
   * Dimensionless ratio or fraction, for e.g. 0.015 is a ratio = numerator/denominator 
   */
  RATIO,

  /**
   * Unit is a discount under par, eg. 2 if 98. 
   */
  DISCOUNT,

  /**
   * Unit is a premium above par, eg. 2 if 102. 
   */
  PREMIUM,

}