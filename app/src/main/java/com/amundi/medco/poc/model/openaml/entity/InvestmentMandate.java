package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Codification;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="investmentmandate", namespace="com.amundi.tech.openaml.entity")
public class InvestmentMandate extends Entity {

    /**
     * Type of the investment mandate, contract : INDIVIDUAL, ASSOCIATION, COMPANY, or JOINT_ACCOUNT, see LVInvestmentMandateType
     */
    private String type;

    /**
     * Business Offer
     */
    private Codification offer;

    /**
     * Client segment
     */
    private Codification clientSegment;

    /**
     * Structure of the mandate
     */
    private Codification structure;

    /**
     * Product nature : SEGREGATED_ACCOUNT, FUND, PENSION_FUND
     */
    private Codification productNature;

    /**
     * Investment mandate currency code
     */
    private String ccy;

    /**
     * The distributor, bank branch for instance, where the investment mandate has been signed.
     */
    private Party distributor;

    /**
     * Investment mandate signature date
     */
    private LocalDate signatureDate;

    /**
     * Investment mandate end date
     */
    private LocalDate endDate;

    /**
     * Investment mandate : management status
     */
    private String managementStatus;

    /**
     * Investment mandate : start date of the management
     */
    private LocalDate managementStartDate;

    /**
     * Investment mandate risk profile
     */
    private Codification riskProfile;

    /**
     * Investment mandate single subscriber or the main subscriber of the co-subscribers in case of joint account.
     */
    private Party subscriber;

    /**
     * Investment mandate co-subscribers.
     */
    private List<Party> coSubscribers;

    /**
     * Investment mandate contacts
     */
    private List<ContactInfo> contacts;

    /**
     * Investment mandate valuation
     */
    private Quotation valuation;

    /**
     * Performance
     */
    private AnalyticsValue performance;

    /**
     * List of portfolios, the segregated investment accounts, that are part of this Investment mandate
     */
    private List<Portfolio> portfolios;

}