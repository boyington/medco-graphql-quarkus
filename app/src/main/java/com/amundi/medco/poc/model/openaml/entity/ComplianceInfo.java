package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.Comment;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="complianceinfo", namespace="com.amundi.tech.openaml.entity")
public class ComplianceInfo {

    /**
     * Compliance Department in charge of these compliance data
     */
    private String department;

    /**
     * KYC process status
     */
    private String kycStatus;

    /**
     * Comment related to the KYC process
     */
    private Comment kycComment;

    /**
     * Legal entity: True if there is a PEP in the entity (idem  pep existence). If Person: True if Politically exposed person.
     */
    private Boolean pep;

    /**
     * True if the party (person/individual or legal entity) is blacklisted
     */
    private Boolean blacklisted;

    /**
     * True if submission bomb, or anti-personnel mine
     */
    private Boolean basmMap;

    /**
     * True if the person related to a politically exposed person (eg : family relation)
     */
    private Boolean pepAffiliated;

    /**
     * True if the Wolfsberg questionnaire has been completed
     */
    private Boolean amlQuestCompleted;

    /**
     * AML Wolfsberg questionnaire signature date
     */
    private LocalDate amlQuestDate;

    /**
     * True if sensitive activity
     */
    private Boolean sensitiveActv;

}