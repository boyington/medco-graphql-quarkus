package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="country", namespace="com.amundi.tech.openaml.entity")
public class Country extends Entity {

    /**
     * Country type, e.g. RISK, INCORPORATION, HEADQUARTERS, ...
     */
    private LVLocationType type;

    /**
     * Country code ISO 3166-1 alpha-3, or alpha-2
     */
    private String ctry;

    /**
     * ISO2 or ISO3 if the country code is expressed on 2 digit or 3 digit
     */
    private LVCountryCodeIso iso;

    /**
     * Country zone, for example Europe, Asia, etc.
     */
    private String ctryZone;

    /**
     * True if part of the European Union
     */
    private Boolean euMember;

    /**
     * True if country within the European Economic Area
     */
    private Boolean eeaMember;

    /**
     * True if the country is part of the Eurozone
     */
    private Boolean eurozone;

    /**
     * True if country is part of the Organisation for Economic Co-operation and Development (OECD)
     */
    private Boolean oecdMember;

    /**
     * True if emerging country
     */
    private Boolean emerging;

    /**
     * Currency code of this country
     */
    private String ccy;

    /**
     * Country code ISO 3166-1-alpha-2
     */
    private String ctryIso2;

    /**
     * List of custom geographic area or zone
     */
    private List<Location> geoAreas;

}