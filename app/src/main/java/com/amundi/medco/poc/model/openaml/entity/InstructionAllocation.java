package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="instructionallocation", namespace="com.amundi.tech.openaml.entity")
public class InstructionAllocation {

    /**
     * Type of instruction allocation
     */
    private String type;

    /**
     * Optional status of the instruction allocation
     */
    private String status;

    /**
     * Weight of the allocation : expressed as a percentage
     */
    private BigDecimal weight;

    /**
     * Allocation amount, if weight is filled, the amt is the estimated amount
     */
    private BigDecimal amt;

    /**
     * Currency code of the amt
     */
    private String ccy;

    /**
     * Portfolio associated to this instruction
     */
    private Portfolio portfolio;

}