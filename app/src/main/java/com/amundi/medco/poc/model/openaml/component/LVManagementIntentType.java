package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVManagementIntentType {

  /**
   * Available for sale assets 
   */
  AFS,

  /**
   * Assets recorded  at fair value 
   */
  FVO,

  /**
   * Held for trading assets 
   */
  HFT,

  /**
   * Held to maturity assets 
   */
  HTM,

  /**
   * Loans and receivables 
   */
  LRE,

  /**
   * Default lines 
   */
  NOR,

  /**
   * Not used 
   */
  NUD,

  /**
   * Exploitation of ground under construction 
   */
  TCE,

  /**
   * Rented investment property under construction 
   */
  TPL,

  /**
   * Not rented investment property under construction 
   */
  TPN,

}