package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVOrderSign {

  /**
   * BUY order 
   */
  BUY,

  /**
   * SELL order 
   */
  SELL,

  /**
   * COMPRESSION order 
   */
  COMPRESSION,

  /**
   * COMPRESSION_INCREASE order : to increase a position on a cleared CDS 
   */
  COMPRESSION_INCREASE,

  /**
   * Unwind of non cleared OTC 
   */
  UNWIND,

}