package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="account", namespace="com.amundi.tech.openaml.component")
public class Account {

    /**
     * Optional account type
     */
    private String type;

    /**
     * Optional account structure
     */
    private String structure;

    /**
     * Optional institution id, such as BIC code of the bank, custodian, or similar institution that owns this account
     */
    private Identifier institutionId;

    /**
     * Account name or label
     */
    private String accountName;

    /**
     * Account number
     */
    private String number;

    /**
     * Agency code
     */
    private String agencyCode;

    /**
     * Cash account : currency code
     */
    private String ccy;

    /**
     * Optional sub-institution id, such as BIC code of the bank, custodian or similar institution that owns this sub-account
     */
    private Identifier subInstitutionId;

    /**
     * Sub-account name or label
     */
    private String subAccountName;

    /**
     * Sub-account number or label
     */
    private String subAccountNumber;

    /**
     * Type of reference account 1
     */
    private String refAccountType1;

    /**
     * Number of reference account 1
     */
    private String refAccountNumber1;

    /**
     * Type of reference account 2
     */
    private String refAccountType2;

    /**
     * Number of reference account 2
     */
    private String refAccountNumber2;

}