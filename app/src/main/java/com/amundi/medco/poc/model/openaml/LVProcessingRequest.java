package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVProcessingRequest {

  /**
   * AnalyticsCalculation object 
   */
  ANALYTICS_CALCULATION,

}