package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="attributedefinition", namespace="com.amundi.tech.openaml.component")
public class AttributeDefinition {

    /**
     * Group name of the attribute
     */
    private String group;

    /**
     * The attribute definition key
     */
    private String code;

    /**
     * Coding scheme of the attribute definition 
     */
    private String codScheme;

    /**
     * Label of this codification
     */
    private String name;

    /**
     * Optional : other name, such as short name
     */
    private String name2;

    /**
     * Optional data linked to the attribute codScheme
     */
    private String lang;

}