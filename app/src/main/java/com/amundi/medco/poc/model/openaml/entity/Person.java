package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.entity.party.PartyRole;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="person", namespace="com.amundi.tech.openaml.entity")
public class Person extends Party {

    /**
     * Person alias/aka.
     */
    private List<PersonName> alias;

    /**
     * Position or function in the company
     */
    private String position;

    /**
     * This person belongs to this company's department.
     */
    private String department;

    /**
     * This person is a company employee and belongs to this company's division
     */
    private String division;

    /**
     * This person is a company employee and belongs to this company's direction
     */
    private String direction;

    /**
     * Name of the occupation or job of a person/individual
     */
    private String profession;

    /**
     * True if acting as professional
     */
    private Boolean actingAsPro;

    /**
     * Roles of the party
     */
    private List<PartyRole> partyRoles;

    /**
     * List of the third-parties held by this person with ownership information (direct owner or beneficial owner)
     */
    private List<PartyLink> ownerOfs;

    /**
     * True if a power of attorney has been signed
     */
    private Boolean powerOfAttorney;

}