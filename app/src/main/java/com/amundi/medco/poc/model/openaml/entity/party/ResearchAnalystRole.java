package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.amundi.medco.poc.model.openaml.entity.Classif;
import com.amundi.medco.poc.model.openaml.entity.Party;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="researchanalystrole", namespace="com.amundi.tech.openaml.entity.party")
public class ResearchAnalystRole extends PartyRole {

    /**
     * List of assets monitored by the research analyst
     */
    private List<Asset> monitoringAssets;

    /**
     * List of sectors monitored by the research analyst
     */
    private List<Classif> monitoringSectors;

    /**
     * List of issuers monitored by the research analyst
     */
    private List<Party> monitoringIssuers;

}