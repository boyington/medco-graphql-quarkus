package com.amundi.medco.poc.model.openaml;

import com.amundi.medco.poc.model.openaml.entity.AnalyticsValue;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="analyticscalculation", namespace="com.amundi.tech.openaml")
public class AnalyticsCalculation extends ProcessingRequest {

    /**
     * Type of the analytics calculation request for 1 or more analytics by asset
     */
    private String type;

    /**
     * Requested calculation date common to all analytics
     */
    private LocalDate calcDate;

    /**
     * Comment of the calculation result or comment to request the calculation
     */
    private String comment;

    /**
     * The asset (security, otc, index, currency) associated to the analytics calculation request
     */
    private Asset asset;

    /**
     * List of analytics to calculate for this asset with type and calcDate
     */
    private List<AnalyticsValue> analyticsValues;

}