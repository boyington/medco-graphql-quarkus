package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="contactinfo", namespace="com.amundi.tech.openaml.entity")
public class ContactInfo extends Entity {

    /**
     * Contact first name
     */
    private String firstName;

    /**
     * Contact last name
     */
    private String lastName;

    /**
     * Contact short name
     */
    private String shortName;

    /**
     * Contact acronym
     */
    private String acronym;

    /**
     * Language code ISO 639-1 (alpha 2), for example ko for Korean language or language code ISO 639-2 (alpha 3)
     */
    private String lang;

    /**
     * Type of contact
     */
    private String type;

    /**
     * Company name
     */
    private String company;

    /**
     * This person is a company employee and belongs to this company's direction
     */
    private String direction;

    /**
     * This person is a company employee and belongs to this company's division
     */
    private String division;

    /**
     * This person belongs to this company's department.
     */
    private String department;

    /**
     * This person belongs to this company's department name.
     */
    private String departmentName;

    /**
     * Position or function in the company
     */
    private String position;

    /**
     * Job title, professional headline, Name of the occupation (profession)
     */
    private String jobtitle;

    /**
     * Email address
     */
    private String email;

    /**
     * Business telephone number, standard telephone number
     */
    private String tel;

    /**
     * Direct telephone number
     */
    private String dir;

    /**
     * Mobile phone number
     */
    private String mobile;

    /**
     * Fax number
     */
    private String fax;

    /**
     * URL address of the company Website
     */
    private String website;

    /**
     * The address of where is the contact person
     */
    private Location address;

    /**
     * Mailing address, postal address
     */
    private Location mailingAddress;

    /**
     * Contact degree or level of study
     */
    private String degree;

    /**
     * Contact biography
     */
    private String bio;

    /**
     * Optional List of authorizations
     */
    private List<Identifier> authorizations;

}