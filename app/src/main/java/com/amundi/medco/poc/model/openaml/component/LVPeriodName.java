package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPeriodName {

  /**
   * 1 Day 
   */
  ONE_DAY,

  /**
   * 5 Days 
   */
  FIVE_DAYS,

  /**
   * 30 Days 
   */
  THIRTY_DAYS,

  /**
   * 1 week 
   */
  ONE_WEEK,

  /**
   * 1 month 
   */
  ONE_MONTHS,

  /**
   * 2 months 
   */
  TWO_MONTHS,

  /**
   * 3 months 
   */
  THREE_MONTHS,

  /**
   * 4 months 
   */
  FOUR_MONTHS,

  /**
   * 5 months 
   */
  FIVE_MONTHS,

  /**
   * 6 months 
   */
  SIX_MONTHS,

  /**
   * 7 months 
   */
  SEVEN_MONTHS,

  /**
   * 8 months 
   */
  EIGHT_MONTHS,

  /**
   * 9 months 
   */
  NINE_MONTHS,

  /**
   * 10 months 
   */
  TEN_MONTHS,

  /**
   * 11 months 
   */
  ELEVEN_MONTHS,

  /**
   * 1 year, 12 months 
   */
  ONE_YEAR,

  /**
   * 2 years 
   */
  TWO_YEARS,

  /**
   * 3 years 
   */
  THREE_YEARS,

  /**
   * 5 years 
   */
  FIVE_YEARS,

  /**
   * 10 years 
   */
  TEN_YEARS,

  /**
   * 15 years 
   */
  FIFTEEN_YEARS,

  /**
   * 20 years 
   */
  TWENTY_YEARS,

  /**
   * 30 years 
   */
  THIRTY_YEARS,

}