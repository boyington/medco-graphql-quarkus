package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPartyCodingScheme {

  /**
   * Legal entity identifier 
   */
  LEI,

  /**
   * Bic code 
   */
  BIC,

  /**
   * MIC code 
   */
  MIC,

  /**
   * Bloomberg ID_BB_COMPANY 
   */
  BB_COMPANY,

  /**
   * Bloomberg ID_BB_GLOBAL 
   */
  BB_GLOBAL,

  /**
   * Bloomberg ID_BB_GLOBAL_COMPANY 
   */
  BB_GLOBAL_COMPANY,

  /**
   * Bloomberg corporate ticker code 
   */
  BB_TICKER,

  /**
   * Bloomberg ID_BB_OBLIGOR_GLOBAL 
   */
  BB_OBLIGOR_GLOBAL,

  /**
   * Ultimate ticker 
   */
  EXCHANGE_TICKER,

  /**
   * GIIN Fatca, global intermediary identification number 
   */
  GIIN,

  /**
   * Duns number - Data Universal Numbering System - provided par Dun and Bradstreet 
   */
  DUNS,

  /**
   * In France, RCS(registre du commerce et des societes): licence place and SIREN of the company 
   */
  RCS,

  /**
   * In France, SIREN of the company 
   */
  SIREN,

  /**
   * In France, identification of company institutions: SIREN + 5 digit for the institution 
   */
  SIRET,

  /**
   * Markit RED code - 6 digit 
   */
  REDCODE,

  /**
   * Markit RED pair code - 9 digit 
   */
  RED_PAIRCODE,

  /**
   * Markit ticker 
   */
  MARKIT_TICKER,

  /**
   * Operating MIC code 
   */
  OPERATING_MIC,

  /**
   * SIX Finalim market code 
   */
  FINALIM,

  /**
   * Licence number: example :  regulated authority licence number 
   */
  LICENCE_NUMBER,

  /**
   * Accelus ORG ID 
   */
  ORG_ID,

  /**
   * Individual id. For instance, in France, it is the INSEE number 
   */
  PERSON_ID,

  /**
   * Tax Identification number 
   */
  TIN,

  /**
   * Foreign Tax Identification number, can be used in case of other country tax identification number (partita iva in Italy for instance) 
   */
  FOREIGN_TIN,

  /**
   * User login 
   */
  LOGIN,

  /**
   * CARPID user 
   */
  CARPID,

  /**
   * User Active Directory login 
   */
  AD_LOGIN,

}