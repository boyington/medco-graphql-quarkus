package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCashAccountRecord {

  /**
   * Cash at bank recorded in the cash account or bank account 
   */
  CASH_AT_BANK,

  /**
   * Forward cash (sum of all future transaction with settlement date in the future) without provision. 
   */
  CASH_FORWARD,

  /**
   * Cash provisions on fees or charges 
   */
  PROVISION,

  /**
   * Cash used as a guarantee by another party, e.g. deposit cash of futures 
   */
  DEPOSIT,

  /**
   * Collateral cash: cash that is the sum of cash movements linked to collateral in or collateral out, such as initial margin, margin call, variable margin 
   */
  COLLATERAL,

  /**
   * Cash: margin call 
   */
  MARGIN_CALL,

  /**
   * Cash: master account 
   */
  MASTER_ACCOUNT,

  /**
   * Cash: provision on management fees 
   */
  PROVISION_MGMT_FEES,

  /**
   * Cash coupon / dividend 
   */
  COUPON,

}