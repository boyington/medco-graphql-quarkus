package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.entity.PartyLink;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="clientrole", namespace="com.amundi.tech.openaml.entity.party")
public class ClientRole extends PartyRole {

    /**
     * Client role type, see LVClientRoleType
     */
    private String type;

    /**
     * Confidentiality letter
     */
    private String confidentiality;

    /**
     * The custody account keeper used in the context of corporate savings plans
     */
    private String custodyAccountKeeper;

    /**
     * External id identifier
     */
    private Identifier externalId;

    /**
     * List of relations with others third-parties
     */
    private List<PartyLink> relations;

}