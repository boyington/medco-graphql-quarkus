package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVOrderRequestType {

  /**
   * Order will be traded following default process 
   */
  CREATION,

  /**
   * Order is in confirmation mode (deal done, need to be input to MCE) 
   */
  CONFIRMATION,

  /**
   * Order is traded by Fund manager 
   */
  DIRECT,

  /**
   * Order is a Contract for difference 
   */
  CFD,

  /**
   * Program trade 
   */
  PROGRAM,

  /**
   * Order is an arbitrage operation 
   */
  ARBITRAGE,

  /**
   * Exchange of futures for physicals 
   */
  EFP,

}