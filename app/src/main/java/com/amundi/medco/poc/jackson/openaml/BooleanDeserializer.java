package com.amundi.medco.poc.jackson.openaml;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class BooleanDeserializer extends StdDeserializer<Boolean> {

    private static final List<String> TRUE_VALUES = Arrays.asList("true", "1");
    private static final List<String> FALSE_VALUES = Arrays.asList("false", "0");
    public BooleanDeserializer() {
        this(null);
    }

    public BooleanDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Boolean deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        String strValue = p.getText();
        if(TRUE_VALUES.contains(strValue)) {
            return true;
        } else if(FALSE_VALUES.contains(strValue)) {
            return false;
        }
        log.debug("Null Boolean: " +strValue);
        return null;
    }
}
