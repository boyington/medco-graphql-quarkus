package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVOptionType {

  /**
   * Buy Call : Buy of a buy option 
   */
  CALL,

  /**
   * Buy Put : Buy of a sell option 
   */
  PUT,

  /**
   * Buy Straddle : Buy Call + Buy Put (same underlying, same strike, same maturity) 
   */
  STRADDLE,

  /**
   * Buy Collar : (Buy Call + Sell Put) or (Sell Call + Buy Put) (same underlying, different strike, same maturity) 
   */
  COLLAR,

  /**
   * Buy Strangle : Buy Call + Buy Put (same underlying, different strike, same maturity) 
   */
  STRANGLE,

  /**
   * Buy Call spread : Buy Call + Sell Call (same underlying, different strike, same maturity) 
   */
  CALL_SPREAD,

  /**
   * Buy Put spread : Buy Put + Sell Put (same underlying, different strike, same maturity) 
   */
  PUT_SPREAD,

  /**
   * Chooser option type 
   */
  CHOOSER,

  /**
   * Other option type if not defined 
   */
  OTHER,

}