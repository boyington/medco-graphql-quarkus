package com.amundi.medco.poc.jackson.openaml.list;

import com.amundi.medco.poc.model.openaml.entity.LVAssetGroup;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class LVAssetGroupDeserializer extends StdDeserializer<LVAssetGroup> {

    public LVAssetGroupDeserializer() {
        this(null);
    }

    public LVAssetGroupDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LVAssetGroup deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        //@TODO
        return null;
    }
}
