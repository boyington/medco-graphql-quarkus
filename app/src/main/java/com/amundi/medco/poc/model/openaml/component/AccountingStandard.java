package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="accountingstandard", namespace="com.amundi.tech.openaml.component")
public class AccountingStandard {

    /**
     * Type of accounting standard : IFRS9, IAS, IFRS(both)
     */
    private LVAccountingStandard type;

    /**
     * Accounting standard name in addition to type
     */
    private String standardName;

    /**
     * IFRS business model : use LVBusinessModel, related to IFRS9
     */
    private String businessModel;

    /**
     * Accounting class (CC), use LVAccountingClass, related to IFRS9
     */
    private String accountingClass;

    /**
     * Management intent, related to IAS39
     */
    private String managementIntent;

}