package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVAssetType {

  /**
   * Equity ordinary 
   */
  EQUITY_ORDINARY,

  /**
   * Equity ADP 
   */
  EQUITY_ADP,

  /**
   * Equity new share 
   */
  EQUITY_NEW_SHARE,

  /**
   * Equity privileged share 
   */
  EQUITY_PRIVILEGED_SHARE,

  /**
   * Equity social share 
   */
  EQUITY_SOCIAL_SHARE,

  /**
   * Equity savings share 
   */
  EQUITY_SAVINGS_SHARE,

  /**
   * Equity : Certificate of investment 
   */
  CERTIFICATE_OF_INVESTMENT,

  /**
   * Equity American Depository Receipt 
   */
  AMERICAN_DEPOSITORY_RECEIPT,

  /**
   * Equity Global Depositary Receipt 
   */
  GLOBAL_DEPOSITARY_RECEIPT,

  /**
   * Equity Bearer Depository Receipt 
   */
  BEARER_DEPOSITORY_RECEIPT,

  /**
   * Equity : Subscription right  
   */
  SUBSCRIPTION_RIGHT,

  /**
   * Equity :  Attribution right  
   */
  ATTRIBUTION_RIGHT,

  /**
   * Equity : Certificate guaranteed value 
   */
  CERTIFICATE_GUARANTEED_VALUE,

  /**
   * Equity : Dividend 
   */
  DIVIDEND,

  /**
   * Equity : Equity linked notes 
   */
  EQUITY_LINKED_NOTES,

  /**
   * Forex / Fx Spot / FX_SPOT : Forex spot 
   */
  FX_SPOT,

  /**
   * Forex / Fx Forward / FX_FORWARD : Forex forward 
   */
  FX_FORWARD,

  /**
   * Forex / Fx Swap / FX_SWAP : Forex swap 
   */
  FX_SWAP,

  /**
   * Forex non-deliverable forward 
   */
  FOREX_NON_DELIVERABLE_FORWARD,

  /**
   * DEPRECATED - Forex / Fx Forward / FX_NDF : Forex non-deliverable forward 
   */
  FX_NDF,

  /**
   * Swaps : Swap on rate 
   */
  SWAP_ON_RATE,

  /**
   * Swaps : Swap on rate forward 
   */
  SWAP_ON_RATE_FORWARD,

  /**
   * Swaps : cross currency swap 
   */
  CROSS_CURRENCY_SWAP,

  /**
   * Cap Floor : cap 
   */
  CAP,

  /**
   * Cap Floor : floor 
   */
  FLOOR,

  /**
   * Performance swap : swap perf non pea 
   */
  SW_PERF_NON_PEA,

  /**
   * Performance swap : swap perf pea 
   */
  SW_PERF_PEA,

  /**
   * Performance swap : swap divid pea 
   */
  SW_DIVID_PEA,

  /**
   * Performance swap : swap perf div pea 
   */
  SW_PERF_DIV_PEA,

  /**
   * Performance swap : swap coupon 
   */
  SW_COUPON,

  /**
   * Performance swap : inflaction swap 
   */
  INFLATION_SWAP,

  /**
   * Performance swap : volatility swap 
   */
  VOLATILITY_SWAP,

  /**
   * Performance swap : Contract for difference 
   */
  CONTRACT_FOR_DIF,

  /**
   * Performance swap : variance swap 
   */
  VARIANCE_SWAP,

  /**
   * Performance swap : swap perf basket 
   */
  SW_PERF_BASKET,

  /**
   * Credit derivative : CDS single name 
   */
  CDS_SINGLE,

  /**
   * Credit derivative : CDS index 
   */
  CDS_BASKET,

  /**
   * Credit derivative : CDS portfolio 
   */
  CDS_PORTFOLIO,

  /**
   * Credit derivative : CDS marketable single name 
   */
  CDS_MARKETABLE_SINGLE,

  /**
   * Credit derivative : CDS marketable basket 
   */
  CDS_MARKETABLE_BASKET,

  /**
   * Credit derivative : CDS marketable portfolio 
   */
  CDS_MARKETABLE_PORTFOLIO,

  /**
   * Credit derivative : credit index ITRAXX, CDX 
   */
  ITRAXX,

  /**
   * Credit derivative : credit index ITRAXX, CDX if tranches 
   */
  ITRAXX_TRANCHED,

  /**
   * Derivative / Credit Default Swap / CRD_CDS : CDS credit default swap, CDS single name 
   */
  CRD_CDS,

  /**
   * Derivative / Credit Default Swap / CRD_INDEX : CDX credit default swap on index 
   */
  CRD_INDEX,

  /**
   * Derivative / Credit Default Swap / CRD_INDEX_TRANCHES :  CDX credit default swap on index tranches 
   */
  CRD_INDEX_TRANCHES,

  /**
   * Derivative / Credit Default Option / CRD_INDEX_OPT : Credit default index option 
   */
  CRD_INDEX_OPT,

  /**
   * Derivative / Interest Rate Swap / IRD_IR_SWAP :  interest rate swap (irs), swap on rate 
   */
  IRD_IR_SWAP,

  /**
   * Derivative / Cross Currency / IRD_CC_SWAP : cross currency swap (ccs) 
   */
  IRD_CC_SWAP,

  /**
   * Derivative / Inflation swap / IRD_INFL_SWAP  
   */
  IRD_INFL_SWAP,

  /**
   * Derivative / Swaption / IRD_SWAPTION : option on swap on rate, irs 
   */
  IRD_SWAPTION,

  /**
   * Derivative / Exotic Swap / IRD_EXOTIC_SWAP 
   */
  IRD_EXOTIC_SWAP,

  /**
   * Derivative / Asset Swap / IRD_ASSET_SWAPS : asset swap (bond + IRS) 
   */
  IRD_ASSET_SWAPS,

  /**
   * Derivative / Interest Rate Swap / IRD_IR_NDS : non deliverable swap (irs, swap on rate) 
   */
  IRD_IR_NDS,

  /**
   * Derivative / Variance swap / EQD_VAR_SWAP 
   */
  EQD_VAR_SWAP,

  /**
   * Derivative / Exotic option / EQD_OTC_OPT : option otc 
   */
  EQD_OTC_OPT,

  /**
   * Derivative / Total return swap / EQD_EQUITY_SWAP : perf Equity swap 
   */
  EQD_EQUITY_SWAP,

  /**
   * Derivative / Total return swap / EQD_NON_OPT_SWAP : perf equity non opt swap 
   */
  EQD_NON_OPT_SWAP,

  /**
   * Derivative / Dividend swap / EQD_DIV_SWAP : Dividend Equity swap 
   */
  EQD_DIV_SWAP,

  /**
   * Derivative / Total return swap / EQD_BASKET_PERF_SWAP : performance swap on basket 
   */
  EQD_BASKET_PERF_SWAP,

  /**
   * Derivative / Total return swap / EQD_SWAP_ETF_EQUITY : Performance swap ETF equity 
   */
  EQD_SWAP_ETF_EQUITY,

  /**
   * Derivative / Total return swap / IRD_TRS_BOND_INDEX : Total return swap on bond index 
   */
  IRD_TRS_BOND_INDEX,

  /**
   * Derivative / Total return swap / IRD_TRS_ON_LOAN : Total return swap on loan 
   */
  IRD_TRS_ON_LOAN,

  /**
   * Derivative / Total return swap / IRD_SWAP_ETF_BOND : Performance swap ETF bond 
   */
  IRD_SWAP_ETF_BOND,

  /**
   * Derivative / Fx Option / FX_VANILLA_OPT : FX option vanilla 
   */
  FX_VANILLA_OPT,

  /**
   * Derivative / Fx Option / FX_NDO : FX option NDO 
   */
  FX_NDO,

  /**
   * Derivative / Fx Option / FX_ONETOUCH_OPT : FX option onetouch 
   */
  FX_ONETOUCH_OPT,

  /**
   * Derivative / Fx Option / FX_DIGITAL_OPT : FX option digital 
   */
  FX_DIGITAL_OPT,

  /**
   * Derivative / Fx Option / FX_KNOCK_IN_OPT : FX option knock in 
   */
  FX_KNOCK_IN_OPT,

  /**
   * Derivative / Fx Option / FX_KNOCK_OUT_OPT : FX option knock out 
   */
  FX_KNOCK_OUT_OPT,

  /**
   * Derivative / Fx Option / FX_KIKO : FX option knock in, knock out 
   */
  FX_KIKO,

  /**
   * Derivative / CapFloor / IRD_CAP_FLOOR  : Cap, Floor, Collar, option on rate 
   */
  IRD_CAP_FLOOR,

  /**
   * Derivative / CapFloor / IRD_INFL_CAP_FLOOR : Inflation cap, floor, collar, option on rate 
   */
  IRD_INFL_CAP_FLOOR,

  /**
   * SFT  / Borrow Lend Securities / SECURITY_LOAN : security or stock lending, Loan securities contract 
   */
  SECURITY_LOAN,

  /**
   * SFT  / Borrow Lend Securities / SECURITY_BORROW : security or stock borrowing, Borrow securities contract 
   */
  SECURITY_BORROW,

  /**
   * SFT  / Repo / REVERSE_REPO : Reverse repo contract (reverse repurchase agreement) 
   */
  REVERSE_REPO,

  /**
   * SFT  / Repo / SPECIFIC_REVERSE_REPO : Specific Reverse repo contract (Specific reverse repurchase agreement) 
   */
  SPECIFIC_REVERSE_REPO,

  /**
   * SFT  / Repo / REPO : Repo contract (repurchase agreement) 
   */
  REPO,

  /**
   * SFT  / Repo / SPECIFIC_REPO : specific repo contract (Specific repurchase agreement) 
   */
  SPECIFIC_REPO,

  /**
   * SFT  / Buy Sell Backs / BUY_SELL_BACK : Buy sell back contract 
   */
  BUY_SELL_BACK,

  /**
   * SFT  / Buy Sell Backs / SELL_BUY_BACK : Sell buy back contract 
   */
  SELL_BUY_BACK,

  /**
   * Derivative / CFD / CFD single stock 
   */
  CFD_SINGLE_STOCK,

  /**
   * Derivative / CFD / CFD basket 
   */
  CFD_BASKET,

  /**
   * Bond : Bank Loan 
   */
  BANK_LOAN,

  /**
   * Fixed Income / Loan / Corporate Loan 
   */
  CORPORATE_LOAN,

  /**
   * Fixed Income / Loan / Real Estate Loan 
   */
  REAL_ESTATE_LOAN,

  /**
   * Fixed Income / Loan / Real Asset Loan 
   */
  REAL_ASSET_LOAN,

  /**
   * Bond : fixed rate bond 
   */
  FIXED_RATE_BOND,

  /**
   * Bond : variable bond 
   */
  VARIABLE_BOND,

  /**
   * Bond : Indexed bond 
   */
  INDEX_BOND,

  /**
   * Bond : Titre participatif 
   */
  TITRE_PARTICIPATIF,

  /**
   * Bond : preferred stock 
   */
  PREFERRED_STOCKS,

  /**
   * Bond : EMTN strutured 
   */
  EMTN_STRUCTURED,

  /**
   * Bond : Loan part notes 
   */
  LOAN_PART_NOTES,

  /**
   * Bond : Asset backed security 
   */
  ASSET_BACK_SECURITY,

  /**
   * Fixed Income / Securitized / ABS : Asset backed security 
   */
  ABS,

  /**
   * Fixed Income / Securitized / MBS_PASSTHROUGH : Mortgage Backed Security pass-through  (MBS) 
   */
  MBS_PASSTHROUGH,

  /**
   * Fixed Income / Securitized / CMBS : Commercial Mortgage Backed Security 
   */
  CMBS,

  /**
   * Bond :  TBA  To be announced 
   */
  TBA,

  /**
   * Fixed Income / Securitized / COVERED : Covered bonds including Pfandbriefe 
   */
  COVERED,

  /**
   * Fixed Income / Securitized / CMO : Collateralized mortgage obligation (CMO) or Real Estate Mortgage Investment Conduit (REMIC) 
   */
  CMO,

  /**
   * Fixed Income / Securitized / CLO : Collateralized loan obligation 
   */
  CLO,

  /**
   * Fixed Income / Securitized / CDO : Collateralized debt obligation 
   */
  CDO,

  /**
   * Bond : SPV_NOTE : Note issued by a special purpose vehicule (SPV) 
   */
  SPV_NOTE,

  /**
   * DEPRECATED - Fixed Income / Structured product / EMTN : Structured EMTN 
   */
  EMTN,

  /**
   * Bond : Yes Note  
   */
  YES_NOTE,

  /**
   * Convertible bond 
   */
  CONVERTIBLE_BOND,

  /**
   * Convertible bond Mutual Fund 
   */
  CONVERT_BOND_MUTUAL_FUND,

  /**
   * Insurance Linked security  : Catastrophe bond 
   */
  CATASTROPHE_BOND,

  /**
   * Insurance Linked security / Collateralized reinsurance for insurers 
   */
  COLLATERALIZED_REINSURANCE_PRIMARY,

  /**
   * Insurance Linked security / Collateralized reinsurance for reinsurers 
   */
  COLLATERALIZED_REINSURANCE_RETRO,

  /**
   * Insurance Linked security / Industry loss warranty (ILW) 
   */
  INDUSTRY_LOSS_WARRANTY,

  /**
   * Insurance Linked security / Quota share such as reinsurance sidecars 
   */
  QUOTA_SHARE,

  /**
   * Index : miscellaneous indexes 
   */
  MISCELLANEOUS_INDEXES,

  /**
   * Index : Stock index / Bond index 
   */
  BOND_INDEX,

  /**
   * Index : Stock index / Equity index 
   */
  EQUITY_INDEX,

  /**
   * Currency, not index 
   */
  CURRENCY_INDEX,

  /**
   * Index : dividend index 
   */
  DIVIDEND_INDEX,

  /**
   * Index : Money market Index 
   */
  MONEY_MARKET_INDEX,

  /**
   * Index : Raw Material index 
   */
  RAW_MATERIAL_INDEX,

  /**
   * Index : Money market Index average 
   */
  MONEY_MARKET_INDEX_AVERAGE,

  /**
   * Index : Money market Index capitalized 
   */
  MONEY_MARKET_INDEX_CAPITALIZED,

  /**
   * Index : Money market Index OIS 
   */
  MONEY_MARKET_INDEX_OIS,

  /**
   * TCN / MMI : Certificate of Deposit 
   */
  CERTIFICAT_DE_DEPOT_NEGOCIABLE,

  /**
   * DEPRECATED - Money Market / Certificate of Deposit / NCD 
   */
  NCD,

  /**
   * Money Market : Term Deposit 
   */
  TERM_DEPOSIT,

  /**
   * Money Market / Certificate of Deposit / IRD_LOAN_DEPOSIT  : same than Term Deposit 
   */
  IRD_LOAN_DEPOSIT,

  /**
   * Money Market : Medium Term Note 
   */
  MEDIUM_TERM_NOTE,

  /**
   * Money Market : Regular Commercial paper 
   */
  COMM_PAPER_REGULAR,

  /**
   * Money Market :  Neu CP (Negotiable european Commercial Paper) 
   */
  NEU_CP,

  /**
   * Money Market / Medium Term Note / Neu MTN (Negotiable european Medium Term Note) 
   */
  NEU_MEDIUM_TERM_NOTE,

  /**
   * Money Market / Medium Term Note / Regular Commercial Paper 
   */
  COMMERCIAL_PAPER,

  /**
   * Money Market / Medium Term Note / Euro Commercial Paper 
   */
  EURO_COMMERCIAL_PAPER,

  /**
   * Money Market : US Commercial Paper 
   */
  US_COMMERCIAL_PAPER,

  /**
   * Money Market : Treasury Note 
   */
  TREASURY_NOTE,

  /**
   * Money Market / Treasury Bill / Treasury Bill 
   */
  TREASURY_BILL,

  /**
   * Money Market: French Treasury Note 
   */
  FRENCH_TREASURY_NOTE,

  /**
   * DEPRECATED - Money Market / Treasury Note / BTF (French Treasury Note) 
   */
  BTF,

  /**
   * Money Market / Treasury Note / BTAN 
   */
  BTAN,

  /**
   * Money Market : Bons Institutions Financieres 
   */
  BONS_INSTITUTIONS_FINANCIERES,

  /**
   * Subscription Right : Equity subscription right 
   */
  EQUITY_SUBSCRIPTION_RIGHT,

  /**
   * Subscription Right : Bond subscription right 
   */
  BOND_SUBSCRIPTION_RIGHT,

  /**
   * DEPRECATED - Derivative / Right /  Bond right 
   */
  BOND_RIGHT,

  /**
   * DEPRECATED - Derivative / Right /  Equity right 
   */
  EQUITY_RIGHT,

  /**
   * DEPRECATED - Equity / Depository Receipt / ADR : American Depository Receipt 
   */
  ADR,

  /**
   * DEPRECATED - Equity / Depository Receipt / GDR : Global Depository Receipt 
   */
  GDR,

  /**
   * DEPRECATED - Equity / Depository Receipt / BDR : Bearer Depository Receipt 
   */
  BDR,

  /**
   * Future :  Future on Equity 
   */
  FUTURE_ON_EQUITY,

  /**
   * Future : Future on Bond 
   */
  FUTURE_ON_BOND,

  /**
   * Future : Future on commodities 
   */
  FUTURE_ON_COMMODITIES,

  /**
   * Future : Future on Currency 
   */
  FUTURE_ON_CURRENCY,

  /**
   * Future : Future on Index 
   */
  FUTURE_ON_INDEX,

  /**
   * Future : Future on Money market 
   */
  FUTURE_ON_MONEY_MARKET,

  /**
   * Future : Future on volatility 
   */
  FUTURE_ON_VOLATILITY,

  /**
   * Future : Future on swap 
   */
  FUTURE_ON_SWAP,

  /**
   * Future : Future on basket 
   */
  FUTURE_ON_BASKET,

  /**
   * Future :  Equity forward 
   */
  EQUITY_FORWARD,

  /**
   * Future : Bond forward 
   */
  BOND_FORWARD,

  /**
   * Derivative / Forward / IRD_BOND_FWD  
   */
  IRD_BOND_FWD,

  /**
   * Derivative / Forward / Forward rate agreement 
   */
  FRA,

  /**
   * Option : swaption 
   */
  SWAPTION,

  /**
   * Option : option otc 
   */
  OPTION_OTC,

  /**
   * Option : option on credit index 
   */
  OPTION_ON_CREDIT_INDEX,

  /**
   * Option : Option on Equity 
   */
  OPTION_ON_EQUITY,

  /**
   * Option : Option on Equity monep 
   */
  OPTION_ON_EQUITY_MONEP,

  /**
   * Option : Option on Bond 
   */
  OPTION_ON_BOND,

  /**
   * Option : Option Real estate 
   */
  OPTION_REAL_ESTATE,

  /**
   * Option : option on currency 
   */
  OPTION_ON_CURRENCY,

  /**
   * Option : Option on Index 
   */
  OPTION_ON_INDEX,

  /**
   * Option : Option on funds 
   */
  OPTION_ON_FUNDS,

  /**
   * Option : Option on Volatility 
   */
  OPTION_ON_VOLATILITY,

  /**
   * Option : Option on equity future 
   */
  OPTION_ON_EQUITY_FUTURE,

  /**
   * Option : Option on bond future 
   */
  OPTION_ON_BOND_FUTURE,

  /**
   * Option : Option on bond future 
   */
  OPTION_ON_COMMODITY_FUTURE,

  /**
   * Option : Option on currency future 
   */
  OPTION_ON_CURRENCY_FUTURE,

  /**
   * Option : Option on index future 
   */
  OPTION_ON_INDEX_FUTURE,

  /**
   * Option : Option on Money market future 
   */
  OPTION_ON_MONEY_MARKET_FUTURE,

  /**
   * Option : Option on future basket 
   */
  OPTION_ON_FUTURE_BASKET,

  /**
   * Derivative / Warrant / Warrant on Equity 
   */
  WARRANT_ON_EQUITY,

  /**
   * Derivative / Warrant / Warrant on Bond 
   */
  WARRANT_ON_BOND,

  /**
   * Warrant : Warrant on market index 
   */
  WARRANT_ON_MARKET_INDEX,

  /**
   * Derivative / Warrant / Warrant on Market index 
   */
  WARRANT_ON_INDEX,

  /**
   * Derivative / Warrant / Warrant on currency 
   */
  WARRANT_ON_CURRENCY,

  /**
   * Derivative / Warrant / Warrant on raw material 
   */
  WARRANT_ON_RAW_MATERIAL,

  /**
   * Derivative / Warrant / Warrant struct 
   */
  WARRANT_STRUCT,

  /**
   * Warrant : Warrant on future 
   */
  WARRANT_ON_FUTURE,

  /**
   * Mutual fund : Mutual Fund EUR Equities 
   */
  MUTUAL_FUND_EUR_EQUITIES,

  /**
   * Mutual fund : Mutual Fund International Equities 
   */
  MUTUAL_FUND_INTL_EQUITIES,

  /**
   * Mutual Fund : Mutual Fund Balanced 
   */
  MUTUAL_FUND_BALANCED,

  /**
   * Mutual Fund : Mutual Fund Euro Convertible Bond  
   */
  MUTUAL_FUND_EURO_CONV_BOND,

  /**
   * Mutual Fund : Mutual Fund International Convertible Bond  
   */
  MUTUAL_FUND_INTL_CONV_BOND,

  /**
   * DEPRECATED - Fund share / Mutual Fund / Mutual Fund Euro Convertible Bond  
   */
  MUTUAL_FUND_EURO_CV_BOND,

  /**
   * Fund share / Mutual Fund / Mutual Fund International Convertible Bond  
   */
  MUTUAL_FUND_INTL_CV_BOND,

  /**
   * Mutual Fund : Mutual Fund Euro Bond 
   */
  MUTUAL_FUND_EURO_BOND,

  /**
   * Mutual Fund : Mutual Fund International Bond 
   */
  MUTUAL_FUND_INTL_BOND,

  /**
   * Mutual Fund : Mutual Fund Money Market 
   */
  MUTUAL_FUND_MONEY_MARKET,

  /**
   * Mutual Fund : FCPE 
   */
  FCPE,

  /**
   * Mutual Fund : ETF and others 
   */
  ETF_AND_OTHERS,

  /**
   * Fund share / Mutual Fund / FCT (Fonds commun de titrisation) 
   */
  FCT,

  /**
   * Fund share / Mutual Fund / Priced Fund, that are not ETF 
   */
  PRICED_FUND,

  /**
   * Mutual Fund : FCPR 
   */
  FCPR,

  /**
   * Mutual Fund : FCPI 
   */
  FCPI,

  /**
   * Mutual Fund : Emerging 
   */
  EMERGING,

  /**
   * Mutual Fund : Hedge Funds 
   */
  HEDGE_FUND,

  /**
   * Mutual Fund : ETF equities  
   */
  ETF_EQUITIES,

  /**
   * Mutual Fund : ETF bonds  
   */
  ETF_BONDS,

  /**
   * Mutual Fund : ETF money market  
   */
  ETF_MM,

  /**
   * Mutual Fund : ETF commodities  
   */
  ETF_COMMODITIES,

  /**
   * DEPRECATED - Fund share / ETF / ETF others  
   */
  ETF_OTHERS,

  /**
   * Mutual Fund : priced fund not ETF 
   */
  PRICED_FD_NOT_ETF,

  /**
   * Mutual Fund : REITS 
   */
  REITS,

  /**
   * Mutual Fund : Managed Account 
   */
  MANAGED_ACCOUNT,

  /**
   * Mutual Fund : Mandates 
   */
  MANDATES,

  /**
   * Mutual Fund : Pocket used in Front 
   */
  POCKET,

  /**
   * Currency 
   */
  CURRENCY,

  /**
   * Currency : cash rglts diff 
   */
  CASH_RGLTS_DIFF,

  /**
   * Currency : cash correct 
   */
  CASH_CORRECT,

  /**
   * Currency : cash provision 
   */
  CASH_PROVISION,

  /**
   * Currency : cash deposit 
   */
  CASH_DEPOSIT,

  /**
   * Misc : Commodity, raw material 
   */
  COMMODITY,

  /**
   * Real estate, building 
   */
  REAL_ESTATE,

  /**
   * Misc : Model 
   */
  MODEL,

  /**
   * Misc : Provision 
   */
  PROVISION,

  /**
   * Misc : Provision 
   */
  PROVISION_ON_GUARANTEED_COMMISSION,

  /**
   * Unknown 
   */
  UNKNOWN,

}