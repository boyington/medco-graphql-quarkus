package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVOrderInvestRational {

  /**
   * Investment rational : MARKET 
   */
  MARKET,

  /**
   * Investment rational : HEDGE 
   */
  HEDGE,

  /**
   * Investment rational : REBAL 
   */
  REBAL,

  /**
   * Investment rational : RISK 
   */
  RISK,

  /**
   * Investment rational : CASH FX 
   */
  CASH_FX,

  /**
   * Investment rational : SR 
   */
  SR,

  /**
   * Investment rational : PNL 
   */
  PNL,

  /**
   * Investment rational : OTHERS 
   */
  OTHERS,

}