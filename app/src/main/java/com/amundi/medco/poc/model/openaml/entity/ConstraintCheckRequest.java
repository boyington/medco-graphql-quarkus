package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="constraintcheckrequest", namespace="com.amundi.tech.openaml.entity")
public class ConstraintCheckRequest extends Entity {

    /**
     * The id of the checked entity
     */
    private Identifier entityId;

    /**
     * Define the perimeter of the rules
     */
    private String constraintSet;

    /**
     * The reference date  of the constraint check, for instance the NAV date if the check is related to portfolio valuation.
     */
    private LocalDate checkRefDate;

    /**
     * The next reference date  of the constraint check, for instance the next NAV date if the check is related to portfolio valuation.
     */
    private LocalDate nextCheckRefDate;

    /**
     * The previous reference date  of the constraint check, for instance the previous NAV date if the check is related to portfolio valuation.
     */
    private LocalDate previousCheckRefDate;

}