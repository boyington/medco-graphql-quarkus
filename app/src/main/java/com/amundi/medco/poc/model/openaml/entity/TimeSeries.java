package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Period;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="timeseries", namespace="com.amundi.tech.openaml.entity")
public class TimeSeries extends Entity {

    /**
     * The type of the Time Series, see LVTimeSeries
     */
    private String type;

    /**
     * Description of the time period that defined this time series
     */
    private Period period;

    /**
     * The number of elements of this series, i.e. the size of quotations
     */
    private Integer size;

    /**
     * The list of quotations
     */
    private List<Quotation> quotations;

}