package com.amundi.medco.poc.jackson.openaml.list;

import com.amundi.medco.poc.model.openaml.LVUnit;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class LVUnitDeserializer extends StdDeserializer<LVUnit> {

    private static final List<String> PERCENT_VALUES = Arrays.asList("%", "P");
    private static final List<String> UNIT_VALUES = Arrays.asList("PIECE/UNIT", "PC");
    public LVUnitDeserializer() {
        this(null);
    }

    public LVUnitDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LVUnit deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        String strValue = p.getText();
        for(LVUnit lvUnit: LVUnit.values()) {
            if(lvUnit.toString().equals(strValue)) {
                return lvUnit;
            }
        }
        if(PERCENT_VALUES.contains(strValue)) {
            return LVUnit.PERCENT;
        }
        if(UNIT_VALUES.contains(strValue)) {
            return LVUnit.UNIT;
        }
        throw new InvalidFormatException(p, String.format("Unable to convert %s to LVUnit", strValue), strValue, LVUnit.class);
    }
}
