package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Amounts;
import com.amundi.medco.poc.model.openaml.component.Period;
import com.amundi.medco.poc.model.openaml.entity.positionkeeping.Movement;
import com.amundi.medco.poc.model.openaml.entity.positionkeeping.Transaction;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="portfolioinventory", namespace="com.amundi.tech.openaml.entity")
public class PortfolioInventory extends Entity {

    /**
     * Structure of the positions or the statement.
     */
    private String type;

    /**
     * Optional subtype of the portfolio inventory
     */
    private String subtype;

    /**
     * Statement identifier number if statement
     */
    private Integer statementNumber;

    /**
     * Sequence number (page number) if more than 1 and same statement number
     */
    private Integer sequenceNumber;

    /**
     * Statement validation status, see LVValidationStatus
     */
    private String validationStatus;

    /**
     * Statement reconciliation rate
     */
    private BigDecimal reconciliationRate;

    /**
     * Producer of this inventory
     */
    private Party producer;

    /**
     * Consumer of this inventory
     */
    private Party consumer;

    /**
     * Date of the positions, or date of the statement
     */
    private LocalDate date;

    /**
     * Date of the positions, or date of the statement
     */
    private LocalDate inventoryDate;

    /**
     * Definition of the time period : start date, end date for the movements or transactions. Use this to add a frequency
     */
    private Period inventoryPeriod;

    /**
     * If statement, this is the statement currency code
     */
    private String ccy;

    /**
     * Portfolio related to the positions, the movements or the transactions
     */
    private Portfolio portfolio;

    /**
     * Benchmark related to the positions
     */
    private Benchmark benchmark;

    /**
     * Party related to the positions
     */
    private Party party;

    /**
     * Asset related to the positions, for instance index
     */
    private Asset asset;

    /**
     * Expected return of the portfolio model
     */
    private AnalyticsValue expectedReturn;

    /**
     * Specifies the minimal position lifecycle step from which the position is part of the portfolio inventory. For instance if ORDERED, the portfolio positions will be composed of holdings and pending orders.  The ordered lifecycle steps are SIMULATED, ORDERED, ALLOCATED, CONFIRMED, BOOKED, SETTLED.
     */
    private LVPosTransLifecycle positionLifecycle;

    /**
     * Specifies the type of balances on which the holding inventory is prepared (contractual, settled, traded)
     */
    private LVBasisDateMode basisDateMode;

    /**
     * Definition of the portfolio inventory
     */
    private InventoryDefinition inventoryDefinition;

    /**
     * Global pricing policy : valuation method, etc.
     */
    private PricingPolicy valuationContext;

    /**
     * Net Book Value (total net book value at portfolio inventory level). Net book value (NBV) is the book value less amortisations and depreciations, also known as depreciation costs 
     */
    private Amounts netBookValue;

    /**
     * Market value (sum of positions MTM or accountant value) and price in statement currency (portfolio currency) at inventory level
     */
    private Amounts marketValue;

    /**
     * Optional market value (sum of positions MTM or accountant value) and price in alternative currency at portfolio inventory level
     */
    private Amounts altMarketValue;

    /**
     * Unrealised Profit and loss amount in portfolio inventory currency
     */
    private BigDecimal unrealisedPnlAmt;

    /**
     * Realised Profit and loss amount in portfolio inventory currency
     */
    private BigDecimal realisedPnlAmt;

    /**
     * Portfolio positions, its holdings in a list, or it can contain a tree structure of positions.
     */
    private List<Position> positionBreakdowns;

    /**
     * Portfolio positions, its holdings in a list, or it can contain a tree structure of positions.
     */
    private List<Position> positions;

    /**
     * Movements related to the portfolio positions
     */
    private List<Movement> movements;

    /**
     * Optional transactions of this inventory
     */
    private List<Transaction> transactions;

}