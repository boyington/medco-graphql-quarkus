package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="personname", namespace="com.amundi.tech.openaml.entity")
public class PersonName {

    /**
     * Language code ISO 639-1 (alpha 2), for example ko for Korean language or language code ISO 639-2 (alpha 3)
     */
    private String lang;

    /**
     * Title or prefix such as M., Mrs, Dr., etc.
     */
    private String title;

    /**
     * Person first name, or given name
     */
    private String firstName;

    /**
     * Person middle name expressed in the language code
     */
    private String middleName;

    /**
     * Person last name, surname, family name
     */
    private String lastName;

    /**
     * Suffix associated to a person name, e.g. Jr=Junior, Sr=Senior, I, II, III, ...
     */
    private String suffix;

}