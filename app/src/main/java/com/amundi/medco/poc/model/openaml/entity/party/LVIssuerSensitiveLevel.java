package com.amundi.medco.poc.model.openaml.entity.party;

/**
 * undefined
 */
public enum LVIssuerSensitiveLevel {

  /**
   * Not sensitive 
   */
  NONE,

  /**
   * Sensitive level 
   */
  SENSITIVE,

  /**
   * Very sensitive level 
   */
  VERY_SENSITIVE,

}