package com.amundi.medco.poc.controller;

import com.amundi.medco.poc.model.Assets;
import com.amundi.medco.poc.model.Criterias;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.amundi.medco.poc.service.AssetService;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

import javax.inject.Inject;

@GraphQLApi
public class DataGraphQLResource {

    @Inject
    AssetService assetService;

    @Query("asset")
    @Description("asset")
    public Asset asset(@Name("id") @Description("asset id") String assetId) {
        return assetService.asset(assetId);
    }

    @Query("assets")
    @Description("assets with criterias")
    public Assets assetsWithCriterias(@Name("criterias") @Description("criterias") Criterias criterias) {
        return assetService.assets(criterias);
    }

}