package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="leadrole", namespace="com.amundi.tech.openaml.entity.party")
public class LeadRole extends PartyRole {

}