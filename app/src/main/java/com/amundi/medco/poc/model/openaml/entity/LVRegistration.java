package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVRegistration {

  /**
   * Legal registration 
   */
  LEGAL,

  /**
   * LEI registration 
   */
  LEI,

}