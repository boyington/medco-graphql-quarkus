package com.amundi.medco.poc.jackson.date;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

@Slf4j
public class CustomLocalDateDeserializer extends StdDeserializer<LocalDate> {

    private static final DateTimeFormatter simpleDate = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private Pattern simpleDatePattern = Pattern.compile("\\d\\d/\\d\\d/\\d\\d\\d\\d");

    public CustomLocalDateDeserializer() {
        this(null);
    }

    public CustomLocalDateDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public LocalDate deserialize(JsonParser p, DeserializationContext deserializationContext) throws IOException, JacksonException {
        if(simpleDatePattern.matcher(p.getText()).matches()) {
            return LocalDate.parse(p.getText(), simpleDate);
        }
        return LocalDate.parse(p.getText(), DateTimeFormatter.ISO_DATE);
    }
}