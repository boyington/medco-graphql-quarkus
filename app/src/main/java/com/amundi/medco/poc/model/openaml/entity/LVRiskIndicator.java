package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVRiskIndicator {

  /**
   * Alpha 
   */
  ALPHA,

  /**
   * Alpha calculated by Jensen method 
   */
  JENSEN_ALPHA,

  /**
   * Beta 
   */
  BETA,

  /**
   * Beta bull : positive beta 
   */
  BETA_BULL,

  /**
   * Beta bear : negative beta 
   */
  BETA_BEAR,

  /**
   * Capture market down ratio 
   */
  CAPTURE_MARKET_DOWN,

  /**
   * Capture market up ratio 
   */
  CAPTURE_MARKET_UP,

  /**
   * Correlation coefficient 
   */
  CORRELATION,

  /**
   * Downside risk ratio 
   */
  DOWNSIDE_RISK_RATIO,

  /**
   * Information ratio 
   */
  INFORMATION_RATIO,

  /**
   * Modified information ratio 
   */
  MODIFIED_INFORMATION_RATIO,

  /**
   * undefined 
   */
  R_SQUARED,

  /**
   * Sharp ratio 
   */
  SHARP_RATIO,

  /**
   * Sortino ration, 
   */
  SORTINO_RATIO,

  /**
   * Tracking error 
   */
  TE,

  /**
   * Treynor ratio 
   */
  TREYNOR_RATIO,

  /**
   * Downside Risk (Volatility) 
   */
  DOWNSIDE_RISK_VOLATILITY,

  /**
   * Upside Risk (Volatility) 
   */
  UPSIDE_RISK_VOLATILITY,

  /**
   * Volatility 
   */
  VOLATILITY,

  /**
   * Value at risk 
   */
  VAR,

  /**
   * Portfolio turnover 
   */
  TURNOVER,

}