package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="comment", namespace="com.amundi.tech.openaml.component")
public class Comment {

    /**
     * Language code ISO 639-1 (alpha 2), for example ko for Korean language, or language code ISO 639-2 (alpha 3)
     */
    private String lang;

    /**
     * The content type of the comment text, for instance html
     */
    private String contentType;

    /**
     * Comment code
     */
    private String code;

    /**
     * Comment text or description
     */
    private String text;

    /**
     * Comment date
     */
    private LocalDate date;

    /**
     * User name, login or user id
     */
    private String createdBy;

}