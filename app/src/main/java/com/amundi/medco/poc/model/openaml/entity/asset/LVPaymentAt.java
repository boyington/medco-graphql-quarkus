package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVPaymentAt {

  /**
   * Upfront payment 
   */
  UPFRONT,

  /**
   * Payment in arrears 
   */
  IN_ARREARS,

  /**
   * Option : The settlement occurs at hit time when the underlying price has hit the barrier level 
   */
  HIT,

  /**
   * Option : settlement occurs at expiration 
   */
  EXPIRY,

}