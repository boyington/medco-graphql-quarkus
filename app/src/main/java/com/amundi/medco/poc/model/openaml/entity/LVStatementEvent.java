package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVStatementEvent {

  /**
   * Statement is created, no validation 
   */
  CREATE,

  /**
   * Statement data validated by fund manager 
   */
  VALIDATE,

  /**
   * Statement data pre-validated, i.e. validated by fund administrator/accountant, but not yet validated by fund manager 
   */
  PRE_VALIDATE,

  /**
   * Statement is modified 
   */
  MODIFY,

}