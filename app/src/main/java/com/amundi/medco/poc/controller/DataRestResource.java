package com.amundi.medco.poc.controller;

import com.amundi.medco.poc.model.Assets;
import com.amundi.medco.poc.model.Criterias;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.amundi.medco.poc.service.AssetService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/data")
public class DataRestResource {

    @Inject
    AssetService assetService;

    @GET
    @Path("/assets/all")
    public Set<String> all() {
        return assetService.ids();
    }

    @GET
    @Path("/asset/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Asset asset(@PathParam("id") String id) {
        return assetService.asset(id);
    }


    @POST
    @Path("/assets")
    @Produces(MediaType.APPLICATION_JSON)
    public Assets assets(Criterias criterias) {
        return assetService.assets(criterias);
    }
}