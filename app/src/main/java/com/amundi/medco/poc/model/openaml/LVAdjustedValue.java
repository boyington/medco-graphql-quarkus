package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVAdjustedValue {

  /**
   * Price value has been adjusted by a factor or ratio such as inflation ratio 
   */
  PRICE_RATIO,

  /**
   * Ratio for hedging 
   */
  HEDGED_TOTAL_RETURN,

  /**
   * Offer to bid NAV per share: adjusted net return with entrance/exit fees 
   */
  OFFER_TO_BID,

  /**
   * No adjustment 
   */
  NONE,

}