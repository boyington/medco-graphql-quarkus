package com.amundi.medco.poc.model.openaml.entity.analytic.rating;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Amount;
import com.amundi.medco.poc.model.openaml.entity.Classif;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="carbondata", namespace="com.amundi.tech.openaml.entity.analytic.rating")
public class CarbonData extends Entity {

    /**
     * undefined
     */
    private String year;

    /**
     * Legal entity value (EV), including capital and debt
     */
    private Amount companyValue;

    /**
     * undefined
     */
    private String yearCO2;

    /**
     * Entity income in EUR
     */
    private Amount income;

    /**
     * Entity income in USD
     */
    private Amount incomeUSD;

    /**
     * Tons of carbon scope1, carbon footprint scope1
     */
    private BigDecimal carbonFootprintScope1;

    /**
     * Tons of carbon scope2
     */
    private BigDecimal carbonFootprintScope2;

    /**
     * Tons of carbon scope3
     */
    private BigDecimal carbonFootprintScope3;

    /**
     * Tons of carbon scope3 at first level
     */
    private BigDecimal carbonFootprintScope3First;

    /**
     * Indirect tons of carbon from suppliers at first level
     */
    private BigDecimal carbonFootprintIndirect;

    /**
     * Direct and indirect First Tier Indirect (tonnes CO2e)
     */
    private BigDecimal carbonFootprintDirectAndIndirect;

    /**
     * Carbon intensity scope1
     */
    private BigDecimal carbonIntensityScope1;

    /**
     * Carbon intensity scope2
     */
    private BigDecimal carbonIntensityScope2;

    /**
     * Carbon intensity scope3
     */
    private BigDecimal carbonIntensityScope3;

    /**
     * Carbon intensity indirect
     */
    private BigDecimal carbonIntensityIndirect;

    /**
     * Carbon intensity direct and first Tier Indirect (tonnes CO2e/USD mn) 
     */
    private BigDecimal carbonIntensityDirectAndIndirect;

    /**
     * year carbon reserve
     */
    private String yearCO2Reserve;

    /**
     * total carbon reserve
     */
    private BigDecimal totalCO2Reserve;

    /**
     * Carbon intensity scope1 reserve
     */
    private BigDecimal carbonIntensityScope1Reserve;

    /**
     * Carbon intensity scope2 reserve
     */
    private BigDecimal carbonIntensityScope2Reserve;

    /**
     * Carbon intensity scope3 reserve
     */
    private BigDecimal carbonIntensityScope3Reserve;

    /**
     * Revenue (USD mn)
     */
    private BigDecimal carbonRevenue;

    /**
     * Capital expenditure (CAPEX) coal  (mn USD)
     */
    private BigDecimal capexCoal;

    /**
     * Carbon sector, for instance Trucost sector
     */
    private Classif sector;

    /**
     * Trucost Sector Revenue Percentage (%)
     */
    private BigDecimal sectorRevenue;

    /**
     * Thermal Coal Mining Sector Revenues (%)
     */
    private BigDecimal thermalCoalRevenues;

    /**
     * Metallurgical Coal Mining Sector Revenues (%)
     */
    private BigDecimal metallurgicalCoalRevenues;

    /**
     * Other Coal Mining Sector Revenues (%)
     */
    private BigDecimal otherCoalRevenues;

    /**
     * undefined
     */
    private String yearCO2Msci;

    /**
     * Water consumption
     */
    private BigDecimal water;

    /**
     * undefined
     */
    private BigDecimal lostTimeIncidentRate;

    /**
     * Percentage of women in board
     */
    private BigDecimal pctWomenBoard;

    /**
     * undefined
     */
    private Boolean combinedCEOChairman;

    /**
     * Percentage of bord independence
     */
    private BigDecimal pctBoardIndepend;

    /**
     * Percentage of labor represent
     */
    private BigDecimal pctLaborRepresent;

    /**
     * Auditor under pay
     */
    private Boolean auditorUnderPay;

}