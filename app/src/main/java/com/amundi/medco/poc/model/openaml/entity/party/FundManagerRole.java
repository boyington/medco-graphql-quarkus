package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="fundmanagerrole", namespace="com.amundi.tech.openaml.entity.party")
public class FundManagerRole extends PartyRole {

    /**
     * Fund manager type: principal, backup, additional
     */
    private LVFundManagerType type;

    /**
     * List of portfolio identifiers for which this fund manager is principal manager, backup manager, or additional manager
     */
    private List<Identifier> portfolioIds;

}