package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="classif", namespace="com.amundi.tech.openaml.entity")
public class Classif extends Entity {

    /**
     * Category of classification: by default this is an asset classification, otherwise you need to indicate SECTOR, ACTIVITY, etc. see LVClassifCategory
     */
    private String category;

    /**
     * Type of classification: BB_INDUSTRY_SECTOR, etc...
     */
    private String type;

    /**
     * Code of the element in the classification, e.g. sector code
     */
    private String code;

    /**
     * Level in the classification
     */
    private String level;

    /**
     * Parent code of the classification element
     */
    private String parentCode;

    /**
     * Parent classification element
     */
    private Classif parent;

    /**
     * List of elements that defined the classification
     */
    private List<Classif> elements;

}