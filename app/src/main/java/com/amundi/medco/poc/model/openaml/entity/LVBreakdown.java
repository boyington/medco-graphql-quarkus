package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVBreakdown {

  /**
   * Break to aggregate by asset 
   */
  ASSET,

  /**
   * Break to aggregate by currency 
   */
  CURRENCY,

  /**
   * Break to aggregate by country 
   */
  COUNTRY,

  /**
   * Break to aggregate by portfolio 
   */
  PORTFOLIO,

  /**
   * Break to aggregate by pocket 
   */
  POCKET,

  /**
   * Break to aggregate by holding type 
   */
  HOLDING_TYPE,

  /**
   * Break to aggregate by transaction type 
   */
  TRANS_TYPE,

  /**
   * Break to aggregate by movement type 
   */
  MVT_TYPE,

  /**
   * Break to aggregate by geographic area 
   */
  GEO_AREA,

  /**
   * Break to aggregate by sector 
   */
  SECTOR,

  /**
   * Break to aggregate by risk process 
   */
  RISK_PROCESS,

  /**
   * Break to aggregate by rating 
   */
  RATING,

  /**
   * Break to aggregate by maturity date 
   */
  MATURITY_DATE,

  /**
   * Break to aggregate by next coupon date 
   */
  NEXT_COUPON_DATE,

  /**
   * Break to aggregate by average life 
   */
  AVERAGE_LIFE,

  /**
   * Break to aggregate by weighted average maturity 
   */
  WAM,

  /**
   * Break to aggregate by rate type 
   */
  RATE_TYPE,

  /**
   * Break to aggregate by pillar 
   */
  PILLAR,

  /**
   * Break to aggregate by bucket or segment 
   */
  BUCKET,

  /**
   * Break to aggregate by yield 
   */
  YIELD,

}