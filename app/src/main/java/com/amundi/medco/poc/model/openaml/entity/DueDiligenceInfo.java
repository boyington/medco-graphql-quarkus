package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.entity.analytic.Rating;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="duediligenceinfo", namespace="com.amundi.tech.openaml.entity")
public class DueDiligenceInfo {

    /**
     * Last review date
     */
    private LocalDate reviewDate;

    /**
     * Next review date
     */
    private LocalDate nextReviewDate;

    /**
     * True if the party depends on a regulated authority
     */
    private Boolean regulated;

    /**
     * Regulated authority with residenceCountry, licence number in ids, status
     */
    private Party regulatedAuthority;

    /**
     * Regulatory expiry date
     */
    private LocalDate regulatedExpiryDate;

    /**
     * status of the party as defined by regulated authority
     */
    private String regulatedPartyStatus;

    /**
     * Risk profil or risk level: low risk, medium risk, high risk
     */
    private String riskProfil;

    /**
     * Due diligence score calculated from party parameters
     */
    private Rating score;

    /**
     * Subject to sanctions
     */
    private Boolean underSanctions;

    /**
     * Detail of underSanctions program
     */
    private String sanctionsProgram;

    /**
     * Entity or subsidiary is located in or do business in a country under sanctions
     */
    private Boolean countryUnderSanctions;

    /**
     * Flag to indicate if there are shares issued in bearer form
     */
    private Boolean bearerShare;

    /**
     * Percentage of bearerShare
     */
    private BigDecimal pctBearerShare;

    /**
     * activity in high risk country
     */
    private Boolean actvHighRiskCountry;

    /**
     * Percentage of activity in high risk country
     */
    private BigDecimal pctHighRiskCountry;

    /**
     * Flag to indicate party has refused to provide requested KYC information
     */
    private Boolean refusalToProvideInfo;

    /**
     * KYC : Fatca status
     */
    private String fatcaStatus;

}