package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVCorpActResponseType {

  /**
   * Response type of a conditional corporate action  
   */
  COND,

  /**
   * Response type of a mandatory corporate action  
   */
  MAND,

}