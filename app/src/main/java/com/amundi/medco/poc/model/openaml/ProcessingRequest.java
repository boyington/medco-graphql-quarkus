package com.amundi.medco.poc.model.openaml;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="processingrequest", namespace="com.amundi.tech.openaml")
public class ProcessingRequest {

    /**
     * Technical field needed for Json (not visible in XML), used to manage object type if extends another object
     */
    private String _typ;

    /**
     * Version library attribute, version of open asset management language library (not filled by default, only if needed)
     */
    private String openaml;

}