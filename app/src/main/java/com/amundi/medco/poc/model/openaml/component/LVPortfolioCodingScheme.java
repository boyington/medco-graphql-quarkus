package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPortfolioCodingScheme {

  /**
   * Asset manager code, code in the asset management system 
   */
  ASSETMANAGER_CODE,

  /**
   * Accountant code, code in the accounting system or fund administration system 
   */
  ACCOUNTANT_CODE,

  /**
   * Mirror accountant code 
   */
  MIRROR_ACCOUNTANT_CODE,

  /**
   * Umbrella code of the multiple compartment fund 
   */
  UMBRELLA_CODE,

  /**
   * Umbrella identifier of the multiple compartment fund 
   */
  UMBRELLA_ID,

  /**
   * OASYS code 
   */
  OASYS,

  /**
   * Clearer account code, the account code in the clearer system 
   */
  CLEARER_CODE,

  /**
   * Back office code of the portfolio 
   */
  BACK_OFFICE,

  /**
   * Fund LEI code 
   */
  LEI,

  /**
   * ALTO : Portfolio code 
   */
  ALTO,

  /**
   * Portfolio external code 
   */
  EXTERNAL_CODE,

  /**
   * Portfolio parent code, for pocket or bucket portfolio for instance 
   */
  PARENT_CODE,

}