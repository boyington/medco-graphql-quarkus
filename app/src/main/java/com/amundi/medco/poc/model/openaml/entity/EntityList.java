package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVEntityType;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="entitylist", namespace="com.amundi.tech.openaml.entity")
public class EntityList extends Entity {

    /**
     * Content type is a list of PARTY, ASSET, ENTITY_LIST, etc.
     */
    private LVEntityType contentType;

    /**
     * Entity list will contain preferably list of same business objects, e.g. list of country, list of asset, list of party, etc.
     */
    private List<Entity> entityList;

}