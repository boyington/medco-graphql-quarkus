package com.amundi.medco.poc.model.openaml.entity.analytic;

/**
 * undefined
 */
public enum LVRatingCategory {

  /**
   * if credit rating for third-party such as issuer 
   */
  PARTY,

  /**
   * if credit rating for security 
   */
  SECURITY,

  /**
   * Equity rating and analysis 
   */
  EQUITY_RATING,

  /**
   * Equity rating and analysis 
   */
  CREDIT_RATING,

  /**
   * SRI/ESG rating for issuer 
   */
  ESG,

  /**
   * SRRI : synthetic risk and reward indicator of a fund 
   */
  SRRI,

}