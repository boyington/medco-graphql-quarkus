package com.amundi.medco.poc.model.openaml.entity.asset;

/**
 * undefined
 */
public enum LVBarrier {

  /**
   * Down And In 
   */
  DOWN_IN,

  /**
   * Down And Out 
   */
  DOWN_OUT,

  /**
   * Up And In 
   */
  UP_IN,

  /**
   * Up And Out 
   */
  UP_OUT,

  /**
   * One touch up 
   */
  ONETOUCH_UP,

  /**
   * One touch down 
   */
  ONETOUCH_DOWN,

  /**
   * No touch up 
   */
  NOTOUCH_UP,

  /**
   * No touch down 
   */
  NOTOUCH_DOWN,

  /**
   * One touch up and no touch down 
   */
  ONETOUCH_UP_NOTOUCH_DOWN,

  /**
   * One touch down  no touch up 
   */
  ONETOUCH_DOWN_NOTOUCH_UP,

  /**
   * Double one touch 
   */
  DOUBLE_ONETOUCH,

  /**
   * Double no touch 
   */
  DOUBLE_NOTOUCH,

}