package com.amundi.medco.poc.model.openaml.entity.asset;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="conversioninfo", namespace="com.amundi.tech.openaml.entity.asset")
public class ConversionInfo {

    /**
     * A number representing the set of convertible terms over a specific period. Every convertible must have at least one set of option terms, and option numbers must be sequential integers, starting with 1
     */
    private Integer conversionNumber;

    /**
     * Conversion type : exchangeable into Equity of a different issuer
     */
    private String type;

    /**
     * Indicates whether the conversion option is AMERICAN, EUROPEAN, BERMUDAN or EVENT_LINKED.
     */
    private LVOptionStyle exerciseStyle;

    /**
     * Frequency at which the conversion may occur (see LVFrequency)
     */
    private String frequency;

    /**
     * Next conversion date
     */
    private LocalDate nextConversionDate;

    /**
     * Determination date provided on term sheet
     */
    private LocalDate determinationDate;

    /**
     * Effective end date
     */
    private LocalDate endDate;

    /**
     * Describes which party controls the conversion option, whether it be the bond holder, the issuer, a combination of both, or if it is a mandatory conversion.
     */
    private String controllingParty;

    /**
     * Accrued interest code describes the contractual treatment of accrued interest upon conversion.
     */
    private String accruedInterestCode;

    /**
     * Delivery type, settlement method : CASH, PHYSICAL or BOTH (see LVDeliveryType)
     */
    private String deliveryType;

    /**
     * Number of days between conversion and settlement
     */
    private Integer daysToSettle;

    /**
     * Day type such as Calendar day,  business day, etc. related to daysToSettle field 
     */
    private String dayType;

    /**
     * Identifiers how each schedule has changed after the initial convertible values. Each set of option terms much have at least an Initial Convertible schedule
     */
    private String scheduleDesc;

    /**
     * For bonds, this is the par value for each minimum tradable increment of the bond. For preferreds, this is the par value for each share/unit
     */
    private BigDecimal scheduleConvParValue;

    /**
     * Price for each share of underlying upon conversion. Can be implied by par value and conversion ratio, or explicitly noted. Generally quoted in the price of the underlying equity.
     */
    private BigDecimal scheduleConvPrice;

    /**
     * The number of shares of underlying equity received at conversion per specific par value
     */
    private BigDecimal scheduleConvRatio;

}