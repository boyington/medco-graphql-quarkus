package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.LVNetGross;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="basketconstituent", namespace="com.amundi.tech.openaml.entity")
public class BasketConstituent {

    /**
     * Daily weight of the basket constituent, benchmark constituent, or index constituent. This weight derived from underlyer daily valuation.
     */
    private BigDecimal dailyWeight;

    /**
     * Constituent weight, basket weight as defined
     */
    private BigDecimal weight;

    /**
     * Number of units (index or securities)
     */
    private BigDecimal quantity;

    /**
     * Quantity expression
     */
    private LVQuantityExprMode quantityExpr;

    /**
     * Asset as part of the basket constituent (index, security or otc)
     */
    private Asset asset;

    /**
     * Benchmark Id if the benchmark constituents is another benchmark
     */
    private Identifier benchmarkId;

    /**
     * The factor, ratio or divisor is a value used to adjust the constituent weight for pricing.
     */
    private BigDecimal factor;

    /**
     * Price or return
     */
    private BigDecimal price;

    /**
     * Price unit : YIELD, PERCENT, RATE, RETURN
     */
    private LVUnit priceUnit;

    /**
     * Price fiscality : Net return, gross return
     */
    private LVNetGross netOrGross;

    /**
     * Type of price, for example: OPEN, CLOSE
     */
    private LVPriceType priceType;

    /**
     * Price date
     */
    private LocalDate priceDate;

    /**
     * Offset in open day of the price date. Values can be 0,-1,-2.
     */
    private Integer priceLag;

    /**
     * Exchange rate : fx rate, source, fx date
     */
    private ExchangeRate exchRate;

    /**
     * Offset in open day of the exchange rate date. Values can be 0,-1,-2.
     */
    private Integer exchRateLag;

    /**
     * The start date at which the constituent is added to the composition of benchmark, or basket.
     */
    private LocalDate chainingDate;

    /**
     * First valuation date of the constituent. If benchmark composition modification, this the first valuation date of the index newly add to the benchmark
     */
    private LocalDate chainingValuationDate;

}