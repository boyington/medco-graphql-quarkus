package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVMatchingMode {

  /**
   * Matching is manual, a person has 
   */
  MANUAL,

  /**
   * Matching is automatic 
   */
  AUTO,

}