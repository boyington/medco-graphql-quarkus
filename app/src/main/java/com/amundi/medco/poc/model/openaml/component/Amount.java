package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="amount", namespace="com.amundi.tech.openaml.component")
public class Amount {

    /**
     * Amount value
     */
    private BigDecimal amt;

    /**
     * Currency code of the amount value
     */
    private String ccy;

    /**
     * Optional type if payed or received (PAY / RECEIVE), or sign (BUY / SELL), etc.
     */
    private String type;

}