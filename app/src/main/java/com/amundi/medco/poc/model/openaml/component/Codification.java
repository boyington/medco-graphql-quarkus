package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="codification", namespace="com.amundi.tech.openaml.component")
public class Codification {

    /**
     * Group code of this codification
     */
    private String group;

    /**
     * Code of this codification
     */
    private String code;

    /**
     * Coding scheme of the code
     */
    private String codScheme;

    /**
     * Label of this codification
     */
    private String name;

    /**
     * Optional 2nd Label of this codification
     */
    private String name2;

    /**
     * Language code ISO 639-1 (alpha 2), for example ko for Korean language or language code ISO 639-2 (alpha 3)
     */
    private String lang;

}