package com.amundi.medco.poc.jackson.openaml.bean;

import com.amundi.medco.poc.jackson.openaml.BeanFromArrayDeserializer;
import com.amundi.medco.poc.model.openaml.entity.asset.DebtInfo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class DebtInfoDeserializer extends BeanFromArrayDeserializer<DebtInfo> {

    public DebtInfoDeserializer(JsonDeserializer<?> defaultDeserializer) {
        super(defaultDeserializer, DebtInfo.class);
    }

    @Override
    protected DebtInfo getFromArray(JsonParser p, DeserializationContext ctxt) throws IOException {
        DebtInfo debtInfo = null;
        while(p.nextToken() == JsonToken.START_OBJECT) {
            if(debtInfo!=null) {
                log.debug("Multiple debtInfo");
                //throw new InvalidFormatException(p, "Unable to get DebtInfo from array with more than one element", null, DebtInfo.class);
            } else {
                debtInfo = (DebtInfo) this.defaultDeserializer.deserialize(p, ctxt);
            }
        }
        return debtInfo;
    }
}
