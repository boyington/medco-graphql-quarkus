package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVConstraintLevel {

  /**
   * The constraint is hard, i.e. is enforced. If not respected, the constraint status is blocking. In the context of optimization, the constraint is mandatory. 
   */
  HARD,

  /**
   * The constraint is soft, i.e. if it is not respected, the constraint status is warning. In the context of optimization, the constraint can be relaxed or updated. 
   */
  SOFT,

  /**
   * It is not a constraint. 
   */
  NONE,

}