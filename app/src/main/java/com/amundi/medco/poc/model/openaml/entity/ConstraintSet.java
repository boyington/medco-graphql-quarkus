package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="constraintset", namespace="com.amundi.tech.openaml.entity")
public class ConstraintSet extends Entity {

    /**
     * Constraint set type
     */
    private String type;

    /**
     * Constraint set subtype
     */
    private String subtype;

    /**
     * Constraint set description
     */
    private String description;

    /**
     * Constraint set level : HARD, SOFT, NONE (see LVConstraintLevel)
     */
    private String level;

    /**
     * List of constraints
     */
    private List<Constraint> constraints;

}