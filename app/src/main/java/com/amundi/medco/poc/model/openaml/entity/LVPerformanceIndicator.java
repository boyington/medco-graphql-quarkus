package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPerformanceIndicator {

  /**
   * Best month and its performance 
   */
  BEST_MONTH,

  /**
   * Maximum drawdown 
   */
  MAX_DRAWDOWN,

  /**
   * Recovery 
   */
  RECOVERY,

  /**
   * Percentage of weeks down 
   */
  WEEKS_DOWN,

  /**
   * Percentage of weeks up 
   */
  WEEKS_UP,

  /**
   * Worst month and its performance 
   */
  WORST_MONTH,

  /**
   * Highest decrease, sharpest drop 
   */
  HIGHEST_DECREASE,

  /**
   * Highest increase, sharpest rise 
   */
  HIGHEST_INCREASE,

  /**
   * Favorable scenario , see PRIIPS 
   */
  FAVORABLE_SCENARIO,

  /**
   * Moderate scenario , see PRIIPS 
   */
  MODERATE_SCENARIO,

  /**
   * Unfavorable scenario , see PRIIPS 
   */
  UNFAVORABLE_SCENARIO,

  /**
   * significantly unfavorable scenario, see PRIIPS 
   */
  SIGNIFICANTLY_UNFAVORABLE_SCENARIO,

}