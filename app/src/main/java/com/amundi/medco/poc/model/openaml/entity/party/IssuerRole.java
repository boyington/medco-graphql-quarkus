package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.component.Amount;
import com.amundi.medco.poc.model.openaml.component.Comment;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="issuerrole", namespace="com.amundi.tech.openaml.entity.party")
public class IssuerRole extends PartyRole {

    /**
     * Market cap
     */
    private Amount marketCap;

    /**
     * Shares outstanding
     */
    private BigDecimal sharesOutstanding;

    /**
     * Issuer total amount outstanding
     */
    private Amount debtOutstanding;

    /**
     * Issuer voting rights
     */
    private BigDecimal votingRights;

    /**
     * Issuer Non voting rights
     */
    private BigDecimal nonVotingRights;

    /**
     * List of issued assets
     */
    private List<Asset> assets;

    /**
     * Comment on issuer, use comment.text for the comment, and comment.createdBy
     */
    private Comment comment;

    /**
     * Risk comment, use comment.text for the comment, and comment.createdBy
     */
    private Comment riskComment;

    /**
     * True if one of issued security is in Emergins Markets Bond Index (EMBI)
     */
    private Boolean inEMBI;

    /**
     * Date when the issuer is in default
     */
    private LocalDate defaultDate;

    /**
     * True if the dealing, trading is authorized
     */
    private Boolean tradingAuthorized;

    /**
     * Do not deal Casa
     */
    private Boolean doNotDealC;

    /**
     * Sensitive level : NONE, SENSITIVE, VERY_SENSITIVE
     */
    private LVIssuerSensitiveLevel sensitiveLevel;

    /**
     * Identifier of the guarantor
     */
    private Identifier guarantorId;

    /**
     * True if solidarity issuer
     */
    private Boolean solidarity;

    /**
     * External identifier of the issuer
     */
    private Identifier externalId;

    /**
     * Preferred agency
     */
    private String preferredAgency;

    /**
     * Preferred rating agency
     */
    private String preferredRatingAgency;

}