package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVShortLong {

  /**
   * Short, for instance short position, CFD short 
   */
  SHORT,

  /**
   * Long, for instance long position, CFD long 
   */
  LONG,

}