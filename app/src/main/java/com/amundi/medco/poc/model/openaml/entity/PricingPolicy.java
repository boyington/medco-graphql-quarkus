package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.LVShifter;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="pricingpolicy", namespace="com.amundi.tech.openaml.entity")
public class PricingPolicy {

    /**
     * Which source and its role : MARKET_DATA_PROVIDER, ACCOUNTANT, CLEARING_BROKER, ASSET_MANAGER, COUNTERPARTY, CUSTODIAN, BROKER
     */
    private Party source;

    /**
     * Which pricing source is used : BGN, BCE, WMR, etc.
     */
    private String pricingSource;

    /**
     * Price type or value type at asset level or at portfolio level : CLOSE, OPEN, ...
     */
    private LVPriceType priceType;

    /**
     * The number of days used by accrued interests price is shift to D, DN, D2, etc.
     */
    private LVShifter accrIntPriceDaysShifter;

    /**
     * True if the price can be forced
     */
    private Boolean hasForcedPrice;

    /**
     * True if the subscription/redemption is done without the definitive price (unknown price) meaning forward pricing, false if the S/R is done at known price (meaning the valuation includes the transactions of the current day D)
     */
    private Boolean hasForwardPricing;

    /**
     * Inventory cost method, valuation method to calculate cost price : WAC, FIFO, LIFO, see LVValuationMethod
     */
    private String valuationMethod;

    /**
     * Optional : pricing method name
     */
    private String pricingMethod;

    /**
     * Optional : income method name to calculate income such as interests : Constant Yield Method, Payment method, etc.
     */
    private String incomeMethod;

}