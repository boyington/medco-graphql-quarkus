package com.amundi.medco.poc.model.openaml.entity.positionkeeping;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.component.*;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import com.amundi.medco.poc.model.openaml.entity.Portfolio;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="movement", namespace="com.amundi.tech.openaml.entity.positionkeeping")
public class Movement extends Entity {

    /**
     * Securities or cash movement type
     */
    private String mvtType;

    /**
     * Securities or cash movement type as defined by external source, see Entity.source
     */
    private String mvtExtType;

    /**
     * The name or label of a Securities or cash movement type as defined by external source, see Entity.source
     */
    private String mvtExtTypeName;

    /**
     * Category of the movement
     */
    private String category;

    /**
     * True if cash movement
     */
    private Boolean isCash;

    /**
     * True if cash inflow/outflow, subscription or redemption flow, or securities inflow/outflow. This can be used to aggregate movements of SR flows.
     */
    private Boolean isFlow;

    /**
     * Identifier of the cancelled movement
     */
    private Identifier cancelId;

    /**
     * Related identifier, use code, codScheme and sourceRole = CANCEL, LINKED, etc.
     */
    private Identifier relatedId;

    /**
     * Account associated to this movement (number, type, ccy)
     */
    private Account account;

    /**
     * Movement processing date 
     */
    private LocalDate processingDate;

    /**
     * The effective date  is the as of date when the movement impacts the account or the position. The accounting date of the movement.
     */
    private LocalDate effectiveDate;

    /**
     * Sign  of the movement, can be DEBIT or CREDIT,  + or -
     */
    private String sign;

    /**
     * Quantity of securities
     */
    private BigDecimal quantity;

    /**
     * Expression mode of the quantity : QUANTITY, NOMINAL, CONTRACT, CASH
     */
    private LVQuantityExprMode quantityExprMode;

    /**
     * Movement currency for netAmt, grossAmt, feesAmt, etc.
     */
    private String ccy;

    /**
     * Movement Net amount Movement
     */
    private BigDecimal netAmt;

    /**
     * Movement Gross amount : grossAmt = netAmt + feesAmt if feesAmt is negative
     */
    private BigDecimal grossAmt;

    /**
     * Total amount of fees. Fees amount = grossAmt - netAmt
     */
    private BigDecimal feesAmt;

    /**
     * Movement : Net price (trading price) associated to stock movement where Net amount = net price * quantity
     */
    private BigDecimal netPrice;

    /**
     * Movement : Gross price (Net price + fees + interest) associated to stock movement where Gross amount = gross price * quantity
     */
    private BigDecimal grossPrice;

    /**
     * The operation date is the date of the order, or the trade date.
     */
    private LocalDate operationDate;

    /**
     * Movement value date, the payment due date, or the settlement date.
     */
    private LocalDate valueDate;

    /**
     * The receipt date of the movement
     */
    private LocalDate receiptDate;

    /**
     * Movement amounts in portfolio currency, or accountant value and price in portfolio currency
     */
    private Amounts marketValue;

    /**
     * Broker fees rate and/or amount
     */
    private FeesRateAmount brokerFees;

    /**
     * VAT fees rate and/or amount
     */
    private FeesRateAmount vatFees;

    /**
     * Taxes rate and/or amount
     */
    private FeesRateAmount taxes;

    /**
     * Free fees,  misceallenous fees rate and/or amount
     */
    private FeesRateAmount miscFees;

    /**
     * Transfer fees or Movement fees : rate, amout, and currency,  CMV in french.
     */
    private FeesRateAmount transferFees;

    /**
     * Mutual fund fees rate and/or amount
     */
    private FeesRateAmount mutualFundFees;

    /**
     * Trading venue (trading place), usually this is the exchange market MIC code
     */
    private String tradingVenue;

    /**
     * Asset associated to the transaction at the origin of this movement. The asset can be security, currency, index, otc, currency
     */
    private Asset asset;

    /**
     * The portfolio that owns the account of this movement
     */
    private Portfolio portfolio;

    /**
     * Position type associated to this movement
     */
    private String posType;

    /**
     * Position external type associated to this movement
     */
    private String posExtType;

    /**
     * Position status associated to this movement
     */
    private String positionStatus;

    /**
     * Transaction type or movement type : MI_FUNDIN / MC_SR_SUB / MI_FNDOUT / MC_SR_RED
     */
    private String transType;

    /**
     * Identifier of the linked transaction (market execution, corporate action, Subscription redemption, etc.)
     */
    private Identifier transactionId;

    /**
     * Identifier of the order allocation from which this movement has originated
     */
    private Identifier orderAllocationId;

    /**
     * Comment associated to the movement
     */
    private Comment comment;

    /**
     * Pending origin if outstanding cash
     */
    private String pendingOrigin;

    /**
     * Pending ref if outstanding cash
     */
    private String pendingRef;

    /**
     * Pending dunning date if ouststanding cash
     */
    private LocalDate pendingDunningDate;

    /**
     * Pending dunning ref if outstanding cash
     */
    private String pendingDunningRef;

    /**
     * Pending dunning date if ouststanding cash
     */
    private LocalDate pendingStartDate;

}