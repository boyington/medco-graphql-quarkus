package com.amundi.medco.poc.jackson;

import com.amundi.medco.poc.jackson.date.CustomLocalDateDeserializer;
import com.amundi.medco.poc.jackson.date.CustomLocalDateTimeDeserializer;
import com.amundi.medco.poc.jackson.openaml.BooleanDeserializer;
import com.amundi.medco.poc.jackson.openaml.bean.DebtInfoDeserializer;
import com.amundi.medco.poc.jackson.openaml.bean.ExchangeMarketDeserializer;
import com.amundi.medco.poc.jackson.openaml.bean.LegalEntityDeserializer;
import com.amundi.medco.poc.jackson.openaml.bean.PillarsDefDeserializer;
import com.amundi.medco.poc.jackson.openaml.list.*;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.LVFrequency;
import com.amundi.medco.poc.model.openaml.entity.ExchangeMarket;
import com.amundi.medco.poc.model.openaml.entity.LVAssetGroup;
import com.amundi.medco.poc.model.openaml.entity.LegalEntity;
import com.amundi.medco.poc.model.openaml.entity.analytic.PillarsDef;
import com.amundi.medco.poc.model.openaml.entity.asset.DebtInfo;
import com.amundi.medco.poc.model.openaml.entity.asset.LVOptionStyle;
import com.amundi.medco.poc.model.openaml.entity.asset.LVRateType;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.quarkus.jackson.ObjectMapperCustomizer;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Slf4j
@Singleton
public class MapperCustomizer implements ObjectMapperCustomizer {
    @Override
    public void customize(ObjectMapper objectMapper) {

        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDateTime.class, new CustomLocalDateTimeDeserializer());
        javaTimeModule.addDeserializer(LocalDate.class, new CustomLocalDateDeserializer());
        objectMapper.registerModule(javaTimeModule);

        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        SimpleModule openAmlModule = new SimpleModule("openaml-module");
        openAmlModule.setDeserializerModifier(new BeanDeserializerModifier() {
            @Override
            public JsonDeserializer<?> modifyDeserializer(DeserializationConfig config, BeanDescription beanDesc, JsonDeserializer<?> deserializer) {
                if (DebtInfo.class.isAssignableFrom(beanDesc.getBeanClass())) {
                    return new DebtInfoDeserializer(deserializer);
                } else if (PillarsDef.class.isAssignableFrom(beanDesc.getBeanClass())) {
                    return new PillarsDefDeserializer(deserializer);
                } else if (LegalEntity.class.isAssignableFrom(beanDesc.getBeanClass())) {
                    return new LegalEntityDeserializer(deserializer);
                } else if (ExchangeMarket.class.isAssignableFrom(beanDesc.getBeanClass())) {
                    return new ExchangeMarketDeserializer(deserializer);
                }
                return deserializer;
            }
        });

        openAmlModule.addDeserializer(Boolean.class, new BooleanDeserializer());
        openAmlModule.addDeserializer(LVUnit.class, new LVUnitDeserializer());
        openAmlModule.addDeserializer(LVOptionStyle.class, new LVOptionStyleDeserializer());
        openAmlModule.addDeserializer(LVAssetGroup.class, new LVAssetGroupDeserializer());
        openAmlModule.addDeserializer(LVFrequency.class, new LVFrequencyDeserializer());
        openAmlModule.addDeserializer(LVRateType.class, new LVRateTypeDeserializer());

        objectMapper.registerModule(openAmlModule);
    }
}



