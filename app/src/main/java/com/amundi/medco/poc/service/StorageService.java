package com.amundi.medco.poc.service;

import com.amundi.medco.poc.model.Root;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import io.quarkus.runtime.StartupEvent;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import one.microstream.configuration.types.ConfigurationLoader;
import one.microstream.configuration.yaml.types.ConfigurationParserYaml;
import one.microstream.reference.Lazy;
import one.microstream.storage.embedded.configuration.types.EmbeddedStorageConfiguration;
import one.microstream.storage.types.StorageManager;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Data
@Slf4j
@Singleton
public class StorageService {

    private StorageManager storageManager;

    @ConfigProperty(name = "app.load-at-startup")
    boolean loadAtStartup;

    @ConfigProperty(name = "app.asset.directory")
    String assetDirectory;

    public StorageService() {
        storageManager = EmbeddedStorageConfiguration.load(
                        ConfigurationLoader.New("/META-INF/microstream/storage.yml"),
                        ConfigurationParserYaml.New()
                )
                .createEmbeddedStorageFoundation()
                .createEmbeddedStorageManager();
        storageManager.start();
    }

    public Root getRoot() {
        if (storageManager.root() == null) {
            Root root = new Root();
            storageManager.setRoot(root);
            storageManager.storeRoot();
            return root;
        } else {
            return (Root) storageManager.root();
        }
    }

    void startup(@Observes StartupEvent event) {

        Root root = getRoot();
        if (root.getAssets().isEmpty() && loadAtStartup) {
            log.info("asset directory: " + assetDirectory);
            if (assetDirectory != null && !assetDirectory.isBlank()) {

                File dir = new File(assetDirectory);
                log.info("asset directory {} exists: {}", dir.getAbsolutePath(), dir.exists());
                if (dir.exists() && dir.listFiles() != null) {
                    Arrays.stream(Objects.requireNonNull(dir.listFiles())).forEach(f -> {
                        ZipFile zipFile;
                        try {
                            zipFile = new ZipFile(f, ZipFile.OPEN_READ);
                        } catch (IOException e) {
                            log.error("Error with {} file", f.getAbsolutePath());
                            throw new RuntimeException(e);
                        }

                        Enumeration<? extends ZipEntry> entries = zipFile.entries();

                        while (entries.hasMoreElements()) {
                            ZipEntry entry = entries.nextElement();
                            InputStream stream = null;
                            try {
                                stream = zipFile.getInputStream(entry);
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            String text = new BufferedReader(
                                    new InputStreamReader(stream, StandardCharsets.UTF_8))
                                    .lines()
                                    .collect(Collectors.joining("\n"));
                            JsonArray assetArray = new JsonArray(text);
                            assetArray.stream().forEach(node -> {
                                Asset asset = (Asset) convertFromNode((JsonObject) node, Asset.class);

                                if (asset != null) {
                                    Lazy<Asset> ref = Lazy.Reference(asset);
                                    root.getAssets().put(asset.get_id(), ref);
                                    log.info("{} assets", root.getAssets().size());
                                    storageManager.store(ref);
                                    ref.clear();
                                    if (root.getAssets().size() % 1000 == 0) {
                                        log.info("{} assets imported", root.getAssets().size());
                                    }
                                }

                            });


                        }
                    });
                    storageManager.storeRoot();
                }
            }
        }
    }

    private static Object convertFromNode(JsonObject node, Class clazz) {
        JsonObject data = null;
        try {
            data = new JsonObject(node.getString("data"));
            data.put("_id", node.getValue("id"));
            followStructure(data, clazz);
            return data.mapTo(clazz);

        } catch (Exception e) {
            log.info("data: " + data.toString());
            log.error(e.getMessage());
        }
        return null;
    }

    private static void followStructure(JsonObject node, Class clazz) {
        getAllFields(new LinkedList<>(), clazz).forEach(f -> {
            if (List.class.isAssignableFrom(f.getType())) {
                Object wrappedList = node.getMap().get(f.getName());
                if (wrappedList != null && Map.class.isAssignableFrom(wrappedList.getClass())) {
                    Map<String, Object> innerObject = (Map<String, Object>) wrappedList;
                    if (innerObject.size() == 1) {
                        String key = innerObject.keySet().stream().collect(Collectors.joining());
                        // Manage uwrapped object when one element in the array
                        Object l = innerObject.get(key);
                        if (!List.class.isAssignableFrom(l.getClass())) {
                            l = new ArrayList<>(List.of(l));
                        }
                        node.getMap().put(f.getName(), l);
                    }
                }

                String innerType = f.getAnnotatedType().getType().toString().replaceAll(".*<|>.*", "");
                wrappedList = node.getMap().get(f.getName());
                if (wrappedList != null) {
                    try {
                        Class innerClass = Class.forName(innerType.toString());
                        ((List) wrappedList).forEach(o -> {
                            if(Map.class.isAssignableFrom(o.getClass())) {
                                followStructure(new JsonObject((Map<String, Object>) o) , innerClass);
                            }
                        });
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }
            } else if (!f.getType().toString().startsWith("java.") && node.containsKey(f.getName())) {
                Object innerObj = node.getMap().get(f.getName());
                if (Map.class.isAssignableFrom(innerObj.getClass())) {
                    followStructure(new JsonObject((Map<String, Object>) innerObj), f.getType());
                }
            }
        });
    }

    private static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }
}
