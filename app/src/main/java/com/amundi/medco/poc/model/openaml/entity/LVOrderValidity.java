package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVOrderValidity {

  /**
   * A buy or sell order that, if not executed expires at the end of the trading day on which it was entered. 
   */
  DAY,

  /**
   * An order to buy or sell that remains in effect until it is either executed or canceled. sometimes called an -open order- 
   */
  GOOD_TILL_CANCEL,

  /**
   * A market or limit-price order to be executed at the opening of the stock or not at all. all or part of any order not executed at the opening is treated as canceled. 
   */
  AT_THE_OPENING,

  /**
   * A market or limit-price order that is to be executed in whole or in part as soon as it is represented in the Trading Crowd. any portion not so executed is to be canceled. Not to be confused with Fill or Kill. 
   */
  IMMEDIATE_OR_CANCEL,

  /**
   * A market or limit-price order that is to be executed in its entirety as soon as it is represented in the Trading Crowd. if not so executed, the order is to be canceled. Not to be confused with Immediate or Cancel 
   */
  FILL_OR_KILL,

  /**
   * An order to buy or sell that is canceled prior to the market entering into an auction, or crossing phase. Typically, markets that support continuous trading will have an auction phase at the beginning and sometimes also at the end of trading to match up orders that have been entered into the exchange's order book during the pre- or post-trading phase (i.e. where no continuous trading was available). 
   */
  GOOD_TILL_CROSSING,

  /**
   * GTD order. An order to buy or sell that remains in effect until the validity-date, either executed or canceled. 
   */
  GOOD_TILL_DATE,

  /**
   * A market or limit-price order to be executed at the closing of the stock or not at all. all or part of any order not executed at the closing is treated as canceled. 
   */
  AT_THE_CLOSE,

}