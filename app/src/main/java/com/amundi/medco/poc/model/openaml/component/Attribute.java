package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="attribute", namespace="com.amundi.tech.openaml.component")
public class Attribute {

    /**
     * Attribute name
     */
    private String name;

    /**
     * Description or attribute display name
     */
    private String desc;

    /**
     * Attribute value of decimal type
     */
    private BigDecimal decimalValue;

    /**
     * Attribute value of string type
     */
    private String stringValue;

    /**
     * Attribute value of dateTime type
     */
    private LocalDateTime dateValue;

    /**
     * Attribute value of boolean type
     */
    private Boolean booleanValue;

    /**
     * Optional : true if the attribute value is inclusive
     */
    private Boolean inclusive;

    /**
     * Optional expression mode of the value, if decimalValue, see LVUnit, if dateValue, see date format
     */
    private String exprMode;

    /**
     * Attribute values of type string. To use other types such as decimal or dates, the cast will need to be done from the string[].
     */
    private List<String> values;

}