package com.amundi.medco.poc.model.openaml.entity.asset;

import com.amundi.medco.poc.model.openaml.entity.AssetEvent;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="corporateaction", namespace="com.amundi.tech.openaml.entity.asset")
public class CorporateAction extends AssetEvent {

    /**
     * Corporate action response type
     */
    private String responseType;

}