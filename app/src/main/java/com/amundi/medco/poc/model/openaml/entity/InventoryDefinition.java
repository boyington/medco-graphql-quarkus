package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="inventorydefinition", namespace="com.amundi.tech.openaml.entity")
public class InventoryDefinition extends Entity {

    /**
     * Id of the inventory scope
     */
    private Identifier inventoryScopeId;

    /**
     * Definition of the inventory scope criteria
     */
    private List<Attribute> inventoryScope;

    /**
     * Id of the inventory policy
     */
    private Identifier inventoryPolicyId;

    /**
     * Definition of the inventory scope criteria
     */
    private List<Attribute> inventoryPolicy;

}