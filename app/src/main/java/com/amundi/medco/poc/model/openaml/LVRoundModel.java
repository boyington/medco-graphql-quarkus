package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVRoundModel {

  /**
   * Rounding mode to assert that the requested operation has an exact result, hence no rounding is necessary. 
   */
  UNNECESSARY,

  /**
   * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case, round towards the even neighbor. Behaves as for ROUND_HALF_UP if the digit to the left of the discarded fraction is odd. behaves as for ROUND_HALF_DOWN if it's even. Note that this is the rounding mode that minimizes cumulative error when applied repeatedly over a sequence of calculations. 
   */
  HALF_EVEN,

  /**
   * Rounding mode to round towards nearest neighbor unless both neighbors are equidistant, in which case  round down. Behaves as for ROUND_UP if the discarded fraction is greater than  0.5 otherwise, behaves as for ROUND_DOWN. 
   */
  HALF_DOWN,

  /**
   * Rounding mode to round towards nearest neighbor unless both neighbors are equidistant, in which case round up. Behaves as for ROUND_UP if the discarded fraction is greater or equals  0.5 otherwise, behaves as for ROUND_DOWN. Note that this is the  rounding mode that most of us were taught in grade school. 
   */
  HALF_UP,

  /**
   * Rounding mode to round towards negative infinity. If the value is positive, behave as for ROUND_DOWN if  negative, behave as for ROUND_UP. Note that this rounding mode  never increases the calculated value. 
   */
  FLOOR,

  /**
   * Rounding mode to round towards positive  infinity. If the value is positive, behaves as for ROUND_UP. if  negative, behaves as for ROUND_DOWN. Note that this rounding mode  never decreases the calculated value. 
   */
  CEILING,

  /**
   * Rounding mode to round towards zero. Never increments the digit prior to a discarded fraction (i.e., truncates). Note that this rounding mode never increases the magnitude of the calculated value. 
   */
  DOWN,

  /**
   * Rounding mode to round away from zero. Always increments the digit prior to a nonzero discarded fraction. Note that this rounding mode never decreases the magnitude of the calculated value. 
   */
  UP,

}