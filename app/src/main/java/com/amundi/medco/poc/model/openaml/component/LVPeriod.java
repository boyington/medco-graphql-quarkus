package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPeriod {

  /**
   * Period = 1 day 
   */
  DAY,

  /**
   * Period = 1 week 
   */
  WEEK,

  /**
   * Period = 1 month 
   */
  MONTH,

  /**
   * Period = 1 quarter of year 
   */
  QUARTER,

  /**
   * Period = 1 half-year 
   */
  HALFYEAR,

  /**
   * Period = 1 year 
   */
  YEAR,

  /**
   * Year to date: since the beginning of the year of the date to-date 
   */
  YTD,

  /**
   * Inception to date: since the beginning of, for example since the beginning of the portfolio specific 
   */
  ITD,

  /**
   * Date to date: between two dates 
   */
  DTD,

  /**
   * Hour 
   */
  HOUR,

}