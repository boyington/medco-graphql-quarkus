package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVPtfLevelType {

  /**
   * Single investment portfolio, stand alone fund, or sub-fund if multi-compartment fund 
   */
  FUND,

  /**
   * Sub-fund if multi-compartment fund, sub-fund of the umbrella legal structure. 
   */
  SUBFUND,

  /**
   * Portfolio pocket (front or accountant), the parent is a fund or a mandate. It is same than bucket. 
   */
  POCKET,

  /**
   * This portfolio is a mandate : institutional mandate (asset management) or client mandate (private banking, wealth management) 
   */
  MANDATE,

  /**
   * This portfolio is a portfolio model 
   */
  MODEL,

  /**
   * This portfolio is a technical portfolio 
   */
  TECHNICAL,

  /**
   * Umbrella fund if the fund is structured as a multi-compartment fund 
   */
  UMBRELLA,

  /**
   * Private banking, wealth management : Client portfolio 
   */
  PORTFOLIO,

  /**
   * Private banking, wealth management : Portfolio bucket as a strategy, the parent is a portfolio or a mandate 
   */
  BUCKET,

  /**
   * A Portfolio that is a ShareClass, Portfolio object instead of ShareClass object 
   */
  SHARE_CLASS,

  /**
   * This portfolio level is unknown 
   */
  UNKNOWN,

}