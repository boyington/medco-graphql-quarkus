package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVMessageType {

  /**
   * Cash movement 
   */
  CASH_MOVEMENT,

  /**
   * Stock movement 
   */
  STOCK_MOVEMENT,

  /**
   * Client 
   */
  CLIENT,

  /**
   * Commercial offer 
   */
  OFFER,

  /**
   * Order allocation 
   */
  ORDER_ALLOCATION,

  /**
   * Portfolio valuation 
   */
  PORTFOLIO_VALUATION,

  /**
   * Mandate valuation 
   */
  MANDATE_VALUATION,

  /**
   * Portfolio indicator 
   */
  PORTFOLIO_INDICATOR,

  /**
   * User 
   */
  USER,

  /**
   * Undefined object 
   */
  UNDEFINED,

  /**
   * Specific object to have a list of business object 
   */
  ENTITY_LIST,

  /**
   * Access right business object, or authorization business object, 
   */
  ACCESS_RIGHT,

  /**
   * Analytics business object 
   */
  ANALYTICS,

  /**
   * Analytics value business object 
   */
  ANALYTICS_VALUE,

  /**
   * Asset business object 
   */
  ASSET,

  /**
   * Asset event business object 
   */
  ASSET_EVENT,

  /**
   * Asset link business object 
   */
  ASSET_LINK,

  /**
   * Basket business object 
   */
  BASKET,

  /**
   * Benchmark business object, 
   */
  BENCHMARK,

  /**
   * BeneficialOwner business object 
   */
  BENEFICIAL_OWNER,

  /**
   * CarbonData business object 
   */
  CARBON_DATA,

  /**
   * Classif business object 
   */
  CLASSIF,

  /**
   * Collateral business object 
   */
  COLLATERAL,

  /**
   * Constraint business object 
   */
  CONSTRAINT,

  /**
   * ConstraintSet business object 
   */
  CONSTRAINT_SET,

  /**
   * ContactInfo business object 
   */
  CONTACT_INFO,

  /**
   * CorporateAction business object 
   */
  CORPORATE_ACTION,

  /**
   * Country business object 
   */
  COUNTRY,

  /**
   * Currency business object 
   */
  CURRENCY,

  /**
   * EDocument business object 
   */
  EDOCUMENT,

  /**
   * ExchangeMarket business object 
   */
  EXCHANGE_MARKET,

  /**
   * ExchangeRate business object, 
   */
  EXCHANGE_RATE,

  /**
   * Execution business object 
   */
  EXECUTION,

  /**
   * Index business object 
   */
  INDEX,

  /**
   * Instruction business object 
   */
  INSTRUCTION,

  /**
   * Investment Mandate business object 
   */
  INVESTMENT_MANDATE,

  /**
   * Legal entity business object that is also a party 
   */
  LEGAL_ENTITY,

  /**
   * LinkedOrder business object, 
   */
  LINKED_ORDER,

  /**
   * Location business object 
   */
  LOCATION,

  /**
   * LocationDistribution business object 
   */
  LOCATION_DISTRIBUTION,

  /**
   * Movement business object 
   */
  MOVEMENT,

  /**
   * Order business object 
   */
  ORDER,

  /**
   * OrderSet business object 
   */
  ORDER_SET,

  /**
   * OTC business object 
   */
  OTC,

  /**
   * Party business object 
   */
  PARTY,

  /**
   * PartyLink business object 
   */
  PARTY_LINK,

  /**
   * Performance business object 
   */
  PERFORMANCE,

  /**
   * Person business object that is also a party 
   */
  PERSON,

  /**
   * Portfolio business object 
   */
  PORTFOLIO,

  /**
   * PortfolioOptimization business object 
   */
  PORTFOLIO_OPTIMIZATION,

  /**
   * ProgramTrade business object 
   */
  PROGRAM_TRADE,

  /**
   * ShareClass business object 
   */
  SHARE_CLASS,

  /**
   * Quotation business object 
   */
  QUOTATION,

  /**
   * Rating business object 
   */
  RATING,

  /**
   * Security business object 
   */
  SECURITY,

  /**
   * Portfolio Inventory business object 
   */
  PORTFOLIO_INVENTORY,

  /**
   * Team business object 
   */
  TEAM_AND_OFFICER,

  /**
   * Time series business object 
   */
  TIME_SERIES,

  /**
   * Transaction business object 
   */
  TRANSACTION,

  /**
   * A list of miscellaneous business objects 
   */
  MISC,

}