package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="assetlink", namespace="com.amundi.tech.openaml.entity")
public class AssetLink extends EntityLink {

    /**
     * List of assets
     */
    private List<Asset> assets;

}