package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVInstructionAllocationType {

  /**
   * Instruction allocation is an investment 
   */
  INVESTMENT,

  /**
   * Instruction allocation is a divestment, or disinvestment 
   */
  DIVESTMENT,

  /**
   * Instruction allocation is an inflow 
   */
  INFLOW,

  /**
   * Instruction allocation is an outflow 
   */
  OUTFLOW,

}