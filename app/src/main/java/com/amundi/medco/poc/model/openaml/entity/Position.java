package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.LVPayReceive;
import com.amundi.medco.poc.model.openaml.LVQuantityExprMode;
import com.amundi.medco.poc.model.openaml.component.*;
import com.amundi.medco.poc.model.openaml.entity.positionkeeping.Movement;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonRootName(value="position", namespace="com.amundi.tech.openaml.entity")
public class Position {

    /**
     * External id: the holding identifier from external provider
     */
    private Identifier externalId;

    /**
     * Position type or holding type, see LVPositionType: AVAILABLE, BLOCKED, ...
     */
    private String type;

    /**
     * Position subtype
     */
    private String subtype;

    /**
     * Position external type or holding external type as it is defined
     */
    private String posExtType;

    /**
     * Name or label of the position external type as it is defined
     */
    private String posExtTypeName;

    /**
     * Position external subtype or holding external subtype as it is defined
     */
    private String posExtSubtype;

    /**
     * Name or label of the position external subtype as it is defined
     */
    private String posExtSubtypeName;

    /**
     * Specifies the position lifecycle step from which the position is part of the portfolio inventory. For instance if ORDERED, the portfolio positions will be composed of holdings and pending orders. The ordered lifecycle steps are SIMULATED, ORDERED, ALLOCATED, CONFIRMED, BOOKED, SETTLED.
     */
    private LVPosTransLifecycle posLifecycle;

    /**
     * Position status, can be stock status
     */
    private String positionStatus;

    /**
     * Optional level (e.g. Global / SubBalance, Strategy) etc. used as an indicative information
     */
    private String level;

    /**
     * Classification of the position that is part of a stock category (asset, liability, IAS or IFRS9), and in addition to the position type.
     */
    private String category;

    /**
     * Account type
     */
    private String accountType;

    /**
     * Account type name
     */
    private String accountTypeName;

    /**
     * Cash nature: The cash is recorded in the cash account or bank account (CASH_AT_BANK), CASH_FORWARD, PROVISION, MARGIN_CALL, COLLATERAL, or DEPOSIT
     */
    private String cashAccountRecord;

    /**
     * Optional subtype of cash, for example, SWING
     */
    private String cashType;

    /**
     * Optional to separate payable and receivable if not CASH_AT_BANK
     */
    private LVPayReceive cashSide;

    /**
     * For otc with multi legs, if the holding corresponds to one leg. For example legType will be PAY or RECEIVE if this is one leg of a swap derivative.
     */
    private String legType;

    /**
     * Position pocket code
     */
    private String pocket;

    /**
     * If position hedging, indicate a code, or a flag
     */
    private String hedging;

    /**
     * For position used in the context of share Class hedging, it is the shareGroup
     */
    private String shareGroup;

    /**
     * Position breakdown code and name if the position level is BREAKDOWN, meaning the position is part of an aggregate
     */
    private Codification breakdown;

    /**
     * Current weight of this position
     */
    private BigDecimal positionWeight;

    /**
     * Position weights provided by benchmark, model or proxy
     */
    private BigDecimal referenceWeight;

    /**
     * Relative weight of the portfolio position to the weights provided by benchmark, model or proxy
     */
    private BigDecimal relativeWeight;

    /**
     * Position quantity : quantity/nominal/#contracts of the position
     */
    private BigDecimal quantity;

    /**
     * The expression mode of the quantity :QUANTITY, NOMINAL, CONTRACT, CASH
     */
    private LVQuantityExprMode quantityExprMode;

    /**
     * Holding ratio for transparency, current weight for look through process
     */
    private BigDecimal holdingRatio;

    /**
     * Optional holding date or date of the position
     */
    private LocalDate holdingDate;

    /**
     * Position previous quantity, or quantity at N-1 if N is the current holding date
     */
    private BigDecimal previousQuantity;

    /**
     * Optional source of the position valuation
     */
    private String source;

    /**
     * True if split holding
     */
    private Boolean split;

    /**
     * Optional party code associated to the position, for instance the bank code of collateral position
     */
    private String partyCode;

    /**
     * Optional third-party such as asset manager of the holding if portfolio holding (use party.role = ASSET_MANAGER), or issuer/counterparty if pool of positions (use party.role = COUNTERPARTY or ISSUER)
     */
    private Party party;

    /**
     * Strategy code
     */
    private String strategyCode;

    /**
     * Portfolio that can be the compartment, the pocket, the class, a mandate, or a portfolio model
     */
    private Portfolio portfolio;

    /**
     * Holding on this asset : security, currency, index, otc
     */
    private Asset asset;

    /**
     * Position link code and name, for instance hedging code
     */
    private Codification link;

    /**
     * True if this is off-balance
     */
    private Boolean offBalance;

    /**
     * Accounting standard type, business model, management intent
     */
    private AccountingStandard accountingStd;

    /**
     * Classification if collateral position
     */
    private CollateralGroup collateral;

    /**
     * Market exposure amount, percentage : marketExposure.amt, marketExposure.weight
     */
    private AmountWeight marketExposure;

    /**
     * Valuation gap amounts
     */
    private AmountWeight valuationGap;

    /**
     * Commitment in amount with currency code
     */
    private AmountWeight commitment;

    /**
     * Net Book value in portfolio currency, based on Net present value (NPV)  if Fifo for instance
     */
    private Amounts netBookValue;

    /**
     * Default market value (mtm or accountant value) and price in portfolio currency, or in statement currency if in a statement
     */
    private Amounts marketValue;

    /**
     * Optional alternative market value (mtm or accountant value) and price if needed in alternative currency or other price
     */
    private Amounts altMarketValue;

    /**
     * Market value (mtm or accountant value) and price in local (asset, security or otc) currency
     */
    private Amounts localMarketValue;

    /**
     * Fifo/lifo position : Buy date and time of the position  (trade date of the transaction that has impacted the position)
     */
    private LocalDateTime buyDateTime;

    /**
     * Fifo/lifo position : Buy amounts (netAmt, netPrice, yield). Note that yield can be the buy rate of return
     */
    private Amounts buyAmounts;

    /**
     * Fifo/lifo position : Sell date and time of the position  (trade date of the transaction that has impacted the position)
     */
    private LocalDateTime sellDateTime;

    /**
     * Fifo/lifo position : Sell amounts (netAmt, netPrice). 
     */
    private Amounts sellAmounts;

    /**
     * Cost price of the position
     */
    private BigDecimal costPrice;

    /**
     * Unit cost price   including  accrIntUcp and feesUcp in portfolio currency
     */
    private BigDecimal ucp;

    /**
     * Accrued interests part of the Unit cost price in portfolio currency
     */
    private BigDecimal accrIntUcp;

    /**
     * Fees part of the Unit cost price in portfolio currrency
     */
    private BigDecimal feesUcp;

    /**
     * Average purchase Exchange rate
     */
    private BigDecimal exchRateUcp;

    /**
     * Cost price in asset ccy of the postion
     */
    private BigDecimal costPriceAsset;

    /**
     * Unit cost price  in asset ccy including  accrIntUcpAsset and feesUcpAsset in  (asset, security or otc) currency
     */
    private BigDecimal ucpAsset;

    /**
     * Accrued interests part of the Unit cost price in  (asset, security or otc) currency
     */
    private BigDecimal accrIntUcpAsset;

    /**
     * Fees part of the Unit cost price in  (asset, security or otc) currrency
     */
    private BigDecimal feesUcpAsset;

    /**
     * Unrealised (latent) profit and loss amount = (market price - ucp) * quantity in portfolio currency
     */
    private BigDecimal unrealisedPnlAmt;

    /**
     * Realised Profit and loss amount in portfolio currency, gain revenue (income) or expenses amount, gain or loss amount. Historical or ex-post PnL, realisedPnlAmt = realisedIncomeAmt + realisedLossAmt 
     */
    private BigDecimal realisedPnlAmt;

    /**
     * Unrealised (latent) profit and loss amount = (market price - ucp) * quantity in local (asset) currency
     */
    private BigDecimal unrealisedLocalPnlAmt;

    /**
     * Realised Profit and loss amount in local (asset) currency, gain revenue (income) or expenses amount, gain or loss amount. Historical or ex-post PnL, realisedPnlAmt = realisedIncomeAmt + realisedLossAmt 
     */
    private BigDecimal realisedLocalPnlAmt;

    /**
     * List of sub-positions or holdings as children
     */
    private List<Position> subPositions;

    /**
     * List of movements that explain this position
     */
    private List<Movement> movements;

    /**
     * List of custom attributes associated to the Position
     */
    private List<Attribute> attributes;

    /**
     * all provisions related to the position.
     */
    private List<Amount> provisions;

    /**
     * PNL details used essentially for FIFO inventories 
     */
    private List<RealizedPnlDetail> realizedPnlDetails;

    /**
     * all custom data organised by universe with each a list fields ( key + value)
     */
    private List<DataUniverse> customData;

}