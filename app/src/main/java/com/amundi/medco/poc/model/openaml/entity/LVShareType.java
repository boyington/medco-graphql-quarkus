package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVShareType {

  /**
   * Accumulation share (A), or Capitalized, share with capitalized income (interests and return) (C) 
   */
  A,

  /**
   * Capitalized share,  income (interests and return) is capitalized (C) or accumulated (A) 
   */
  C,

  /**
   * Distribution share, income (interests and return) is distributed to bearers 
   */
  D,

  /**
   * Partially Distributed share, income (interests and return) is distributed to bearers until a limit 
   */
  P,

  /**
   * The share type is unknown 
   */
  UNKNOWN,

}