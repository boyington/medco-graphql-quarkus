package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.amundi.medco.poc.model.openaml.LVUnit;
import com.amundi.medco.poc.model.openaml.component.Attribute;
import com.amundi.medco.poc.model.openaml.component.LVBusinessDayConvention;
import com.amundi.medco.poc.model.openaml.component.LVPeriod;
import com.amundi.medco.poc.model.openaml.entity.analytic.PillarsDef;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonRootName(value="analytics", namespace="com.amundi.tech.openaml.entity")
public class Analytics extends Entity {

    /**
     * Analytics date
     */
    private LocalDate analyticsDate;

    /**
     * Analytics currency code
     */
    private String analyticsCcy;

    /**
     * Date of the total net asset (here in marketCap)
     */
    private LocalDate totalNetAssetDate;

    /**
     * Asset market capitalization, market value in analytics currency code
     */
    private BigDecimal marketCap;

    /**
     * CURRENT_MARKET_CAP_SHARE_CLASS
     */
    private BigDecimal marketCapShareClass;

    /**
     * Average market capitalization 60 days / 2 months, AVR_MKT_CAP_2M
     */
    private BigDecimal avgMarketCap60D;

    /**
     * Fund currency code, and currency code of fund total net asset
     */
    private String fundCcy;

    /**
     * Date of fund total net asset
     */
    private LocalDate fundTotalAssetDate;

    /**
     * Fund AUM, Aggregate fund value, fund total net asset (Fund's total market value less all liabilities as of any historical TNA date), expressed in millions
     */
    private BigDecimal fundTotalNetAsset;

    /**
     * Fund total gross asset, expressed in millions
     */
    private BigDecimal fundTotalGrossAsset;

    /**
     * Outstanding amount, or issuer debt outstanding
     */
    private BigDecimal outstandingAmt;

    /**
     * Issuer outstanding debt and loans, BB DDIS in asset currency
     */
    private BigDecimal outstandingDebtAndLoansAmt;

    /**
     * Issuer outstanding shares
     */
    private BigDecimal outstandingShares;

    /**
     * Outstanding voting rights
     */
    private BigDecimal outstandingVotingRights;

    /**
     * voting rights multiplier
     */
    private BigDecimal votingRightsMultiplier;

    /**
     * Facility outstanding amount in asset currency
     */
    private BigDecimal facilityOutstandingAmt;

    /**
     * Issuer gross margin
     */
    private BigDecimal grossMargin;

    /**
     * Issuer interest coverage ratio
     */
    private BigDecimal interestCoverageRatio;

    /**
     * liquidity score
     */
    private BigDecimal liquidityScore;

    /**
     * composite liquidity score
     */
    private BigDecimal compositeLiquidityScore;

    /**
     * Values unit expression if need to indicate
     */
    private LVUnit unit;

    /**
     * FI: Yield used to compute the modified duration (MD) or sensitivity
     */
    private BigDecimal yield;

    /**
     * FI: Macaulay duration
     */
    private BigDecimal duration;

    /**
     * Rate modified duration in percentage. MD = duration / (1+yield)
     */
    private BigDecimal rateSensitivity;

    /**
     * Credit sensitivity, credit modified duration in percentage
     */
    private BigDecimal creditSensitivity;

    /**
     * Inflation bond/swap : Sensitivity to break-even inflation, i.e. the difference between  the nominal yield on a fixed-rate investment and the real yield (fixed spread) on an inflation-linked investment  of similar maturity and credit quality.
     */
    private BigDecimal breakevenSensitivity;

    /**
     * Convexity
     */
    private BigDecimal convexity;

    /**
     * Fixed rate that will cancel the NPV plus the accrued margin included (First Leg NPV - Second Leg NPV = Accrual with margin != 0) for swap trades.
     */
    private BigDecimal fairRate;

    /**
     * Indicator alpha
     */
    private BigDecimal alpha;

    /**
     * Indicator beta
     */
    private BigDecimal beta;

    /**
     * Option : First order greek option. It's a Black Scholes Delta corresponding to the sensitivity of the market value to the Fx Rate movement (at spot date) - Delta is in percentage. OTC Swap delta = DV01 is the interest rate risk in the MtM with respect to a 1bp bump of the rate curves (zero coupon curve), also called BPV (basis value point).
     */
    private BigDecimal delta;

    /**
     * Option : First order greek option that measures sensitivity to volatitily, calculated as a percentage (at spot date)
     */
    private BigDecimal vega;

    /**
     * Option : Second order greek option measures the rate of change in the delta. The Gamma is the second derivative with respect to the zero rate curve
     */
    private BigDecimal gamma;

    /**
     * Option : First order greek option that measures sensitivity to local interest rate (at spot date) of the MtM currency
     */
    private BigDecimal rho;

    /**
     * Option : First order greek option that measures the sensitivity to time value (time decay). Derivative for one day decay
     */
    private BigDecimal theta;

    /**
     * Implied volatility used to price the option
     */
    private BigDecimal impliedVolatility;

    /**
     * Delta bond used to calculate bond exposure, delta bond = 1 - delta equity
     */
    private BigDecimal deltaBond;

    /**
     * Delta equity used to calculate equity exposure
     */
    private BigDecimal deltaEquity;

    /**
     * Beta vol : equity risk indicator, PFI_OPT_BETA_VOL
     */
    private BigDecimal betaVol;

    /**
     * EQY_BETA_STD_DEV_ERR_6M called rawBetaVol
     */
    private BigDecimal rawBetaVol;

    /**
     * EQY_RAW_BETA called rawBeta
     */
    private BigDecimal rawBeta;

    /**
     * EQY_DVD_YLD_IND : The most recently announced dividend amount, annualized based on the Dividend Frequency (DV016, DVD_FREQ), then divided by the current market price. 
     */
    private BigDecimal dividendIndicatedYield;

    /**
     * DVD_SH_12M : dividends before fiscality
     */
    private BigDecimal dividends;

    /**
     * By default in # of years, but could be in # of days, # of months
     */
    private LVPeriod maturitiesUnit;

    /**
     * If the date has been computed, indicate if it follows a business day convention
     */
    private LVBusinessDayConvention businessDayConv;

    /**
     * Risk maturity date
     */
    private LocalDate riskMaturityDate;

    /**
     * Average life, by default in # of years, or in maturitiesUnit
     */
    private BigDecimal averageLife;

    /**
     * Date corresponding to the average life
     */
    private LocalDate averageLifeDate;

    /**
     * Weighted average life
     */
    private BigDecimal wal;

    /**
     * Date corresponding to the weighted average life
     */
    private LocalDate walDate;

    /**
     * Weighted average maturity
     */
    private BigDecimal wam;

    /**
     * Date corresponding to the weighted average maturity
     */
    private LocalDate wamDate;

    /**
     * Weighted average maturity specific called WAMR
     */
    private BigDecimal wamr;

    /**
     * Weighted average life specific called WALF
     */
    private BigDecimal walf;

    /**
     * Weighted average life specific called WALP
     */
    private BigDecimal walp;

    /**
     * Weighted average coupon
     */
    private BigDecimal wAvgCoupon;

    /**
     * Date corresponding to the weighted average coupon
     */
    private LocalDate wAvgCouponDate;

    /**
     * Time between today and the maturity, usually in #of years
     */
    private BigDecimal timeToMaturity;

    /**
     * number of days until option expiration date
     */
    private BigDecimal daysToMaturity;

    /**
     * Credit risk life
     */
    private BigDecimal creditLife;

    /**
     * Credit risk maturity date
     */
    private LocalDate creditMaturityDate;

    /**
     * Low end of the maturity bucket based on credit risk maturity date. Low end of the maturity bucket, the lower limit
     */
    private String creditMaturityBucketLow;

    /**
     * High end of the maturity bucket based on credit risk maturity date. High end of the maturity bucket, the upper limit
     */
    private String creditMaturityBucketHigh;

    /**
     * Maturity pillar based on credit risk maturity date
     */
    private String creditMaturityPillar;

    /**
     * Rate risk life
     */
    private BigDecimal rateLife;

    /**
     * Rate risk maturity date
     */
    private LocalDate rateMaturityDate;

    /**
     * Low end of the maturity bucket based on rate risk maturity date. Low end of the term bucket, the lower limit
     */
    private String rateMaturityBucketLow;

    /**
     * High end of the term bucket based on rate risk maturity date. High end of the term bucket, the upper limit
     */
    private String rateMaturityBucketHigh;

    /**
     * Maturity pillar based on rate risk maturity date
     */
    private String rateMaturityPillar;

    /**
     * Date of maturity at worst (at wort for the investor). BLOOM_WRST_MTY for example.
     */
    private LocalDate maturityAtWorstDate;

    /**
     * Maturity max of the security or underlying
     */
    private BigDecimal maturityMax;

    /**
     * Maturity min of the security or underlying
     */
    private BigDecimal maturityMin;

    /**
     * CNVX_OAS_MID : modification of bond duration with respect to a yield change following OAS (Option Adjusted Spread analysis) method
     */
    private BigDecimal convexityOASMid;

    /**
     * DUR_ADJ_MTY_MID : sensitivity mid at maturity
     */
    private BigDecimal durationAdjMaturityMid;

    /**
     * DUR_ADJ_OAS_MID : sensitivity to a change of 100bp of reference rate curve, OAS_MID_ADJ_DURATION
     */
    private BigDecimal durationAdjOASMid;

    /**
     * INFLATION_ADJ_CNVX_MID : Bond inflation convexity
     */
    private BigDecimal inflationAdjConvexityMid;

    /**
     * INFLATION_ADJ_DUR_MID : sensitivity of inflation linked instrument based on MID price
     */
    private BigDecimal inflationAdjDurationMid;

    /**
     * RSI_14D - Relative Strength Index (RSI)
     */
    private BigDecimal rsi;

    /**
     * SPD_MOD_DUR_MID : spread modified duration zero mid
     */
    private BigDecimal spreadModDurationMid;

    /**
     * SPREAD_TO_TSY_ISSUE
     */
    private BigDecimal spreadToTsyIssue;

    /**
     * Worst spread
     */
    private BigDecimal worstSpread;

    /**
     * Carry
     */
    private BigDecimal carry;

    /**
     * SPREAD_CARRY_GOV (Yield_to_maturity - OASSpread)  - Carry
     */
    private BigDecimal spreadCarryGov;

    /**
     * PFI_SPREAD_VS_EONIA
     */
    private BigDecimal spreadVsEonia;

    /**
     * BGN ask spread comparable
     */
    private BigDecimal askSpreadComparable;

    /**
     * BGN bid spread comparable
     */
    private BigDecimal bidSpreadComparable;

    /**
     * OAS_SPREAD_MID
     */
    private BigDecimal oasSpreadMid;

    /**
     * OAS_SPREAD_YESTERDAY
     */
    private BigDecimal oasSpreadYesterday;

    /**
     * OAS_CURVE_ID :
     */
    private String oasCurveId;

    /**
     * id of yield curve used for AOAS calculations
     */
    private String aoasCurveId;

    /**
     * YLD_SOV_SPREAD_MID :
     */
    private BigDecimal yieldSovSpreadMid;

    /**
     * YTM yield to maturity
     */
    private BigDecimal yieldToMaturity;

    /**
     * YLD_MID : yield to maturity calculated from MID prices
     */
    private BigDecimal yieldToMaturityMid;

    /**
     * YLD_WITH_INFLATION_MID :
     */
    private BigDecimal yieldWithInflationMid;

    /**
     * YTW yield to worst
     */
    private BigDecimal yieldToWorst;

    /**
     * CHG_PCT_YTD -  yield variation
     */
    private BigDecimal yieldVariation;

    /**
     * EARN_YLD -  equity :  dividendRate, Index : earn yield
     */
    private BigDecimal yieldEarnings;

    /**
     * Yield calculated at the next option date
     */
    private BigDecimal yieldToNext;

    /**
     * yield to put, CV_YTP convertible
     */
    private BigDecimal yieldToPut;

    /**
     * CV_UNDRLYG_SENSITIVITY : sensitivity of the underlying equity (delta / premium+1)
     */
    private BigDecimal elasticityToMaturity;

    /**
     * CV_PREMIUM, conversion premium
     */
    private BigDecimal premium;

    /**
     * The bond floor is the value at which the convertible option becomes worthless because the underlying stock price has fallen substantially below the conversion value.
     */
    private BigDecimal bondFloor;

    /**
     * Distance to the convertible bond floor (downside risk)
     */
    private BigDecimal distToBondFloor;

    /**
     * CV_CONVEXITY - , negative convexity
     */
    private BigDecimal convexityNeg;

    /**
     * CV_CONVEXITY + , positive convexity
     */
    private BigDecimal convexityPos;

    /**
     * WORKOUT_MARKET_MID_MOD_DUR
     */
    private BigDecimal workoutMarketMidModDur;

    /**
     * VOLATILITY, VOLATILITY_30D
     */
    private BigDecimal volatility;

    /**
     * VOLATILITY_360D
     */
    private BigDecimal volatility360D;

    /**
     * VOLATILITY_260D
     */
    private BigDecimal volatility260D;

    /**
     * VOLATILITY_180D : the annualized standard deviation of the relative price change for the 180 most recent trading days closing price
     */
    private BigDecimal volatility180D;

    /**
     * VOLATILITY_90D, VOLATILITY_3M
     */
    private BigDecimal volatility90D;

    /**
     * VOLATILITY_60D : the annualized standard deviation of the relative price change for the 60 most recent trading days closing price
     */
    private BigDecimal volatility60D;

    /**
     * VOLATILITY_30D : the annualized standard deviation of the relative price change for the 30 most recent trading days closing price
     */
    private BigDecimal volatility30D;

    /**
     * listed derivative : underlying volatility
     */
    private BigDecimal volatilityLast;

    /**
     * listed derivative : underlying volatility based on ask price
     */
    private BigDecimal volatilityAsk;

    /**
     * listed derivative : underlying volatility based on bid price
     */
    private BigDecimal volatilityBid;

    /**
     * Derivative : open interest is the total number of open or outstanding (not closed or delivered) options and/or futures contracts that exist on a given day
     */
    private BigDecimal openInterest;

    /**
     * Derivative :  open interest aggregation of all future contract expirations
     */
    private BigDecimal futAggrOpenInterest;

    /**
     * Future on Bond : FUT_IMPLIED_REPO_RT, repo rate implied by the cheapest
     */
    private BigDecimal futImpliedRepoRt;

    /**
     * Future on Bond : FUT_CTD_CNV_RISK : convexity of the cheapest to deliver
     */
    private BigDecimal futCTDCnvRisk;

    /**
     * Future on Bond : FUT_CTD_CNV_MOD_DUR : sensitivity of the cheapest divided by current bond price
     */
    private BigDecimal futCTDCnvModDur;

    /**
     * Future  : FUT_CNV_RISK_FRSK
     */
    private BigDecimal futCnvRiskFrsk;

    /**
     * Future on Bond  :  FUT_CTD_CNV_CNVX, FUT_CNV_CNVX_BASED_ON_CTD
     */
    private BigDecimal futCTDCnvCnvx;

    /**
     * Future FUT_EQV_DUR_NOTL  :  Equivalent duration of a future's notional bond
     */
    private BigDecimal futEqvDurNotl;

    /**
     * Concordance factor, scaling factor
     */
    private BigDecimal concordanceFactor;

    /**
     * Tracking error
     */
    private BigDecimal trackingError;

    /**
     * Time to reset rate
     */
    private BigDecimal timeToReset;

    /**
     * Variance ex-post or past variance
     */
    private BigDecimal variance;

    /**
     * Specific delta calculated with a cap, ratio needed for AIFM reporting
     */
    private BigDecimal euroDeltaWithCap;

    /**
     * Convertible : CV_MODEL_DELTA_S
     */
    private BigDecimal cvModelDeltaS;

    /**
     * Convertible CV_MODEL_EFF_CNVX
     */
    private BigDecimal cvModelEffCnvx;

    /**
     * Convertible CV_MODEL_EFF_DUR
     */
    private BigDecimal cvModelEffDur;

    /**
     * Convertible CV_MODEL_SPREAD_DUR
     */
    private BigDecimal cvModelSpreadDur;

    /**
     * Convertible CV_MODEL_STOCK_VOL
     */
    private BigDecimal cvModelStockVol;

    /**
     * Convertible CV_MODEL_TOTAL_PREM
     */
    private BigDecimal cvModelTotalPrem;

    /**
     * Convertible CV_MODEL_VEGA
     */
    private BigDecimal cvModelVega;

    /**
     * Convertible CV_PARITY
     */
    private BigDecimal cvParity;

    /**
     * Model yield : Monte Carlo model yield (YieldSba)
     */
    private BigDecimal modelYield;

    /**
     * Model OAS
     */
    private BigDecimal modelOAS;

    /**
     * Model wal
     */
    private BigDecimal modelWal;

    /**
     * Libor option adjusted duration
     */
    private BigDecimal liborOAD;

    /**
     * Muni option adjusted duration
     */
    private BigDecimal muniOAD;

    /**
     * Option adjusted spread duration
     */
    private BigDecimal oasDuration;

    /**
     * Libor option adjusted spread
     */
    private BigDecimal liborOAS;

    /**
     * Libor option adjusted spread duration
     */
    private BigDecimal liborOASD;

    /**
     * Libor Key rate durations values at different maturity
     */
    private PillarsDef liborKeyRateDurations;

    /**
     * Libor option adjusted convexity
     */
    private BigDecimal liborOptionAdjConvexity;

    /**
     * Option adjusted convexity
     */
    private BigDecimal optionAdjConvexity;

    /**
     * Spread convexity
     */
    private BigDecimal spreadConvexity;

    /**
     * Option adjusted spread convexity
     */
    private BigDecimal oasConvexity;

    /**
     * Libor Option adjusted spread convexity
     */
    private BigDecimal liborOASConvexity;

    /**
     * spread between bond and swap curve, ASSET_SWAP_SPD_MID
     */
    private BigDecimal assetSwapSpread;

    /**
     * Spread Swap Elasticity
     */
    private BigDecimal spreadSwapElasticity;

    /**
     * Spread Treasury Elasticity
     */
    private BigDecimal spreadTreasuryElasticity;

    /**
     * lvl25TveModelElasticity
     */
    private BigDecimal lvl25TveModelElasticity;

    /**
     * Volatility Duration
     */
    private BigDecimal volatilityDuration;

    /**
     * Key rate duration 20Y
     */
    private BigDecimal keyRateDuration20Y;

    /**
     * Key rate durations values at different maturity
     */
    private PillarsDef keyRateDurations;

    /**
     * Definition and values of a list of asset pillars
     */
    private PillarsDef pillarsDef;

    /**
     * Definition and values of a list of maturity pillars
     */
    private PillarsDef maturityPillars;

    /**
     * Definition and values of a list of sensibility pillars
     */
    private PillarsDef sensiPillars;

    /**
     * STD_SIZE_NEGO : standard size of trading
     */
    private BigDecimal tradingStandardSize;

    /**
     * Average volume 1 day
     */
    private BigDecimal volume1D;

    /**
     * Average volume on last 5 opened days, similar to 1 week
     */
    private BigDecimal volume5D;

    /**
     * VOLUME_AVG_20D_TOTAL : Average daily volume for the past 20 trading days for all option contracts (call and put - all strike prices and expiration dates) outstanding for a particular security
     */
    private BigDecimal volume20DTotal;

    /**
     * VOLUME_AVG_20D_CALL : Average daily volume for the past 20 trading days for all call option contracts (all strike prices and expiration dates) outstanding for a particular security
     */
    private BigDecimal volume20DCall;

    /**
     * VOLUME_AVG_20D_PUT : Average daily volume for the past 20 trading days for all put option contracts (all strike prices and expiration dates) outstanding for a particular security
     */
    private BigDecimal volume20DPut;

    /**
     * Average volume on 30 days, similar to 1 month
     */
    private BigDecimal volume30D;

    /**
     * Average volume 3 months
     */
    private BigDecimal volume3M;

    /**
     * Average volume 6 months
     */
    private BigDecimal volume6M;

    /**
     * Average volume 1 year
     */
    private BigDecimal volume1Y;

    /**
     * Global average volume 5D (same ISIN)
     */
    private BigDecimal globalVolume5D;

    /**
     * Global average volume 30D (same ISIN)
     */
    private BigDecimal globalVolume30D;

    /**
     * Global average volume 3 months (same ISIN)
     */
    private BigDecimal globalVolume3M;

    /**
     * Global average volume 6 months (same ISIN)
     */
    private BigDecimal globalVolume6M;

    /**
     * Global average volume 1 year (same ISIN)
     */
    private BigDecimal globalVolume1Y;

    /**
     * List of attributes
     */
    private List<Attribute> moreAttributes;

    /**
     * Calculation yield flag
     */
    private String yieldType;

    /**
     * Breakeven
     */
    private BigDecimal breakeven;

    /**
     * Duration opt adj
     */
    private BigDecimal durationOptAdj;

    /**
     * Effective duration
     */
    private BigDecimal effectiveDuration;

    /**
     * Sensitivity, modified duration
     */
    private BigDecimal modifiedDuration;

    /**
     * CNVX_MID : modification of bond duration for yield change
     */
    private BigDecimal convexityMid;

    /**
     * DISC_MRGN_MID :
     */
    private BigDecimal discountMarginMid;

    /**
     * DUR_ADJ_BID : yield change based on BID
     */
    private BigDecimal durationAdjBid;

    /**
     * DUR_ADJ_MID : sensitivity to dirty MID price change with respect to a 100bp bump of the yield then drop
     */
    private BigDecimal durationAdjMid;

    /**
     * DUR_ADJ_MID_SEMI_ANN : adjusted duration based on MID
     */
    private BigDecimal durationAdjMidSemiAnnual;

    /**
     * GOVT_CNV_SPREAD_MID : spread between bond and interpolation on govt curve
     */
    private BigDecimal govtCnvSpreadMid;

    /**
     * SPREAD_DURATION : spread duration
     */
    private BigDecimal spreadDuration;

    /**
     * z-Spread expressed in unit
     */
    private BigDecimal zspread;

    /**
     * Swap spread in bp, this is the zspread to swap curve, can be computed, same as field spreadLibor
     */
    private BigDecimal swapSpread;

    /**
     * Gov spread in bp, this is the zspread to gov curve, can be computed,  same as field zspreadToGov
     */
    private BigDecimal govSpread;

    /**
     * Spread libor, this is the zspread to swap curve
     */
    private BigDecimal spreadLibor;

    /**
     * Spread opt adj, OAStoWrsE
     */
    private BigDecimal spreadOptAdj;

    /**
     * PFI_SPREAD_TO_TSY
     */
    private BigDecimal spreadToTsy;

    /**
     * SPREAD_TO_TSY_MID
     */
    private BigDecimal spreadToTsyMid;

    /**
     * SWAP_CNV_SPREAD_MID : Mid spread (rate) with interpolation on swap curve
     */
    private BigDecimal swapCnvSpreadMid;

    /**
     * swMD = spreadDuration x OAS spread
     */
    private BigDecimal spreadWeightMod;

    /**
     * PFI_ZSPREAD_GOVT : zspread to gov curve
     */
    private BigDecimal zspreadToGov;

    /**
     * Sensi by spread global
     */
    private BigDecimal spsGlobal;

    /**
     * Sensi by spread specific
     */
    private BigDecimal spsSpec;

    /**
     * Sensi by spread  systemic  = spsGlobal - spsSpec
     */
    private BigDecimal spsSys;

    /**
     * OAS_SPREAD_DUR_MID : sensitivity MID without curve variation
     */
    private BigDecimal oasSpreadDurationMid;

    /**
     * PFI_OAS_SPREAD
     */
    private BigDecimal oasSpread;

    /**
     * RISK_MID
     */
    private BigDecimal riskMid;

    /**
     * MID_PX_VAL_BP : Bond price variation
     */
    private BigDecimal variation;

    /**
     * WORKOUT_OAS_MID_MOD_DUR
     */
    private BigDecimal workoutOasMidModDur;

    /**
     * WORKOUT_PX_MID
     */
    private BigDecimal workoutPriceMid;

    /**
     * WORKOUT_DT_MID
     */
    private LocalDate workoutDateMid;

    /**
     * YAS_ASW_SPREAD
     */
    private BigDecimal yasAswSpread;

    /**
     * YAS_BNCHMRK_BOND
     */
    private String yasBenchmarkBond;

    /**
     * YAS_BOND_YLD : Yield calculated by YAS (Yield and spread analysis) method
     */
    private BigDecimal yasBondYield;

    /**
     * YAS_YLD_SPREAD :  yields spread from benchmark
     */
    private BigDecimal yasYieldSpread;

    /**
     * YAS_WORKOUT_DT
     */
    private LocalDate yasWorkoutDate;

    /**
     * YAS_CURVE_ID : id of curve used to calculate the bond yield in YAS method
     */
    private String yasCurveId;

    /**
     * YAS_ISPRD_CRV_ID : id of curve used in ispread calculation
     */
    private String yasISpreadCurveId;

    /**
     * YAS_ISPREAD  ispread from swap curve interpolation in YAS method
     */
    private BigDecimal yasISpread;

    /**
     * YAS_ISPREAD_TO_GOVT : ispread from Govie curve interpolation in YAS method
     */
    private BigDecimal yasISpreadToGovt;

    /**
     * YAS_MOD_DUR : sensi for a yasBondYield change, contains Modified Duration Convertible Bond Bloomberg
     */
    private BigDecimal yasModDur;

    /**
     * YAS_ZSPREAD : zspread from yas bond yield
     */
    private BigDecimal yasZSpread;

    /**
     * YLD_ANNUAL_MID : bond yield based on MID price
     */
    private BigDecimal yieldAnnualMid;

    /**
     * YLD_CNV_MID
     */
    private BigDecimal yieldCnvMid;

    /**
     * YLD_SEMI_ANNUAL_MID
     */
    private BigDecimal yieldSemiAnnualMid;

    /**
     * YLD_YTM_ASK
     */
    private BigDecimal ytmAsk;

    /**
     * yield to maturity calculated from BID prices
     */
    private BigDecimal ytmBid;

    /**
     * YLD_MID : yield to maturity calculated from MID prices
     */
    private BigDecimal ytmMid;

    /**
     * REAL_YIELD bond inflation : yield - breakeven
     */
    private BigDecimal realYield;

    /**
     * YLD_VOLATILITY_260D : yield volatility on 260D (half-year)
     */
    private BigDecimal yieldVolatility260D;

    /**
     * YLD_SPREAD_TO_SELECTED_BOND
     */
    private BigDecimal yieldSpreadToSelectedBond;

    /**
     * Derivative : contract value is the value of 1 derivative (contractSize * point value)
     */
    private BigDecimal contractValue;

    /**
     * Derivative : option price X contract size
     */
    private BigDecimal optionPremium;

    /**
     * VEGA_MID
     */
    private BigDecimal optVegaMid;

    /**
     * GAMMA_MID
     */
    private BigDecimal optGammaMid;

    /**
     * Deprecated DO NOT USE
     */
    private BigDecimal optVgModelRho;

    /**
     * OPT_DIV_YIELD
     */
    private BigDecimal optDivYield;

    /**
     * OPT_CTD_CNV_CNVX
     */
    private BigDecimal optCTDCnvCnvx;

    /**
     * OPT_CNV_MOD_DUR_NOTL
     */
    private BigDecimal optCnvModDurNotl;

    /**
     * OPT_CNV_CNVX_NOTL
     */
    private BigDecimal optCnvCnvxNotl;

    /**
     * FUT_CNV_CNVX_NOTL
     */
    private BigDecimal futCnvCnvxNotl;

    /**
     * Future on Bond : FUT_ACTUAL_REPO_RT, actual repo rate of the cheapest
     */
    private BigDecimal futActualRepoRt;

    /**
     * Price Earnings Ratio
     */
    private BigDecimal priceEarningsRatio;

    /**
     * Carbon Risk Score
     */
    private BigDecimal carbonRiskScore;

    /**
     * Surveyed KIID SRRI (Valeur de 1 � 7)
     */
    private String surveyedKiidSrri;

    /**
     * Equity Style Growth
     */
    private BigDecimal equityStyleGrowth;

    /**
     * Equity Style Value
     */
    private BigDecimal equityStyleValue;

    /**
     * Equity Style Core
     */
    private BigDecimal equityStyleCore;

    /**
     * Asset Allocation Cash % (Net)
     */
    private BigDecimal assetAllocationCash;

    /**
     * Asset Allocation Equity % (Net)
     */
    private BigDecimal assetAllocationEquity;

    /**
     * Asset Allocation Bond % (Net)
     */
    private BigDecimal assetAllocationBond;

    /**
     * Asset Allocation Others % (Net)
     */
    private BigDecimal assetAllocationOthers;

    /**
     * Average volume on last 10 days : Number of shares traded on average for the past 10 trading days. 
     */
    private BigDecimal volume10D;

    /**
     *  Z Spread Change On Day : The base is the previous day
     */
    private BigDecimal zspreadChange1D;

    /**
     * The average daily value traded for the past 3 trading months
     */
    private BigDecimal averageDailyTurnover3M;

    /**
     * Bond theoretical price, as calculated by the Convertible Valuation (OVCV) pricer
     */
    private BigDecimal cvModelBondVal;

    /**
     * Delta of the convertible bond, as calculated by the Convertible Valuation (OVCV) pricer
     */
    private BigDecimal cvModelDeltaV;

    /**
     * Present value of straight fixed income components of the bond without any conversion features. 
     */
    private BigDecimal cvModelFixedIncVal;

    /**
     * Current value of the bond equity component, calculated as a bond price minus bond floor. Bond floor used in this calculation is present value of straight fixed income components of the bond without any conversion features, Cv Model Fixed Inc. Value (CM012, 
     */
    private BigDecimal cvModelCnvsPrem;

    /**
     * Duration based on the Mid Price
     */
    private BigDecimal durationMid;

    /**
     * Standard deviation of weekly returns in the last 100 weeks
     */
    private BigDecimal standardDeviation100w;

    /**
     * Effective duration mid for mortgage or any other asset
     */
    private BigDecimal effectiveDurationMid;

}