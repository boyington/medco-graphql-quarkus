package com.amundi.medco.poc.model.openaml.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonRootName(value="documentfile", namespace="com.amundi.tech.openaml.entity")
public class DocumentFile {

    /**
     * File name
     */
    private String name;

    /**
     * Extension of the file, for instance .csv if Comma-separated values (CSV), .json if JSON format, iepub if Electronic publication (EPUB)
     */
    private String extension;

    /**
     * Media type of the file, i.e. the MIME type, for instance, text/csv, application/pdf, etc.
     */
    private String mediaType;

    /**
     * File URL
     */
    private String url;

    /**
     * Size of the file expressed in bytes
     */
    private BigDecimal size;

    /**
     * Preview URL (to enable preview of the document)
     */
    private String preview;

}