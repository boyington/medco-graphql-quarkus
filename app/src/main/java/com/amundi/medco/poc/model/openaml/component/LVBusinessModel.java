package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVBusinessModel {

  /**
   * Hold to collect 
   */
  HTC,

  /**
   * Hold to collect and sell 
   */
  HTCS,

  /**
   * Trading, default value 
   */
  T,

  /**
   * Miscellaneous 
   */
  MISC,

}