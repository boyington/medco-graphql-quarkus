package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVTradingMode {

  /**
   * Traded mode of the asset, listed on exchange market 
   */
  LISTED,

  /**
   * Traded mode of the asset, not listed, quoted on exchange market 
   */
  QUOTED,

  /**
   * Traded mode of the asset, unlisted on exchange market 
   */
  UNLISTED,

  /**
   * Other if OTC 
   */
  OTHER,

}