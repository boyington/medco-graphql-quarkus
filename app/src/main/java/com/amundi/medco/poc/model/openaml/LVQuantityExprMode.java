package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVQuantityExprMode {

  /**
   * Quantity 
   */
  QUANTITY,

  /**
   * Nominal 
   */
  NOMINAL,

  /**
   * in number of contracts 
   */
  CONTRACT,

  /**
   * if cash amount 
   */
  CASH,

  /**
   * Quantity expressed as an amount representing the current amortised face amount of a bond, for example, a periodic reduction/increase of a bond's principal amount. 
   */
  AMORTISED_VALUE,

  /**
   * Net amount, similar to face amount 
   */
  NET_AMOUNT,

  /**
   * undefined 
   */
  PERCENTAGE,

}