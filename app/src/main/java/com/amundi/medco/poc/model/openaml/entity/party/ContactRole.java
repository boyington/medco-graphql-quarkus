package com.amundi.medco.poc.model.openaml.entity.party;

import com.amundi.medco.poc.model.openaml.component.Identifier;
import com.amundi.medco.poc.model.openaml.entity.PartyLink;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.List;

@Data
@JsonRootName(value="contactrole", namespace="com.amundi.tech.openaml.entity.party")
public class ContactRole extends PartyRole {

    /**
     * External id identifier
     */
    private Identifier externalId;

    /**
     * List of relations with other third-parties
     */
    private List<PartyLink> relations;

}