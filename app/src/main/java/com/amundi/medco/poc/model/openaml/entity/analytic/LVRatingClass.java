package com.amundi.medco.poc.model.openaml.entity.analytic;

/**
 * undefined
 */
public enum LVRatingClass {

  /**
   * Long term 
   */
  LT,

  /**
   * Short term 
   */
  ST,

  /**
   * Short term 
   */
  CT,

  /**
   * Short term Local currency 
   */
  CTL,

  /**
   * Short term Local currency 
   */
  STL,

  /**
   * Global, or calculated 
   */
  GLO,

}