package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.Entity;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonRootName(value="calendarday", namespace="com.amundi.tech.openaml.entity")
public class CalendarDay extends Entity {

    /**
     * Date of the day
     */
    private LocalDate date;

    /**
     * Type of the day : open, specific day
     */
    private String type;

}