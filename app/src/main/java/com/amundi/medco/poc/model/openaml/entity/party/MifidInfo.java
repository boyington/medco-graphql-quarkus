package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="mifidinfo", namespace="com.amundi.tech.openaml.entity.party")
public class MifidInfo {

    /**
     * MIFID category, client classification
     */
    private String category;

    /**
     * Exclusion reason if the client is not MIFID
     */
    private String exclusionReason;

    /**
     * True if the balance sheet total is at least 20M EUR
     */
    private Boolean balanceSheetAtLeast;

    /**
     * True if the net turnover is at least 40M EUR
     */
    private Boolean revenueAtLeast;

    /**
     * True if the equity, capital or own funds are at least 2M EUR
     */
    private Boolean capitalAtLeast;

}