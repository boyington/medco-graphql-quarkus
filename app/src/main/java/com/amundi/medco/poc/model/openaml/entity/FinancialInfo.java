package com.amundi.medco.poc.model.openaml.entity;

import com.amundi.medco.poc.model.openaml.component.Amount;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@JsonRootName(value="financialinfo", namespace="com.amundi.tech.openaml.entity")
public class FinancialInfo {

    /**
     * Source of the financial information
     */
    private String source;

    /**
     * As of date of the financial information
     */
    private LocalDate asOfDate;

    /**
     * Closing date of financial year, reference year of revenue for a person
     */
    private String fiscalYearEnd;

    /**
     * Currency code associated to the financial amounts
     */
    private String ccy;

    /**
     * Total capital amount
     */
    private BigDecimal totalCapital;

    /**
     * Total debt amount, total liability, short debt + long term debt
     */
    private BigDecimal totalDebt;

    /**
     * Total assets
     */
    private BigDecimal totalAssets;

    /**
     * Total shareholder's equity for legal entity
     */
    private BigDecimal totalEquity;

    /**
     * Total revenue (US), or net turnover (UK), or individual income
     */
    private BigDecimal totalRevenue;

    /**
     * Market capitalization
     */
    private BigDecimal marketCap;

    /**
     * Enterprise value (EV)
     */
    private BigDecimal enterpriseValue;

    /**
     * Party gross margin
     */
    private BigDecimal grossMargin;

    /**
     * Total asset and currency, or individual assets valuation
     */
    private Amount assetSize;

    /**
     * Capital cover
     */
    private Amount capitalCover;

    /**
     * Percentage of the capital that is free float
     */
    private BigDecimal pctFreeFloat;

    /**
     * Percentage of own capital
     */
    private BigDecimal pctOwnCapital;

    /**
     * Total asset under management and currency, book value
     */
    private Amount totalAUM;

    /**
     * Ratio net debt / total asset, debt ratio
     */
    private BigDecimal ratioDebtTotalAsset;

    /**
     * Ratio net debt / total capital
     */
    private BigDecimal ratioDebtTotalCapital;

    /**
     * Ratio net debt / EBITDA
     */
    private BigDecimal ratioDebtEbitda;

}