package com.amundi.medco.poc.service;

import com.amundi.medco.poc.model.Assets;
import com.amundi.medco.poc.model.Criterias;
import com.amundi.medco.poc.model.Root;
import com.amundi.medco.poc.model.openaml.entity.Asset;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import java.util.Set;

@Slf4j
@ApplicationScoped
public class AssetService {

    private StorageService storageService;
    private Root root;

    public AssetService(StorageService storageService) {
        this.storageService = storageService;
        root = this.storageService.getRoot();
    }

    public Asset asset(String assetId) {
        if(assetId!=null) {
            if(root.getAssets().containsKey(assetId)) {
                return root.getAssets().get(assetId).get();
            }
        }
        return null;
    }

    public Assets assets(Criterias criterias) {
        return null;
    }

    public Set<String> ids() {
        return root.getAssets().keySet();
    }
}
