package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVFrequency {

  /**
   * Frequency is every 5 years 
   */
  EVERY_5_YEARS,

  /**
   * Frequency is every 4 years 
   */
  EVERY_4_YEARS,

  /**
   * Frequency is every 3 years 
   */
  EVERY_3_YEARS,

  /**
   * Frequency is every 3 quarters, 270 days 
   */
  EVERY_3_QUARTERS,

  /**
   * Frequency is every 2 years 
   */
  EVERY_2_YEARS,

  /**
   * Frequency is annual, once a year, annually or yearly 
   */
  YEARLY,

  /**
   * Frequency is at least annual, once a year, annually or yearly 
   */
  AT_LEAST_YEARLY,

  /**
   * Frequency is semi-annual, biannual, half yearly, every 6 months, twice a year, or 2 times per year 
   */
  BIANNUAL,

  /**
   * Frequency is thrice yearly : every 4 months, 3 times per year 
   */
  THRICE_YEARLY,

  /**
   * Frequency is quarterly, every 3 months, or 4 times per years 
   */
  QUARTERLY,

  /**
   * Frequency is bimonthly, every 2 months, or 6 times per year 
   */
  BIMONTHLY,

  /**
   * Frequency is monthly, every month, once a month, or 12 times per year 
   */
  MONTHLY,

  /**
   * Frequency is semimonthly : twice a month, 24 times per year, Fortnightly 
   */
  SEMIMONTHLY,

  /**
   * Frequency is weekly, every week, or 52 times per year 
   */
  EVERY_4_MONTHS,

  /**
   * Frequency is 3 months, or 4 times per year 
   */
  EVERY_3_MONTHS,

  /**
   * Frequency is 2 months, or 6 times per year 
   */
  EVERY_2_MONTHS,

  /**
   * Frequency is 52 weeks (1 month is 28 days) 
   */
  EVERY_52_WEEKS,

  /**
   * Frequency is 4 weeks (1 month is 28 days), or 4 times per year 
   */
  EVERY_12_WEEKS,

  /**
   * Frequency is 4 weeks (1 month is 28 days), or 12 times per year 
   */
  EVERY_4_WEEKS,

  /**
   * Frequency is 3 weeks (1 month is 28 days) 
   */
  EVERY_3_WEEKS,

  /**
   * Frequency is 2 weeks (1 month is 28 days), or 24 times per year 
   */
  EVERY_2_WEEKS,

  /**
   * Frequency is 10th, 20th and last day of month (end of month) 
   */
  EVERY_10TH_20TH_EOM,

  /**
   * Frequency is 10 days 
   */
  EVERY_10_DAYS,

  /**
   * Frequency is weekly, every week, or 52 times per year 
   */
  WEEKLY,

  /**
   * Frequency is weekly, each Monday 
   */
  WEEKLY_ON_MONDAY,

  /**
   * Frequency is weekly, each Tuesday 
   */
  WEEKLY_ON_TUESDAY,

  /**
   * Frequency is weekly, each Wednesday 
   */
  WEEKLY_ON_WEDNESDAY,

  /**
   * Frequency is weekly, each Thursday 
   */
  WEEKLY_ON_THURSDAY,

  /**
   * Frequency is weekly, each Friday 
   */
  WEEKLY_ON_FRIDAY,

  /**
   * Frequency is biweekly, twice a week, or 104 times per year 
   */
  BIWEEKLY,

  /**
   * Frequency is 5 days 
   */
  EVERY_5_DAYS,

  /**
   * Frequency is 3 days 
   */
  EVERY_3_DAYS,

  /**
   * Frequency is 2 days 
   */
  EVERY_2_DAYS,

  /**
   * Frequency is daily, every day, if valuation frequency, it means 252 trading days per years 
   */
  DAILY,

  /**
   * Frequency is overnight 
   */
  OVERNIGHT,

  /**
   * Frequency is intra-day, mainly for index 
   */
  INTRADAY,

  /**
   * Frequency is other, i.e. any other than used values, can be called adhoc 
   */
  OTHER,

  /**
   * No frequency, a single period product, for example no rate reset or payment until maturity. This can be a case such as AT_MATURITY or STRAIGHT payment. 
   */
  NONE,

}