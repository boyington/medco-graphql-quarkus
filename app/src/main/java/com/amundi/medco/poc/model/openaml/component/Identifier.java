package com.amundi.medco.poc.model.openaml.component;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="identifier", namespace="com.amundi.tech.openaml.component")
public class Identifier {

    /**
     * Identifier code of coding scheme and/or source
     */
    private String code;

    /**
     * Prefix code to complete the identifier code if needed
     */
    private String prefix;

    /**
     * Suffix code to complete the code if needed, for example, the version of the Murex contract number
     */
    private String suffix;

    /**
     * Coding scheme of the code, for example: ISIN, CLC, etc.
     */
    private String codScheme;

    /**
     * Optional : source of the code, for example: AMUNDI, etc.
     */
    private String source;

    /**
     * Optional : role of the source, or of the id, for example: ACCOUNTANT, etc.
     */
    private String sourceRole;

}