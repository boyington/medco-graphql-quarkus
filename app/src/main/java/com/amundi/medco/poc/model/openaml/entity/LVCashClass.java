package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVCashClass {

  /**
   * Cash in the context of buy or sell 
   */
  BUY_SELL,

  /**
   * Cash for corporate action 
   */
  CORP_ACTION,

  /**
   * Cash for subscritpion redemption 
   */
  SR,

  /**
   * Cash for fees 
   */
  FEES,

  /**
   * Cash for forex 
   */
  FOREX,

  /**
   * Cash for future, option 
   */
  LISTED_DERIV,

  /**
   * Cash for OTC 
   */
  OTC,

  /**
   * Cash for Repos 
   */
  REPO,

  /**
   * Cash other if not part of others cash class values 
   */
  CASH_OTHER,

}