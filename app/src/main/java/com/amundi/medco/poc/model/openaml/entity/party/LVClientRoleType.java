package com.amundi.medco.poc.model.openaml.entity.party;

/**
 * undefined
 */
public enum LVClientRoleType {

  /**
   * A party that is a client with existing commercial relation 
   */
  CLIENT,

  /**
   * A party that is a prospect, a potential client 
   */
  PROSPECT,

  /**
   * A lead contact, in commercial relations, a contact can a lead before becoming a prospect, and then a client/customer 
   */
  LEAD,

}