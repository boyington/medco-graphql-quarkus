package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum LVPeriodAt {

  /**
   * At beginning, or first coupon, or first calculation period 
   */
  BEGIN,

  /**
   * At the end, in arrears, or last coupon, or last calculation period 
   */
  END,

  /**
   * At beginning and at the end, or first and last coupon 
   */
  BOTH,

  /**
   * Neither begin, nor end 
   */
  NONE,

}