package com.amundi.medco.poc.model.openaml;

/**
 * undefined
 */
public enum LVNetGross {

  /**
   * Net value, i.e. without fees 
   */
  NET,

  /**
   * Gross value, i.e. with fees 
   */
  GROSS,

}