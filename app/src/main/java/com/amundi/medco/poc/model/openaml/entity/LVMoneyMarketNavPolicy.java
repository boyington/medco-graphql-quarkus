package com.amundi.medco.poc.model.openaml.entity;

/**
 * undefined
 */
public enum LVMoneyMarketNavPolicy {

  /**
   * Variable NAV money market mutual fund 
   */
  VNAV,

  /**
   * Low-volatility NAV money market mutual fund 
   */
  LVNAV,

  /**
   * Constant NAV money market mutual fund 
   */
  CNAV,

}