package com.amundi.medco.poc.model.openaml.entity.party;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName(value="tripartyagentrole", namespace="com.amundi.tech.openaml.entity.party")
public class TripartyAgentRole extends PartyRole {

}