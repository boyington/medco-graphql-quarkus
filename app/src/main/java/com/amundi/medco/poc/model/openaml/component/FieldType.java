package com.amundi.medco.poc.model.openaml.component;

/**
 * undefined
 */
public enum FieldType {

  /**
   * Boolean : TRUE/FALSE 
   */
  BOOLEAN,

  /**
   * Text, String 
   */
  TEXT,

  /**
   * Integer 
   */
  INTEGER,

  /**
   * Decimal 
   */
  DECIMAL,

  /**
   * Date iso format : yyyy-MM-dd 
   */
  DATE,

  /**
   * List of values : text value separated by comma 
   */
  LIST,

}